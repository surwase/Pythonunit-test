from rest_framework import routers

from .views import *
from django.conf.urls import include, url
from rest_framework_bulk.routes import BulkRouter

app_name = "client"

# BULK VIEWS
bulk_router = BulkRouter()
bulk_router.register(r'^compliance_checklist', ClientComplianceCheckListModelViewSet)
bulk_router.register(r'^compliance_evaluation', ClientComplianceEvaluationModelViewSet)

urlpatterns = [
    # url(r'^export/(?P<company_id>\d+)/$', CompanyExportAPIView.as_view({'get': 'list', 'post': 'create'})),
    url(r'^$', ClientModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='client'),
    url(r'^mini/$', ClientMicroModelViewSet.as_view(
        {'get': 'list'}),
        name='client_mini'),
    url(r'^mini/(?P<pk>\d+)/$', ClientMiniModelViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_mini'),
    url(r'^(?P<pk>\d+)/$', ClientModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='client'),
    url(r'^detail/(?P<pk>\d+)/$', ClientModelDetailViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_micro_detail'),
    url(r'^client_risk_status/(?P<pk>\d+)/$', ClientModelRiskStatusViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_risk_status'),
    url(r'^client_project_status/(?P<pk>\d+)/$', ClientModelProjectStatusViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_project_status'),
    url(r'^projects/(?P<pk>\d+)/$', ClientModelProjectViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_project_list'),
    url(r'^plants/(?P<pk>\d+)/$', ClientModelPlantViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_plant_list'),
    url(r'^plantsandprojects/(?P<pk>\d+)/$', ClientModelPlantAndProjectViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_plant_project_list'),
    url(r'^images/$', ClientImageModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='client_images'),
    url(r'^images/(?P<pk>\d+)/$', ClientImageModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='client_images'),
    url(r'^dashboard/$', ClientDashboardModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='dashboard'),
    url(r'^dashboard/(?P<pk>\d+)/$', ClientDashboardModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='dashboard'),
    url(r'^client_option/$', ClientOptionViewSet.as_view(
        {'get': 'list'}),
        name='client_options'),
    url(r'^files/(?P<pk>\d+)/$', ClientFilesModelViewset.as_view(),
        name='client_files'),
    url(r'^files/(?P<pk>\d+)/(?P<file_pk>[\w\-]+)/$', ClientFilesModelViewset.as_view(),
        name='client_files'),
    url(r'^client_user/$', ClientUserModelViewSet.as_view(
        {'get': 'list'}),
        name='client_user'),
    url(r'^client_user/(?P<pk>\d+)/$', ClientUserModelViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_user'),
    url(r'^mini_client_user/$', MiniClientUserModelViewSet.as_view(
        {'get': 'list'}),
        name='mini_client_user'),
    url(r'^client_overview/$', ClientOverviewModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='client_overview'),
    url(r'^client_overview/(?P<client>\d+)/$', ClientOverviewModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='client_overview'),
    url(r'^client_detail/$', ClientDetailModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='client_detail'),
    url(r'^client_detail/(?P<client>\d+)/$', ClientDetailModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='client_detail'),
    url(r'^client_settings/(?P<client>\d+)/$', ClientSettingsModelViewSet.as_view(
        {'get': 'retrieve', 'patch': 'partial_update'}),
        name='client_settings'),
    url(r'^client_folder/$', CustomFolderModelViewSet.as_view(
        {'post': 'create'}),
        name='custom_folder'),
    url(r'^client_folder/(?P<client>\d+)/(?P<folder_id>[\w\-]+)/$', CustomFolderModelViewSet.as_view(
        {'delete': 'destroy'}),
        name='custom_folder'),
    url(r'^invite_user/$', InviteUserAPIView.as_view()),
    url(r'^check_user_edit_permission/$', CheckUserEditPermissionAPIView.as_view(), name="check_user_edit_permission"),
    url(r'^delete_user/(?P<user_id>\d+)/$', DeleteUserAPIView.as_view(), name="delete_user"),
    url(r'^unadded_clients/$', UserUnAddedClientsAPIView.as_view(), name="user_un_add_client_api_view"),
    url(r'^user_clients/$', UserClientsAPIView.as_view(), name="user_client_api_view"),
    # url(r'^remove_user_client/$', RemoveClientUserAPIView.as_view()),
    url(r'^rename_folder/(?P<folder_id>[\w\-]+)/$', RenameFolderAPIView.as_view()),
    url(r'^compliance_schedule/(?P<client_id>\d+)/$', ClientComplianceScheduleAPIView.as_view(), name="check_compliance_schedule"),
    url(r'^check_client_name/$', CheckClientNameAPIView.as_view(), name="check_client_name"),
    url(r'^check_compliance_list/$', CheckComplianceListAPIView.as_view(), name='check_compliance_list'),
    url(r'^sync_client_data/$', SyncClientTablesAPIView.as_view(), name='sync_client_tables'),
    url(r'^client_database_detail/$',ClientDatabaseDetailModelViewset.as_view({'get': 'list'}), name='client_detail'),
    url(r'^client_database_detail/(?P<pk>\d+)/$', ClientDatabaseDetailModelViewset.as_view({'get': 'retrieve'}), name='client_detail'),
    url(r'^', include(bulk_router.urls)),
    url(r'^images/download/$', ClientImageDownload.as_view(), name='client_images_download'),
]
