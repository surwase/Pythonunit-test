# Generated by Django 2.2.16 on 2021-03-18 12:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0148_auto_20210318_1018'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectoutcome',
            name='is_custom_workflow',
        ),
        migrations.RemoveField(
            model_name='projectoutcome',
            name='outcome_calculation',
        ),
        migrations.RemoveField(
            model_name='projectoutcome',
            name='outcome_indicator',
        ),
        migrations.RemoveField(
            model_name='projectoutput',
            name='is_custom_workflow',
        ),
        migrations.RemoveField(
            model_name='projectoutput',
            name='output_calculation',
        ),
        migrations.RemoveField(
            model_name='projectoutput',
            name='output_indicator',
        ),
        migrations.AddField(
            model_name='projectoutputandoutcome',
            name='is_custom_workflow',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='projectoutputandoutcome',
            name='outcome_calculation',
            field=models.CharField(choices=[('SUM', 'SUM'), ('AVERAGE', 'AVERAGE')], default='SUM', max_length=63),
        ),
        migrations.AddField(
            model_name='projectoutputandoutcome',
            name='outcome_indicator',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='projectoutputandoutcome',
            name='output_calculation',
            field=models.CharField(choices=[('SUM', 'SUM'), ('AVERAGE', 'AVERAGE')], default='SUM', max_length=63),
        ),
        migrations.AddField(
            model_name='projectoutputandoutcome',
            name='output_indicator',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
