import csv

from django.db import models
from v2.client.models import Client, ClientDetail, ClientOverview

class CSVException(Exception):
    def __init__(self, errors):
        self.errors = errors

class CreateClient:
    def __init__(self):
        self.errors = []
    def read_csv(self, csv_file):
        reader = csv.reader(
            csv_file,
            quotechar='"',
            delimiter=',',
            quoting=csv.QUOTE_ALL,
            skipinitialspace=True
        )
        titles = next(reader)
        data = []
        for row in reader:
            row_data = {}
            for i, value in enumerate(row):
                if i >= len(titles):
                    raise CSVException(f"CSV Read Error: Index out of range: Titles: {titles}, Row: {row}")
                title = titles[i]
                row_data[title] = value
            data.append(row_data)
        return data
    def create_client(self, data):
        for row_no, item in enumerate(data):
            client = Client.objects.create(
                client_name=item.get('Company Name'),
                display_name=item.get('Display Name'),
                website_url=item.get('Website URL'),
                client_cin=item.get('Company CIN'),
                license_type=item.get('Shift License Type'),
                engagement_model=item.get('Shift Engagement Model Type'),
                is_kobo_enabled=item.get('Enable Shift Observe(True/False)'),
                csr_user=item.get('No of Client Users'),
                csr_admin=item.get('No of Client Admin'),
                ngo_user=item.get('No of NGO Users'),
                data_collector=item.get('No of Data Collectors')
            )
            clientDetail = ClientDetail.objects.create(
                contact_name=item.get('Contact Name'),
                contact_email=item.get('Company Email Address'),
                primary_phone=item.get('Mobile Number'),
                address=item.get('Address'),
                pin_code=item.get('Pin Code'),
                state=item.get('State'),
                city=item.get('City'),
                client=client
            )
            client_overview = ClientOverview.objects.create(
                vision=item.get('Vision'),
                mission=item.get('Mission'),
                client=client
            )
    def format_data(self, data):
        REQUIRED_FIELDS = [
            'Company Name',
            'Display Name',
            'Contact Name',
            'Company Email Address',
            'Mobile Number',
            'Address',
            'Pin Code',
            'State',
            'City',
            'Vision',
            'Mission',
            'Shift License Type',
            'Shift Engagement Model Type',
            'No of Client Users',
            'No of Client Admin',
            'No of NGO Users',
            'No of Data Collectors'
        ]
        for row_no, row in enumerate(data):
            for key, value in row.items():
                if key in REQUIRED_FIELDS and not value:
                    self.errors.append(
                        f"Required field empty for row: {row_no + 1}, column: {key}"
                    )
        return data
    def upload_data(self, file):
        csv_file = open(file, encoding='utf-8-sig')
        file_data = self.read_csv(csv_file)
        formatted_data = self.format_data(file_data)
        if self.errors:
            raise CSVException(self.errors)
        self.create_client(formatted_data)


obj = CreateClient()
obj.upload_data("shiftclient.csv")
