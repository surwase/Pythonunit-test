Sattva Data Backend
=======

This application provides REST API for front end applications to connect and get data. The project structure is based on [CookieCutter template](http://cookiecutter-django.readthedocs.io)


Getting started
--------------

This application can be started in two ways:

1. With docker-compose, which will build this app as a docker container and start all dependent services including database
2. As a stand-alone application. Here we need to take care of all internal steps and dependencies.


### Running as a docker-compose  

##### Prerequisite
Docker need to be installed in the system

##### Steps

It should be quick to get up and running with the application.

`docker-compose -f local.yml up`

DB migration is already taken care of., So no need to execute them.

On a different terminal, execute management commands:

```
docker-compose -f local.yml run --rm django python manage.py createsuperuser
#As of now, this is saikat user is getting loaded with fixtures. Once user management get's shape there will be some changes here

Note: 'django' is the name of service in local.yml for the sattva app

```

#### Data load

We have couple of datasets that need to be loaded. In future, some of them may not be needed.

```

Execute the following from project base directory to load data 

docker-compose -f local.yml run --rm django python manage.py loaddata geolibraries/fixtures/*.json
docker-compose -f local.yml run --rm django python manage.py loaddata domainlibraries/fixtures/*.json
docker-compose -f local.yml run --rm django python manage.py loaddata programs/fixtures/*.json
docker-compose -f local.yml run --rm django python manage.py loaddata projects/fixtures/*.json

Note: 'django' is the name of service in local.yml for the sattva app

``` 

#### Connecting to Django postgres for debugging issues

1. SSH to the hosting system.
2. Connect to the docker container
`docker exec -it core_postgres_1 /bin/sh`
3. Connect to postgres
`psql -U debug -d core` 
(the database name, username and password are coming from directory -> .env/(environment)/.postgres - This is checked in version control.



#### Running as a stand-alone application

1. Create python virtual environment for this project and activate it.
2. Install dependent libraries - `pip3 install -r requirements/local.txt`

--- To be continued ---