from django.urls import reverse
from .test_setup import TestSetUpwatcher
from rest_framework import status
from core.users.models import User,UserActivityLog
from v2.user_management.models import ClientFeature,ClientFeaturePermission,ProjectFeature,ProjectFeaturePermission
from v2.user_management.views import UserModelViewSet,UserMiniDetailModelViewSet,UserDetailModelViewSet,UserActivityLogViewSet,ClientFeaturesViewSet,ProjectFeaturesViewSet,ProjectFeaturePermissionViewSet,ClientFeaturePermissionViewSet,UserProjectFeatureAPIView,UserClientFeatureAPIView,CurrentUserAPIView,SaveClientFeaturePermissionsForUserAPIView,SaveProjectFeaturePermissionsForUserAPIView
from v2.project.models import Project,ProjectUser

class TestUserModelViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()
        self.userr = User.objects.create_user(
            username='sonika',
            password='password',
            first_name='sonika',
            last_name='sharan',
            name='sonikasharan',
            email='sonika@test.com'
        )
        self.data={
            "username":'monika',
            "password":'password',
            "first_name":'monika',
            "last_name":'sharan',
            "name":'monikasharan',
            "email":'sharan@test.com'
        }
        self.client_feature=ClientFeature.objects.create(
            feature_name = "f1",
            feature_code = "c2",
            default_permissions = ['view'],
            applicable_roles = ['csr-watcher']
        )
        self.project_feature=ProjectFeature.objects.create(
            feature_name = "f1",
            feature_code = "c2",
            default_permissions = ['view'],
            applicable_roles = ['csr-watcher']
        )
        self.project=Project.objects.create(
            project_name = "xyz",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        self.project_user= ProjectUser.objects.create(
          project = self.project,
          user = self.user
        )

        self.project_feature_permission=ProjectFeaturePermission.objects.create(
            project =self.project,
            feature = self.project_feature,
            permission = 'View'
        )

        self.client_feature_permission=ClientFeaturePermission.objects.create(
            client =self.client_obj,
            feature = self.client_feature,
            permission = 'View'
        )
        self.activity_log= UserActivityLog.objects.create(
            user = self.user,
            old_value = ['xyz'],
            new_value = ['ppp'],
            action = "abcd",
            update_text ="text"
        )


        #urls
        self.user_url= reverse('v2:user_management:user')
        self.detail_url= reverse('v2:user_management:user',kwargs={'pk': self.userr.id})
        self.project_feature_permission_url= reverse('v2:user_management:user',kwargs={'user_id':self.user.id,'project_id':self.project.id})
        self.client_feature_permission_url= reverse('v2:user_management:user',kwargs={'user_id':self.user.id,'client_id':self.client_obj.id})
        self.user_activity_log_url= reverse('v2:user_management:user')
        self.client_feature_url= reverse('v2:user_management:client_feature')
        self.project_feature_url= reverse('v2:user_management:project_feature')
        self.clientt_feature_permission_url=reverse('v2:user_management:client_feature_permission')
        self.projectt_feature_permission_url=reverse('v2:user_management:project_feature_permission')
        self.current_user_url=reverse('v2:user_management:current user')

    def test_user_list(self):
        request = self.factory.get(self.user_url)
        request.user = self.user
        view = UserModelViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'],0)

    def test_user_detail(self):
        request = self.factory.get(self.detail_url)
        request.user = self.user
        view = UserMiniDetailModelViewSet.as_view({'get': 'retrieve'})
        response = view(request, pk=self.userr.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['username'], self.userr.username)

    def test_user_minidetail(self):
        request = self.factory.get(self.detail_url)
        request.user = self.user
        view = UserMiniDetailModelViewSet.as_view({'get': 'retrieve'})
        response = view(request, pk=self.userr.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['username'], self.userr.username)

    def test_user_detail(self):
        request = self.factory.get(self.detail_url)
        request.user = self.user
        view = UserDetailModelViewSet.as_view({'get': 'retrieve'})
        response = view(request, pk=self.userr.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['username'], self.userr.username)

    def test_project_feature_permission_url(self):
        request = self.factory.get(self.project_feature_permission_url)
        request.user = self.user
        view =UserProjectFeatureAPIView.as_view()
        response = view(request,user_id=self.user.id,project_id=self.project.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_client_feature_permission_url(self):
        request = self.factory.get(self.client_feature_permission_url)
        request.user = self.user
        view =UserClientFeatureAPIView.as_view()
        response = view(request,user_id=self.user.id,client_id=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_activity_log_url(self):
        request = self.factory.get(self.user_activity_log_url)
        request.user = self.user
        view =UserActivityLogViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),4)

    def test_client_feature_url(self):
        request = self.factory.get(self.client_feature_url)
        request.user = self.user
        view =ClientFeaturesViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data) ,1)

    def test_project_feature_url(self):
        request = self.factory.get(self.project_feature_url)
        request.user = self.user
        view = ProjectFeaturesViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data) ,1)

    def test_projectt_feature_permission_url(self):
        request = self.factory.get(self.projectt_feature_permission_url)
        request.user = self.user
        view = ProjectFeaturePermissionViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),4)


    def test_clientt_feature_permission_url(self):
        request = self.factory.get(self.clientt_feature_permission_url)
        request.user = self.user
        view =ClientFeaturePermissionViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),4)
