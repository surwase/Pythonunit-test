from django.urls import reverse
from .test_setup import TestSetUpwatcher
from rest_framework import status
from core.users.models import User
from v2.ngo.models import NGOPartner
from v2.ngo.views import NGOPartnerModelViewSet
from django.core.files.uploadedfile import SimpleUploadedFile

class TestNGOPartnerModelViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()
        self.ngo=NGOPartner.objects.create(
            ngo_name = "ngo1",
            company_type = "type1",
            website_url = "www.ngo.com",
            about = "hi i am ngo",
            address = "lucknow",
            employee_count = 100,
            annual_budget = 100000,
            experience = 10,
            awards = "award1",
            state = "up",
            logo =  SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            key_funders = ['sonika'],
            key_work = ['owner'],
            )

        self.data={
            "ngo_name" : "ngo2",
            "company_type" : "type1",
            "website_url" : "www.ngo.com",
            "about" : "hi i am ngo",
            "address" : "lucknow",
            "employee_count" : 100,
            "annual_budget" : 100000,
            "experience" : 10,
            "awards" : "award1",
            "state" : "up",
            #logo =  SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "key_funders" : "['sonika']",
            "key_work" : "['owner']",

            }

        #urls
        self.ngo_partner_url= reverse('v2:ngo:ngo_partner')

    def test_ngo_partner_list(self):
        request = self.factory.get(self.ngo_partner_url)
        request.user = self.user
        view = NGOPartnerModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),0)

    def test_ngo_partner_create(self):
        request = self.factory.post(self.ngo_partner_url,self.data, format='json')
        request.user = self.user
        view = NGOPartnerModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['ngo_name'],self.data['ngo_name'])

    def test_ngo_partner_get(self):
        request = self.factory.get(self.ngo_partner_url, self.data, format='json')
        request.user = self.user
        view = NGOPartnerModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.ngo.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_ngo_partner_put(self):
        request = self.factory.put(self.ngo_partner_url, self.data, format='json')
        request.user = self.user
        view = NGOPartnerModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.ngo.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_ngo_partner_patch(self):
        request = self.factory.patch(self.ngo_partner_url, self.data, format='json')
        request.user = self.user
        view = NGOPartnerModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.ngo.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_ngo_partner_delete(self):
        request = self.factory.delete(self.ngo_partner_url, self.data, format='json')
        request.user = self.user
        view = NGOPartnerModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.ngo.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
