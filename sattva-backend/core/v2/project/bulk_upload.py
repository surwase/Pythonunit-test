import csv
import datetime
from io import StringIO
import dateutil
from django.db import connection
from django.core.files.storage import default_storage
from v2.ngo.models import NGOPartner
from v2.project.models import Project, ProjectFocusArea, ProjectTag, ProjectSubTargetSegment, ProjectSubFocusArea, \
    ProjectTargetSegment,ProjectIndicator, ProjectOutcome,ProjectOutput,ProjectSDGGoals,ProjectScheduleVII, ProjectLocation, Milestone, MilestoneNote, Task, Disbursement, ProjectUser, Utilisation, DisbursementStatus,UtilisationExpense,\
    TaskComment, Template, TemplateType, TemplateAttribute, CSVUpload, ProjectMCA
from v2.client.models import Client
from v2.plant.models import Plant
from domainlibraries.models import FocusArea, SubFocusArea, TargetSegment, SubTargetSegment, ScheduleVIIActivity, SDGGoal, DataType, MCASector
from googleplaces import GooglePlaces, types, lang
import ssl
from django.db.models import Sum

def validate_file(file,header_list):
    errors = []
    header_expected=[]
    with default_storage.open(file, 'rb') as f:
        fl = f.read().decode("utf-8")
        csvfile = StringIO(fl)
        reader = csv.reader(csvfile, delimiter=',')
        for i in reader:
            for j in i:
                field=j.strip()
                if field not in header_list:
                    errors.append(f"Invalid field name {field}")
                header_expected.append(field)
            break;

        if header_list !=header_expected:
            errors.append(f"Invalid column order")

        if len(header_list) !=len(header_expected):
            errors.append(f"Invalid column count")

    if not errors:
            return True,[]
    else:
        return False, errors

def get_location_details(location):
    ssl._create_default_https_context = ssl._create_unverified_context
    YOUR_API_KEY = 'AIzaSyAiwlW_3doxcsL475e7g2oC0oRU3aIuP2M'
    google_places = GooglePlaces(YOUR_API_KEY)
    # You may prefer to use the text_search API, instead.
    query_result = google_places.nearby_search(location=location)
    location_obj = {}
    place = query_result.places[0]
    place.get_details()
    location_obj['details'] = place.details
    location_obj['latitude'] = float(place.details.get('geometry', {}).get('location', {}).get('lat'))
    location_obj['longitude'] = float(place.details.get('geometry', {}).get('location', {}).get('lng'))
    location_obj['map_location'] = place.details.get('formatted_address')
    for comp in place.details['address_components']:
        if 'political' in comp.get('types', []) and 'locality' in comp.get('types', []):
            location_obj['area'] = comp.get('long_name', '')
        elif 'political' in comp.get('types', []) and 'administrative_area_level_2' in comp.get('types', []):
            location_obj['city'] = comp.get('long_name', '')
        elif 'political' in comp.get('types', []) and 'administrative_area_level_1' in comp.get('types', []):
            location_obj['state'] = comp.get('long_name', '')
        elif 'political' in comp.get('types', []) and 'country' in comp.get('types', []):
            location_obj['country'] = comp.get('long_name', '')
    if 'area' not in location_obj and 'city' in location_obj:
        location_obj['area'] = location_obj['city']
    if 'city' not in location_obj and 'area' in location_obj:
        location_obj['city'] = location_obj['area']
    if 'city' not in location_obj and 'area' not in location_obj:
        location_obj['city'] = location_obj['state']
        location_obj['area'] = location_obj['state']
    location_obj['details']['geometry']['location']['lat'] = float(
        location_obj['details']['geometry']['location']['lat'])
    location_obj['details']['geometry']['location']['lng'] = float(
        location_obj['details']['geometry']['location']['lng'])
    location_obj['details']['geometry']['viewport'] = {}
    return location_obj

def validate_project(file):
    errors = []
    header_list = ['project_name','project_description','client_name','plant_name','start_date','end_date','budget','ngo_partner_name','project_location','focus_area','sub_focus_area','target_segment','sub_target_segment','tags','sdg_goals','schedule_vii','mca_sector']
    valid,file_errors=validate_file(file,header_list)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            row_index=0
            for row in reader:
                row_index =row_index + 1
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                client_name = row[2].strip()
                start_date = row[4].strip()
                end_date = row[5].strip()
                budget = row[6].strip()
                focus_area = row[9].strip().split(';')
                sub_focus_area = row[10].strip().split(';')
                target_segment = row[11].strip().split(';')
                sub_target_segment = row[12].strip().split(';')
                sdg_goals = row[14].strip().split(';')
                schedule_vii = row[15].strip().split(';')
                mca_sector = row[16].strip().split(';')
                if project_name=='':
                        errors.append(f"Invalid data for column project name at row:{row_index}")
                else:
                    try:
                        client=Client.objects.get(client_name=client_name)
                        try:
                            proj=Project.objects.get(project_name=project_name,client=client)
                            if proj:
                                errors.append(f"Project with name {project_name} already exist please change name at row:{row_index}")

                        except Project.DoesNotExist:
                            pass
                    except Client.DoesNotExist:
                        pass

                if client_name=='':
                    errors.append(f"Invalid Client Name for column client_name at row:{row_index}")
                else:
                    try:
                        Client.objects.get(client_name=client_name)
                    except Client.DoesNotExist:
                        errors.append(f"Invalid Client at row:{row_index}")

                if start_date !='':
                    try:
                        datetime.datetime.strptime(start_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect start date format, should be DD/MM/YYYY for row: {row_index} ")

                if end_date !='':
                    try:
                        datetime.datetime.strptime(end_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect end date format, should be DD/MM/YYYY for row: {row_index}")

                if budget!='':
                    x=budget
                    try:
                        int(x)
                    except ValueError:
                        errors.append(f"Invalid budget for column budget at row:{row_index}")


                if row[9]=='':
                    errors.append(f"Focus Area required at row:{row_index}")
                else:
                    for x in focus_area:
                        try:
                            int(x)
                            p=FocusArea.objects.filter(focus_area_id__in=focus_area)
                            if not p:
                                    errors.append(f"Invalid Focus Area for column focus_area at row: {row_index}")

                        except ValueError:
                            errors.append(f"Invalid focus area for column focus area at row:{row_index}")

                if row[10]!='':
                    for x in sub_focus_area:
                        try:
                            int(x)
                            x=SubFocusArea.objects.filter(sub_focus_area_id__in=sub_focus_area)
                            if not x:
                                errors.append(f"Invalid Sub Focus Area for column sub_focus_area at row: {row_index}")
                        except ValueError:
                            errors.append(f"Invalid Sub Focus Area for column sub_focus_area at row: {row_index}")

                if row[11]=='':
                        errors.append(f"Target Segment required at row:{row_index}")
                else:
                    for x in target_segment:
                        try:
                            int(x)
                            x=TargetSegment.objects.filter(target_segment_id__in=target_segment)
                            if not x:
                                errors.append(f"Invalid target segment for column target_segment at row: {row_index}")
                        except ValueError:
                            errors.append(f"Invalid target segment for column target_segment at row: {row_index}")

                if row[12]!='':
                    for x in sub_target_segment:
                            try:
                                int(x)
                                x=SubTargetSegment.objects.filter(sub_target_segment_id__in=sub_target_segment)
                                if not x:
                                    errors.append(f"Invalid sub target segment for column sub_target_segment at row: {row_index}")
                            except ValueError:
                                errors.append(f"Invalid sub target segment for column sub_target_segment at row: {row_index}")

                if row[14]!='':
                    for x in sdg_goals:
                        try:
                            int(x)
                            x=SDGGoal.objects.filter(sdg_goal_id__in=sdg_goals)
                            if not x:
                                errors.append(f"Invalid SDG Goal for column sdg_goals at row: {row_index}")
                        except ValueError:
                            errors.append(f"Invalid SDG Goal for column sdg_goals at row: {row_index}")


                if row[15]!='':
                    for x in schedule_vii:
                        try:
                            int(x)
                            x=ScheduleVIIActivity.objects.filter(id__in=schedule_vii)
                            if not x:
                                errors.append(f"Invalid Schedule VII Activity for column schedule_vii at row: {row_index}")
                        except ValueError:
                            errors.append(f"Invalid Schedule VII Activity for column schedule_vii at row: {row_index}")

                if row[16]!='':
                    for x in mca_sector:
                        try:
                            int(x)
                            x=MCASector.objects.filter(mca_id__in=mca_sector)
                            if not x:
                                errors.append(f"Invalid MCA Sector for column mca_sector at row: {row_index}")
                        except ValueError:
                            errors.append(f"Invalid MCA Sector for column mca_sector at row: {row_index}")

            if not errors:
                return True,[]
            else:
                return False,errors
    else:
        return False,file_errors

def upload_projects(file):
    valid,file_errors= validate_project(file)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            for row in reader:
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                project_description = row[1].strip()
                client_name = row[2].strip()
                plant_name = row[3].strip()
                start_date = row[4].strip()
                end_date = row[5].strip()
                budget = row[6].strip()
                ngo_partner = row[7].strip()
                locations = row[8].strip().split(';')
                focus_area = row[9].strip().split(';')
                sub_focus_area = row[10].strip().split(';')
                target_segment = row[11].strip().split(';')
                sub_target_segment = row[12].strip().split(';')
                tags = row[13].strip().split(';')
                sdg_goals = row[14].strip().split(';')
                schedule_vii = row[15].strip().split(';')
                mca_sector = row[16].strip().split(';')
                client = Client.objects.get(client_name=client_name)
                ngo, created = NGOPartner.objects.get_or_create(ngo_name=ngo_partner)
                project = Project()
                project.project_name = project_name
                project.project_description = project_description
                if start_date:
                    project.start_date = datetime.datetime.strptime(start_date, '%d/%m/%Y')
                if end_date:
                    project.end_date = datetime.datetime.strptime(end_date, '%d/%m/%Y')
                project.budget = budget
                project.ngo_partner = ngo
                project.client = client
                if plant_name:
                    plant, created = Plant.objects.get_or_create(plant_name=plant_name, client=client)
                    project.plant = plant
                tag_list = []
                for tag in tags:
                    if tag != '':
                        tag_list.append(tag)
                project.tags = tag_list
                project.save()
                focus_area = FocusArea.objects.filter(focus_area_id__in=focus_area)
                for fa in focus_area:
                    ProjectFocusArea.objects.create(project=project, focus_area=fa.focus_area_name)
                sub_focus_area = SubFocusArea.objects.filter(sub_focus_area_id__in=sub_focus_area)
                for sfa in sub_focus_area:
                    ProjectSubFocusArea.objects.create(project=project, sub_focus_area=sfa.sub_focus_area_name)
                target_segment = TargetSegment.objects.filter(target_segment_id__in=target_segment)
                for ts in target_segment:
                    ProjectTargetSegment.objects.create(project=project, target_segment=ts.target_segment)
                sub_target_segment = SubTargetSegment.objects.filter(sub_target_segment_id__in=sub_target_segment)
                for sts in sub_target_segment:
                    ProjectSubTargetSegment.objects.create(project=project, sub_target_segment=sts.sub_target_segment)
                sdg_goal_objects = SDGGoal.objects.filter(sdg_goal_id__in=sdg_goals)
                for sg in sdg_goal_objects:
                    sdg_obj = {
                        'id': sg.sdg_goal_id,
                        'name': sg.sdg_goal
                    }
                    ProjectSDGGoals.objects.create(project=project, sdg_goal_name=sg.sdg_goal, sdg_goal=sdg_obj)
                schedule_vii_objects = ScheduleVIIActivity.objects.filter(id__in=schedule_vii)
                for sv in schedule_vii_objects:
                    ProjectScheduleVII.objects.create(project=project, schedule_vii_activity=sv)
                for tag in tag_list:
                    ProjectTag.objects.create(project=project, tag=tag)
                for location in locations:
                    location_obj = get_location_details(location)
                    ProjectLocation.objects.create(project=project, location=location_obj, area=location_obj['area'],
                                                city=location_obj['city'], state=location_obj['state'],
                                                country=location_obj['country'], latitude=location_obj['latitude'],
                                                longitude=location_obj['longitude'])
                mca_sector_objects = MCASector.objects.filter(mca_id__in=mca_sector)
                # mca_sector_distinct = mca_sector_objects.distinct('mca_focus_area_name').values()
                # data = {}
                # for mca in mca_sector_objects:
                #     if data.get(mca.mca_focus_area_name):
                #         data[mca.mca_focus_area_name] = data.get(mca.mca_focus_area_name) + [
                #             mca.mca_sub_focus_area_name]
                #     else:
                #         data[mca.mca_focus_area_name] = [mca.mca_sub_focus_area_name]
                # for mca in mca_sector_distinct:
                #     if data.get(mca.get('mca_focus_area_name')):
                #         mca['mca_sub_focus_area_name'] = data[mca.get('mca_focus_area_name')]
                #     else:
                #         mca['mca_sub_focus_area_name'] = [mca.get('mca_sub_focus_area_name')]
                for mca in mca_sector_objects:
                    mca_obj = {
                        'mca_focus_area_name': mca.mca_focus_area_name,
                        'mca_sub_focus_area_name': mca.mca_sub_focus_area_name
                    }
                    ProjectMCA.objects.create(project=project, mca_focus_area=mca.mca_focus_area_name,
                                              mca_sub_focus_area=mca.mca_sub_focus_area_name, mca=mca_obj)

            return True,[]
    else:
        return False,file_errors

# upload_projects('data_scripts/sample_csv/project.csv')

def Validate_milestone(file,client_id):
    errors = []
    header_list = ['project_name','milestone_name','milestone_description','status','start_date','end_date','sub_focus_area','sub_target_segment','location']
    valid,file_errors=validate_file(file,header_list)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            row_index=0
            for row in reader:
                row_index=row_index + 1
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                milestone_name = row[1].strip()
                milestone_description = row[2].strip()
                status = row[3].strip()
                start_date = row[4].strip()
                end_date = row[5].strip()
                sub_focus_area = row[6].split(';')
                sub_target_segment = row[7].split(';')

                if project_name=='':
                        errors.append(f"Invalid data for column project name at row:{row_index}")
                else:
                    try:
                        proj=Project.objects.get(project_name=project_name,client_id=client_id)
                    except Project.DoesNotExist:
                        errors.append(f"Invalid Project at row:{row_index}")

                if milestone_name=='':
                    errors.append(f"Invalid data for column milestone name at row:{row_index}")
                else:
                    try:
                        proj=Project.objects.get(project_name=project_name,client_id=client_id)
                        try:
                            mile=Milestone.objects.get(milestone_name=milestone_name, project=proj)
                            if mile:
                                errors.append(f"Milestone with name {milestone_name} already exists, row:{row_index}")
                        except Milestone.DoesNotExist:
                            pass
                    except Project.DoesNotExist:
                            pass

                if milestone_description=='':
                    errors.append(f"Description is required at row:{row_index}")

                if status=='':
                    errors.append(f"Status is required at row:{row_index}")

                if start_date !='':
                    try:
                        datetime.datetime.strptime(start_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect start date format, should be DD/MM/YYYY for row: {row_index} ")

                if end_date !='':
                    try:
                        datetime.datetime.strptime(end_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect end date format, should be DD/MM/YYYY for row: {row_index}")

                if row[6]!='':
                    try:
                        x=SubFocusArea.objects.filter(sub_focus_area_id__in=sub_focus_area)
                        if not x:
                            errors.append(f"Invalid Sub Focus Area for column sub_focus_area at row: {row_index}")
                    except ValueError:
                        errors.append(f"Invalid Sub Focus Area for column sub_focus_area at row: {row_index}")

                if row[7]!='':
                    try:
                        x=SubTargetSegment.objects.filter(sub_target_segment_id__in=sub_target_segment)
                        if not x:
                            errors.append(f"Invalid sub target segment for column sub_target_segment at row: {row_index}")
                    except ValueError:
                        errors.append(f"Invalid sub target segment for column sub_target_segment at row: {row_index}")

            if not errors:
                return True,[]
            else:
                return False,errors
    else:
        return False,file_errors


def upload_milestones(file, client_id):
    valid,file_errors= Validate_milestone(file, client_id)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            for row in reader:
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                milestone_name = row[1].strip()
                milestone_description = row[2].strip()
                status = row[3].strip()
                start_date = row[4].strip()
                end_date = row[5].strip()
                sub_focus_area = row[6].split(';')
                sub_target_segment = row[7].split(';')
                stg_list = []
                sfa_list = []
                for stg in sub_target_segment:
                    stg_list.append(stg.strip())
                for sfa in sub_focus_area:
                    sfa_list.append(sfa.strip())
                location = row[8].strip()
                project = Project.objects.get(project_name=project_name, client_id=client_id)
                milestone = Milestone()
                milestone.project = project
                milestone.milestone_description = milestone_description
                milestone.milestone_name = milestone_name
                if start_date:
                    milestone.start_date = datetime.datetime.strptime(start_date, '%d/%m/%Y')
                if end_date:
                    milestone.end_date = datetime.datetime.strptime(end_date, '%d/%m/%Y')
                milestone.status = status
                if len(sfa_list) > 0 and sfa_list[0]:
                    milestone.sub_focus_area = list(
                        SubFocusArea.objects.filter(sub_focus_area_id__in=sfa_list).values_list(
                            'sub_focus_area_name', flat=True))
                if len(stg_list) > 0 and stg_list[0]:
                    milestone.sub_target_segment = list(
                        SubTargetSegment.objects.filter(sub_target_segment_id__in=stg_list).values_list(
                            'sub_target_segment', flat=True))
                if location:
                    milestone.location = get_location_details(location)
                milestone.save()
            return True,[]
    else:
        return False,file_errors

# upload_milestones('data_scripts/sample_csv/milestone.csv', 15)

def validate_milestone_notes(file):
    errors = []
    header_list = ['project_name','milestone_name','milestone_notes','Date']
    valid,file_errors=validate_file(file,header_list)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            row_index=0
            for row in reader:
                row_index=row_index + 1
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                milestone_name = row[1].strip()
                note = row[2].strip()
                date = row[3].strip()
                if project_name=='':
                        errors.append(f"Invalid data for column project name at row:{row_index}")
                else:
                    try:
                        Project.objects.get(project_name=project_name)
                    except Project.DoesNotExist:
                        errors.append(f"Invalid Project at row:{row_index}")

                if milestone_name=='':
                    errors.append(f"Milestone required at row:{row_index}")
                else:
                    try:
                        project=Project.objects.get(project_name=project_name)
                        try:
                            Milestone.objects.get(milestone_name=milestone_name, project=project)
                        except Milestone.DoesNotExist:
                            errors.append(f"Invalid milestone name for column milestone_name at row:{row_index}")

                    except Project.DoesNotExist:
                        pass

                if note=='':
                    errors.append(f"Note required at row:{row_index}")

                if date!='':
                    try:
                        datetime.datetime.strptime(date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect date format, should be DD/MM/YYYY for row: {row_index} ")
            if not errors:
                return True,[]
            else:
                return False,errors
    else:
        return False,file_errors

def upload_milestone_notes(file):
    valid,file_errors=validate_milestone_notes(file)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            for row in reader:
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                milestone_name = row[1].strip()
                note = row[2].strip()
                date = row[3].strip()
                project = Project.objects.get(project_name=project_name)
                milestone = Milestone.objects.get(milestone_name=milestone_name, project=project)
                milestone_note = MilestoneNote()
                milestone_note.milestone = milestone
                milestone_note.note = note
                milestone_note.created_date = datetime.datetime.strptime(date, '%d/%m/%Y')
                milestone_note.save()
            return True,[]
    else:
        return False,file_errors


# upload_milestone_notes('data_scripts/sample_csv/milestonenotes.csv')

def validate_task(file, client_id):
    errors = []
    header_list = ['project_name','task_name','task_description','priority','status','start_date','end_date','tags','milestone_name']
    valid,file_errors=validate_file(file,header_list)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            row_index=0
            for row in reader:
                row_index =row_index + 1
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                task_name = row[1].strip()
                priority = row[3].strip()
                status = row[4].strip()
                start_date = row[5].strip()
                end_date = row[6].strip()
                milestone_name = row[8].strip()

                if project_name!='':
                    try:
                        Project.objects.get(project_name=project_name, client_id=client_id)
                    except Project.DoesNotExist:
                        errors.append(f"Invalid Project at row:{row_index}")

                if task_name=='':
                    errors.append(f"Task name required at row:{row_index}")
                else:
                    try:
                        proj=Project.objects.get(project_name=project_name,client_id=client_id)
                        try:
                            task=Task.objects.get(project=proj, task_name=task_name)
                            if task:
                                errors.append(f"Task with name {task_name} already exists, row:{row_index}")
                        except Task.DoesNotExist:
                            pass
                    except Project.DoesNotExist:
                            pass

                if priority=='':
                    errors.append(f"Priority is required at row:{row_index}")

                if status=='':
                    errors.append(f"Status is required at row:{row_index}")

                if start_date !='':
                    try:
                        datetime.datetime.strptime(start_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect start date format, should be DD/MM/YYYY for row: {row_index} ")

                if end_date !='':
                    try:
                        datetime.datetime.strptime(end_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect end date format, should be DD/MM/YYYY for row: {row_index}")

                if milestone_name!='':
                    try:
                        project=Project.objects.get(project_name=project_name, client_id=client_id)
                        try:
                            Milestone.objects.get(milestone_name=milestone_name, project=project)
                        except Milestone.DoesNotExist:
                            errors.append(f"Invalid Milestone at row:{row_index}")
                    except Project.DoesNotExist:
                        pass
            if not errors:
                    return True,[]
            else:
                return False,errors
    else:
        return False,file_errors

def upload_taskss(file, client_id):
    valid,file_errors=validate_task(file, client_id)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            for row in reader:
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                task_name = row[1].strip()
                task_description = row[2].strip()
                priority = row[3].strip()
                status = row[4].strip()
                start_date = row[5].strip()
                end_date = row[6].strip()
                tags = row[7].strip().split(';')
                tags = [] if tags == [''] else tags
                milestone_name = row[8].strip()
                project = Project.objects.get(project_name=project_name, client_id=client_id)
                task = Task()
                task.project = project
                task.task_description = task_description
                task.task_name = task_name
                if start_date:
                    task.start_date = datetime.datetime.strptime(start_date, '%d/%m/%Y')
                if end_date:
                    task.end_date = datetime.datetime.strptime(end_date, '%d/%m/%Y')
                task.percentage_completed =100
                task.status = status
                task.priority = priority
                task.tags = tags
                client = Client.objects.get(id=client_id)
                task.client = client
                if milestone_name:
                    milestone = Milestone.objects.get(milestone_name=milestone_name, project=project)
                    task.milestone = milestone
                task.save()
            return True,[]
    else:
        return False,file_errors


# upload_tasks('data_scripts/sample_csv/task.csv', 15)

def Validate_task_comment(file, client_id):
    errors = []
    header_list = ['project_name','task_name','comment','date']
    valid,file_errors=validate_file(file,header_list)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            row_index=0
            for row in reader:
                row_index=row_index + 1
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                task_name = row[1].strip()
                comment = row[2].strip()
                date = row[3].strip()

                if project_name!='':
                    try:
                        Project.objects.get(project_name=project_name, client_id=client_id)
                    except Project.DoesNotExist:
                        errors.append(f"Invalid Project at row:{row_index}")

                if task_name=='':
                    errors.append(f"Task required at row:{row_index}")
                else:
                    try:
                        project=Project.objects.get(project_name=project_name)
                        try:
                            Task.objects.get(project=project, task_name=task_name)
                        except Task.DoesNotExist:
                            errors.append(f"Invalid Task for column task_name at row:{row_index}")
                    except Project.DoesNotExist:
                        pass

                if comment=='':
                    errors.append(f"Comment is required at row:{row_index}")

                if date!='':
                    try:
                        datetime.datetime.strptime(date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect date format, should be DD/MM/YYYY for row: {row_index} ")
            if not errors:
                return True,[]
            else:
                return False,errors
    else:
        return False,file_errors


def upload_task_comment(file, client_id):
    valid,file_errors=Validate_task_comment(file, client_id)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            for row in reader:
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                task_name = row[1].strip()
                comment = row[2].strip()
                date = row[3].strip()
                project = Project.objects.get(project_name=project_name, client_id=client_id)
                task = Task.objects.get(project=project, task_name=task_name)
                task_comment = TaskComment()
                task_comment.task = task
                task_comment.comment = comment
                if date:
                    task_comment.created_date = datetime.datetime.strptime(date, '%d/%m/%Y')
                task_comment.save()
            return True,[]
    else:
        return False,file_errors


# upload_task_comment('data_scripts/sample_csv/taskcomment.csv', 15)

def validate_impact_indicator(file):
    errors = []
    FREQUENCY_BUCKETS = ['MONTHLY','QUARTERLY','HALF-YEARLY','ANNUALLY']
    CALCULATION_CHOICE = ['SUM','AVERAGE']
    header_list = ['project_name','milestone','output_indicator','outcome_indicator','start_date','end_date','planned_output','actual_output','planned_outcome','actual_outcome','output_calculation','outcome_calculation', 'location']
    valid,file_errors=validate_file(file,header_list)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            row_index=0
            for row in reader:
                row_index=row_index + 1
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                milestone_name = row[1].strip()
                start_date = row[4].strip()
                end_date = row[5].strip()
                # frequency = row[6].strip()
                planned_output = row[6].strip()
                actual_output = row[7].strip()
                planned_outcome = row[8].strip()
                actual_outcome = row[9].strip()
                output_calculation = row[10].strip()
                outcome_calculation = row[11].strip()

                if project_name=='':
                        errors.append(f"project name is required at row:{row_index}")
                else:
                    try:
                        proj=Project.objects.get(project_name=project_name)
                    except Project.DoesNotExist:
                        errors.append(f"Invalid Project at row:{row_index}")

                if milestone_name!='':
                    try:
                        proj=Project.objects.get(project_name=project_name)
                        try:
                            Milestone.objects.get(milestone_name=milestone_name, project=proj)
                        except Milestone.DoesNotExist:
                            errors.append(f"Invalid Milestone at row:{row_index}")
                    except Project.DoesNotExist:
                            pass
                if start_date =='':
                    errors.append(f"start date required at row:{row_index}")
                else:
                    try:
                        datetime.datetime.strptime(start_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect start date format, should be DD/MM/YYYY for row: {row_index} ")

                if end_date !='':
                    try:
                        datetime.datetime.strptime(end_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect end date format, should be DD/MM/YYYY for row: {row_index}")
                # if frequency=='':
                #     print("###",frequency)
                #     errors.append(f"frequency required at row:{row_index}")
                # else:
                #     if frequency not in FREQUENCY_BUCKETS:
                #         errors.append(f"Invalid frequency choice at row:{row_index}")

                if planned_output!='':
                    x=planned_output
                    try:
                        float(x)
                    except ValueError:
                        errors.append(f"Invalid planned output for column planned_output at row:{row_index}")

                if actual_output!='':
                    x=actual_output
                    try:
                        float(x)
                    except ValueError:
                        errors.append(f"Invalid actual output for column actual_output at row:{row_index}")

                if planned_outcome!='':
                    x=planned_outcome
                    try:
                        float(x)
                    except ValueError:
                        errors.append(f"Invalid planned outcome for column planned_outcome at row:{row_index}")

                if actual_outcome!='':
                    x=actual_outcome
                    try:
                        float(x)
                    except ValueError:
                        errors.append(f"Invalid actual outcome for column actual_outcome at row:{row_index}")

                if output_calculation!='':
                    if output_calculation not in CALCULATION_CHOICE:
                        errors.append(f"Invalid output calculation choice at row:{row_index}")

                if outcome_calculation!='':
                    if outcome_calculation not in CALCULATION_CHOICE:
                        errors.append(f"Invalid outcome calculation choice at row:{row_index}")


            if not errors:
                return True,[]
            else:
                return False,errors
    else:
        return False,file_errors

def upload_impact_indicator(file):
    valid,file_errors= validate_impact_indicator(file)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            for row in reader:
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                milestone_name = row[1].strip()
                output_indicator = row[2].strip()
                outcome_indicator = row[3].strip()
                start_date = row[4].strip()
                end_date = row[5].strip()
                # frequency = row[6].strip()
                planned_output = row[6].strip()
                actual_output = row[7].strip()
                planned_outcome = row[8].strip()
                actual_outcome = row[9].strip()
                output_calculation = row[10].strip()
                outcome_calculation = row[11].strip()
                location = row[12].strip()
                project = Project.objects.get(project_name=project_name)
                indicator = ProjectIndicator()
                indicator.project = project
                indicator.output_indicator = output_indicator
                indicator.outcome_indicator = outcome_indicator
                indicator.frequency = 'ANNUALLY'
                indicator.output_calculation = output_calculation if output_calculation or output_calculation !='' else 'SUM'
                indicator.outcome_calculation = outcome_calculation if outcome_calculation or outcome_calculation !='' else 'SUM'
                if location:
                    indicator.location = get_location_details(location)
                if milestone_name:
                    milestone = Milestone.objects.get(milestone_name=milestone_name)
                    indicator.milestone = milestone
                indicator.save()
                p=float(planned_outcome) if planned_outcome else 0
                q=float(actual_outcome) if actual_outcome else 0
                ProjectOutcome.objects.create(
                    project_indicator=indicator,
                    project=project,
                    scheduled_date=datetime.datetime.strptime(start_date, '%d/%m/%Y').date(),
                    period_end_date=datetime.datetime.strptime(end_date, '%d/%m/%Y').date(),
                    planned_outcome=p,
                    actual_outcome = q,
                    period_name="Year 1",
                )
                ProjectOutput.objects.create(
                    project_indicator=indicator,
                    project=project,
                    scheduled_date=datetime.datetime.strptime(start_date, '%d/%m/%Y').date(),
                    period_end_date=datetime.datetime.strptime(end_date, '%d/%m/%Y').date(),
                    planned_output = float(planned_output) if planned_output else 0,
                    actual_output = float(actual_output) if actual_output else 0,
                    period_name = "Year 1",
                )
            return True,[]
    else:
        return False,file_errors



# upload_impact_indicator('data_scripts/sample_csv/indicator.csv')


# def read_csv_header_column_list(file):
#     with default_storage.open(file, 'rb') as csvfile:
#         fl = csvfile.read().decode("utf-8")
#         f = StringIO(fl)
#         reader = csv.reader(f, delimiter=',')
#         header = next(reader)
#         # header = header[3:]
#         formatted_header = []
#         for col in header:
#             print(col)
#             words = col.split('(')
#             words = words[::-1]
#             word = {
#                 'type': words[0].strip()[:-1],
#                 'name': words[1].strip()[:-1]
#             }
#             formatted_header.append(word)
#     return formatted_header


# ATTRIBUTE_TYPE_MAP = {
#     'TEXT': 'varchar',
#     'TAGREF': 'varchar',
#     'TIMERANGE': 'varchar',
#     'NUMBER': 'numeric',
#     'DATE': 'date',
#     'PROFILEREF': 'varchar',
#     'PARAGRAPH': 'text',
#     'BOOLEAN': 'boolean'
# }


# def create_template(project, headers, template_name, focus_area, template_type):
#     template = Template()
#     template.template_name = template_name.split('.')[0]
#     template.project = project
#     template.client = project.client
#     template.is_added = True
#     template.focus_area = FocusArea.objects.get(focus_area_id=focus_area)
#     template.template_type = TemplateType.objects.get(template_type=template_type)
#     template.save()
#     template_attribute_list = []
#     order = 0
#     for template_attribute in headers:
#         if (len(template_attribute.get('name')) > 63):
#             print(template_attribute.get('name'))
#             print(len(template_attribute.get('name')))
#         order = order + 1
#         template_attribute_list.append(
#             TemplateAttribute(template=template,
#                               attribute_name=template_attribute.get('name'),
#                               attribute_type=DataType.objects.get(
#                                   data_type=ATTRIBUTE_TYPE_MAP[template_attribute.get('type')]),
#                               attribute_order=order,
#                               attribute_requirement=True,
#                               attribute_required=True,
#                               attribute_grouping='Basic'))
#     TemplateAttribute.objects.bulk_create(template_attribute_list)
#     return template


# def insert_data(file, column_list, client_id, project_id, template_id, batch_id):
#     table_name = get_parent_table_name(client_id, project_id, template_id)
#     db_columns = db_column_details(table_name.lower())
#     with default_storage.open(file) as f:
#         fl = f.read().decode("utf-8")
#         csvfile = StringIO(fl)
#         reader = csv.reader(csvfile, delimiter=',')
#         records = []
#         row_count = 0
#         for row in reader:
#             col_values = ''
#             col_count = -1
#             count = 0
#             for col in row:
#                 col_count = col_count + 1
#                 # if col_count < 3:
#                 #     continue
#                 if len(col) > 100:
#                     print(len(col))
#                     print(col)
#                     print("*" * 100)
#                 if col == '':
#                     col_values += """null, """
#                 elif row_count > 0 and len(db_columns) > count and db_columns[count].get('data_type') == 'date':
#                     date_value = dateutil.parser.parse(col)
#                     date_value = datetime.datetime.strftime(date_value, '%Y-%m-%d')
#                     col_values += """'""" + date_value + """', """
#                 else:
#                     if "'" in col:
#                         col = col.replace("'", "''")
#                     col_values += """'""" + col + """', """
#                 count = count + 1
#             if count < len(db_columns):
#                 while count < len(db_columns):
#                     col_values += """null, """
#                     count = count + 1
#             col_values = col_values[:-2]
#             col_values = """'""" + str(batch_id) + """', """ + col_values
#             row_item = col_values
#             records.append(row_item)
#             row_count += 1
#     records.pop(0)
#     # new_column_list = column_list.split(",")
#     new_column_list = column_list
#     new_column_list.insert(0, 'batch_id')
#     values_sql_list = ''
#     columns = '", "'.join(new_column_list)
#     columns = '"' + columns + '"'
#     insert_sql = """insert into """ + table_name + """(""" + columns + """) values """
#     for record in records:
#         values_sql_list += """(""" + record + """), """
#     insert_sql += values_sql_list[:-2]
#     query = """select * from """ + table_name + """ where deleted = false order by 1, 2"""
#     try:
#         with connection.cursor() as cursor:
#             cursor.execute(insert_sql)
#             recnum = cursor.rowcount
#             cursor.execute(query)
#             result = cursor.fetchall()
#             data_inserted = [dict(zip([key[0] for key in cursor.description], row)) for row in result]
#             inserted_data = [{k: v for k, v in d.items() if k not in ('deleted', 'id')} for d in data_inserted]
#     except Exception as error:
#         return -1, -1, -1, str(error)
#     return batch_id, inserted_data, -1, {}



# def upload_impact_data(file):
#     with default_storage.open(file, 'rb') as f:
#         fl = f.read().decode("utf-8")
#         csvfile = StringIO(fl)
#         reader = csv.reader(csvfile, delimiter=',')
#         row_count = 0
#         for row in reader:
#             if row_count == 0:
#                 row_count = row_count + 1
#                 continue
#             else:
#                 row_count = row_count + 1
#             project_name = row[0].strip()
#             template_name = row[1].strip()
#             template_file = row[2].strip()
#             focus_area = row[3].strip()
#             template_type = row[4].strip()
#             project = Project.objects.get(project_name=project_name)
#             headers = read_csv_header_column_list(template_file)
#             template = create_template(project, headers, template_name, focus_area, template_type)
#             column_list = create_parent_table(project.client.id, project.id, template.id)
#             table = get_parent_table_name(project.client.id, project.id, template.id)
#             batch_id = get_new_batch_id_for_table(table)
#             batch_id_new, response, data_inserted, errors = insert_data(template_file, column_list, project.client.id,
#                                                                         project.id,
#                                                                         template.id, batch_id)
#             CSVUpload.objects.create(
#                 csv_file=template_file,
#                 client=project.client,
#                 project=project,
#                 template=template,
#                 table_name=table,
#                 batch_id=batch_id
#             )
#             print(response)
#             print(data_inserted)
#             print(errors)


# upload_impact_data('data_scripts/sample_csv/impact.csv')

def validate_disbursement(file):
    errors=[]
    choice=[
        "REQUEST_PENDING",
        "NEW_REQUEST",
        "VERIFIED",
        "APPROVED",
        "APPROVAL_REQUESTED",
        "APPROVED",
        "SATTVA_APPROVED",
        "CLIENT_APPROVED",
        "REJECTED",
        "PAYMENT_RELEASED",
        "PAYMENT_RECEIVED"
    ]
    header_list = ['project_name','milestone_name','disbursement_name','disbursement_description','start_date','end_date','expected_date','actual_date','invoice_request_date','approval_date','status','expected_amount','actual_amount','expense_category','due_diligence','tags']
    valid,file_errors=validate_file(file,header_list)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            f2 = f.read().decode("utf-8")
            csvfile = StringIO(f2)
            readerr = csv.reader(csvfile, delimiter=',')
            rowcount = 0
            row_index=0
            for i in readerr:
                row_index=row_index+1
                if rowcount == 0:
                    rowcount = rowcount + 1
                    continue
                else:
                    rowcount = rowcount + 1

                projectname = i[0].strip()
                milestonename = i[1].strip()
                disbursementname= i[2].strip()
                disbursementdescription= i[3].strip()
                startdate = i[4].strip()
                enddate = i[5].strip()
                expecteddate=i[6].strip()
                actualdate = i[7].strip()
                invoicerequest_date= i[8].strip()
                approvaldate= i[9].strip()
                statuss= i[10].strip()
                expectedamount= i[11].strip()
                actualamount= i[12].strip()
                due_diligence= i[14].strip()

                if projectname=='':
                    errors.append(f"Invalid data for column project name at row:{row_index}")

                else:
                    try:
                        Project.objects.get(project_name=projectname)
                    except Project.DoesNotExist:
                        errors.append(f"Invalid project for row:{row_index}")

                if disbursementname =='':
                    errors.append(f"Invalid data for column disbursement_name at row:{row_index}")

                else:
                    try:
                        project=Project.objects.get(project_name=projectname)
                        try:
                            dis=Disbursement.objects.get(disbursement_name=disbursementname,project=project)
                            if dis:
                                errors.append(f"Disbursement with name {disbursementname} already exist please change name at row:{row_index}")
                        except Disbursement.DoesNotExist:
                            pass
                    except Project.DoesNotExist:
                            pass


                if disbursementdescription=='':
                    errors.append(f"Invalid data for column disbursement_description at row:{row_index}")

                if startdate !='':
                    try:
                        datetime.datetime.strptime(startdate, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect start date format, should be DD/MM/YYYY for row: {row_index} ")

                if enddate !='':
                    try:
                        datetime.datetime.strptime(enddate, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect end date format, should be DD/MM/YYYY for row: {row_index}")

                if invoicerequest_date !='':
                    try:
                        datetime.datetime.strptime(invoicerequest_date, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect invoice request date format, should be DD/MM/YYYY for row: {row_index}")

                if approvaldate !='':
                    try:
                        datetime.datetime.strptime(approvaldate, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect approval date format, should be DD/MM/YYYY for row: {row_index}")

                if expecteddate !='':
                    try:
                        datetime.datetime.strptime(expecteddate, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect expected date format, should be DD/MM/YYYY for row: {row_index} ")

                if actualdate !='':
                    try:
                        datetime.datetime.strptime(actualdate, '%d/%m/%Y')
                    except ValueError:
                        errors.append(f"Incorrect actual date format, should be DD/MM/YYYY for row: {row_index} ")

                if statuss not in choice:
                    errors.append(f"Invalid choice for column status at row:{row_index}")

                if due_diligence !='':
                    if due_diligence != 'True' and  due_diligence !='False':
                        errors.append(f"Invalid boolean value for column due_diligence at row:{row_index}")

                if expectedamount=='':
                    errors.append(f"Invalid expected amount for column expected amount at row:{row_index}")

                else:
                    x=expectedamount
                    try:
                        float(x)
                    except ValueError:
                        errors.append(f"Invalid expected amount for column expected amount at row:{row_index}")

                if actualamount !='':
                    x=actualamount
                    try:
                        float(x)
                    except ValueError:
                        errors.append(f"Invalid expected amount for column expected amount at row:{row_index}")

                if milestonename !='':
                    try:
                        project=Project.objects.get(project_name=projectname)
                        try:
                            Milestone.objects.get(milestone_name=milestonename, project=project)
                        except Milestone.DoesNotExist:
                            errors.append(f"Invalid milestone for row:{row_index}")
                    except  Project.DoesNotExist:
                        errors.append(f"Invalid milestone project for row:{row_index}")

            if not errors:
                return True,[]
            else:
                return False,errors
    else:
        return False,file_errors

def upload_disbursement(file):
    valid,file_errors= validate_disbursement(file)
    if valid==True:
        with default_storage.open(file, 'rb') as f:
            fl = f.read().decode("utf-8")
            csvfile = StringIO(fl)
            reader = csv.reader(csvfile, delimiter=',')
            row_count = 0
            for row in reader:
                if row_count == 0:
                    row_count = row_count + 1
                    continue
                else:
                    row_count = row_count + 1
                project_name = row[0].strip()
                milestone_name = row[1].strip()
                disbursement_name= row[2].strip()
                disbursement_description= row[3].strip()
                start_date = row[4].strip()
                end_date = row[5].strip()
                expected_date=row[6].strip()
                actual_date = row[7].strip()
                invoice_request_date= row[8].strip()
                approval_date= row[9].strip()
                status= row[10].strip()
                expected_amount= row[11].strip()
                actual_amount= row[12].strip()
                expense_category= row[13].strip()
                due_diligence= row[14].strip()
                tags = row[15].strip().split(';')
                project = Project.objects.get(project_name=project_name)
                disbursement=Disbursement()
                if milestone_name:
                    milestone = Milestone.objects.get(milestone_name=milestone_name, project=project)
                    disbursement.milestone = milestone
                disbursement.project=project
                disbursement.disbursement_name=disbursement_name
                disbursement.disbursement_description=disbursement_description
                if start_date !='':
                    disbursement.start_date=datetime.datetime.strptime(start_date, '%d/%m/%Y')
                if end_date !='':
                    disbursement.end_date=datetime.datetime.strptime(end_date, '%d/%m/%Y')
                if invoice_request_date !='':
                    disbursement.invoice_request_date=datetime.datetime.strptime(invoice_request_date, '%d/%m/%Y')
                if approval_date !='':
                    disbursement.approval_date=datetime.datetime.strptime(approval_date, '%d/%m/%Y')
                if expected_date !='':
                    disbursement.expected_date=datetime.datetime.strptime(expected_date, '%d/%m/%Y')
                if actual_date !='':
                    disbursement.actual_date=datetime.datetime.strptime(actual_date, '%d/%m/%Y')
                disbursement.expected_amount=expected_amount
                disbursement.actual_amount=actual_amount
                disbursement.expense_category=expense_category
                disbursement.due_diligence=due_diligence
                disbursement.tags=tags
                disbursement.status=status
                disbursement.is_custom_workflow=False
                disbursement.save()
            return True,[]
    else:
        return False,file_errors

# upload_disbursement('data_scripts/sample_csv/disbursement2.csv')

# def upload_Utilisation(file):
#     with default_storage.open(file, 'rb') as f:
#         fl = f.read().decode("utf-8")
#         csvfile = StringIO(fl)
#         reader = csv.reader(csvfile, delimiter=',')
#         row_count = 0
#         for row in reader:
#             if row_count == 0:
#                 row_count = row_count + 1
#                 continue
#             else:
#                 row_count = row_count + 1
#             project_name = row[0].strip()
#             disbursement_name= row[1].strip()
#             unit_cost= row[2].strip()
#             number_of_units= row[3].strip()
#             planned_cost= row[4].strip()
#             expense_category= row[5].strip()
#             description= row[6].strip()
#             particulars= row[7].strip()
#             # actual_cost=row[8].strip()
#             start_date=row[8].strip()
#             end_date= row[9].strip()
#             is_custom_workflow= row[10].strip()
#             previous_utilisation_check= row[11].strip()
#             tags = row[12].strip().split(';')
#             position= row[13].strip()
#             project = Project.objects.get(project_name=project_name)
#             utilisation= Utilisation()
#             if disbursement_name:
#                 disbursement = Disbursement.objects.get(disbursement_name=disbursement_name, project=project)
#                 utilisation.disbursement=disbursement
#             utilisation.project=project
#             utilisation.unit_cost=unit_cost
#             utilisation.number_of_units=number_of_units
#             utilisation.planned_cost=planned_cost
#             utilisation.expense_category=expense_category
#             utilisation.description=description
#             utilisation.particulars=particulars
#             utilisation.start_date= datetime.datetime.strptime(start_date, '%d/%m/%Y')
#             utilisation.end_date=datetime.datetime.strptime(end_date, '%d/%m/%Y')
#             utilisation.is_custom_workflow=is_custom_workflow
#             utilisation.previous_utilisation_check=previous_utilisation_check
#             utilisation.tags=tags
#             utilisation.position=position
#             # utilisation.actual_cost=actual_cost
#             # utilisation.avg_unit=avg_unit_sum
#             # utilisation.avg_unit_cost=avg_unit_cost
#             # utilisation.total_estimate_cost=total_estimate_cost
#             utilisation.save()

# # upload_Utilisation('utl.csv')
