from django.test import TestCase
from .test_setup import TestSetUp
from rest_framework.fields import FileField
from v2.help.models import HelpSections,HelpSubSections,HelpArticles,VideoDetails
from ckeditor_uploader.fields import RichTextUploadingField

class TestHelpSections(TestSetUp):

    def setUp(self):
        super().setUp()
        HelpSections.objects.create(
                section_name = "section",
                path = "xyz"
        )

    def test_values(self):
        hp=HelpSections.objects.get(section_name = "section")
        self.assertEqual(hp.section_name , "section")
        self.assertEqual(hp.path , "xyz")


    def test__str__(self):
        hp=HelpSections.objects.get(section_name = "section")
        self.assertEqual(hp.__str__() , "section")


class TestHelpSubSections(TestSetUp):

    def setUp(self):
        super().setUp()
        HelpSections.objects.create(
        section_name = "section",
        path = "xyz"
        )

        hp=HelpSections.objects.get(section_name = "section")
        HelpSubSections.objects.create(
                section = hp,
                title = "help",
                path = "path",
                code = "c1",
                roles = ['admin'],
                level = 10.5,
                is_head = True
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
             HelpSubSections.objects.create(
                section =12,
                title = "help",
                path = 123,
                code = "c1",
                roles = ['admin'],
                level = 10.5,
                is_head = True
             )

    def test_values(self):
        hss=HelpSubSections.objects.get(title = "help")
        self.assertEqual(hss.section.section_name , "section")
        self.assertEqual(hss.title , "help")
        self.assertEqual(hss.path , "path")
        self.assertEqual(hss.code , "c1")
        self.assertEqual(hss.roles , ['admin'])
        self.assertEqual(hss.level , 10.5)
        self.assertEqual(hss. is_head , True)

    def test__str__(self):
        hss=HelpSubSections.objects.get(title = "help")
        self.assertEqual(hss.__str__(), "c1")


class TestHelpArticles(TestSetUp):

    def setUp(self):
        super().setUp()
        HelpSections.objects.create(
                section_name = "section",
                path = "xyz"
        )

        hs=HelpSections.objects.get(section_name = "section")
        HelpSubSections.objects.create(
                section = hs,
                title = "help",
                path = "path",
                code = "c1",
                roles = ['admin'],
                level = 10.5,
                is_head = True
        )

        helpsubsection1=HelpSubSections.objects.get(section = hs)
        HelpArticles.objects.create(
                content = RichTextUploadingField("hi"),
                title = "article",
                sub_section = helpsubsection1,
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            HelpArticles.objects.create(
                content = RichTextUploadingField("hi"),
                title = "article",
                sub_section = 122,
           )

    def test_values(self):
        helparticle1=HelpArticles.objects.get(title = "article")
        self.assertEqual( helparticle1.sub_section.section.section_name , "section")
        self.assertEqual( helparticle1.title , "article")


    def test__str__(self):
        helparticle1=HelpArticles.objects.get(title = "article")
        self.assertEqual(helparticle1.__str__() , "article")


class TestVideoDetails(TestSetUp):

    def setUp(self):
        super().setUp()
        HelpSections.objects.create(
                section_name = "section",
                path = "xyz"
        )

        hs=HelpSections.objects.get(section_name = "section")
        HelpSubSections.objects.create(
                section = hs,
                title = "help",
                path = "path",
                code = "c1",
                roles = ['admin'],
                level = 10.5,
                is_head = True
        )

        hss=HelpSubSections.objects.get(section = hs)
        VideoDetails.objects.create(
                link = "link1",
                title = "video",
                description = "ddd",
                sub_section = hss
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            VideoDetails.objects.create(
                link = "link1",
                title = "video",
                description = "ddd",
                sub_section = 123
        )

    def test_values(self):
        video=VideoDetails.objects.get( title = "video")
        self.assertEqual( video.link , "link1")
        self.assertEqual( video.title , "video")
        self.assertEqual( video.description , "ddd")
        self.assertEqual( video.sub_section.section.section_name , "section")


    def test__str__(self):
        video=VideoDetails.objects.get( title = "video")
        self.assertEqual( video.__str__() , "video")

