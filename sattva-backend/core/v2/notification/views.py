import django_filters
from core.users.models import User
from django_filters import rest_framework as filters
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import pagination
from datetime import datetime, timedelta

from core.utils import get_value_or_default
from v2.authentication import KeycloakUserUpdateAuthentication
from v2.client.models import UserNotificationSettings
from v2.notification import settings_choices
from v2.notification.models import Notification, NotificationSentLog
from v2.notification.serializers import NotificationSentLogSerializer, NotificationCountSerializer
from v2.user_management.permissions import ProjectFeaturePermission

"""
   Filter for NotificationSentLog which will filter NotificationSentLog by exact user id
"""
class NotificationFilter(django_filters.FilterSet):
    class Meta:
        model = NotificationSentLog
        fields = {
            'user': ['exact'],
        }

"""
   Pagination class for notification default page size is 5 and max size is 100
"""
class NotificationPagination(pagination.PageNumberPagination):
    page_size = 5
    max_page_size = 100
    page_size_query_param = 'records'

    def get_paginated_response(self, data):
        """
        This function will fetch the response in paginated manner
        """
        return Response({
            'count': self.page.paginator.count,
            'current': self.page.number,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'results': data
        })

"""
   Viewset for NotificationSentLog allows CRUD operations
"""
class NotificationViewSet(viewsets.ModelViewSet):
    serializer_class = NotificationSentLogSerializer
    pagination_class = NotificationPagination
    queryset = NotificationSentLog.objects.all().order_by('-created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = NotificationFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]
    permission_classes = [ProjectFeaturePermission]

    def get_queryset(self):
        """
        This function will return the queryset if category is None it will fetch notification by created date
        else by category
        """
        queryset = NotificationSentLog.objects.all().order_by('-created_date')
        category = self.request.query_params.get('category')
        if category is not None:
            queryset = queryset.filter(notification__category=category)
        last_month = datetime.today() - timedelta(days=31)
        queryset = queryset.filter(created_date__gte=last_month)
        return queryset

"""
   Viewset for  NotificationCount allows read operation
"""
class NotificationCountViewSet(viewsets.ModelViewSet):
    serializer_class = NotificationCountSerializer
    queryset = User.objects.all()
    authentication_classes = [KeycloakUserUpdateAuthentication]
    permission_classes = [ProjectFeaturePermission]


"""
   Api view for User Notifications Mark All Read
"""
class UserNotificationsMarkAllRead(APIView):
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def post(self, request):
        """
        This function is used for marking the notification asread
        """
        notification_pk_list = get_value_or_default(request.data, 'notification_ids', [])
        mark_all = self.request.query_params.get('mark_all')
        user = self.request.query_params.get('user')
        if mark_all is not None and user is not None:
            notification_pk_list = NotificationSentLog.objects.filter(user=user)
            print(user, notification_pk_list, mark_all)
        Notification.mark_notifications_as_read(notification_pk_list)
        return Response({'success': True}, status=status.HTTP_200_OK)

"""
   Api view for User Notifications Settings
"""
class UserNotificationsSettings(APIView):
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request):
        """
        This function is used for getting the notification settings.
        """
        try:
            user_obj = request.user
            user_notification_settings = UserNotificationSettings.objects.filter(user=user_obj)
            user_notification_list = []
            for user_notification in settings_choices.TYPE_CHOICES:
                current_user_notification_settings = user_notification_settings.filter(
                    settings_name=user_notification[0])
                current_user_notification_status = current_user_notification_settings.first().is_enabled \
                    if current_user_notification_settings else True
                current_user_email_status = current_user_notification_settings.first().is_email_enabled \
                    if current_user_notification_settings else False
                days = current_user_notification_settings.first().days \
                    if current_user_notification_settings else 1
                user_notification_list.append({"settings_name": user_notification[1],
                                               "settings_status": current_user_notification_status,
                                               "settings_email_status": current_user_email_status,
                                               "type": settings_choices.SETTINGS_CATEGORY[
                                                   user_notification[0]].get('type', ''),
                                               "category": settings_choices.SETTINGS_CATEGORY[
                                                   user_notification[0]].get('category', ''),
                                               "is_periodic": settings_choices.SETTINGS_CATEGORY[
                                                   user_notification[0]].get('is_periodic', False),
                                               "days": days,
                                               "is_after": settings_choices.SETTINGS_CATEGORY[
                                                   user_notification[0]].get('is_after', False),
                                               "settings_id": user_notification[0]})
        except Exception as ex:
            user_notification_list = []
        return Response(user_notification_list, status=status.HTTP_200_OK)

    def post(self, request):
        """
        This function is used for creating the user notification settings.
        """
        try:
            user_obj = request.user
            user_notification_settings = UserNotificationSettings.objects.filter(user=user_obj)
            settings_id = get_value_or_default(request.data, 'settings_id', None)
            settings_status = request.data.get('setting_status', None)
            settings_email_status = request.data.get('setting_email_status', None)
            number_of_days = request.data.get('days', 7)
            if settings_id is not None:
                settings_id = int(settings_id)
                current_user_notification_settings = user_notification_settings.filter(settings_name=settings_id)
                if current_user_notification_settings:
                    user_notification_obj = current_user_notification_settings.first()
                    if settings_status is not None:
                        user_notification_obj.is_enabled = settings_status
                    elif settings_email_status is not None:
                        user_notification_obj.is_email_enabled = settings_email_status
                    if number_of_days:
                        user_notification_obj.days = number_of_days
                    user_notification_obj.save()
                else:
                    UserNotificationSettings.objects.create(
                        user=user_obj,
                        settings_name=settings_id,
                        is_enabled=settings_status if settings_status is not None else True,
                        is_email_enabled=settings_email_status if settings_email_status is not None else False,
                        days=number_of_days,
                    )
            message = {'success': True}
            return Response(message, status=status.HTTP_200_OK)
        except Exception as ex:
            message = {'success': False, 'message': str(ex)}
            return Response(message, status=status.HTTP_200_OK)
