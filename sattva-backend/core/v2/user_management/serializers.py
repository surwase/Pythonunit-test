from rest_framework import serializers

from core.users.models import User
from v2.client.models import Client, ClientUser
from v2.plant.models import Plant, PlantUser
from v2.plant.serializers import PlantOptionSerializer, PlantSerializer
from v2.project.models import Project, ProjectUser
from v2.project.serializers import ProjectOptionSerializer, ProjectSerializer, ProjectMiniMicroSerializer
from v2.user_management.models import ClientFeature, ProjectFeature, ClientFeaturePermission, ProjectFeaturePermission


"""
    This class will serialize ClientFeaturePermission model.
"""
class ClientFeaturePermissionMiniSerializer(serializers.ModelSerializer):
    feature = serializers.SlugRelatedField('feature_code', read_only=True)

    class Meta:
        model = ClientFeaturePermission
        fields = ('client', 'feature', 'permission')


"""
    This class will serialize ProjectFeaturePermission model.
"""
class ProjectFeaturePermissionMiniSerializer(serializers.ModelSerializer):
    feature = serializers.SlugRelatedField('feature_code', read_only=True)

    class Meta:
        model = ProjectFeaturePermission
        fields = ('project', 'feature', 'permission')



"""
    This class will serialize ClientFeaturePermission model.
"""
class ClientFeaturePermissionMicroSerializer(serializers.ModelSerializer):
    feature = serializers.SlugRelatedField('feature_code', read_only=True)
    client_name = serializers.SerializerMethodField()

    def get_client_name(self, user):
        return user.client.client_name

    class Meta:
        model = ClientFeaturePermission
        fields = ('client', 'feature', 'permission', 'client_name')


"""
    This class will serialize ProjectFeaturePermission model and return fields project, feature, permission, project_name.
"""
class ProjectFeaturePermissionMicroSerializer(serializers.ModelSerializer):
    feature = serializers.SlugRelatedField('feature_code', read_only=True)
    project_name = serializers.SerializerMethodField()

    def get_project_name(self, user):
        return user.project.project_name

    class Meta:
        model = ProjectFeaturePermission
        fields = ('project', 'feature', 'permission', 'project_name')


"""
    This class will serialize user model and return fields first_name, last_name, profile_picture,
                  username, id.
"""
class UserMicroSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'profile_picture',
                  'username', 'id']

"""
    This class will serialize user model.
"""
class UserMiniSerializer(serializers.ModelSerializer):
    application_role = serializers.SlugRelatedField(read_only=True, slug_field='role_name')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'profile_picture',
                  'username', 'email', 'status', 'application_role', 'id', 'client_feature_permission',
                  'project_feature_permission', 'ngo')


"""
    This class will serialize user model application_role and id.
"""
class UserApplicationRoleSerializer(serializers.ModelSerializer):
    application_role = serializers.SlugRelatedField(read_only=True, slug_field='role_name')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'application_role', 'id', 'ngo')


"""
    This class will serialize user model.
"""
class UserSerializer(serializers.ModelSerializer):
    clients = serializers.SerializerMethodField('get_user_clients')
    projects = serializers.SerializerMethodField('get_user_projects')
    plants = serializers.SerializerMethodField('get_user_plants')
    application_role = serializers.SlugRelatedField(read_only=True, slug_field='role_name')
    standalone_projects = serializers.SerializerMethodField()

    def get_standalone_projects(self, user):
        client_id = self.context.get('request').query_params.get('client')
        projects = ProjectUser.objects.filter(user=user, project__plant=None)
        if client_id:
            projects = projects.filter(project__client__id=client_id)
        projects = projects.values_list('project__project_name', flat=True)
        return projects

    def get_user_projects(self, user):
        client_id = self.context.get('request').query_params.get('client')
        projects = ProjectUser.objects.filter(user=user)
        if client_id:
            projects = projects.filter(project__client__id=client_id)
        projects = projects.values_list('project__project_name', flat=True)
        return projects

    def get_user_plants(self, user):
        client_id = self.context.get('request').query_params.get('client')
        plants = PlantUser.objects.filter(user=user)
        if client_id:
            plants = plants.filter(plant__client__id=client_id)
        plants = plants.values_list('plant__plant_name', flat=True)
        return plants

    def get_user_clients(self, user):
        clients = Client.objects.filter(
            id__in=ClientUser.objects.filter(user=user).values_list('client', flat=True)).values_list('client_name',
                                                                                                      flat=True)
        return clients

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'clients', 'projects', 'plants', 'profile_picture',
                  'username', 'email', 'status', 'application_role', 'id', 'client_feature_permission',
                  'project_feature_permission', 'standalone_projects', 'ngo', 'date_joined')


"""
    This class will serialize user model.
"""
class UserPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


"""
    This class will serialize Client model for write operation.
"""
class ClientWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class UserMiniDetailedSerializer(UserMiniSerializer):
    project_feature_permission = ProjectFeaturePermissionMiniSerializer(many=True)
    client_feature_permission = ClientFeaturePermissionMiniSerializer(many=True)


"""
    This class will serialize user details.
"""
class UserDetailedSerializer(UserSerializer):

    @staticmethod
    def get_standalone_projects(user):
        projects = ProjectUser.objects.filter(user=user).values_list('project', flat=True)
        project_objects = Project.objects.filter(id__in=projects, plant=None)
        serializer = ProjectMiniMicroSerializer(project_objects, many=True)
        return serializer.data

    @staticmethod
    def get_user_projects(user):
        projects = ProjectUser.objects.filter(user=user).values_list('project', flat=True)
        project_objects = Project.objects.filter(id__in=projects)
        serializer = ProjectMiniMicroSerializer(project_objects, many=True)
        return serializer.data

    @staticmethod
    def get_user_plants(user):
        plants = PlantUser.objects.filter(user=user).values_list('plant', flat=True)
        plant_objects = Plant.objects.filter(id__in=plants)
        serializer = PlantOptionSerializer(plant_objects, many=True, context={'user': user})
        return serializer.data

    @staticmethod
    def get_user_clients(user):
        from v2.client.serializers import ClientOptionReadSerializer
        clients = Client.objects.filter(
            id__in=ClientUser.objects.filter(user=user).values_list('client', flat=True))
        serializer = ClientOptionReadSerializer(clients, many=True)
        return serializer.data

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'clients', 'projects', 'plants', 'profile_picture',
                  'username', 'email', 'status', 'application_role', 'id', 'standalone_projects', 'ngo', 'date_joined')


"""
    This class will serialize user details and return firlds first_name, last_name, clients, projects, plants, profile_picture,
                  username, email, status, application_role, id, standalone_projects, ngo, date_joined.
"""
class UserMicroDetailedSerializer(serializers.ModelSerializer):
    clients = serializers.SerializerMethodField('get_user_clients')
    projects = serializers.SerializerMethodField('get_user_projects')
    plants = serializers.SerializerMethodField('get_user_plants')
    application_role = serializers.SlugRelatedField(read_only=True, slug_field='role_name')
    standalone_projects = serializers.SerializerMethodField()

    @staticmethod
    def get_standalone_projects(user):
        projects = ProjectUser.objects.filter(user=user).values_list('project', flat=True)
        project_objects = Project.objects.filter(id__in=projects, plant=None)
        serializer = ProjectOptionSerializer(project_objects, many=True)
        return serializer.data

    @staticmethod
    def get_user_projects(user):
        projects = ProjectUser.objects.filter(user=user).values_list('project', flat=True)
        project_objects = Project.objects.filter(id__in=projects)
        serializer = ProjectOptionSerializer(project_objects, many=True)
        return serializer.data

    @staticmethod
    def get_user_plants(user):
        plants = PlantUser.objects.filter(user=user).values_list('plant', flat=True)
        plant_objects = Plant.objects.filter(id__in=plants)
        serializer = PlantOptionSerializer(plant_objects, many=True, context={'user': user})
        return serializer.data

    @staticmethod
    def get_user_clients(user):
        from v2.client.serializers import ClientOptionReadSerializer
        clients = Client.objects.filter(
            id__in=ClientUser.objects.filter(user=user).values_list('client', flat=True))
        serializer = ClientOptionReadSerializer(clients, many=True)
        return serializer.data

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'clients', 'projects', 'plants', 'profile_picture',
                  'username', 'email', 'status', 'application_role', 'id', 'standalone_projects', 'ngo', 'date_joined')


"""
    This class will serialize user model and return fields first_name, last_name, username, id.
"""
class UserSimpleSerializer(serializers.ModelSerializer):
    # application_role = serializers.SlugRelatedField(read_only=True, slug_field='role_name')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'id')


"""
    This class will serialize ClientFeature.
"""
class ClientFeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientFeature
        fields = '__all__'


"""
    This class will serialize ProjectFeature.
"""
class ProjectFeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectFeature
        fields = '__all__'


"""
    This class will serialize ClientFeaturePermission.
"""
class ClientFeaturePermissionSerializer(serializers.ModelSerializer):
    feature = ClientFeatureSerializer(allow_null=True, read_only=True)

    class Meta:
        model = ClientFeaturePermission
        fields = '__all__'


"""
    This class will serialize ProjectFeaturePermission.
"""
class ProjectFeaturePermissionSerializer(serializers.ModelSerializer):
    feature = ProjectFeatureSerializer(allow_null=True, read_only=True)

    class Meta:
        model = ProjectFeaturePermission
        fields = '__all__'
