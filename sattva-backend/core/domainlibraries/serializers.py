from rest_framework import serializers
from .models import *

"""
    This class will serialize FocusArea.
"""
class FocusAreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = FocusArea
        fields = '__all__'

"""
    This class will serialize SubFocusArea.
"""
class SubFocusAreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubFocusArea
        fields = '__all__'

"""
    This class will serialize Institution.
"""
class InstitutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institution
        fields = '__all__'

"""
    This class will serialize SDGGoal.
"""
class SDGGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = SDGGoal
        fields = '__all__'

"""
    This class will serialize ApplicationRole.
"""
class ApplicationRoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationRole
        fields = '__all__'

"""
    This class will serialize DataType.
"""
class DataTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataType
        fields = '__all__'

"""
    This class will serialize TargetSegment.
"""
class TargetSegmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TargetSegment
        fields = '__all__'

"""
    This class will serialize SubTargetSegment.
"""
class SubTargetSegmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubTargetSegment
        fields = '__all__'

"""
    This class will serialize Frequency.
"""
class FrequencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Frequency
        fields = '__all__'

"""
    This class will serialize EntityType.
"""
class EntityTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EntityType
        fields = '__all__'

"""
    This class will serialize Entity for create operations.
"""
class EntityCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = '__all__'

"""
    This class will serialize Entity for read operations.
"""
class EntityReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = ['id', 'entity_name', 'attribute_name', 'attribute_requirement',
                  'attribute_type', 'attribute_length', 'attribute_order',
                  'attribute_grouping', 'attribute_lead_indicator_flag',
                  'attribute_classification_indicator_flag']

"""
    This class will serialize AggregateFunction.
"""
class AggregateFunctionSerializer(serializers.ModelSerializer):
    class Meta:
        model = AggregateFunction
        fields = '__all__'

"""
    This class will serialize DocumentType.
"""
class DocumentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentType
        fields = '__all__'

"""
    This class will serialize DocumentClass.
"""
class DocumentClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentClass
        fields = '__all__'

"""
    This class will serialize CSRComplianceCheckList.
"""
class CSRComplianceCheckListSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSRComplianceCheckList
        fields = '__all__'

"""
    This class will serialize NGOSelectionCheckList.
"""
class NGOSelectionCheckListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NGOSelectionCheckList
        fields = '__all__'

"""
    This class will serialize RiskImpact.
"""
class RiskImpactSerializer(serializers.ModelSerializer):
    class Meta:
        model = RiskImpact
        fields = '__all__'

"""
    This class will serialize RiskProbability.
"""
class RiskProbabilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = RiskProbability
        fields = '__all__'

"""
    This class will serialize RiskStatus.
"""
class RiskStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = RiskStatus
        fields = '__all__'

"""
    This class will serialize MitigationStatus.
"""
class MitigationStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = MitigationStatus
        fields = '__all__'

"""
    This class will serialize RiskType.
"""
class RiskTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = RiskType
        fields = '__all__'

"""
    This class will serialize RiskOwner.
"""
class RiskOwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = RiskOwner
        fields = '__all__'

"""
    This class will serialize TaskCategory.
"""
class TaskCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskCategory
        fields = '__all__'


"""
    This class will serialize MCASector.
"""
class MCASectorSerializer(serializers.ModelSerializer):
    class Meta:
        model = MCASector
        # fields = '__all__'
        fields = ['id', 'mca_id', 'mca_focus_area_name', 'mca_sub_focus_area_name', 'status']


# """
#     This class will serialize MCASubFocusAreaSerializer.
# """
# class MCASubFocusAreaSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = MCASector
#         fields = '__all__'
