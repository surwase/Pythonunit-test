def get_location(location):
    area_list = []
    address = []
    if location:
        for loc in location:
            # area_list.append(loc.get('area'))
            area_list.append(loc.get('city'))
            area_list.append(loc.get('state'))
            area_list.append(loc.get('country'))
            address.append(', '.join(area_list))
            area_list.clear()
    print(address)
    return address


def append_in_context(content, context, name):
    if content:
        i = 1
        for item in content:
            key = str(name + "_" + str(i))
            context.update({key: item})
            i += 1
        return context
