import django_filters
import requests

from django_filters import rest_framework as filters
from django.conf import settings
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView

from v2.authentication import KeycloakUserUpdateAuthentication
from v2.kobo.models import KoboAssets
from v2.kobo.permissions import ObservePermission
from v2.kobo.serializers import KoboAssetSerializer


class KoboAssetFilter(django_filters.FilterSet):
    class Meta:
        model = KoboAssets
        fields = {
            'shift_project': ['exact']
        }


class KoboAssetModelViewSet(viewsets.ModelViewSet):
    serializer_class = KoboAssetSerializer
    queryset = KoboAssets.objects.all().order_by('shift_project')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = KoboAssetFilter
    permission_classes = [ObservePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class KoboSyncDbAPIView(APIView):
    permission_classes = [ObservePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        form_uid = request.GET.get('form_uid', '')
        access_token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[-1]
        if settings.KOBO_KPI_URL:
            try:
                sync_dict = {
                    "form_uid": form_uid,
                    "access_token": access_token,
                }
                resp = requests.post(settings.KOBO_KPI_URL + 'remote/sync_db/', json=sync_dict)
                return Response({'success': True}, status=status.HTTP_200_OK)
            except Exception as ex:
                print(ex)
                return Response({'success': False}, status=status.HTTP_400_BAD_REQUEST)
