from rest_framework.test import APIClient, APIRequestFactory, APITestCase, force_authenticate
from django.urls import reverse

from core.users.models import User

from domainlibraries.models import ApplicationRole
from v2.client.models import Client,ClientUser

class TestSetUp(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username='diksha',
            password='password',
            first_name='Diksha',
            last_name='agrawal',
            name='Diksha agrawal',
            email='diksha@test.com'
        )
        self.user.application_role, created = ApplicationRole.objects.get_or_create(role_name='sattva-admin')
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        # self.client.login(user=self.user)
        self.factory = APIRequestFactory()
        self.request = self.factory.get('/accounts/django-superstars/')
        self.client.force_login(self.user)

        self.client_obj = Client.objects.create(
            client_name='client Test10',
            display_name='client Test10',
            website_url='www.company2.com',
            client_cin='VHSP32HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Half-yearly',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )
        return super().setUp()

    def tearDown(self):
        return super().tearDown()


class TestSetUpSattvaUser(APITestCase):

    def setUp(self):

        #sattva-user
        self.user = User.objects.create_user(
            username='alok',
            password='password',
            first_name='alok',
            last_name='sharan',
            name='alok sharan',
            email='alok@test.com'
        )
        self.user.application_role, created = ApplicationRole.objects.get_or_create(role_name='sattva-user')
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        # self.client.login(user=self.user)
        self.factory = APIRequestFactory()
        self.request = self.factory.get('/accounts/django-superstars/')
        self.client.force_login(self.user)

        self.client_obj = Client.objects.create(
            client_name='client Test10',
            display_name='client Test10',
            website_url='www.company2.com',
            client_cin='VHSP32HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Half-yearly',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )
        return super().setUp()

    def tearDown(self):
        return super().tearDown()

class TestSetUpwatcher(APITestCase):

    def setUp(self):

        #watcher
        self.watcher = User.objects.create_user(
            username='sulok',
            password='password',
            first_name='sulok',
            last_name='sharan',
            name='sulok sharan',
            email='sulok@test.com'
        )
        self.watcher.application_role, created = ApplicationRole.objects.get_or_create(role_name='csr-watcher')
        self.watcher.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.watcher)
        # self.client.login(user=self.watcher)
        self.factory = APIRequestFactory()
        self.request = self.factory.get('/accounts/django-superstars/')
        self.client.force_login(self.watcher)

        self.client_obj = Client.objects.create(
            client_name='client Test10',
            display_name='client Test10',
            website_url='www.company2.com',
            client_cin='VHSP32HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Half-yearly',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )
        return super().setUp()

    def tearDown(self):
        return super().tearDown()

class TestSetUpNgoUser(APITestCase):

    def setUp(self):

        #ngouser
        self.user = User.objects.create_user(
            username='sulok',
            password='password',
            first_name='sulok',
            last_name='sharan',
            name='sulok sharan',
            email='sulok@test.com'
        )
        self.user.application_role, created = ApplicationRole.objects.get_or_create(role_name='ngo-user')
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        # self.client.login(user=self.user)
        self.factory = APIRequestFactory()
        self.request = self.factory.get('/accounts/django-superstars/')
        self.client.force_login(self.user)

        self.client_obj = Client.objects.create(
            client_name='client Test10',
            display_name='client Test10',
            website_url='www.company2.com',
            client_cin='VHSP32HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Half-yearly',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )
        return super().setUp()

    def tearDown(self):
        return super().tearDown()

class TestSetUpCsrUser(APITestCase):

    def setUp(self):

        #csruser
        self.user = User.objects.create_user(
            username='sulok',
            password='password',
            first_name='sulok',
            last_name='sharan',
            name='sulok sharan',
            email='sulok@test.com'
        )
        self.user.application_role, created = ApplicationRole.objects.get_or_create(role_name='csr-user')
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        # self.client.login(user=self.user)
        self.factory = APIRequestFactory()
        self.request = self.factory.get('/accounts/django-superstars/')
        self.client.force_login(self.user)

        self.client_obj = Client.objects.create(
            client_name='client Test10',
            display_name='client Test10',
            website_url='www.company2.com',
            client_cin='VHSP32HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Half-yearly',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )
        return super().setUp()

    def tearDown(self):
        return super().tearDown()

class TestSetUpCsrAdmin(APITestCase):

    def setUp(self):

        #csradmin
        self.user = User.objects.create_user(
            username='sulok',
            password='password',
            first_name='sulok',
            last_name='sharan',
            name='sulok sharan',
            email='sulok@test.com'
        )
        self.user.application_role, created = ApplicationRole.objects.get_or_create(role_name='csr-admin')
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        # self.client.login(user=self.user)
        self.factory = APIRequestFactory()
        self.request = self.factory.get('/accounts/django-superstars/')
        self.client.force_login(self.user)

        self.client_obj = Client.objects.create(
            client_name='client Test10',
            display_name='client Test10',
            website_url='www.company2.com',
            client_cin='VHSP32HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Half-yearly',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )
        return super().setUp()

    def tearDown(self):
        return super().tearDown()

class TestSetUpdatacollector(APITestCase):

    def setUp(self):

        #datacollector
        self.user = User.objects.create_user(
            username='sulok',
            password='password',
            first_name='sulok',
            last_name='sharan',
            name='sulok sharan',
            email='sulok@test.com'
        )
        self.user.application_role, created = ApplicationRole.objects.get_or_create(role_name='data-collector')
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        # self.client.login(user=self.user)
        self.factory = APIRequestFactory()
        self.request = self.factory.get('/accounts/django-superstars/')
        self.client.force_login(self.user)

        self.client_obj = Client.objects.create(
            client_name='client Test10',
            display_name='client Test10',
            website_url='www.company2.com',
            client_cin='VHSP32HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Half-yearly',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )
        return super().setUp()

    def tearDown(self):
        return super().tearDown()
