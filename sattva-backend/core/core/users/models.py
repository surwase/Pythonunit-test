# import jwt

from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.models import (BaseUserManager, PermissionsMixin, AbstractUser)
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.db import models
from geolibraries.models import AuditFields
# from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from domainlibraries.models import ApplicationRole
from v2.ngo.models import NGOPartner
from v2.user_management.models import ClientFeaturePermission, ProjectFeaturePermission


class UserManager(BaseUserManager):
    def create_user(self, username, email, first_name, last_name, name, password=None):
        if username is None:
            raise TypeError('Users must have a username.')

        if email is None:
            raise TypeError('Users must have an email address.')

        user = self.model(username=username, email=self.normalize_email(email), first_name=first_name,
                          last_name=last_name, name=name)
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, username, email, first_name, last_name, name, password):
        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(username, email, first_name, last_name, name, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return user


class FeaturePermissionsMixin(models.Model):
    """
    Add the fields and methods necessary to support the Features permission
    """
    client_feature_permission = models.ManyToManyField(
        ClientFeaturePermission, blank=True)
    project_feature_permission = models.ManyToManyField(
        ProjectFeaturePermission, blank=True)

    class Meta:
        abstract = True


class NGOMixin(models.Model):
    ngo = models.ForeignKey(NGOPartner, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        abstract = True


class User(AbstractUser, FeaturePermissionsMixin, NGOMixin):
    STATUS = (('INVITED', 'INVITED'), ('ACTIVE', 'ACTIVE'), ('DISABLED', 'DISABLED'))
    name = models.CharField(_('Name of User'), blank=True, max_length=255, null=True)
    email = models.EmailField(db_index=True, unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'name']
    profile_picture = models.FileField(null=True, blank=True)
    status = models.CharField(max_length=63, choices=STATUS, default='INVITED')
    application_role = models.ForeignKey(ApplicationRole, on_delete=models.CASCADE, null=True)

    objects = UserManager()

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})

    #     @property
    #     def token(self):
    #         return self._generate_jwt_token()

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    def save(self, activity=None, *args, **kwargs):
        # Overriding the save method to create activity log
        from core.users.serializers import UserSerializer
        action = 'user_created'
        old_value = {}
        login_change = False
        if self.pk:
            action = 'user_details_edited'
            old_obj = User.objects.get(pk=self.pk)
            old_value = UserSerializer(old_obj).data
            if self.last_login != old_obj.last_login:
                login_change = True
        if activity is not None:
            action = activity
        super(User, self).save(*args, **kwargs)
        if not login_change:
            UserActivityLog.objects.create(
                object_id=self.pk,
                user=self,
                action=action,
                new_value=UserSerializer(self).data,
                old_value=old_value
            )

    def delete(self):
        # Overriding the delete method to create activity log
        from core.users.serializers import UserSerializer
        action = 'user_deleted'
        old_obj = User.objects.get(pk=self.pk)
        old_value = UserSerializer(old_obj).data
        UserActivityLog.objects.create(
            object_id=self.pk,
            user=self,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(User, self).delete()



#     def _generate_jwt_token(self):
#         # fixme : configrue jwt expiry
#         dt = datetime.now() + timedelta(minutes=settings.JWT_TOKEN_EXPIRY_MINUTES)
#
#         token = jwt.encode({
#             'id': self.pk,
#             'exp': int(dt.strftime('%s'))
#         }, settings.SECRET_KEY, algorithm='HS256')
#
#         return token.decode('utf-8')

class UserActivityLog(AuditFields):
    """
    Model to store Activity Log for User, User Permission
    """
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    old_value = JSONField(default={})
    new_value = JSONField(default={})
    projects_changed = JSONField(default={})
    plants_changed = JSONField(default={})
    clients_changed = JSONField(default={})
    action = models.CharField(max_length=127)
    update_text = models.TextField(null=True, blank=True)
    object_id = models.CharField(max_length=191, null=True)


    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        super(UserActivityLog, self).save(*args, **kwargs)


class AuditLog(models.Model):
    ACTION_CHOICES = (('created', 'created'), ('updated', 'updated'), ('deleted', 'deleted'))
    object_id = models.CharField(
        max_length=191,
        help_text="Primary key of the model under version control.",
    )

    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        help_text="Content type of the model under version control.",
    )
    action_user = models.ForeignKey(User, null=True,
                                    on_delete=models.SET_NULL)
    audit_date = models.DateTimeField(default=datetime.now, blank=True, null=True)
    action = models.CharField(choices=ACTION_CHOICES, max_length=31)
    comment = models.TextField()


def populate_audit_date():
    from django.contrib.contenttypes.models import ContentType
    from django.core.exceptions import ObjectDoesNotExist
    from core.users.models import AuditLog
    audit_logs = AuditLog.objects.all()
    for log in audit_logs:
        if log.action == 'deleted':
            continue
        ct = ContentType.objects.get_for_id(log.content_type_id)
        try:
            obj = ct.get_object_for_this_type(pk=log.object_id)
        except ObjectDoesNotExist:
            continue
        if log.action == 'created':
            log.audit_date = obj.created_date
            log.save()
        elif log.action == 'updated':
            log.audit_date = obj.updated_date
            log.save()
