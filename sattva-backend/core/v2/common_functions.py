import copy
from openpyxl.styles import Font
from openpyxl.utils import get_column_letter
from openpyxl import Workbook
from django.http import HttpResponse
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.styles import Alignment
from v2.column_description import get_label_to_excel


def as_text(value):
    if value is None:
        return ""
    return str(value)


def set_header(sheet, objects, comments, members):
    if len(members)>1:
        sheet.append(objects+['member '+str(i) for i in range(1, len(max(members.values(), key=len))+1)])
    elif comments:
        sheet.append(objects+['comment '+str(i) for i in range(1, 11)])
    else:
        sheet.append(objects)


def set_data_in_sheet(sheet, objects, comments, members):
    risk_status = {
        0: 'Low',
        1: 'Medium',
        2: 'High',
        3: 'Critical'
    }
    governance_type = {
        0: 'Client Call',
        1: 'Client Visit',
        2: 'Partner Call',
        3: 'Partner Visit'
    }
    status = {
        0: 'Open',
        1: 'Pending',
        2: 'Closed',
        3: 'Cancelled'
    }
    for data in objects:
        pop_id = data.pop('id') if data.get('id') else "None"
        if 'tags' in data:
            if data.get('tags'):
                data['tags'] = ', '.join(data['tags'])
            else:
                data['tags'] = None
        if 'sub_focus_area' in data:
            if data.get('sub_focus_area'):
                if isinstance(data.get('sub_focus_area'), list):
                    data['sub_focus_area'] = ', '.join(data['sub_focus_area'])
                else:
                    data['sub_focus_area'] = ', '.join([data['sub_focus_area']])
            else:
                data['sub_focus_area'] =None
        if 'sub_target_segment' in data:
            if data.get('sub_target_segment'):
                if isinstance(data.get('sub_target_segment'), list):
                    data['sub_target_segment'] = ', '.join(data['sub_target_segment'])
                else:
                    data['sub_target_segment'] = ', '.join([data['sub_target_segment']])
            else:
                data['sub_target_segment'] = None
        if 'status' in data:
            if data.get('status'):
                data['status'] = status.get(data['status']) if status.get(data['status']) else data.get('status')
            else:
                data['status']  = None
        if 'severity' in data:
            if data.get('severity'):
                data['severity'] = risk_status.get(data['severity'])
            else:
                data['severity'] =None
        if 'governance_type' in data:
            if data.get('governance_type'):
                data['governance_type'] = governance_type.get(data['governance_type'])
            else:
                data['governance_type'] = None
        if comments.get(pop_id) or members.get(pop_id):
            if comments.get(pop_id):
                sheet.append(list(data.values()) + comments.get(pop_id))
            if members.get(pop_id):
                sheet.append(list(data.values()) + members.get(pop_id))
        else:
            sheet.append(list(data.values()))


def set_column_dimensions(sheet):
    sheet.row_dimensions[1].height = 50
    for column_cells in sheet.columns:
        # length = max(len(as_text(cell.value)) + 2 for cell in column_cells)
        sheet.column_dimensions[get_column_letter(column_cells[0].column)].width = 20 #length


def set_font_to_cell(sheet):
    for cell in sheet["1:1"]:
        cell.font = Font(name='Cambria', size="9", italic=True)
        cell.alignment = Alignment(horizontal='left', vertical='top', wrap_text=True)
    for cell in sheet["2:2"]:
        cell.font = Font(bold=True)
        cell.alignment = Alignment(horizontal='left', vertical='top', wrap_text=True)


def set_labels(sheet, labels):
    sheet.append(labels)


def create_xlsx_file(titles, headers, objects, comments=None, members=None):
    workbook_name = 'shift-data.xlsx'
    wb = Workbook()
    sheet = wb.active
    copy_titles = copy.copy(titles)
    sheet.title = titles.pop(0)
    sheets = [sheet]
    if titles:
        for index, title in enumerate(titles):
            sheet = wb.create_sheet(title=titles[index])
            sheets.append(sheet)
    comment_obj = [{"id": "comment"}, {"id": "comment"}] if comments is None else comments
    member_obj = [{"id": "member"}, {"id": "member"}] if members is None else members
    for obj, sheet, header, comment, member, title in zip(objects, sheets, headers, comment_obj, member_obj, copy_titles):
        labels = get_label_to_excel(header, member, comments, title)
        set_labels(sheet, labels)
        set_header(sheet, header, comments, member)
        set_data_in_sheet(sheet, obj, comment, member)
        set_column_dimensions(sheet)
        set_font_to_cell(sheet)
    response = HttpResponse(save_virtual_workbook(wb),
                            content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename={}".format(workbook_name)
    return response




