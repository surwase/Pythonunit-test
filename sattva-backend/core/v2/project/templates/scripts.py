from django.db import connection

from v2.client.models import Client, ClientDocument, ClientImage, ClientUser, ClientOverview, ClientDetail
from v2.governance.models import Governance
from v2.plant.models import Plant, PlantDocument, PlantImage, PlantLocation, PlantFocusArea, PlantSubFocusArea, PlantTargetSegment, PlantSubTargetSegment, PlantSDGGoals, PlantScheduleVII
from v2.project.models import Project, ProjectLocation, ProjectFocusArea, ProjectSubFocusArea, ProjectTargetSegment, ProjectScheduleVII, ProjectSDGGoals, ProjectSubTargetSegment, ProjectTag, ProjectImage, ProjectDocument, Milestone, MilestoneDocument, Task, TaskDocument, Disbursement, DisbursementDocument, Utilisation, UtilisationDocument, ProjectOutputOutcome, ProjectRemark, Template, TemplateAttribute


def copy_client(client_id, client_name):
    client = Client.objects.get(pk=client_id)
    client.pk = None
    client.client_name = client_name
    client.display_name = client_name
    client.save()
    return client


def copy_client_documents(old_client, new_client):
    old_documents = ClientDocument.objects.filter(client=old_client)
    for document in old_documents:
        document.pk = None
        document.client = new_client
        document.save()


def copy_client_image(old_client, new_client):
    old_images = ClientImage.objects.filter(client=old_client)
    for image in old_images:
        image.pk = None
        image.client = new_client
        image.save()


def copy_client_overview(old_client, new_client):
    old_overviews = ClientOverview.objects.filter(client=old_client)
    for overview in old_overviews:
        overview.pk = None
        overview.client = new_client
        overview.save()


def copy_client_details(old_client, new_client):
    old_details = ClientDetail.objects.filter(client=old_client)
    for overview in old_details:
        overview.pk = None
        overview.client = new_client
        overview.save()


def copy_client_governance(old_client, new_client):
    old_governances = Governance.objects.filter(client=old_client)
    for governance in old_governances:
        governance.pk = None
        governance.client = new_client
        governance.save()
        governance.projects.clear()


def copy_plant(plant_id, new_client, plant_name):
    plant = Plant.objects.get(pk=plant_id)
    plant.pk = None
    plant.plant_name = plant_name
    plant.client = new_client
    plant.save()
    return plant


def copy_plant_documents(old_plant, new_plant):
    documents = PlantDocument.objects.filter(plant=old_plant)
    for document in documents:
        document.pk = None
        document.plant = new_plant
        document.save()


def copy_plant_images(old_plant, new_plant):
    images = PlantImage.objects.filter(plant=old_plant)
    for image in images:
        image.pk = None
        image.plant = new_plant
        image.save()


def copy_plant_location(old_plant, new_plant):
    locations = PlantLocation.objects.filter(plant=old_plant)
    for location in locations:
        location.pk = None
        location.plant = new_plant
        location.save()


def copy_plant_focus_area(old_plant, new_plant):
    focus_areas = PlantFocusArea.objects.filter(plant=old_plant)
    for focus_area in focus_areas:
        focus_area.pk = None
        focus_area.plant = new_plant
        focus_area.save()


def copy_plant_sub_focus_area(old_plant, new_plant):
    sub_focus_areas = PlantSubFocusArea.objects.filter(plant=old_plant)
    for sub_focus_area in sub_focus_areas:
        sub_focus_area.pk = None
        sub_focus_area.plant = new_plant
        sub_focus_area.save()


def copy_plant_target_segment(old_plant, new_plant):
    target_segments = PlantTargetSegment.objects.filter(plant=old_plant)
    for target_segment in target_segments:
        target_segment.pk = None
        target_segment.plant = new_plant
        target_segment.save()


def copy_plant_sub_target_segment(old_plant, new_plant):
    sub_target_segments = PlantSubTargetSegment.objects.filter(plant=old_plant)
    for sub_target_segment in sub_target_segments:
        sub_target_segment.pk = None
        sub_target_segment.plant = new_plant
        sub_target_segment.save()


def copy_plant_sdg_goals(old_plant, new_plant):
    sdg_goals = PlantSDGGoals.objects.filter(plant=old_plant)
    for sdg_goal in sdg_goals:
        sdg_goal.pk = None
        sdg_goal.plant = new_plant
        sdg_goal.save()


def copy_plant_schedule_vii(old_plant, new_plant):
    schedule_vii = PlantScheduleVII.objects.filter(plant=old_plant)
    for activity in schedule_vii:
        activity.pk = None
        activity.plant = new_plant
        activity.save()


def copy_project(project_id, new_client, new_plant, project_name):
    project = Project.objects.get(id=project_id)
    project.pk = None
    project.plant = new_plant
    project.project_name = project_name
    project.client = new_client
    project.save()
    return project


def copy_project_location(old_project, new_project):
    locations = ProjectLocation.objects.filter(project=old_project)
    for location in locations:
        location.pk = None
        location.project = new_project
        location.save()


def copy_project_focus_area(old_project, new_project):
    focus_areas = ProjectFocusArea.objects.filter(project=old_project)
    for focus_area in focus_areas:
        focus_area.pk = None
        focus_area.project = new_project
        focus_area.save()


def copy_project_sub_focus_area(old_project, new_project):
    sub_focus_areas = ProjectSubFocusArea.objects.filter(project=old_project)
    for sub_focus_area in sub_focus_areas:
        sub_focus_area.pk = None
        sub_focus_area.project = new_project
        sub_focus_area.save()


def copy_project_target_segment(old_project, new_project):
    target_segments = ProjectTargetSegment.objects.filter(project=old_project)
    for target_segment in target_segments:
        target_segment.pk = None
        target_segment.project = new_project
        target_segment.save()


def copy_project_sub_target_segment(old_project, new_project):
    sub_target_segments = ProjectSubTargetSegment.objects.filter(project=old_project)
    for sub_target_segment in sub_target_segments:
        sub_target_segment.pk = None
        sub_target_segment.project = new_project
        sub_target_segment.save()


def copy_project_sdg_goals(old_project, new_project):
    sdg_goals = ProjectSDGGoals.objects.filter(project=old_project)
    for sdg_goal in sdg_goals:
        sdg_goal.pk = None
        sdg_goal.project = new_project
        sdg_goal.save()


def copy_project_schedule_vii(old_project, new_project):
    schedule_vii = ProjectScheduleVII.objects.filter(project=old_project)
    for activity in schedule_vii:
        activity.pk = None
        activity.project = new_project
        activity.save()


def copy_project_tags(old_project, new_project):
    tags = ProjectTag.objects.filter(project=old_project)
    for tag in tags:
        tag.pk = None
        tag.project = new_project
        tag.save()


def copy_project_images(old_project, new_project):
    images = ProjectImage.objects.filter(project=old_project)
    for image in images:
        image.pk = None
        image.project = new_project
        image.save()


def copy_project_documents(old_project, new_project):
    documents = ProjectDocument.objects.filter(project=old_project)
    for document in documents:
        document.pk = None
        document.project = new_project
        document.save()


def copy_milestone_document(old_milestone, new_milestone):
    documents = MilestoneDocument.objects.filter(milestone=old_milestone)
    for document in documents:
        document.pk = None
        document.milestone = new_milestone
        document.save()


def copy_task_documents(old_task, new_task):
    documents = TaskDocument.objects.filter(task=old_task)
    for document in documents:
        document.pk = None
        document.task = new_task
        document.save()


def copy_milestone_tasks(old_milestone, new_milestone, new_project):
    tasks = Task.objects.filter(milestone=old_milestone)
    for task in tasks:
        old_task = Task.objects.get(id=task.id)
        task.pk = None
        task.assignee = None
        task.milestone = new_milestone
        task.project = new_project
        task.client = new_project.client
        task.save()
        task.participants.clear()
        copy_task_documents(old_task, task)


def copy_project_milestones(old_project, new_project):
    milestones = Milestone.objects.filter(project=old_project)
    for milestone in milestones:
        old_milestone = Milestone.objects.get(id=milestone.id)
        new_milestone = milestone
        new_milestone.pk = None
        new_milestone.project = new_project
        new_milestone.save()
        new_milestone.participants.clear()
        copy_milestone_document(old_milestone, new_milestone)
        copy_milestone_tasks(old_milestone, new_milestone, new_project)


def copy_project_standalone_tasks(old_project, new_project):
    tasks = Task.objects.filter(project=old_project, milestone=None)
    for task in tasks:
        old_task = Task.objects.get(id=task.id)
        task.pk = None
        task.assignee = None
        task.project = new_project
        task.client = new_project.client
        task.save()
        task.participants.clear()
        copy_task_documents(old_task, task)


def copy_disbursement_documents(old_disbursement, new_disbursement):
    documents = DisbursementDocument.objects.filter(disbursement=old_disbursement)
    for document in documents:
        document.pk = None
        document.disbursement = new_disbursement
        document.save()


def copy_utilisation_documents(old_utilisation, new_utilisation):
    documents = UtilisationDocument.objects.filter(utilisation=old_utilisation)
    for document in documents:
        document.pk = None
        document.disbursement = new_utilisation
        document.save()


def copy_utilisations(old_disbursement, new_disbursement):
    utilisations = Utilisation.objects.filter(disbursement=old_disbursement)
    for utilisation in utilisations:
        old_utilisation = Utilisation.objects.get(id=utilisation.id)
        utilisation.pk = None
        utilisation.disbursement = new_disbursement
        utilisation.project = new_disbursement.project
        utilisation.save()
        copy_utilisation_documents(old_utilisation, utilisation)


def copy_project_disbursements(old_project, new_project):
    disbursements = Disbursement.objects.filter(project=old_project)
    for disbursement in disbursements:
        old_disbursement = Disbursement.objects.get(id=disbursement.id)
        disbursement.pk = None
        disbursement.project = new_project
        disbursement.milestone = None
        disbursement.sattva_approver = None
        disbursement.client_approver = None
        disbursement.save()
        disbursement.participants.clear()
        copy_disbursement_documents(old_disbursement, disbursement)
        copy_utilisations(old_disbursement, disbursement)


def copy_project_standalone_utilisation(old_project, new_project):
    utilisations = Utilisation.objects.filter(disbursement=None, project=old_project)
    for utilisation in utilisations:
        old_utilisation = Utilisation.objects.get(id=utilisation.id)
        utilisation.pk = None
        utilisation.project = new_project
        utilisation.save()
        copy_utilisation_documents(old_utilisation, utilisation)


def copy_project_indicators(old_project, new_project):
    indicators = ProjectOutputOutcome.objects.filter(project=old_project)
    for indicator in indicators:
        indicator.pk = None
        indicator.project = new_project
        indicator.save()


def copy_project_remark(old_project, new_project):
    remarks = ProjectRemark.objects.filter(project=old_project)
    for remark in remarks:
        remark.pk = None
        remark.project = new_project
        remark.save()


def copy_template_attributes(old_template, new_template):
    template_attributes = TemplateAttribute.objects.filter(template=old_template)
    for attribute in template_attributes:
        attribute.pk = None
        attribute.template = new_template
        attribute.save()


def check_if_table_exists(table_name):
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            return True
        else:
            return False
    return False


def create_table_for_template(old_template, new_template):
    with connection.cursor() as cursor:
        old_table_name = 'C' + str(old_template.project.client.id) + '_' + 'P' + str(old_template.project.id) + '_' \
                         + 'T' + str(old_template.id)
        new_table_name = 'C' + str(new_template.project.client.id) + '_' + 'P' + str(new_template.project.id) + '_' \
                         + 'T' + str(new_template.id)
        if check_if_table_exists(old_table_name):
            sql = 'CREATE TABLE {} AS SELECT * FROM {}'.format(new_table_name, old_table_name)
            cursor.execute(sql)


def copy_project_templates(old_project, new_project):
    templates = Template.objects.filter(project=old_project)
    for template in templates:
        old_template = Template.objects.get(id=template.id)
        template.pk = None
        template.project = new_project
        template.save()
        copy_template_attributes(old_template, template)
        create_table_for_template(old_template, template)


def copy_projects_for_plant(projects, old_plant, new_plant):
    for project in projects:
        old_project_id = project.get('project_id')
        project_name = project.get('project_name')
        old_project = Project.objects.get(id=old_project_id)
        new_project = copy_project(old_project_id, new_plant.client, new_plant, project_name)
        copy_project_documents(old_project, new_project)
        copy_project_focus_area(old_project, new_project)
        copy_project_sub_focus_area(old_project, new_project)
        copy_project_target_segment(old_project, new_project)
        copy_project_sub_target_segment(old_project, new_project)
        copy_project_sdg_goals(old_project, new_project)
        copy_project_schedule_vii(old_project, new_project)
        copy_project_tags(old_project, new_project)
        copy_project_documents(old_project, new_project)
        copy_project_images(old_project, new_project)
        copy_project_milestones(old_project, new_project)
        copy_project_standalone_tasks(old_project, new_project)
        copy_project_disbursements(old_project, new_project)
        copy_project_standalone_utilisation(old_project, new_project)
        copy_project_indicators(old_project, new_project)
        copy_project_remark(old_project, new_project)
        copy_project_templates(old_project, new_project)


def copy_plant_for_client(new_client, plants):
    for plant in plants:
        old_plant_id = plant.get('plant_id')
        plant_name = plant.get('plant_name')
        projects = plant.get('projects')
        old_plant = Plant.objects.get(id=old_plant_id)
        new_plant = copy_plant(old_plant_id, new_client, plant_name)
        copy_plant_documents(old_plant, new_plant)
        copy_plant_focus_area(old_plant, new_plant)
        copy_plant_sub_focus_area(old_plant, new_plant)
        copy_plant_target_segment(old_plant, new_plant)
        copy_plant_sub_target_segment(old_plant, new_plant)
        copy_plant_images(old_plant, new_plant)
        copy_plant_location(old_plant, new_plant)
        copy_plant_schedule_vii(old_plant, new_plant)
        copy_plant_sdg_goals(old_plant, new_plant)
        copy_projects_for_plant(projects, old_plant, new_plant)


def copy_standalone_projects(projects, old_client, new_client):
    for project in projects:
        old_project_id = project.get('project_id')
        project_name = project.get('project_name')
        old_project = Project.objects.get(id=old_project_id)
        new_project = copy_project(old_project_id, new_client, None, project_name)
        copy_project_documents(old_project, new_project)
        copy_project_focus_area(old_project, new_project)
        copy_project_sub_focus_area(old_project, new_project)
        copy_project_target_segment(old_project, new_project)
        copy_project_sub_target_segment(old_project, new_project)
        copy_project_sdg_goals(old_project, new_project)
        copy_project_schedule_vii(old_project, new_project)
        copy_project_tags(old_project, new_project)
        copy_project_documents(old_project, new_project)
        copy_project_images(old_project, new_project)
        copy_project_milestones(old_project, new_project)
        copy_project_standalone_tasks(old_project, new_project)
        copy_project_disbursements(old_project, new_project)
        copy_project_standalone_utilisation(old_project, new_project)
        copy_project_indicators(old_project, new_project)
        copy_project_remark(old_project, new_project)
        copy_project_templates(old_project, new_project)


def duplicate_client_and_projects(data):
    client = data.get('client')
    plants = client.get('plants')
    projects = client.get('projects')
    client_id = client.get('client_id')
    old_client = Client.objects.get(id=client_id)
    new_client = copy_client(client_id, client.get('client_name'))
    copy_client_details(old_client, new_client)
    copy_client_documents(old_client, new_client)
    copy_client_image(old_client, new_client)
    copy_client_governance(old_client, new_client)
    copy_client_overview(old_client, new_client)
    copy_plant_for_client(new_client, plants)
    copy_standalone_projects(projects, old_client, new_client)


data = {
    'client': {
        'client_id': 18,
        'client_name': 'Bangalore CSR_Demo_NSE',
        'plants': [{
            'plant_id': 34,
            'plant_name': 'Skilling_Demo_NSE',
            'projects': [
                {'project_id': 81,
                 'project_name': 'Jansewa _Demo_NSE'}
            ]
        }],
        'projects': []
    }
}


def assign_project_to_governances():
    project = Project.objects.get(project_name='Jansewa _Demo_NSE')
    governances = Governance.objects.filter(client__client_name='Bangalore CSR_Demo_NSE')
    for governance in governances:
        governance.projects.add(project)
        governance.save()


duplicate_client_and_projects(data)
assign_project_to_governances()
