# coding=utf-8

from django.core.management.base import BaseCommand
import csv

from v2.project.triggers import create_project_task_alerts_on_due_date


class Command(BaseCommand):
    help = 'Creating Alerts for Project'

    def handle(self, *args, **options):
        try:
            create_project_task_alerts_on_due_date()
        except:
            pass
