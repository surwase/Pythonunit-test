import datetime

from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.timezone import now

from core.authentication.CurrentUser import get_current_user


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf', '.doc', '.docx', '.jpeg', '.jpg', '.png', '.xlsx', '.xls', '.mp4', '.txt', '.svg',
                        '.csv', '.ppt', '.pptx', '.mov', '.wmv', '.flv', '.avi', '.avchd', '.webm', '.mkv']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Supported document types are: .pdf, .doc, .docx, jpeg, .jpg, .png, .xlsx, .xls, '
                              '.mp4, .txt, .svg, .csv, .ppt, .pptx, .mov, .wmv, .flv, .avi, .avchd, .webm, .mkv Only')

"""
   This model will be used to store the AuditFields.
"""
class AuditFields(models.Model):
    created_date = models.DateTimeField(default=now, blank=True)
    created_by = models.CharField(max_length=50, blank=True)
    created_by_username = models.CharField(max_length=50, blank=True)
    updated_date = models.DateTimeField(blank=True, null=True)
    updated_by = models.CharField(max_length=50, null=True, blank=True)
    updated_by_username = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        abstract = True

    def create_audit_log(self, user, action):
        from core.users.models import AuditLog
        content_type = ContentType.objects.get_for_model(self)
        comment = user.username + ' ' + action + ' a ' + content_type.model + ' with id - ' + str(self.pk)
        audit_log = AuditLog.objects.create(
            object_id=self.pk,
            content_type=content_type,
            action_user=user if not user.is_anonymous else None,
            action=action,
            comment=comment
        )

    def save(self, created_date=None, *args, **kwargs):
        adding = self._state.adding
        date = datetime.datetime.now()
        if created_date:
            date = created_date
        user = get_current_user()
        if adding:
            if not user.is_anonymous:
                self.created_by = user.first_name + " " + user.last_name
                self.created_by_username = user.username
            self.created_date = date
            action = 'created'
        else:
            if not user.is_anonymous:
                self.updated_by = user.first_name + " " + user.last_name
                self.updated_by_username = user.username
            self.updated_date = date
            action = 'updated'
        super(AuditFields, self).save(*args, **kwargs)
        self.create_audit_log(user, action)

    def delete(self):
        action = 'deleted'
        user = get_current_user()
        self.create_audit_log(user, action)
        super(AuditFields, self).delete()

"""
   This model will be used to store the Country.
"""
class Country(AuditFields):
    country_id = models.AutoField(primary_key=True)
    country_name = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.country_name

    class Meta:
        db_table = 'country'
        # permissions = (
        #     ('view_country', 'Can view country'),
        # )

"""
   This model will be used to store the State.
"""
class State(AuditFields):
    state_id = models.AutoField(primary_key=True)
    country_name = models.ForeignKey(Country, to_field='country_name', db_column='country_name',
                                     on_delete=models.CASCADE)
    state_name = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.state_name

    class Meta:
        db_table = 'state'
        # permissions = (
        #     ('view_state', 'Can view state'),
        # )

"""
   This model will be used to store the District.
"""
class District(AuditFields):
    district_id = models.AutoField(primary_key=True)
    state_name = models.ForeignKey(State, to_field='state_name', db_column='state_name', on_delete=models.CASCADE)
    district_name = models.CharField(max_length=35)

    def __str__(self):
        return self.district_name

    class Meta:
        unique_together = ('state_name', 'district_name',)
        db_table = 'district'
        # permissions = (
        #     ('view_district', 'Can view district'),
        # )

"""
   This model will be used to store the Location Category.
"""
class LocationCategory(AuditFields):
    location_category_choices = (
        ('urban', 'Urban'),
        ('rural', 'Rural'),
        ('peri-urban', 'Peri-Urban')
    )
    location_category_id = models.AutoField(primary_key=True)
    location_category = models.CharField(max_length=10, choices=location_category_choices, unique=True)

    def __str__(self):
        return self.location_category

    class Meta:
        db_table = 'location_category'
        # permissions = (
        #     ('view_locationcategory', 'Can view location category'),
        # )
