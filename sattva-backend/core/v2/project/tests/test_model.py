from django.test import TestCase
from v2.project.models import *
from v2.plant.models import Plant
from v2.ngo.models import NGOPartner
from .test_setup import TestSetUp
from unittest.mock import patch
from django.core.exceptions import ValidationError
from django.core.files.uploadedfile import SimpleUploadedFile

class TestProject(TestSetUp):
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):

        super().setUp()

        self.ngo_obj = NGOPartner.objects.create(
            ngo_name="ngo1",
            company_type="type1",
            website_url="www.ngo1.com",
            about="hi i am ngo test",
            address="indira nagar",
            employee_count=100,
            annual_budget=1000,
            experience=5,
            awards="abc award",
            state="up"
        )
        self.plant_obj = Plant.objects.create(
            plant_name="plant1",
            plant_description="plantdec",
            client=self.client_obj
        )
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            plant=self.plant_obj,
            budget=1000,
            ngo_partner=self.ngo_obj,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=10,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        milestone1 = Milestone.objects.get(milestone_name="milestone1")
        Task.objects.create(
            task_name="task1",
            task_description="i am task",
            priority="high",
            status="pending",
            task_tag="tag1",
            tags=['tag1'],
            milestone=milestone1,
            client=self.client_obj,
            project=project1,
            percentage_completed=79.6,
            position=100
        )

        ProjectLocation.objects.create(
            project=project1,
            location=['lucknow'],
            area="indira nagar",
            city="lucknow",
            state="up",
            country="India",
            latitude=59.9,
            longitude=60.3)

        ProjectFocusArea.objects.create(
            project=project1,
            focus_area="children")

        ProjectSubFocusArea.objects.create(
            project=project1,
            sub_focus_area="child")

        ProjectTargetSegment.objects.create(
            project=project1,
            target_segment="target1")

        ProjectSubTargetSegment.objects.create(
            project=project1,
            sub_target_segment="subtarget1")

        ProjectSDGGoals.objects.create(
            project=project1,
            sdg_goal_name="sdg1",
            sdg_goal=['sdg'])

        ProjectTag.objects.create(
            project=project1,
            tag=['tag12'])

        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        ProjectImage.objects.create(
            project=project1,
            image_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            image_size=10,
            document_tag=['tag1'],
            path="image")

        ProjectDocument.objects.create(
            project=project1,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size=10,
            document_tag=['tag1'],
            path="image")

    def test_validation(self):
            with self.assertRaises(ValueError):
                self.ngo_obj = NGOPartner.objects.create(
                    ngo_name="ngo1",
                    company_type="type1",
                    website_url="www.ngo1.com",
                    about="hi i am ngo test",
                    address="indira nagar",
                    employee_count=100,
                    annual_budget=1000,
                    experience=5,
                    awards="abc award",
                    state="up"
                )
                self.plant_obj = Plant.objects.create(
                    plant_name="plant12",
                    plant_description="plantdec",
                    client=self.user
                )
                Project.objects.create(
                    project_name="xyzz",
                    project_description="hi i am testedd",
                    client=self.client_obj,
                    plant=self.plant_obj,
                    budget="hi",
                    ngo_partner=self.ngo_obj,
                    project_status="hi",
                    total_disbursement_amount="amount",
                    planned_disbursement_amount="planned",
                    total_utilised_amount="utilised"
                )

    def test_values(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.project_name, "xyz")
        self.assertEqual(project1.project_description, "hi i am tested")
        self.assertEqual(project1.client.client_name, "client Test10")
        self.assertEqual(project1.ngo_partner.ngo_name, "ngo1")
        self.assertEqual(project1.plant.plant_name, "plant1")
        self.assertEqual(project1.budget, 1000)
        self.assertEqual(project1.project_status, 72.22)
        self.assertEqual(project1.total_disbursement_amount, 8804465)
        self.assertEqual(project1.planned_disbursement_amount, 8804465)
        self.assertEqual(project1.total_utilised_amount, 7000000)

    def test__str__(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.__str__(), "client Test10" " - "  "xyz")

    def test_get_partner_display(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_partner_display(), "ngo1")

    def test_get_milestones(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_milestones().count(),1)

    def test_get_name(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_name(), "xyz")

    def test_get_all_project_users(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_project_users().count(),1)

    def test_get_plant(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_plant().id, 1)

    def test_get_completed_milestones(self, milestones=None):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_completed_milestones().count(),0)

    def test_get_milestone_tasks(self):
        milestone1 = Milestone.objects.get(milestone_name="milestone1")
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_milestone_tasks(milestone1).count(),1)

    def test_get_project_status(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_project_status(), 79.6)

    def test_get_locations(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_locations().count(),1)

    def test_get_focus_area(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_focus_area().count(),1)

    def test_get_sub_focus_area(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_sub_focus_area().count(),1)

    def test_get_target_segment(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_target_segment().count(),1)

    def test_get_sub_target_segment(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_sub_target_segment().count(),1)

    def test_get_sdg_goals(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_sdg_goals().count(),1)

    def test_get_tags(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_tags().count(),1)

    def test_get_all_disbursements(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_disbursements().count(),0)

    def test_get_all_utilisations(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_utilisations().count(),0)

    def test_get_all_disbursements_documents(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_disbursements_documents(),None)

    def test_get_schedule_vii_activites(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_schedule_vii_activites().count(),0)

    def test_get_all_project_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_project_file().count(),1)

    def test_get_all_disbursement_project_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_disbursement_project_file().count(),0)

    def test_get_all_milestones_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_milestones_file().count(),0)

    def test_get_all_utilisations_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_utilisations_file().count(),0)

    def test_get_all_utilisation_upload_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_utilisation_upload_file().count(),0)

    def test_get_all_utilisation_expense_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_utilisation_expense_file().count(),0)

    def test_get_all_tasks_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_tasks_file().count(),0)

    def test_get_all_output_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_output_file().count(),0)

    def test_get_all_outcome_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_outcome_file().count(),0)

    def test_get_all_indicators_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_indicators_file().count(),0)

    def test_get_all_casestudies_file(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_casestudies_file().count(),0)

    def test_get_all_images(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_all_images().count(),1)

    def test_get_all_disbursement_total_amount(self, disbursement=None):
        project1 = Project.objects.get(project_name="xyz")
        if disbursement is None:
            self.assertEqual(project1.get_all_disbursement_total_amount(), 0)

    def test_get_all_disbursement_planned_amount(self, disbursement=None):
        project1 = Project.objects.get(project_name="xyz")
        if disbursement is None:
            self.assertEqual(project1.get_all_disbursement_planned_amount(), 0)

    def test_get_total_utilised_amount(self, disbursement=None):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_total_utilised_amount(),0)

    def test_get_percentage_of_disbursement_by_category(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_percentage_of_disbursement_by_category(),{})

    def test_get_percentage_of_utilisation_by_category(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_percentage_of_utilisation_by_category(), {})

    def test_get_percentages_of_milestones_status(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_percentages_of_milestones_status(),{'complete': 100.0})

    def test_get_count_of_risk_status(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_count_of_risk_status(),{})

    def test_get_dashboard(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_dashboard().count(),0)

    def test_get_plant(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.get_plant().id, 210)

    def test_is_plant_mapped(self):
        project1 = Project.objects.get(project_name="xyz")
        self.assertEqual(project1.is_plant_mapped(), True)

class TestDisbursement(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        project1=Project.objects.get(project_name = "ppp")
        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )
        project1=Project.objects.get(project_name = "ppp")
        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )

    def test_validation(self):
            with self.assertRaises(ValueError):
                project1=Project.objects.get(project_name = "ppp")
                user=ProjectUser.objects.get(project = project1)
                milestone=Milestone.objects.get(milestone_name = "milestone1")
                Disbursement.objects.create(
                    disbursement_name = "db123",
                    disbursement_description = "i am dbbb",
                    project = project1,
                    milestone = milestone,
                    start_date = "2020-06-22T18:30:00Z",
                    end_date ="2021-03-22T18:30:00Z",
                    invoice_request_date = "2020-06-23T18:30:00Z",
                    expected_date = "2020-06-24T18:30:00Z",
                    actual_date = "2020-06-24T18:30:00Z",
                    approval_date ="2020-06-26T18:30:00Z",
                    status = 0,
                    expected_amount = "hi",
                    actual_amount = "hi",
                    expense_category = "xyz",
                    sattva_approver = user,
                    client_approver = user,
                    due_diligence = True,
                    sattva_approval_required = True,
                    sattva_approval_status =True,
                    tags = 'tag1',
                    client_approval_status = True,
                    is_custom_workflow =True,
                    previous_utilisation_check = True,
                    position =True
                    )


    def test_values(self):
        dis1=Disbursement.objects.get(disbursement_name="db1")
        self.assertEqual(dis1.disbursement_name ,"db1",)
        self.assertEqual(dis1.disbursement_description ,"i am db")
        self.assertEqual(dis1.project.project_name , "ppp")
        self.assertEqual(dis1.status , "APPROVED")
        self.assertEqual(dis1.expected_amount , 100000)
        self.assertEqual(dis1.actual_amount , 10000)
        self.assertEqual(dis1.expense_category , "xyz")
        self.assertEqual(dis1.client_approval_status , True)

    def test__str__(self):
        dis1=Disbursement.objects.get(disbursement_name = "db1")
        self.assertEqual(dis1.__str__() ,"db1" )

    def test_get_all_utilisations(self):
        from v2.project.models import Utilisation
        dis1=Disbursement.objects.get(disbursement_name = "db1")
        self.assertEqual(dis1.get_all_utilisations().count() , Utilisation.objects.filter(disbursement=dis1).count())

class TestDisbursementcomment(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        project1=Project.objects.get(project_name = "ppp")
        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )
        project1=Project.objects.get(project_name = "ppp")
        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )
        dis=Disbursement.objects.get(disbursement_name = "db1")
        Comment=DisbursementComment.objects.create(
            disbursement = dis,
            comment = "comment1",
            is_read = True
        )

    def test_validation(self):
            with self.assertRaises(ValidationError):
                dis=Disbursement.objects.get(disbursement_name = "db1")
                DisbursementComment.objects.create(
                    disbursement = dis,
                    comment = 123,
                    is_read = 123
                )

    def test_values(self):
        dis=Disbursement.objects.get(disbursement_name = "db1")
        comment = DisbursementComment.objects.get(comment = "comment1")
        self.assertEqual(comment.disbursement, dis)
        self.assertEqual(comment.comment , "comment1")
        self.assertEqual(comment.is_read , True)

    def test__str__(self):
        comment = DisbursementComment.objects.get(comment = "comment1")
        self.assertEqual(comment.__str__(), "db1 - 1")

class TestDisbursementdocument(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        project1=Project.objects.get(project_name = "ppp")
        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )
        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )
        dis=Disbursement.objects.get(disbursement_name = "db1")
        DisbursementDocument.objects.create(
            disbursement = dis,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 20,
            document_tag = ['tag1'],
            path = "path1"
        )

    def test_validation(self):
            with self.assertRaises(ValueError):
                dis=Disbursement.objects.get(disbursement_name = "db1")
                DisbursementDocument.objects.create(
                    disbursement = dis,
                    document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                    doc_size = "abc",
                    document_tag = ['tag1'],
                    path = 123
                )

    def test_values(self):
        dis=Disbursement.objects.get(disbursement_name = "db1")
        doc = DisbursementDocument.objects.get(path = "path1")
        self.assertEqual(doc.disbursement, dis)
        self.assertEqual(doc.doc_size , 20)
        self.assertEqual(doc.path , "path1")

    def test__str__(self):
        doc = DisbursementDocument.objects.get(path = "path1")
        self.assertEqual(doc.__str__(), "db1 - 1")

class TestDisbursement(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        project1=Project.objects.get(project_name = "ppp")
        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )
        project1=Project.objects.get(project_name = "ppp")
        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )


    def test_values(self):
        dis1=Disbursement.objects.get(disbursement_name="db1")
        self.assertEqual(dis1.disbursement_name ,"db1",)
        self.assertEqual(dis1.disbursement_description ,"i am db")
        self.assertEqual(dis1.project.project_name , "ppp")
        self.assertEqual(dis1.status , "APPROVED")
        self.assertEqual(dis1.expected_amount , 100000)
        self.assertEqual(dis1.actual_amount , 10000)
        self.assertEqual(dis1.expense_category , "xyz")
        self.assertEqual(dis1.client_approval_status , True)

    def test__str__(self):
        dis1=Disbursement.objects.get(disbursement_name = "db1")
        self.assertEqual(dis1.__str__() ,"db1" )

    def test_get_all_utilisations(self):
        from v2.project.models import Utilisation
        dis1=Disbursement.objects.get(disbursement_name = "db1")
        self.assertEqual(dis1.get_all_utilisations().count() , Utilisation.objects.filter(disbursement=dis1).count())

class TestDisbursementcomment(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        project1=Project.objects.get(project_name = "ppp")
        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )
        project1=Project.objects.get(project_name = "ppp")
        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )
        dis=Disbursement.objects.get(disbursement_name = "db1")
        Comment=DisbursementComment.objects.create(
            disbursement = dis,
            comment = "comment1",
            is_read = True
        )

    def test_values(self):
        dis=Disbursement.objects.get(disbursement_name = "db1")
        comment = DisbursementComment.objects.get(comment = "comment1")
        self.assertEqual(comment.disbursement, dis)
        self.assertEqual(comment.comment , "comment1")
        self.assertEqual(comment.is_read , True)

    def test__str__(self):
        comment = DisbursementComment.objects.get(comment = "comment1")
        self.assertEqual(comment.__str__(), "db1 - 1")

class TestDisbursementdocument(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        project1=Project.objects.get(project_name = "ppp")
        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )
        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )
        dis=Disbursement.objects.get(disbursement_name = "db1")
        DisbursementDocument.objects.create(
            disbursement = dis,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 20,
            document_tag = ['tag1'],
            path = "path1"
        )

    def test_values(self):
        dis=Disbursement.objects.get(disbursement_name = "db1")
        doc = DisbursementDocument.objects.get(path = "path1")
        self.assertEqual(doc.disbursement, dis)
        self.assertEqual(doc.doc_size , 20)
        self.assertEqual(doc.path , "path1")

    def test__str__(self):
        doc = DisbursementDocument.objects.get(path = "path1")
        self.assertEqual(doc.__str__(), "db1 - 1")


class TestUtilisation(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )
        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )

        dis=Disbursement.objects.get(disbursement_name = "db1")
        Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =dis,
            project = project1,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )

    def test_validation(self):
            with self.assertRaises(ValueError):
                dis=Disbursement.objects.get(disbursement_name = "db1")
                project1=Project.objects.get(project_name = "ppp")
                Utilisation.objects.create(
                    unit_cost = "cost",
                    number_of_units = "units",
                    planned_cost = "cost",
                    avg_unit = "cost",
                    avg_unit_cost = "cost",
                    total_estimate_cost = "cost",
                    expense_category = 1233,
                    description = 334,
                    particulars = 67788,
                    actual_cost = "cost",
                    disbursement =dis,
                    project = project1,
                    is_custom_workflow =True,
                    previous_utilisation_check = True,
                    tags = ['tag1'],
                    position = 0
                        )

    def test_values(self):
        dis=Disbursement.objects.get(disbursement_name = "db1")
        utl=Utilisation.objects.get(expense_category = "category")
        self.assertEqual(utl.unit_cost , 10.2)
        self.assertEqual(utl.number_of_units , 11.2)
        self.assertEqual(utl.planned_cost , 57.2)
        self.assertEqual(utl.expense_category , "category")
        self.assertEqual(utl.description , "text")
        self.assertEqual(utl.particulars , "particular")
        self.assertEqual(utl.disbursement, dis)
        self.assertEqual(utl.previous_utilisation_check , True)
        self.assertEqual(utl.tags , ['tag1'])

    def test__str__(self):
        utl=Utilisation.objects.get(expense_category = "category")
        self.assertEqual(utl.__str__(), "particular")

class TestUtilisationcomment(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )
        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )

        dis=Disbursement.objects.get(disbursement_name = "db1")
        Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =dis,
            project = project1,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )
        utl=Utilisation.objects.get(expense_category = "category")
        UtilisationComment.objects.create(
            utilisation = utl,
            comment = "comment1",
            is_read = True
        )
        UtilisationDocument.objects.create(
            utilisation = utl,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 20,
            document_tag = ['tag1'],
            path = "path1"
        )

    def test_values(self):
        utl=Utilisation.objects.get(expense_category = "category")
        comment = UtilisationComment.objects.get(is_read = True)
        self.assertEqual(comment.utilisation , utl)
        self.assertEqual(comment.comment , "comment1")
        self.assertEqual(comment.is_read , True)

    def test__str__(self):
        cmnt = UtilisationComment.objects.get(is_read = True)
        self.assertEqual(cmnt.__str__(), "particular - Comment 1")

class TestUtilisationdocument(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )

        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )
        dis=Disbursement.objects.get(disbursement_name = "db1")
        Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =dis,
            project = project1,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )
        utl=Utilisation.objects.get(expense_category = "category")
        UtilisationDocument.objects.create(
            utilisation = utl,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 20,
            document_tag = ['tag1'],
            path = "path1"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            utl=Utilisation.objects.get(expense_category = "category")
            UtilisationDocument.objects.create(
                utilisation = utl,
                document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                doc_size = "abc",
                document_tag = ['tag1'],
                path = 123
            )

    def test_values(self):
        dis=Utilisation.objects.get(expense_category = "category")
        doc = UtilisationDocument.objects.get(path = "path1")
        self.assertEqual(doc.utilisation ,dis)
        self.assertEqual(doc.doc_size , 20)
        self.assertEqual(doc.path , "path1")

    def test__str__(self):
        doc = UtilisationDocument.objects.get(path = "path1")
        self.assertEqual(doc.__str__(), "particular - 3")

class TestUtilisationUpload(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )

        UtilisationUpload.objects.create(
            upload_name = "upload1",
            csv_file=SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            batch_id = "batch2",
            file_name ="file1",
            upload_type = "jpg",
            project = project1,
            tags = ['tag1'],
            is_verified = True
        )

    def test_values(self):
        upload = UtilisationUpload.objects.get(upload_name = "upload1")
        self.assertEqual(upload.upload_name , "upload1")
        self.assertEqual(upload.batch_id , "batch2")
        self.assertEqual(upload.file_name ,"file1")
        self.assertEqual(upload.upload_type , "jpg")
        self.assertEqual(upload.tags , ['tag1'])


class TestUtilisationUploadDocument(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )

        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )

        UtilisationUpload.objects.create(
            upload_name = "upload1",
            csv_file=SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            batch_id = "batch2",
            file_name ="file1",
            upload_type = "jpg",
            project = project1,
            tags = ['tag1'],
            is_verified = True
        )
        upload = UtilisationUpload.objects.get(upload_name = "upload1")
        UtilisationUploadDocument.objects.create(
            utilisation_upload = upload,
            document_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 10,
            document_tag = ['tag1'],
            path = "xuz",
            parent = "parent"
        )

    def test_values(self):
        upload = UtilisationUploadDocument.objects.get(doc_size = 10)
        self.assertEqual(upload.doc_size , 10)
        self.assertEqual(upload.document_tag , ['tag1'])
        self.assertEqual(upload.path , "xuz")
        self.assertEqual(upload.parent , 'C676_PL0_PR291_UTIL_FL7')


class TestUtilisationExpense(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )
        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )

        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )

        dis=Disbursement.objects.get(disbursement_name = "db1")
        Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =dis,
            project = project1,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )

        UtilisationUpload.objects.create(
            upload_name = "upload1",
            batch_id = "batch2",
            file_name ="file1",
            upload_type = "jpg",
            project = project1,
            tags = ['tag1'],
            is_verified = True
        )
        upload = UtilisationUpload.objects.get(upload_name = "upload1")
        utl=Utilisation.objects.get(expense_category = "category")
        UtilisationExpense.objects.create(
            description ="hi i am expense",
            split_frequency = "f1",
            estimate_cost = 100000.0,
            number_of_units = 2.5,
            unit_cost = 10000.7,
            actual_cost = 19000999,
            planned_cost = 19000999,
            remarks ="i am remark",
            comments ="comment1",
            utilisation = utl,
            utilisation_upload = upload
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            upload = UtilisationUpload.objects.get(upload_name = "upload1")
            utl=Utilisation.objects.get(expense_category = "category")
            UtilisationExpense.objects.create(
                description ="hi i am expense",
                split_frequency = "f1",
                estimate_cost = "hi",
                number_of_units = "hii",
                unit_cost = "hi",
                actual_cost = "zzzz",
                planned_cost = "zzzz",
                remarks =123,
                comments =123,
                utilisation = utl,
                utilisation_upload = upload
            )

    def test_values(self):
        expense = UtilisationExpense.objects.get(description = "hi i am expense")
        self.assertEqual(expense.description ,"hi i am expense")
        self.assertEqual(expense.estimate_cost , 100000.0)
        self.assertEqual(expense.number_of_units , 2.5)
        self.assertEqual(expense.unit_cost , 10000.7)
        self.assertEqual(expense.comments ,"comment1")
        self.assertEqual(expense.remarks ,"i am remark")
        self.assertEqual(expense.planned_cost , 19000999)

class TestUtilisationExpensedocument(TestSetUp):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Project.objects.create(
        project_name = "ppp",
        project_description = "hi i am tested",
        client = self.client_obj,
        budget = 1000,
        project_status = 72.22,
        total_disbursement_amount = 8804465,
        planned_disbursement_amount = 8804465,
        total_utilised_amount = 7000000
        )
        project1=Project.objects.get(project_name = "ppp")
        Milestone.objects.create(
        milestone_name = "milestone1",
        milestone_description = "hi i am milestone",
        status = "complete",
        project = project1,
        position = 10,
        sub_focus_area = ['malnutrition'],
        sub_target_segment = ['children'],
        location =['lucknow'],
        tags = ['tag1','tag2'],
        )

        ProjectUser.objects.create(
        project = project1,
        user = self.user
        )

        user=ProjectUser.objects.get(project = project1)
        milestone=Milestone.objects.get(milestone_name = "milestone1")
        Disbursement.objects.create(
        disbursement_name = "db1",
        disbursement_description = "i am db",
        project = project1,
        milestone = milestone,
        start_date = "2020-06-22T18:30:00Z",
        end_date ="2021-03-22T18:30:00Z",
        invoice_request_date = "2020-06-23T18:30:00Z",
        expected_date = "2020-06-24T18:30:00Z",
        actual_date = "2020-06-24T18:30:00Z",
        approval_date ="2020-06-26T18:30:00Z",
        status = "APPROVED",
        expected_amount = 100000,
        actual_amount = 10000,
        expense_category = "xyz",
        sattva_approver = user,
        client_approver = user,
        due_diligence = True,
        sattva_approval_required = True,
        sattva_approval_status =True,
        tags = ['tag1'],
        client_approval_status = True,
        is_custom_workflow =True,
        previous_utilisation_check = True,
        position =1
        )

        dis=Disbursement.objects.get(disbursement_name = "db1")
        Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =dis,
            project = project1,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )

        UtilisationUpload.objects.create(
            upload_name = "upload1",
            batch_id = "batch2",
            file_name ="file1",
            upload_type = "jpg",
            project = project1,
            tags = ['tag1'],
            is_verified = True
        )
        upload = UtilisationUpload.objects.get(upload_name = "upload1")
        utl=Utilisation.objects.get(expense_category = "category")
        UtilisationExpense.objects.create(
            description ="hi i am expense",
            split_frequency = "f1",
            estimate_cost = 100000.0,
            number_of_units = 2.5,
            unit_cost = 10000.7,
            actual_cost = 19000999,
            planned_cost = 19000999,
            remarks ="i am remark",
            comments ="comment1",
            utilisation = utl,
            utilisation_upload = upload
        )
        exp=UtilisationExpense.objects.get(description ="hi i am expense")
        UtilisationExpenseDocument.objects.create(
            utilisation_expense = exp,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 20,
            document_tag = ['tag1'],
            path = "path1"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            exp=UtilisationExpense.objects.get(description ="hi i am expense")
            UtilisationExpenseDocument.objects.create(
                utilisation_expense = exp,
                document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                doc_size = "zzzz",
                document_tag = ['tag1'],
                path = 123
            )


    def test_values(self):
        expense = UtilisationExpenseDocument.objects.get(doc_size = 20)
        self.assertEqual(expense.doc_size , 20)
        self.assertEqual(expense.document_tag , ['tag1'])
        self.assertEqual(expense.path , "path1")

class TestImpactWorkflowRuleSet(TestSetUp):

    def setUp(self):
        super().setUp()

        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        project1=Project.objects.get(project_name = "xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        ImpactWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )
        ruleset=ImpactWorkflowRuleSet.objects.get(rule_name = "rule1")
        ImpactWorkflowRule.objects.create(
            ruleset = ruleset,
            rule_type = "REQUESTER",
            level = 1,
            level_name = "level1"
        )
        rule=ImpactWorkflowRule.objects.get(rule_type = "REQUESTER")
        user=ProjectUser.objects.get(project = project1)
        self.assignee=ImpactWorkflowRuleAssignee.objects.create(
            project = project1,
            user = user,
            rule = rule
        )

    def test_values(self):
        ruleset=ImpactWorkflowRuleSet.objects.get(rule_name = "rule1")
        rule=ImpactWorkflowRule.objects.get(rule_type = "REQUESTER")
        assignee=ImpactWorkflowRuleAssignee.objects.get(rule = rule)
        project1=Project.objects.get(project_name = "xyz")
        user=ProjectUser.objects.get(project = project1)
        self.assertEqual(ruleset.rule_name , "rule1")
        self.assertEqual(ruleset.tags , ['tag1'])
        self.assertEqual(ruleset.enabled , True)
        self.assertEqual(rule.rule_type , "REQUESTER")
        self.assertEqual(rule.level , 1)
        self.assertEqual(rule.level_name , "level1")
        self.assertEqual(assignee.project , project1)
        self.assertEqual(assignee.user , user)
        self.assertEqual(assignee.rule , rule)


class TestProjectLocation(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectLocation.objects.create(
            project=project1,
            location=['lucknow'],
            area="indira nagar",
            city="lucknow",
            state="up",
            country="India",
            latitude=59.9,
            longitude=60.3)

    def test_validation(self):
        with self.assertRaises(ValueError):
            project1 = Project.objects.get(project_name="xyz")
            ProjectLocation.objects.create(
                project=project1,
                location=['lucknow'],
                area=123,
                city=123,
                state="up",
                country="India",
                latitude="hi",
                longitude="hi")

    def test_values(self):
        plocation1 = ProjectLocation.objects.get(area="indira nagar")
        self.assertEqual(plocation1.location, ['lucknow'])
        self.assertEqual(plocation1.area, "indira nagar")
        self.assertEqual(plocation1.city, "lucknow")
        self.assertEqual(plocation1.state, "up")
        self.assertEqual(plocation1.country, "India")
        self.assertEqual(plocation1.latitude, 59.9)
        self.assertEqual(plocation1.longitude, 60.3)

    def test__str__(self):
        plocation1 = ProjectLocation.objects.get(area="indira nagar")
        self.assertEqual(plocation1.__str__(), "xyz")


class TestProjectFocusArea(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectFocusArea.objects.create(
            project=project1,
            focus_area="children")

    def test_values(self):
        pf1 = ProjectFocusArea.objects.get(focus_area="children")
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.focus_area, "children")

    def test__str__(self):
        pf1 = ProjectFocusArea.objects.get(focus_area="children")
        self.assertEqual(pf1.__str__(), "xyz")


class TestProjectSubFocusArea(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectSubFocusArea.objects.create(
            project=project1,
            sub_focus_area="child")

    def test_values(self):
        pf1 = ProjectSubFocusArea.objects.get(sub_focus_area="child")
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.sub_focus_area, "child")

    def test__str__(self):
        pf1 = ProjectSubFocusArea.objects.get(sub_focus_area="child")
        self.assertEqual(pf1.__str__(), "xyz")


class TestProjectTargetSegment(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectTargetSegment.objects.create(
            project=project1,
            target_segment="target1")

    def test_values(self):
        pf1 = ProjectTargetSegment.objects.get(target_segment="target1")
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.target_segment, "target1")

    def test__str__(self):
        pf1 = ProjectTargetSegment.objects.get(target_segment="target1")
        self.assertEqual(pf1.__str__(), "xyz")


class TestProjectSubTargetSegment(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectSubTargetSegment.objects.create(
            project=project1,
            sub_target_segment="subtarget1")

    def test_values(self):
        pf1 = ProjectSubTargetSegment.objects.get(sub_target_segment="subtarget1")
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.sub_target_segment, "subtarget1")

    def test__str__(self):
        pf1 = ProjectSubTargetSegment.objects.get(sub_target_segment="subtarget1")
        self.assertEqual(pf1.__str__(), "xyz")


class TestProjectSDGGoals(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectSDGGoals.objects.create(
            project=project1,
            sdg_goal_name="sdg1",
            sdg_goal=['sdg'])

    def test_values(self):
        pf1 = ProjectSDGGoals.objects.get(sdg_goal_name="sdg1")
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.sdg_goal_name, "sdg1")
        self.assertEqual(pf1.sdg_goal, ['sdg'])

    def test__str__(self):
        pf1 = ProjectSDGGoals.objects.get(sdg_goal_name="sdg1")
        self.assertEqual(pf1.__str__(), "xyz")


class TestProjectTag(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectTag.objects.create(
            project=project1,
            tag=['tag12'])

    def test_values(self):
        pf1 = ProjectTag.objects.get(tag=['tag12'])
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.tag, "['tag12']")

    def test__str__(self):
        pf1 = ProjectTag.objects.get(tag=['tag12'])
        self.assertEqual(pf1.__str__(), "xyz")


class TestProjectUser(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

    def test_values(self):
        project1 = Project.objects.get(project_name="xyz")
        pf1 = ProjectUser.objects.get(project=project1)
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.user.username, 'diksha')

    def test__str__(self):
        project1 = Project.objects.get(project_name="xyz")
        pf1 = ProjectUser.objects.get(project=project1)
        self.assertEqual(pf1.__str__(), "xyz"" - " "Diksha")

class TestProjectIndicator(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        mile=Milestone.objects.get(milestone_name="milestone1")
        ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = mile,
            period_name = "period1",
            project = project1,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )

    def test_values(self):
        indicator=ProjectIndicator.objects.get(frequency = "MONTHLY")
        self.assertEqual(indicator.frequency , "MONTHLY")
        self.assertEqual(indicator.period_name , "period1")
        self.assertEqual(indicator.output_indicator , "indicatoroutput")
        self.assertEqual(indicator.output_calculation , "SUM")
        self.assertEqual(indicator.outcome_indicator ,"indicatoroutcome")
        self.assertEqual(indicator.outcome_calculation , "AVERAGE")
        self.assertEqual(indicator.is_beneficiary , False)
        self.assertEqual(indicator.output_status , "Verified")


class TestIndicatorDocument(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        mile=Milestone.objects.get(milestone_name="milestone1")
        ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = mile,
            period_name = "period1",
            project = project1,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )
        ind=ProjectIndicator.objects.get(output_indicator = "indicatoroutput")
        IndicatorDocument.objects.create(
            indicator = ind,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            document_tag = ['tag1'],
            doc_size = 10,
            path = "xyz"
        )
        IndicatorComment.objects.create(
            indicator = ind,
            comment = "comment1"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            ind=ProjectIndicator.objects.get(output_indicator = "indicatoroutput")
            IndicatorDocument.objects.create(
                indicator = ind,
                document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                doc_size = "xyz",

            )


    def test_values(self):
        indicator=ProjectIndicator.objects.get(frequency = "MONTHLY")
        doc=IndicatorDocument.objects.get(path = "xyz")

        self.assertEqual(indicator.frequency , "MONTHLY")
        self.assertEqual(indicator.period_name , "period1")
        self.assertEqual(indicator.output_indicator , "indicatoroutput")
        self.assertEqual(indicator.output_calculation , "SUM")
        self.assertEqual(indicator.outcome_indicator ,"indicatoroutcome")
        self.assertEqual(indicator.outcome_calculation , "AVERAGE")
        self.assertEqual(indicator.is_beneficiary , False)
        self.assertEqual(indicator.output_status , "Verified")
        self.assertEqual(doc.doc_size , 10)
        self.assertEqual(doc.document_tag , ['tag1'])
        self.assertEqual(doc.path , "xyz")

class TestProjectIndicatorOutcome(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        mile=Milestone.objects.get(milestone_name="milestone1")
        ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = mile,
            period_name = "period1",
            project = project1,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )
        ind=ProjectIndicator.objects.get(output_indicator = "indicatoroutput")
        ProjectIndicatorOutcome.objects.create(
            project_indicator = ind,
            outcome = "outcome2",
            outcome_type = "SHORT"
        )

    def test_values(self):
        outcome= ProjectIndicatorOutcome.objects.get(outcome = "outcome2")
        self.assertEqual(outcome.outcome, "outcome2")
        self.assertEqual(outcome.outcome_type, "SHORT")


class TestIndicatorComment(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        mile=Milestone.objects.get(milestone_name="milestone1")
        ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = mile,
            period_name = "period1",
            project = project1,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )
        ind=ProjectIndicator.objects.get(output_indicator = "indicatoroutput")
        IndicatorComment.objects.create(
            indicator = ind,
            comment = "comment1"
        )
        IndicatorActivityLog.objects.create(
            indicator = ind,
            old_value = ['old'],
            new_value = ['new'],
            action = "action",
            update_text = "text"
        )

    def test_values(self):
        cmnt= IndicatorComment.objects.get(comment = "comment1")
        activity=IndicatorActivityLog.objects.get(old_value = ['old'])
        self.assertEqual(cmnt.comment , "comment1")
        self.assertEqual(activity.old_value , ['old'])
        self.assertEqual(activity.new_value , ['new'])
        self.assertEqual(activity.action , "action")
        self.assertEqual(activity.update_text , "text")

class TestProjectOutcome(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        mile=Milestone.objects.get(milestone_name="milestone1")
        ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = mile,
            period_name = "period1",
            project = project1,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )
        ind=ProjectIndicator.objects.get(output_indicator = "indicatoroutput")
        ProjectOutcome.objects.create(
            project_indicator = ind,
            scheduled_date = "2020-06-22",
            period_end_date ="2021-03-22",
            # planned_output = 12.5,
            # actual_output = 15.5,
            period_name = "project1",
            project =project1,
            is_included = True,
            comments = "comment1"
        )

    def test_values(self):
        outcome = ProjectOutcome.objects.get(period_name = "project1")
        self.assertEqual(outcome.period_name , "project1")
        self.assertEqual(outcome.is_included , True)
        self.assertEqual(outcome.comments , "comment1")
        self.assertEqual(outcome.scheduled_date , datetime.date(2020, 6, 22))

class TestProjectOutcomeDocument(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        mile=Milestone.objects.get(milestone_name="milestone1")
        ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = mile,
            period_name = "period1",
            project = project1,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )
        ind=ProjectIndicator.objects.get(output_indicator = "indicatoroutput")
        ProjectOutcome.objects.create(
            project_indicator = ind,
            scheduled_date = "2020-06-22",
            period_end_date ="2021-03-22",
            # planned_output = 12.5,
            # actual_output = 15.5,
            period_name = "project1",
            project =project1,
            is_included = True,
            comments = "comment1"
        )
        outcome = ProjectOutcome.objects.get(period_name = "project1")
        ProjectOutcomeDocument.objects.create(
            project_outcome=outcome,
            document_file=SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 10,
            document_tag = ['tag1'],
            path = "xuz",
            parent = "parent"
        )

    def test_values(self):
        outcome = ProjectOutcomeDocument.objects.get(doc_size = 10)
        self.assertEqual(outcome.doc_size , 10)
        self.assertEqual(outcome.document_tag , ['tag1'])
        self.assertEqual(outcome.path , "xuz")
        self.assertEqual(outcome.parent , "parent")


class TestProjectOutput(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        mile=Milestone.objects.get(milestone_name="milestone1")
        ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = mile,
            period_name = "period1",
            project = project1,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )
        ind=ProjectIndicator.objects.get(output_indicator = "indicatoroutput")
        ProjectOutput.objects.create(
            project_indicator = ind,
            # scheduled_date = "2020-06-22",
            # period_end_date ="2021-03-22",
            planned_output = 12.5,
            actual_output = 15.5,
            period_name = "project1",
            project = project1,
            is_included = True,
            comments = "comment1"
        )

    def test_values(self):
        output = ProjectOutput.objects.get(period_name = "project1")
        self.assertEqual(output.period_name , "project1")
        self.assertEqual(output.is_included , True)
        self.assertEqual(output.comments , "comment1")


class TestProjectOutputDocument(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectUser.objects.create(
            project=project1,
            user=self.user)

        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        mile=Milestone.objects.get(milestone_name="milestone1")
        ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = mile,
            period_name = "period1",
            project = project1,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )
        ind=ProjectIndicator.objects.get(output_indicator = "indicatoroutput")
        ProjectOutput.objects.create(
            project_indicator = ind,
            # scheduled_date = "2020-06-22",
            # period_end_date ="2021-03-22",
            planned_output = 12.5,
            actual_output = 15.5,
            period_name = "project1",
            project = project1,
            is_included = True,
            comments = "comment1"
        )
        output = ProjectOutput.objects.get(period_name = "project1")
        ProjectOutputDocument.objects.create(
            project_output=output,
            document_file=SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 10,
            document_tag = ['tag1'],
            path = "xuz",
            parent = "parent"
        )

    def test_values(self):
        output = ProjectOutputDocument.objects.get(doc_size = 10)
        self.assertEqual(output.doc_size , 10)
        self.assertEqual(output.document_tag , ['tag1'])
        self.assertEqual(output.path , "xuz")
        self.assertEqual(output.parent , "parent")

class TestProjectImage(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectImage.objects.create(
            project=project1,
            image_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            image_size=10,
            document_tag=['tag1'],
            path="image")

    def test_validation(self):
        with self.assertRaises(ValueError):
            project1 = Project.objects.get(project_name="xyz")
            ProjectImage.objects.create(
                project=project1,
                image_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                image_size="abc",
                document_tag=['tag1'],
                path=123)

    def test_values(self):
        pf1 = ProjectImage.objects.get( path="image")
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.document_tag, ['tag1'])
        self.assertEqual(pf1.path, "image")

    def test__str__(self):
        pf1 = ProjectImage.objects.get( path="image")
        self.assertEqual(pf1.__str__(), "xyz"" - ")


class TestProjectDocument(TestSetUp):

    def setUp(self):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectDocument.objects.create(
            project=project1,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size=10,
            document_tag=['tag1'],
            path="image")

    def test_validation(self):
        with self.assertRaises(ValueError):
            project1 = Project.objects.get(project_name="xyz")
            ProjectDocument.objects.create(
                project=project1,
                document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                doc_size="abc",
                document_tag=['tag1'],
                path=123)

    def test_values(self):
        pf1 = ProjectDocument.objects.get(path="image")
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.doc_size, 10)
        self.assertEqual(pf1.document_tag, ['tag1'])
        self.assertEqual(pf1.path, "image")

    def test__str__(self):
        pf1 = ProjectDocument.objects.get(path="image")
        self.assertEqual(pf1.__str__(), "xyz"" - ")


class TestMilestone(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

    def test_values(self):
        pf1 = Milestone.objects.get( milestone_name="milestone1")
        self.assertEqual(pf1.project.project_name, "xyz")
        self.assertEqual(pf1.milestone_name, "milestone1")
        self.assertEqual(pf1.position, 1)
        self.assertEqual(pf1.sub_focus_area, ['malnutrition'])
        self.assertEqual(pf1.sub_target_segment, ['children'])
        self.assertEqual(pf1.location, ['lucknow'])
        self.assertEqual(pf1.tags, ['tag1', 'tag2'])

    def test_get_name(self):
        pf1 = Milestone.objects.get( milestone_name="milestone1")
        self.assertEqual(pf1.get_name(),  "milestone1")


class TestMilestoneNote(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        m1 = Milestone.objects.get(milestone_name="milestone1")
        MilestoneNote.objects.create(
            milestone=m1,
            note="hi i am note",
            is_read=True
        )

    def test_values(self):
        pf1 = MilestoneNote.objects.get(note="hi i am note")
        self.assertEqual(pf1.milestone.milestone_name, "milestone1")
        self.assertEqual(pf1.note, "hi i am note")
        self.assertEqual(pf1.is_read, True)

    def test__str__(self):
        pf1 = MilestoneNote.objects.get(note="hi i am note")
        self.assertEqual(pf1.__str__(),  "milestone1")


class TestMilestoneDocument(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        m1 = Milestone.objects.get(milestone_name="milestone1")
        MilestoneDocument.objects.create(
            milestone=m1,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size=10,
            document_tag=['tag1'],
            path="image"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            m1 = Milestone.objects.get(milestone_name="milestone1")
            MilestoneDocument.objects.create(
                milestone=m1,
                document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                doc_size="abc",
                document_tag=['tag1'],
                path=123
            )

    def test_validation(self):
        with self.assertRaises(ValueError):
            m1 = Milestone.objects.get(milestone_name="milestone1")
            MilestoneDocument.objects.create(
                milestone=m1,
                document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                doc_size="abc",
                document_tag=['tag1'],
                path=123
            )

    def test_values(self):
        pf1 = MilestoneDocument.objects.get(path="image")
        self.assertEqual(pf1.doc_size, 10)
        self.assertEqual(pf1.document_tag, ['tag1'])
        self.assertEqual(pf1.path, "image")

    def test__str__(self):
        pf1 = MilestoneDocument.objects.get(path="image")
        self.assertEqual(pf1.__str__(),  "milestone1")


class TestTask(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        m1 = Milestone.objects.get(milestone_name="milestone1")
        Task.objects.create(
            task_name="task1",
            task_description="i am task",
            priority="high",
            status="pending",
            task_tag="tag1",
            tags=['tag1'],
            milestone=m1,
            client=self.client_obj,
            project=project1,
            percentage_completed=79.6,
            position=100
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            m1 = Milestone.objects.get(milestone_name="milestone1")
            project1 = Project.objects.get(project_name="xyz")
            Task.objects.create(
                task_name="task1",
                task_description="i am task",
                priority="high",
                status="pending",
                task_tag="tag1",
                tags=['tag1'],
                milestone=m1,
                client=self.client_obj,
                project=project1,
                percentage_completed="indicatoroutput",
                position="category"
            )

    def test_values(self):
        ts1 = Task.objects.get(task_name="task1")
        self.assertEqual(ts1.task_description, "i am task")
        self.assertEqual(ts1.priority, "high")
        self.assertEqual(ts1.status, "pending")
        self.assertEqual(ts1.tags, ['tag1'])

    def test__str__(self):
        ts1 = Task.objects.get(task_name="task1")
        self.assertEqual(ts1.__str__(), "i am task")


class TestTask(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        m1 = Milestone.objects.get(milestone_name="milestone1")
        Task.objects.create(
            task_name="task1",
            task_description="i am task",
            priority="high",
            status="pending",
            task_tag="tag1",
            tags=['tag1'],
            milestone=m1,
            client=self.client_obj,
            project=project1,
            percentage_completed=79.6,
            position=100
        )
        task = Task.objects.get(task_name="task1")
        TaskComment.objects.create(
            task=task,
            comment="task1",
            is_read=True
        )

    def test_values(self):
        ts1 = TaskComment.objects.get(comment="task1")
        self.assertEqual(ts1.comment, "task1")
        self.assertEqual(ts1.is_read, True)

    def test__str__(self):
        ts1 = TaskComment.objects.get(comment="task1")
        self.assertEqual(ts1.__str__(), "task1" " - " "1")


class TestTaskDocument(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        m1 = Milestone.objects.get(milestone_name="milestone1")
        Task.objects.create(
            task_name="task1",
            task_description="i am task",
            priority="high",
            status="pending",
            task_tag="tag1",
            tags=['tag1'],
            milestone=m1,
            client=self.client_obj,
            project=project1,
            percentage_completed=79.6,
            position=100
        )
        task = Task.objects.get(task_name="task1")
        TaskDocument.objects.create(
            task=task,
            document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size=10,
            document_tag=['tag1'],
            path="image"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            task = Task.objects.get(task_name="task1")
            TaskDocument.objects.create(
                task=task,
                document_file= SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
                doc_size="abc",
                document_tag=['tag1'],
                path=123
            )

    def test_values(self):
        ts1 = TaskDocument.objects.get(path="image")
        self.assertEqual(ts1.doc_size, 10)
        self.assertEqual(ts1.document_tag, ['tag1'])
        self.assertEqual(ts1.path, "image")

    def test__str__(self):
        ts1 = TaskDocument.objects.get(path="image")
        self.assertEqual(ts1.__str__(), "task1" " - " "1")

class TestTemplate(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        TemplateType.objects.create(
            template_type_id = 1,
            template_type ="hi"
        )
        type=TemplateType.objects.get(template_type_id = 1)
        Template.objects.create(
            template_name = "temp1",
            template_type = type,
            project = project1,
            client = self.client_obj,
            is_added = True,
            is_standard = True,
            standardization_requested = True,
            table_created = False,
            data_status = "Verified"
        )

    def test_values(self):
        temp = Template.objects.get(template_name = "temp1")
        self.assertEqual(temp.template_name , "temp1")
        self.assertEqual(temp.is_added , True)
        self.assertEqual(temp.is_standard , True)
        self.assertEqual(temp.standardization_requested , True)
        self.assertEqual(temp.table_created , False)
        self.assertEqual(temp.data_status , "Verified")


class TestImpactCaseStudy(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ImpactCaseStudy.objects.create(
            project = project1,
            title = "title",
            description = "hi i am impact",
            focus_area = ['focus'],
            sub_focus_area = ['subfocus'],
            target_segment = ['target'],
            sub_target_segment = ['subtarget'],
            location = ['lucknow'],
            tags = ['tag1'],
            logo = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            is_key_case_study = True
        )

    def test_values(self):
        case_study = ImpactCaseStudy.objects.get(title = "title")
        self.assertEqual(case_study.title , "title")
        self.assertEqual(case_study.description , "hi i am impact")
        self.assertEqual(case_study.focus_area , ['focus'])
        self.assertEqual(case_study.location , ['lucknow'])
        self.assertEqual(case_study.sub_focus_area , ['subfocus'])
        self.assertEqual(case_study.tags , ['tag1'])

class TestProjectDashboard(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        ProjectDashboard.objects.create(
            project = project1,
            provider = 0,
            dashboard_name = "dashboardname",
            url = "www.url.com",
            embedded_content = "content",
            password = "password"
        )

    def test_values(self):
        dash = ProjectDashboard.objects.get(provider = 0)
        self.assertEqual(dash.provider , 0)
        self.assertEqual(dash.dashboard_name , "dashboardname")
        self.assertEqual(dash.url , "www.url.com")
        self.assertEqual(dash.embedded_content , "content")
        self.assertEqual(dash.password , "password")

class TestCSVUpload(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        project1 = Project.objects.get(project_name="xyz")
        TemplateType.objects.create(
            template_type_id = 1,
            template_type ="hi"
        )
        type=TemplateType.objects.get(template_type_id = 1)
        Template.objects.create(
            template_name = "temp1",
            template_type = type,
            project = project1,
            client = self.client_obj,
            is_added = True,
            is_standard = True,
            standardization_requested = True,
            table_created = False,
            data_status = "Verified"
        )
        temp = Template.objects.get(template_name = "temp1")
        CSVUpload.objects.create(
            csv_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            client = self.client_obj,
            project = project1,
            template = temp,
            status = 'Active',
            table_name = "t1",
            batch_id = 1,
            upload_successful = True,
            is_deleted = False,
            is_custom_workflow = True
                )

    def test_values(self):
        csv = CSVUpload.objects.get(status = 'Active')
        self.assertEqual(csv.status , 'Active')
        self.assertEqual(csv.table_name , "t1")
        self.assertEqual(csv.batch_id , 1)
        self.assertEqual(csv.upload_successful , True)
        self.assertEqual(csv.is_deleted , False)
        self.assertEqual(csv.is_custom_workflow , True)
