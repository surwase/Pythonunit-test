# Generated by Django 2.0.5 on 2019-11-25 03:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0019_auto_20191124_2112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='utilisation',
            name='expense_category',
            field=models.CharField(blank=True, max_length=511, null=True),
        ),
    ]
