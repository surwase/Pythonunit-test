# Generated by Django 2.0.5 on 2020-02-06 06:50

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('ngo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NGOFocusArea',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('created_by_username', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('updated_by_username', models.CharField(blank=True, max_length=50, null=True)),
                ('focus_area', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NGOLocation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('created_by_username', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('updated_by_username', models.CharField(blank=True, max_length=50, null=True)),
                ('location', django.contrib.postgres.fields.jsonb.JSONField(default=[])),
                ('area', models.CharField(default='', max_length=256, null=True)),
                ('city', models.CharField(default='', max_length=256, null=True)),
                ('state', models.CharField(default='', max_length=256, null=True)),
                ('country', models.CharField(default='', max_length=256, null=True)),
                ('latitude', models.FloatField(default=0, max_length=31, null=True)),
                ('longitude', models.FloatField(default=0, max_length=31, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NGOPointOfContact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('created_by_username', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('updated_by_username', models.CharField(blank=True, max_length=50, null=True)),
                ('name', models.CharField(max_length=511)),
                ('phone_number', models.CharField(max_length=31)),
                ('email_address', models.EmailField(max_length=511)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NGOSubFocusArea',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('created_by_username', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('updated_by_username', models.CharField(blank=True, max_length=50, null=True)),
                ('sub_focus_area', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NGOSubTargetSegment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('created_by_username', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('updated_by_username', models.CharField(blank=True, max_length=50, null=True)),
                ('sub_target_segment', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='NGOTargetSegment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('created_by_username', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('updated_by_username', models.CharField(blank=True, max_length=50, null=True)),
                ('target_segment', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='about',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='address',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='annual_budget',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='awards',
            field=models.CharField(default='', max_length=1027),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='company_type',
            field=models.CharField(default='', max_length=127),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='employee_count',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='experience',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='logo',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='state',
            field=models.CharField(default='', max_length=63),
        ),
        migrations.AddField(
            model_name='ngopartner',
            name='website_url',
            field=models.CharField(default='', max_length=1024),
        ),
        migrations.AddField(
            model_name='ngotargetsegment',
            name='ngo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ngo.NGOPartner'),
        ),
        migrations.AddField(
            model_name='ngosubtargetsegment',
            name='ngo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ngo.NGOPartner'),
        ),
        migrations.AddField(
            model_name='ngosubfocusarea',
            name='ngo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ngo.NGOPartner'),
        ),
        migrations.AddField(
            model_name='ngopointofcontact',
            name='ngo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ngo.NGOPartner'),
        ),
        migrations.AddField(
            model_name='ngolocation',
            name='ngo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ngo.NGOPartner'),
        ),
        migrations.AddField(
            model_name='ngofocusarea',
            name='ngo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ngo.NGOPartner'),
        ),
    ]
