# Generated by Django 2.0.5 on 2020-06-24 02:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0032_remove_customfolder_folder_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='clientdocument',
            name='parent',
            field=models.CharField(default='', max_length=255),
        ),
    ]
