from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import User, AuditLog, UserActivityLog

from geolibraries.models import Country, State, District
from domainlibraries.models import FocusArea, SubFocusArea, TargetSegment, SubTargetSegment, Entity, DocumentClass, \
    CSRComplianceCheckList, NGOSelectionCheckList


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):
    error_message = UserCreationForm.error_messages.update(
        {"duplicate_username": "This username has already been taken."}
    )

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username

        raise forms.ValidationError(self.error_messages["duplicate_username"])


@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (("User Profile", {
        "fields": (
            "name", "client_feature_permission", "project_feature_permission", "status", "ngo",
            "application_role")}),) + AuthUserAdmin.fieldsets
    list_display = ("username", "name", "is_superuser")
    search_fields = ["name"]


class AuditLogAdmin(admin.ModelAdmin):
    list_display = ('action_user', 'object_id', 'action', 'comment', 'audit_date', 'content_type',)
    list_filter = ('action',)
    search_fields = ["comment"]
admin.site.register(AuditLog, AuditLogAdmin)


class UserActivityLogAdmin(admin.ModelAdmin):
    list_display = ('user', 'object_id', 'action', 'created_date', 'updated_date', 'updated_by')
    list_filter = ('action',)
    search_fields = ["object_id"]
admin.site.register(UserActivityLog, UserActivityLogAdmin)



# GeoLibraries
admin.site.register(Country)
admin.site.register(State)
admin.site.register(District)

# DomainLibraries
admin.site.register(FocusArea)
admin.site.register(SubFocusArea)
admin.site.register(TargetSegment)
admin.site.register(SubTargetSegment)
admin.site.register(Entity)
admin.site.register(DocumentClass)
admin.site.register(CSRComplianceCheckList)
admin.site.register(NGOSelectionCheckList)
