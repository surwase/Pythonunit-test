import json

from django.contrib.postgres.fields import JSONField
from django.db import models

from core.constants import SHADOW_MODEL_APP_LABEL


class ReadOnlyModelError(ValueError):
    pass


class ShadowModel(models.Model):
    """
    Allows identification of writeable and read-only shadow models
    """

    class Meta:
        managed = False
        abstract = True
        # TODO find out why it raises a warning when user logs in.
        # ```
        #   RuntimeWarning: Model '...' was already registered.
        #   Reloading models is not advised as it can lead to inconsistencies,
        #   most notably with related models
        # ```
        # Maybe because `SHADOW_MODEL_APP_LABEL` is not declared in `INSTALLED_APP`
        # It's just used for `DefaultDatabaseRouter` conditions.
        app_label = SHADOW_MODEL_APP_LABEL


class ReadOnlyModel(ShadowModel):
    class Meta(ShadowModel.Meta):
        abstract = True

    def save(self, *args, **kwargs):
        raise ReadOnlyModelError('Cannot save read-only-model')

    def delete(self, *args, **kwargs):
        raise ReadOnlyModelError('Cannot delete read-only-model')


class KoboModel(ReadOnlyModel):
    class Meta(ShadowModel.Meta):
        abstract = True
        app_label = SHADOW_MODEL_APP_LABEL


class KoboUser(KoboModel):
    username = models.CharField(max_length=150)
    email = models.EmailField()

    class Meta(KoboModel.Meta):
        db_table = "auth_user"


class KoboAssets(KoboModel):
    name = models.CharField(max_length=255, blank=True, default='')
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    settings = JSONField(default=dict)
    shift_project = models.IntegerField(null=True, blank=True)
    uid = models.CharField(max_length=22)
    owner = models.ForeignKey(KoboUser, on_delete=models.DO_NOTHING)
    _deployment_data = JSONField(default=dict)

    def get_is_active(self):
        deployment_data = json.loads(self._deployment_data)
        is_active = deployment_data.get('active', False)
        return is_active

    class Meta(KoboModel.Meta):
        db_table = "kpi_asset"
