# Generated by Django 2.2.16 on 2021-01-04 12:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0136_auto_20210104_1142'),
    ]

    operations = [
        migrations.AddField(
            model_name='utilisationupload',
            name='upload_name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
