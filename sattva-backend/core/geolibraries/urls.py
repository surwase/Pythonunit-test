from .views import CountryViewSet, StateViewSet, DistrictViewSet, LocationCategoryViewSet
from rest_framework import routers
from django.conf.urls import url

app_name = "geolibraries"

# router = routers.DefaultRouter()
# router.register(r'^countries', CountryViewSet, 'countries')
# urlpatterns = router.urls

urlpatterns = [

    url(r'^countries/$', CountryViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='country-list'),
    url(r'^countries/(?P<pk>\d+)/$', CountryViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='country-detail'),

    url(r'^states/$', StateViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='state-list'),
    url(r'^states/(?P<pk>\d+)/$', StateViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='state-detail'),

    url(r'^districts/$', DistrictViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='district-list'),
    url(r'^districts/(?P<pk>\d+)/$', DistrictViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='district-detail'),

    url(r'^locationcategories/$', LocationCategoryViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='locationcategory-list'),
    url(r'^locationcategories/(?P<pk>\d+)/$', LocationCategoryViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='locationcategory-detail'),

]
