from core.users.models import User
from v2.client.models import ClientUser
from v2.project.models import Project, ProjectUser
from v2.user_management.models import ClientFeature, ProjectFeature, ClientFeaturePermission, ProjectFeaturePermission

default_client_features = [
    {
        "feature": "CLIENT_OVERVIEW",
        "name": "Client Overview",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
            ]
        }
    },
    {
        "feature": "PROGRAM_OVERVIEW",
        "name": "Program Overview",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
            ]
        }
    },
    {
        "feature": "ALL_TASKS",
        "name": "All Tasks",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user', 'ngo-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
                "View",
                "Change",
                "Delete"
            ]
        }
    },
    {
        "feature": "GOVERNANCE",
        "name": "Governance",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
            ]
        }
    },
    {
        "feature": "COMPLIANCE",
        "name": "Compliance",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": []
        }
    },
    {
        "feature": "DOCUMENTS",
        "name": "Documents",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user', 'ngo-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
                "View",
                "Change",
                "Delete"
            ]
        }
    },
    {
        "feature": "MANAGE_USERS",
        "name": "Manage Users",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
            ]
        }
    }
]

default_project_features = [
    {
        "feature": "PROJECT_OVERVIEW",
        "name": "Project Overview",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user', 'ngo-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
                "View",
                "Change",
                "Delete"
            ]
        }
    },
    {
        "feature": "PROJECT_PLAN",
        "name": "Project Plan",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user', 'ngo-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
                "View",
                "Change",
                "Delete"
            ]
        }
    },
    {
        "feature": "FINANCIALS",
        "name": "Financials",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user', 'ngo-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
                "View",
                "Change",
                "Delete"
            ]
        }
    },
    {
        "feature": "IMPACT",
        "name": "Impact",
        "applicable_roles": ['sattva-user', 'csr-admin', 'csr-user', 'ngo-user'],
        "default_permissions": {
            "sattva-user": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-admin": [
                "View",
                "Change",
                "Delete"
            ],
            "csr-user": [
                "View",
                "Change",
                "Delete"
            ],
            "ngo-user": [
                "View",
                "Change",
                "Delete"
            ]
        }
    }
]


def delete_existing_features():
    client_features = ClientFeature.objects.all()
    client_features.delete()
    project_features = ProjectFeature.objects.all()
    project_features.delete()


delete_existing_features()


def create_client_features():
    for client_feature in default_client_features:
        print(client_feature.get('applicable_roles'))
        ClientFeature.objects.update_or_create(feature_code=client_feature.get('feature'),
                                               feature_name=client_feature.get('name'), defaults={
                'default_permissions': client_feature.get('default_permissions'),
                'applicable_roles': client_feature.get('applicable_roles'),
            })


def create_project_features():
    for project_feature in default_project_features:
        print(project_feature.get('applicable_roles'))
        ProjectFeature.objects.update_or_create(feature_code=project_feature.get('feature'),
                                                feature_name=project_feature.get('name'), defaults={
                'default_permissions': project_feature.get('default_permissions'),
                'applicable_roles': project_feature.get('applicable_roles'),
            })


create_client_features()
create_project_features()


def assign_permission_to_user():
    client_features = ClientFeature.objects.all()
    project_features = ProjectFeature.objects.all()
    users = User.objects.all().exclude(username='sattva-admin').exclude(application_role=None)
    for user in users:
        user_role = user.application_role.role_name
        clients = ClientUser.objects.filter(user=user).values_list('client', flat=True)
        for client in clients:
            for client_feature in client_features:
                if user_role in client_feature.applicable_roles:
                    permissions = client_feature.default_permissions[user_role]
                    client_permissions = ClientFeaturePermission.objects.filter(client=client, feature=client_feature,
                                                                                permission__in=permissions)
                    user.client_feature_permission.add(*client_permissions)
            for project in ProjectUser.objects.filter(user=user).values_list('project', flat=True):
                for project_feature in project_features:
                    if user_role in project_feature.applicable_roles:
                        permissions = project_feature.default_permissions[user_role]
                        project_permissions = ProjectFeaturePermission.objects.filter(project=project,
                                                                                      feature=project_feature,
                                                                                      permission__in=permissions)
                        user.project_feature_permission.add(*project_permissions)


assign_permission_to_user()
