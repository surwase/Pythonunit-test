# Generated by Django 2.0.5 on 2018-07-26 12:14

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AggregateFunction',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('agg_func_id', models.AutoField(primary_key=True, serialize=False)),
                ('agg_func', models.CharField(max_length=20, unique=True)),
            ],
            options={
                'db_table': 'aggregate_function',
                'permissions': (('view_aggregatefunction', 'Can view aggregate function'),),
            },
        ),
        migrations.CreateModel(
            name='ApplicationRole',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('role_id', models.AutoField(primary_key=True, serialize=False)),
                ('role_name', models.CharField(
                    choices=[('SuperAdmin', 'SuperAdmin'), ('SattvaAdmin', 'SattvaAdmin'), ('CSRAdmin', 'CSRAdmin'),
                             ('CSRLeadership', 'CSRLeadership'), ('CSRSpoc', 'CSRSpoc'),
                             ('CSRSpocManager', 'CSRSpocManager'), ('CSRVolunteer', 'CSRVolunteer'),
                             ('CSREmployee', 'CSREmployee'), ('CSRProjectApprover', 'CSRProjectApprover'),
                             ('CSRComplianceApprover', 'CSRComplianceApprover'),
                             ('CSRContractApprover', 'CSRContractApprover'), ('NGOUploader', 'NGOUploader'),
                             ('NGOViewer', 'NGOViewer'), ('NGOAdmin', 'NGOAdmin')], max_length=21, unique=True)),
            ],
            options={
                'db_table': 'application_role',
                'permissions': (('view_applicationrole', 'Can view application role'),),
            },
        ),
        migrations.CreateModel(
            name='CSRComplianceCheckList',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('csr_compliance_checklist_id', models.AutoField(primary_key=True, serialize=False)),
                ('area', models.CharField(
                    choices=[('revenue', 'Revenue'), ('board', 'Board'), ('policy', 'Policy'), ('spend', 'Spend'),
                             ('programs', 'Programs'), ('reporting', 'Reporting')], max_length=10)),
                ('indicator', models.CharField(max_length=250, unique=True)),
            ],
            options={
                'db_table': 'csr_compliance_checklist',
                'permissions': (('view_csrcompliancechecklist', 'Can view csr compliance check list'),),
            },
        ),
        migrations.CreateModel(
            name='DataType',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('data_type_id', models.AutoField(primary_key=True, serialize=False)),
                ('data_type', models.CharField(
                    choices=[('boolean', 'BOOL'), ('varchar', 'CHAR'), ('integer', 'INT'), ('date', 'DATE'),
                             ('timestamp', 'TIMESTAMP'), ('numeric', 'NUMBER')], max_length=9, unique=True)),
            ],
            options={
                'db_table': 'data_type',
                'permissions': (('view_datatype', 'Can view data type'),),
            },
        ),
        migrations.CreateModel(
            name='DocumentClass',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('document_list_id', models.AutoField(primary_key=True, serialize=False)),
                ('document_source',
                 models.CharField(choices=[('CSR', 'csrdocs'), ('NGO', 'ngodocs'), ('SATTVA', 'sattvadocs')],
                                  max_length=6, null=True)),
                ('document_status',
                 models.CharField(choices=[('valid', 'Valid'), ('invalid', 'Invalid')], default='valid', max_length=7)),
                ('document_requirement',
                 models.CharField(choices=[('optional', 'Optional'), ('mandatory', 'Mandatory')], default='optional',
                                  max_length=9)),
                ('document_class_name', models.CharField(max_length=50, unique=True)),
            ],
            options={
                'db_table': 'document_class',
                'permissions': (('view_documentclass', 'Can view document class'),),
            },
        ),
        migrations.CreateModel(
            name='DocumentType',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('document_type_id', models.AutoField(primary_key=True, serialize=False)),
                ('document_type', models.CharField(max_length=35, unique=True)),
            ],
            options={
                'db_table': 'document_type',
                'permissions': (('view_documenttype', 'Can view document type'),),
            },
        ),
        migrations.CreateModel(
            name='Entity',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('entity_name', models.CharField(max_length=35)),
                ('attribute_name', models.CharField(max_length=120)),
                ('attribute_length', models.SmallIntegerField(null=True)),
                ('attribute_order', models.SmallIntegerField()),
                ('attribute_requirement',
                 models.CharField(choices=[('Mandatory', 'Mandatory'), ('Optional', 'Optional')], default='varchar',
                                  max_length=9, null=True)),
                ('attribute_grouping', models.CharField(max_length=60)),
                ('attribute_sub_grouping', models.CharField(max_length=60, null=True)),
                ('attribute_value_range', models.CharField(max_length=500, null=True)),
                ('attribute_default_value', models.CharField(max_length=40, null=True)),
                ('attribute_collection_method', models.CharField(max_length=20, null=True)),
                ('attribute_source', models.CharField(max_length=50, null=True)),
                ('attribute_source_link', models.CharField(max_length=100, null=True)),
                ('attribute_lead_indicator_flag', models.BooleanField(default=False)),
                ('attribute_classification_indicator_flag', models.BooleanField(default=False)),
                ('attribute_type',
                 models.ForeignKey(db_column='attribute_type', on_delete=django.db.models.deletion.CASCADE,
                                   to='domainlibraries.DataType', to_field='data_type')),
            ],
            options={
                'db_table': 'entity',
                'permissions': (('view_entity', 'Can view entity'),),
            },
        ),
        migrations.CreateModel(
            name='EntityType',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('entity_type_id', models.AutoField(primary_key=True, serialize=False)),
                ('entity_type', models.CharField(
                    choices=[('Beneficiary', 'Beneficiary'), ('Institution', 'Institution'),
                             ('Environment', 'Environment')], max_length=11, unique=True)),
            ],
            options={
                'db_table': 'entity_type',
                'permissions': (('view_entitytype', 'Can view entity type'),),
            },
        ),
        migrations.CreateModel(
            name='FocusArea',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('focus_area_id', models.AutoField(primary_key=True, serialize=False)),
                ('focus_area_name', models.CharField(max_length=40, unique=True)),
            ],
            options={
                'db_table': 'focus_area',
                'permissions': (('view_focusarea', 'Can view focus area'),),
            },
        ),
        migrations.CreateModel(
            name='Frequency',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('frequency_id', models.AutoField(primary_key=True, serialize=False)),
                ('frequency', models.CharField(
                    choices=[('daily', 'Daily'), ('weekly', 'Weekly'), ('fortnightly', 'Fortnightly'),
                             ('monthly', 'Monthly'), ('quarterly', 'Quarterly'), ('semiannually', 'SemiAnnually'),
                             ('annually', 'Annually')], max_length=12, unique=True)),
            ],
            options={
                'db_table': 'frequency',
                'permissions': (('view_frequency', 'Can view frequency'),),
            },
        ),
        migrations.CreateModel(
            name='Institution',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('institution_id', models.AutoField(primary_key=True, serialize=False)),
                ('institution_name', models.CharField(max_length=35, unique=True)),
            ],
            options={
                'db_table': 'institution',
                'permissions': (('view_institution', 'Can view institution'),),
            },
        ),
        migrations.CreateModel(
            name='NGOSelectionCheckList',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('ngo_selection_checklist_id', models.AutoField(primary_key=True, serialize=False)),
                ('head', models.CharField(choices=[('Legal Document Checklist', 'Legal Document Checklist'), (
                'Key considerations for selection of NGO', 'Key considerations for selection of NGO'),
                                                   ('About the NGO', 'About the NGO')], max_length=40)),
                ('detail', models.CharField(max_length=100, unique=True)),
                ('notes', models.CharField(max_length=100, null=True)),
            ],
            options={
                'db_table': 'ngo_selection_checklist',
                'permissions': (('view_ngoselectionchecklist', 'Can view ngo selection check list'),),
            },
        ),
        migrations.CreateModel(
            name='SDGGoal',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('sdg_goal_id', models.AutoField(primary_key=True, serialize=False)),
                ('sdg_goal', models.CharField(max_length=50, unique=True)),
            ],
            options={
                'db_table': 'sdg_goal',
                'permissions': (('view_sdggoal', 'Can view sdg goal'),),
            },
        ),
        migrations.CreateModel(
            name='SubFocusArea',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('sub_focus_area_id', models.AutoField(primary_key=True, serialize=False)),
                ('sub_focus_area_name', models.CharField(max_length=60, unique=True)),
                ('focus_area_name',
                 models.ForeignKey(db_column='focus_area_name', on_delete=django.db.models.deletion.CASCADE,
                                   to='domainlibraries.FocusArea', to_field='focus_area_name')),
            ],
            options={
                'db_table': 'sub_focus_area',
                'permissions': (('view_subfocusarea', 'Can view sub focus area'),),
            },
        ),
        migrations.CreateModel(
            name='SubTargetSegment',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('sub_target_segment_id', models.AutoField(primary_key=True, serialize=False)),
                ('sub_target_segment', models.CharField(max_length=35, unique=True)),
            ],
            options={
                'db_table': 'sub_target_segment',
                'permissions': (('view_subtargetsegment', 'Can view sub target segment'),),
            },
        ),
        migrations.CreateModel(
            name='TargetSegment',
            fields=[
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(max_length=50, null=True)),
                ('target_segment_id', models.AutoField(primary_key=True, serialize=False)),
                ('target_segment', models.CharField(max_length=35, unique=True)),
            ],
            options={
                'db_table': 'target_segment',
                'permissions': (('view_targetsegment', 'Can view target segment'),),
            },
        ),
        migrations.AddField(
            model_name='subtargetsegment',
            name='target_segment',
            field=models.ForeignKey(db_column='target_segment', on_delete=django.db.models.deletion.CASCADE,
                                    to='domainlibraries.TargetSegment', to_field='target_segment'),
        ),
        migrations.AddField(
            model_name='entity',
            name='entity_focus_area',
            field=models.ForeignKey(db_column='focus_area_name', on_delete=django.db.models.deletion.CASCADE,
                                    to='domainlibraries.FocusArea', to_field='focus_area_name'),
        ),
        migrations.AddField(
            model_name='entity',
            name='entity_sub_focus_area',
            field=models.ForeignKey(db_column='sub_focus_area_name', on_delete=django.db.models.deletion.CASCADE,
                                    to='domainlibraries.SubFocusArea', to_field='sub_focus_area_name'),
        ),
        migrations.AddField(
            model_name='entity',
            name='entity_type',
            field=models.ForeignKey(db_column='entity_type', on_delete=django.db.models.deletion.CASCADE,
                                    to='domainlibraries.EntityType', to_field='entity_type'),
        ),
        migrations.AddField(
            model_name='documentclass',
            name='document_type',
            field=models.ForeignKey(db_column='document_type', on_delete=django.db.models.deletion.CASCADE,
                                    to='domainlibraries.DocumentType', to_field='document_type'),
        ),
        migrations.AlterUniqueTogether(
            name='entity',
            unique_together={('entity_name', 'entity_type', 'attribute_name'),
                             ('entity_name', 'entity_type', 'attribute_order')},
        ),
        migrations.AlterUniqueTogether(
            name='documentclass',
            unique_together={('document_source', 'document_type')},
        ),
    ]
