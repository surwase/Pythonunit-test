from django.conf.urls import url

from v2.notification.views import NotificationViewSet, NotificationCountViewSet, UserNotificationsMarkAllRead, \
    UserNotificationsSettings

app_name = "notification"
urlpatterns = [
    url(r'^$', NotificationViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='user_notifications'),
    url(r'^(?P<pk>\d+)/$', NotificationViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='user_notifications'),
    url(r'^count/(?P<pk>\d+)/$', NotificationCountViewSet.as_view(
        {'get': 'retrieve'}),
        name='user_notifications'),
    url(r'^mark-as-read/$', UserNotificationsMarkAllRead.as_view(),
        name='user_notifications'),
    url(r'^user-settings/$', UserNotificationsSettings.as_view(),
        name='user_settings'),
]
