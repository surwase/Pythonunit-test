from .base import *  # noqa
from .base import env
import os

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', default=True)
DEBUG = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env('DJANGO_SECRET_KEY', default='yO0IscwfPZiZaa7czZNOR7COSBuRxI2DxXXOje06EnGllpfJdS9aBmZxpHiRUyz6')
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS = [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
    "13.233.94.100",
    "13.232.148.12",
    "qa.shift.sattva.co.in",

]

# CACHES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': ''
    }
}

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG  # noqa F405

# # EMAIL
# # ------------------------------------------------------------------------------
# # https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
# EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core._mail.backends.console.EmailBackend')
# # https://docs.djangoproject.com/en/dev/ref/settings/#email-host
# EMAIL_HOST = env('EMAIL_HOST', default='mailhog')
# # https://docs.djangoproject.com/en/dev/ref/settings/#email-port
# EMAIL_PORT = 1025

CELERY_BROKER_URL = 'redis://0.0.0.0:6379/0'

# django-debug-toolbar
# ------------------------------------------------------------------------------
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites
INSTALLED_APPS += ['debug_toolbar']  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
}
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ['127.0.0.1', '10.0.2.2']

# django-extensions
# ------------------------------------------------------------------------------
# https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration
INSTALLED_APPS += ['django_extensions']  # noqa F405
# Celery
# ------------------------------------------------------------------------------
# http://docs.celeryproject.org/en/latest/userguide/configuration.html#std:setting-task_always_eager
CELERY_ALWAYS_EAGER = False
# Your stuff...
# ------------------------------------------------------------------------------

# Logging output detailed info while development
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s :(%(module)s) %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        # 'app-log-file': {
        #     'level': 'INFO',
        #     'class': 'logging.handlers.RotatingFileHandler',
        #     'filename': '/var/log/sattva-app.log',
        #     'maxBytes': 1024 * 1024 * 5,
        #     'backupCount': 5,
        #     'formatter': 'standard',
        # },
        # 'django-log-file': {
        #     'level': 'DEBUG',
        #     'class': 'logging.handlers.RotatingFileHandler',
        #     'filename': '/tmp/sattva-django.log',
        #     'maxBytes': 1024 * 1024 * 5,
        #     'backupCount': 5,
        #     'formatter': 'standard',
        # },
        # 'db-log-file': {
        #     'level': 'DEBUG',
        #     'class': 'logging.handlers.RotatingFileHandler',
        #     'filename': '/tmp/sattva-db.log',
        #     'maxBytes': 1024 * 1024 * 5,
        #     'backupCount': 5,
        #     'formatter': 'standard',
        # },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
        # 'sentry': {
        #     'level': 'DEBUG',  # To capture more than ERROR, change to WARNING, INFO, etc.
        #     'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        #     'tags': {'custom-tag': 'x'},
        # },
        'watchtower': {
            'level': 'DEBUG',
            'class': 'watchtower.CloudWatchLogHandler',
            'boto3_session': boto3_session,
            'log_group': env('CW_LOG_GROUP'),
            'stream_name': env('CW_STREAM_NAME'),
            'formatter': 'standard',
        },
    },
    'loggers': {
        # '': {
        #     'handlers': ['console', 'watchtower'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },
        # 'django': {
        #     'handlers': ['console', 'watchtower'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },
        'django.request': {
            'handlers': ['console', 'watchtower'],
            'level': 'DEBUG',
            'propagate': False,
        },
        # 'django.db.backends': {
        #     'level': 'INFO',
        #     'handlers': ['console', 'watchtower'],
        #     'propagate': True,
        # }
    },
}

# Fetch this from Keycloak Admin UI
KEYCLOAK_CLIENT_PUBLIC_KEY = """-----BEGIN PUBLIC KEY-----
""" + env('KEYCLOAK_CLIENT_PUBLIC_KEY') + """
-----END PUBLIC KEY-----"""
# KEYCLOAK_CLIENT_PUBLIC_KEY = ""

KEYCLOAK_CONFIG.update({
    'KEYCLOAK_SERVER_URL': env('KEYCLOAK_SERVER_URL'),
    'KEYCLOAK_CLIENT_SECRET_KEY': env('KEYCLOAK_CLIENT_SECRET_KEY'),
    'KEYCLOAK_CLIENT_PUBLIC_KEY': KEYCLOAK_CLIENT_PUBLIC_KEY,
})

# INSTALLED_APPS += ['raven.contrib.django.raven_compat']
# RAVEN_CONFIG = {
#     'dsn': 'https://4e339490f048438ba505a51fec4cd08a:7c9548fb164b457c89fea7480d78927f@qa.shift.sattva.co.in:9001/7?verify_ssl=0',
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'core',
        'HOST': '35.154.203.2',
        'PORT': '5432',
        'USER': 'debug',
        'PASSWORD':'debug'

    },
    # 'kpi': {
    #     'ENGINE': 'django.db.backends.postgresql',
    #     'NAME': env('KOBO_KPI_DB_NAME'),
    #     'PASSWORD': env('KOBO_KPI_DB_PASSWORD'),
    #     'USER': env('KOBO_KPI_DB_USER'),
    #     'HOST': env('KOBO_KPI_DB_HOST'),
    #     'PORT': env('KOBO_KPI_DB_PORT'),
    # },
}

DEFAUlT_PASSWORD = 'sattvaCSR321'
USE_TZ = False
