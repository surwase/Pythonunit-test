# Generated by Django 2.0.5 on 2019-12-09 08:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0015_auto_20191207_1320'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usernotificationsettings',
            name='settings_name',
            field=models.SmallIntegerField(choices=[(0, 'Alert to over due of any milestone - Portal'), (1, 'Alert to over due of any milestone - Email'), (2, 'Alert to over due of any task - Portal'), (3, 'Alert to over due of any task - Email')]),
        ),
    ]
