from core.constants import SHADOW_MODEL_APP_LABEL


class DefaultDatabaseRouter:

    def db_for_read(self, model, **hints):
        """
        Reads go to `kc` when `model` is a ShadowModel
        """
        if model._meta.app_label == SHADOW_MODEL_APP_LABEL:
            return "kpi"
        return "default"

    def db_for_write(self, model, **hints):
        """
        Writes go to `kc` when `model` is a ShadowModel
        """
        if model._meta.app_label == SHADOW_MODEL_APP_LABEL:
            return "kpi"
        return "default"

    def allow_relation(self, obj1, obj2, **hints):
        """
        Relations between objects are allowed
        """
        return True

    def allow_migrate(self, db, app_label, model=None, **hints):
        """
        All default models end up in this pool.
        """
        if app_label == SHADOW_MODEL_APP_LABEL:
            return False
        return True
