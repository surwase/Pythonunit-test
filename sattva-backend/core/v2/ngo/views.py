import django_filters
from rest_framework import viewsets, status
from rest_framework.response import Response

from v2.authentication import KeycloakUserUpdateAuthentication
from v2.client.models import ClientUser
from v2.ngo.models import NGOPartner, NGOLocation, NGOFocusArea, NGOSubFocusArea, NGOTargetSegment, NGOSubTargetSegment, \
    NGOPointOfContact, NGOSDGGoals
from v2.ngo.serializers import NGOPartnerSerializer, NGOPartnerReadSerializer
from django_filters import rest_framework as filters

from v2.user_management.permissions import ProjectFeaturePermission


"""
   Filter for NGOPartner which will filter NGOPartner by exact id and exact client id
"""
class NGOFilter(django_filters.FilterSet):
    class Meta:
        model = NGOPartner
        fields = {
            'id': ['exact', 'in'],
            'clients__id': ['exact']
        }


"""
  Viewset for NGOPartner allows CRUD operations
"""
class NGOPartnerModelViewSet(viewsets.ModelViewSet):
    serializer_class = NGOPartnerSerializer
    queryset = NGOPartner.objects.all().order_by('ngo_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = NGOFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This function will get the serializer class
        if method is GET it will return NGOPartnerReadSerializer else if method is post,put,patch it will return NGOPartnerSerializer
        """
        method = self.request.method
        if method == 'GET':
            return NGOPartnerReadSerializer
        if method == 'PUT' or method == 'POST' or method == 'PATCH':
            return NGOPartnerSerializer

    def get_queryset(self):
        """
        This function will return the queryset
        """
        queryset = NGOPartner.objects.all().order_by('ngo_name')
        if self.request.user.application_role.role_name not in ('sattva-admin', 'sattva-user'):
            user_clients = ClientUser.objects.filter(user=self.request.user).values_list('client__id', flat=True)
            queryset = queryset.filter(clients__in=user_clients)
        return queryset

    @staticmethod
    def add_locations(locations, ngo_id):
        """
        This function will fetch the existing_locations and delete them and create the new ngolocation of particular ngo
        """
        existing_locations = NGOLocation.objects.filter(ngo__id=ngo_id)
        existing_locations.delete()
        location_objects = []
        for location in locations:
            if location.get('details'):
                location_objects.append(NGOLocation(
                    location=location,
                    area=location.get('area', ''),
                    city=location.get('city', ''),
                    state=location.get('state', ''),
                    country=location.get('country', ''),
                    latitude=location.get('latitude', 0),
                    longitude=location.get('longitude', 0),
                    ngo=NGOPartner.objects.get(id=ngo_id)))
        NGOLocation.objects.bulk_create(location_objects)

    @staticmethod
    def add_sdg_goals(sdg_goals, ngo_id):
        """
        This function will fetch the existing_sdg_goals and delete them and create the new sdg_goals of particular ngo
        """
        existing_sdg_goals = NGOSDGGoals.objects.filter(ngo__id=ngo_id)
        existing_sdg_goals.delete()
        sub_sdg_goals_objects = []
        for sdg_goal in sdg_goals:
            sub_sdg_goals_objects.append(
                NGOSDGGoals(sdg_goal=sdg_goal,
                            sdg_goal_name=sdg_goal['name'],
                            ngo=NGOPartner.objects.get(id=ngo_id)))
        NGOSDGGoals.objects.bulk_create(sub_sdg_goals_objects)

    @staticmethod
    def add_focus_area(focus_areas, ngo_id):
        """
        This function will fetch the existing_focus_area and delete them and create the new focus_area of particular ngo
        """
        existing_focus_area = NGOFocusArea.objects.filter(ngo__id=ngo_id)
        existing_focus_area.delete()
        focus_area_objects = []
        for focus_area in focus_areas:
            focus_area_objects.append(
                NGOFocusArea(focus_area=focus_area, ngo=NGOPartner.objects.get(id=ngo_id)))
        NGOFocusArea.objects.bulk_create(focus_area_objects)

    @staticmethod
    def add_sub_focus_area(sub_focus_areas, ngo_id):
        """
        This function will fetch the existing_sub_focus_area and delete them and create the new sub focus_area of particular ngo
        """
        existing_sub_focus_area = NGOSubFocusArea.objects.filter(ngo__id=ngo_id)
        existing_sub_focus_area.delete()
        sub_focus_area_objects = []
        for sub_focus_area in sub_focus_areas:
            sub_focus_area_objects.append(
                NGOSubFocusArea(sub_focus_area=sub_focus_area, ngo=NGOPartner.objects.get(id=ngo_id)))
        NGOSubFocusArea.objects.bulk_create(sub_focus_area_objects)

    @staticmethod
    def add_target_segment(target_segments, ngo_id):
        """
        This function will fetch the existing_target_segment and delete them and create the new target_segment of particular ngo
        """
        existing_target_segment = NGOTargetSegment.objects.filter(ngo__id=ngo_id)
        existing_target_segment.delete()
        target_segment_objects = []
        for target_segment in target_segments:
            target_segment_objects.append(
                NGOTargetSegment(target_segment=target_segment, ngo=NGOPartner.objects.get(id=ngo_id)))
        NGOTargetSegment.objects.bulk_create(target_segment_objects)

    @staticmethod
    def add_sub_target_segment(sub_target_segments, ngo_id):
        """
        This function will fetch the existing_sub_target_segment and delete them and create the new sub target_segment of particular ngo
        """
        existing_sub_target_segment = NGOSubTargetSegment.objects.filter(ngo__id=ngo_id)
        existing_sub_target_segment.delete()
        sub_target_segment_objects = []
        for sub_target_segment in sub_target_segments:
            sub_target_segment_objects.append(
                NGOSubTargetSegment(sub_target_segment=sub_target_segment,
                                    ngo=NGOPartner.objects.get(id=ngo_id)))
        NGOSubTargetSegment.objects.bulk_create(sub_target_segment_objects)

    @staticmethod
    def add_point_of_contact(point_of_contacts, ngo_id):
        """
        This function will fetch the existing_point_of_contact and delete them and create the new point_of_contact of particular ngo
        """
        existing_point_of_contact = NGOPointOfContact.objects.filter(ngo__id=ngo_id)
        existing_point_of_contact.delete()
        point_of_contact_objects = []
        for point_of_contact in point_of_contacts:
            point_of_contact_objects.append(
                NGOPointOfContact(name=point_of_contact['name'],
                                  phone_number=point_of_contact['phone_number'],
                                  email_address=point_of_contact['email_address'],
                                  ngo=NGOPartner.objects.get(id=ngo_id)))
        NGOPointOfContact.objects.bulk_create(point_of_contact_objects)

    def add_child_data_for_ngo(self, request, serializer):
        """
        This function will add the child data for particular ngo
        """
        ngo_id = serializer.data['id']
        locations = request.data.get('location')
        if locations:
            self.add_locations(locations, ngo_id)
        focus_area = request.data.get('focus_area')
        if focus_area:
            self.add_focus_area(focus_area, ngo_id)
        sub_focus_area = request.data.get('sub_focus_area')
        if sub_focus_area:
            self.add_sub_focus_area(sub_focus_area, ngo_id)
        target_segment = request.data.get('target_segment')
        if target_segment:
            self.add_target_segment(target_segment, ngo_id)
        sub_target_segment = request.data.get('sub_target_segment')
        if sub_target_segment:
            self.add_sub_target_segment(sub_target_segment, ngo_id)
        sdg_goals = request.data.get('sdg_goals')
        if sdg_goals:
            self.add_sdg_goals(sdg_goals, ngo_id)
        point_of_contact = request.data.get('point_of_contact')
        if point_of_contact:
            self.add_point_of_contact(point_of_contact, ngo_id)

    def create(self, request, *args, **kwargs):
        """
        This function will create the ngo partner
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.add_child_data_for_ngo(request, serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the ngo partner
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        self.add_child_data_for_ngo(request, serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
