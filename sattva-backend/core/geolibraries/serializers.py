from rest_framework import serializers
from .models import Country, State, District, LocationCategory

"""
    This class will serialize Country.
"""
class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'

"""
    This class will serialize State.
"""
class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State
        fields = '__all__'

"""
    This class will serialize District.
"""
class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = '__all__'

"""
    This class will serialize LocationCategory.
"""
class LocationCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LocationCategory
        fields = '__all__'
