from rest_framework.permissions import BasePermission

from v2.user_management.models import ClientFeaturePermission as ClientFeaturePermissionModel

from core.users.models import User

class ClientFeaturePermission(BasePermission):
    """
    A base class from which all permission classes should inherit.
    """

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        # if not request.user.is_anonymous and request.user.application_role == 'ngo-user' and request.method == 'DELETE':
        #     return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        return True


class ProjectFeaturePermission(BasePermission):
    """
        A base class from which all permission classes should inherit.
        """

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        # if not request.user.is_anonymous and request.user.application_role.role_name == 'ngo-user' and request.method == 'DELETE':
        #     return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        return True


class InviteUserPermission(BasePermission):
    """
        A base class from which all permission classes should inherit.
    """

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        # TODO: Make this proper.
        # if request.user.is_anonymous:
        #     return False
        # if request.user.application_role.role_name == 'sattva-admin':
        #     return True
        # if request.user.application_role.role_name == 'csr_admin':
        #     return True
        # return False
        return True

class DeleteUserPermission(BasePermission):
    """
        For user delete permission.
    """

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        if request.user.application_role.role_name in ['sattva-admin', 'sattva-user']:
            return True
        return False


class EditUserPermission(BasePermission):
    """
        For user edit permission.
    """

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        if request.method == 'POST':
            return True
        if request.method == 'PATCH':
            if request.user.application_role.role_name in ['sattva-admin', 'sattva-user']:
                return True
            if request.user.application_role.role_name == 'csr-admin':
                user_email = request.data.get('email')
                user = User.objects.get(email=user_email)
                if user.application_role.role_name not in ['sattva-admin', 'sattva-user']:
                    return True
        return False

class ClientModelPermission(BasePermission):
    """
        For Client Model CRUD operations
    """

    def has_permission(self, request, view):
        return True
