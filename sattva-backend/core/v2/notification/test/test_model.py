from django.test import TestCase
from .test_setup import TestSetUp
from v2.notification.models import Notification,NotificationSentLog

class TestNotification(TestSetUp):

    def setUp(self):
        super().setUp()
        Notification.objects.create(
            text = "text1",
            title ="title1",
            notification_type = 0,
            notification_tag = "tag1",
            category = 0,
            action_user = self.user,
            action_performed_on_user =self.user,
            action_params = ['prem1'],
            app_url = 'https://newsapi.org/v2/everything?q=tesla&from=2021-08-16&sortBy=publishedAt&apiKey=API_KEY',
            )

    def test_validation(self):
        with self.assertRaises(ValueError):
            Notification.objects.create(
            text = "text1",
            title ="title1",
            notification_type = 0,
            notification_tag =12,
            category = 0,
            action_user = "ac",
            action_performed_on_user =self.user,
            action_params = ['prem1'],
             )

    def test_values(self):
        notification=Notification.objects.get(text = "text1")
        self.assertEqual(notification.text , "text1")
        self.assertEqual(notification.title,"title1")
        self.assertEqual(notification.notification_type , 0)
        self.assertEqual(notification.notification_tag ,"tag1")
        self.assertEqual(notification.category , 0)
        self.assertEqual(notification.action_user.username,'diksha')
        self.assertEqual(notification.action_performed_on_user.username,'diksha')
        self.assertEqual(notification.action_params , ['prem1'])
        self.assertEqual(notification.app_url , 'https://newsapi.org/v2/everything?q=tesla&from=2021-08-16&sortBy=publishedAt&apiKey=API_KEY')


class TestNotificationSentLog(TestSetUp):

    def setUp(self):
        super().setUp()
        Notification.objects.create(
            text = "text1",
            title ="title1",
            notification_type = 0,
            notification_tag = "tag1",
            category = 0,
            action_user = self.user,
            action_performed_on_user =self.user,
            action_params = ['prem1'],
            app_url = 'https://newsapi.org/v2/everything?q=tesla&from=2021-08-16&sortBy=publishedAt&apiKey=API_KEY',
            )
        notification1=Notification.objects.get(text="text1")
        NotificationSentLog.objects.create(
            notification = notification1,
            user = self.user,
            read = False
            )

    def test_validation(self):
        with self.assertRaises(ValueError):
            notification1=Notification.objects.get(text="text1")
            NotificationSentLog.objects.create(
            notification = notification1,
            user = "abc",
            read = False
            )

    def test_values(self):
        notificationread=NotificationSentLog.objects.get(read = False)
        self.assertEqual(notificationread.notification.text , "text1")
        self.assertEqual(notificationread.user.username,'diksha')
        self.assertEqual(notificationread.read , False)



