from .models import *
from .serializers import *
from rest_framework import viewsets
from django_filters import rest_framework as filters
import django_filters
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


# Create your views here.

"""
    Filter for Country where we filter Country by exact country_name.
"""
class CountryFilter(django_filters.FilterSet):
    class Meta:
        model = Country
        fields = {
            'country_name': ['icontains'],
        }

"""
    Viewset for Country allows CRUD operation
"""
class CountryViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'country:view',
        'POST': 'country:add',
        'PUT': 'country:change',
        'DELETE': 'country:delete',
        'PATCH': 'country:patch'
    }
    serializer_class = CountrySerializer
    queryset = Country.objects.all().order_by('country_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CountryFilter

"""
    Filter for State where we filter State by exact country_name or state_name.
"""
class StateFilter(django_filters.FilterSet):
    class Meta:
        model = State
        fields = {
            'country_name': ['exact'],
            'state_name': ['icontains'],
        }

"""
    Viewset for State allows CRUD operation
"""
class StateViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'state:view',
        'POST': 'state:add',
        'PUT': 'state:change',
        'DELETE': 'state:delete',
        'PATCH': 'state:patch'
    }
    serializer_class = StateSerializer
    queryset = State.objects.all().order_by('state_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = StateFilter

"""
    Filter for District where we filter District by exact district_name or state_name.
"""
class DistrictFilter(django_filters.FilterSet):
    class Meta:
        model = District
        fields = {
            'state_name': ['exact'],
            'district_name': ['icontains'],
        }

"""
    Viewset for District allows CRUD operation
"""
class DistrictViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'district:view',
        'POST': 'district:add',
        'PUT': 'district:change',
        'DELETE': 'district:delete',
        'PATCH': 'district:patch'
    }
    serializer_class = DistrictSerializer
    queryset = District.objects.all().order_by('district_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DistrictFilter


"""
    Viewset for Location Category allows CRUD operation
"""
class LocationCategoryViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'locationcategory:view',
        'POST': 'locationcategory:add',
        'PUT': 'locationcategory:change',
        'DELETE': 'locationcategory:delete',
        'PATCH': 'locationcategory:patch'
    }
    serializer_class = LocationCategorySerializer
    queryset = LocationCategory.objects.all().order_by('location_category')
