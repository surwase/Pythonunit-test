import csv
import datetime
import itertools
import logging
from collections import defaultdict
from io import StringIO
from operator import itemgetter
import urllib.request
from io import BytesIO
import zipfile

from v2.project.api_functions import get_location, append_in_context
from django.template.loader import get_template
from xhtml2pdf import pisa

import dateutil
import django_filters
from django.core.files.storage import default_storage
from django.db import IntegrityError, connection, transaction
from django.db.models import Q
from django.http import HttpResponse
from django.core import serializers
from django_filters import rest_framework as filters
from rest_framework import status, viewsets
from rest_framework.filters import SearchFilter
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import pagination
from rest_framework_bulk import BulkCreateModelMixin, BulkModelViewSet
from core.users.models import UserActivityLog, User
from core.utils import get_value_or_default, check_null_and_replace_to_none
from v2.authentication import KeycloakUserUpdateAuthentication
from v2.client.models import Client, ClientDocument, ClientSettings
from v2.plant.models import Plant, PlantUser
from v2.plant.serializers import PlantSerializer
from v2.project.models import *
from v2.common_functions import create_xlsx_file
from v2.project.permissions import ProjectPermission, ProjectTagPermission, ProjectDocumentPermission, \
    ProjectPlanPermission, ImpactIndicatorPermission, TemplateActivityLogPermission, \
    MilestoneEntityPermission, TaskEntityPermission, FinancialPermission, DisbursementEntityPermission, \
    UtilisationEntityPermission, ProjectManageUserPermission, DeleteProjectUserPermission, ProjectUserPermission, \
    ImpactPermission, DeleteImpactPermission, ProjectSettingsPermission, UtilisationUploadVerificationPermission, \
    UtilisationUploadEntityPermission, UtilisationExpenseEntityPermission, ImpactCaseStudyPermission, DeleteImpactTemplatePermission
from v2.project.serializers import ProjectOutcomeMicroSerializer, ProjectOutputMicroSerializer, ProjectTagSerializer, CSVUploadSerializer, DisbursementCommentSerializer, \
    DisbursementDocumentSerializer, ProjectListSerializer, \
    DisbursementSerializer, DisbursementMiniSerializer, DisbursementWriteSerializer, FilesCommonDisbursementSerializer, \
    FilesCommonProjectImageSerializer, FilesCommonProjectSerializer, IndicatorOutcomeSerializer, \
    MilestoneDocumentSerializer, MilestoneListSerializer, MilestoneMiniSerializer, MilestoneNoteSerializer, \
    MilestoneSerializer, MilestoneActivityLogSerializer, TaskActivityLogSerializer, \
    MilestoneWriteSerializer, ProjectDashboardSerializer, ProjectDetailSerializer, ProjectDocumentSerializer, \
    ProjectImageSerializer, ProjectMicroSerializer, \
    ProjectRemarkSerializer, ProjectScheduleVIIActivitySerializer, ProjectSerializer, ProjectUserSerializer, \
    ProjectWriteSerializer, SubTaskSerializer, TaskCommentSerializer, TaskDocumentSerializer, TaskReadSerializer, \
    TaskWriteSerializer, TemplateAttributeSerializer, TemplateSerializer, TemplateTypeSerializer, \
    UtilisationCommentSerializer, UtilisationDocumentSerializer, UtilisationSerializer, \
    DisbursementWorkflowRuleSetSerializer, DisbursementWorkflowRuleSerializer, UtilisationWorkflowRuleSetSerializer, \
    UtilisationWorkflowRuleSerializer, DisbursementWorkflowAssigneeSerializer, UtilisationWorkflowAssigneeSerializer, \
    DisbursementStatusSerializer, DisbursementActivityLogSerializer, UtilisationStatusSerializer, \
    UtilisationActivityLogSerializer, UtilisationUploadSerializer, UtilisationExpenseSerializer, \
    ProjectSettingSerializer, UtilisationExpenseDocumentSerializer, UtilisationUploadDocumentSerializer, \
    ListProjectSerializer, ImpactWorkflowRuleSerializer, ImpactWorkflowRuleSetSerializer, ImpactWorkflowAssigneeSerializer, \
    ProjectOutputDocumentSerializer, ProjectOutcomeDocumentSerializer, ProjectIndicatorSerializer, \
    ProjectIndicatorReadSerializer, ProjectOutputMiniSerializer, ProjectOutcomeMiniSerializer, ProjectIndicatorOutcomeSerializer, \
    ProjectOutcomeStatusSerializer, ProjectOutputStatusSerializer, ImpactUploadStatusSerializer, IndicatorActivityLogSerializer, \
    IndicatorCommentSerializer, IndicatorDocumentSerializer, ImpactCaseStudySerializer, CaseStudyDocumentsSerializer, CaseStudyActivityLogSerializer, \
    ImpactDataAndTemplateActivityLogSerializer, ProjectDetailIndicatorSerializer, BulkUploadSerializer, UserSerializerForProject,BulkfileSerializer
from v2.project.triggers import create_action_alert_on_disbursement_action_required, \
    create_partner_action_alert_on_impact_data_upload, create_partner_action_alert_on_indicator_creation, \
    create_partner_action_alert_on_indicator_update, create_partner_action_alert_on_task_creation, \
    create_partner_action_alert_on_task_update, create_partner_action_alert_on_template_creation, \
    create_partner_action_alert_on_template_update, create_partner_action_alert_on_milestone_creation, \
    create_partner_action_alert_on_milestone_update
from v2.user_management.models import ProjectFeature, ProjectFeaturePermission as ProjectFeaturePermissionModel
from v2.user_management.permissions import ProjectFeaturePermission
from v2.project.bulk_utilisation import BulkUtilisation, BulkUtilisationException
from v2.project.tasks import indicator_update_on_workflow
from v2.project.bulk_upload import upload_projects,upload_milestones,upload_milestone_notes,upload_taskss,upload_task_comment,upload_impact_indicator,upload_disbursement

logger = logging.getLogger(__name__)

"""
   Filter for Project which will filter Project by exact client id and exact plant id or by list of plant ids
"""
class ProjectFilter(django_filters.FilterSet):
    class Meta:
        model = Project
        fields = {
            'client': ['exact'],
            'plant': ['exact', 'isnull']
        }


"""
    Viewset for Project Micro Model allows read operations
"""
class ProjectMicroModelViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectMicroSerializer
    queryset = Project.objects.all().order_by('client')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectFilter
    permission_classes = [ProjectPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query will fetch all projects
        If current user is not sattva admin then it will fetch the projects of the particular user.
        """
        queryset = Project.objects.all().order_by('client')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(id__in=user_projects)
        return queryset


"""
    Viewset for Project Model allows CRUD operations
"""
class ProjectModelViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectSerializer
    queryset = Project.objects.all().order_by('client')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectFilter
    permission_classes = [ProjectPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This function will get the serializer class
        If method is PUT,POST or PATCH then it will return ProjectWriteSerializer
        If action is list then ListProjectSerializer
        else it will return ProjectSerializer
        """
        method = self.request.method
        if method == 'PUT' or method == 'POST' or method == 'PATCH':
            return ProjectWriteSerializer
        elif self.action == 'list':
            return ListProjectSerializer
        else:
            return ProjectSerializer

    def get_queryset(self):
        """
        This function will return the queryset
        query will fetch all projects
        If current user is not sattva admin then it will fetch the projects of the particular user.
        """
        queryset = Project.objects.all().order_by('client')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(id__in=user_projects)
        return queryset

    @staticmethod
    def add_locations(locations, project_id):
        """
        This function will fetch the existing locations and delete them and create new location for particular project.
        """
        existing_locations = ProjectLocation.objects.filter(project__id=project_id)
        existing_locations.delete()
        location_objects = []
        for location in locations:
            if location.get('details'):
                location_objects.append(ProjectLocation(
                    location=location,
                    area=location.get('area', ''),
                    city=location.get('city', ''),
                    state=location.get('state', ''),
                    country=location.get('country', ''),
                    latitude=location.get('latitude', 0),
                    longitude=location.get('longitude', 0),
                    project=Project.objects.get(id=project_id)))
        ProjectLocation.objects.bulk_create(location_objects)

    @staticmethod
    def add_focus_area(focus_areas, project_id):
        """
        This function will fetch the existing focus_area and delete them and create new focus_area for particular project
        """
        if type(focus_areas) == str:
            focus_areas = [focus_areas]
        existing_focus_area = ProjectFocusArea.objects.filter(project__id=project_id)
        existing_focus_area.delete()
        focus_area_objects = []
        for focus_area in focus_areas:
            focus_area_objects.append(
                ProjectFocusArea(focus_area=focus_area, project=Project.objects.get(id=project_id)))
        ProjectFocusArea.objects.bulk_create(focus_area_objects)

    @staticmethod
    def add_sub_focus_area(sub_focus_areas, project_id):
        """
        This function will fetch the existing sub_focus_area and delete them and create new sub focus_area for particular project
        """
        existing_sub_focus_area = ProjectSubFocusArea.objects.filter(project__id=project_id)
        existing_sub_focus_area.delete()
        sub_focus_area_objects = []
        for sub_focus_area in sub_focus_areas:
            sub_focus_area_objects.append(
                ProjectSubFocusArea(sub_focus_area=sub_focus_area, project=Project.objects.get(id=project_id)))
        ProjectSubFocusArea.objects.bulk_create(sub_focus_area_objects)

    @staticmethod
    def add_target_segment(target_segments, project_id):
        """
        This function will fetch the existing target_segment and delete them and create new target_segment for particular project
        """
        if type(target_segments) == str:
            target_segments = [target_segments]
        existing_target_segment = ProjectTargetSegment.objects.filter(project__id=project_id)
        existing_target_segment.delete()
        target_segment_objects = []
        for target_segment in target_segments:
            target_segment_objects.append(
                ProjectTargetSegment(target_segment=target_segment, project=Project.objects.get(id=project_id)))
        ProjectTargetSegment.objects.bulk_create(target_segment_objects)

    @staticmethod
    def add_sub_target_segment(sub_target_segments, project_id):
        """
        This function will fetch the existing sub_target_segment and delete them and create new sub_target_segment for particular project
        """
        if type(sub_target_segments) == str:
            sub_target_segments = [sub_target_segments]
        existing_sub_target_segment = ProjectSubTargetSegment.objects.filter(project__id=project_id)
        existing_sub_target_segment.delete()
        sub_target_segment_objects = []
        for sub_target_segment in sub_target_segments:
            sub_target_segment_objects.append(
                ProjectSubTargetSegment(sub_target_segment=sub_target_segment,
                                        project=Project.objects.get(id=project_id)))
        ProjectSubTargetSegment.objects.bulk_create(sub_target_segment_objects)

    @staticmethod
    def add_sdg_goals(sdg_goals, project_id):
        """
        This function will fetch the existing sdg_goals and delete them and create new sdg_goals for particular project
        """
        existing_sdg_goals = ProjectSDGGoals.objects.filter(project__id=project_id)
        existing_sdg_goals.delete()
        sub_sdg_goals_objects = []
        for sdg_goal in sdg_goals:
            sub_sdg_goals_objects.append(
                ProjectSDGGoals(sdg_goal=sdg_goal,
                                sdg_goal_name=sdg_goal['name'],
                                project=Project.objects.get(id=project_id)))
        ProjectSDGGoals.objects.bulk_create(sub_sdg_goals_objects)

    @staticmethod
    def add_tags(tags, project_id):
        """
        This function will fetch the existing tags and delete them and create new tags for particular project
        """
        existing_tags = ProjectTag.objects.filter(project__id=project_id)
        existing_tags.delete()
        tag_objects = []
        for tag in tags:
            tag_objects.append(
                ProjectTag(tag=tag, project=Project.objects.get(id=project_id)))
        ProjectTag.objects.bulk_create(tag_objects)

    @staticmethod
    def add_mca_sector(mca_sector, project_id):
        """
        This function will fetch the existing mca_focus_area and delete them and create new mca_focus_area for particular project
        """
        existing_mca = ProjectMCA.objects.filter(project__id=project_id)
        existing_mca.delete()
        sub_mca_focus_area_objects = []
        for mca in mca_sector:
            sub_mca_focus_area_objects.append(
                ProjectMCA(mca=mca,
                           mca_focus_area=mca['mca_focus_area_name'],
                           mca_sub_focus_area=mca['mca_sub_focus_area_name'],
                           project=Project.objects.get(id=project_id)))
        ProjectMCA.objects.bulk_create(sub_mca_focus_area_objects)

    def add_child_data_for_project(self, request, serializer, instance=None):
        """
        This function will create the child data for particular project
        """
        project_id = serializer.data['id']
        locations = request.data.get('location', [])
        if locations or ProjectLocation.objects.filter(project=instance).count():
            self.add_locations(locations, project_id)
        focus_area = request.data.get('focus_area', [])
        if focus_area or ProjectFocusArea.objects.filter(project=instance).count():
            self.add_focus_area(focus_area, project_id)
        sub_focus_area = request.data.get('sub_focus_area', [])
        if sub_focus_area or ProjectSubFocusArea.objects.filter(project=instance).count():
            self.add_sub_focus_area(sub_focus_area, project_id)
        target_segment = request.data.get('target_segment', [])
        if target_segment or ProjectTargetSegment.objects.filter(project=instance).count():
            self.add_target_segment(target_segment, project_id)
        sub_target_segment = request.data.get('sub_target_segment', [])
        if sub_target_segment or ProjectSubTargetSegment.objects.filter(project=instance).count():
            self.add_sub_target_segment(sub_target_segment, project_id)
        sdg_goals = request.data.get('sdg_goals', [])
        if sdg_goals or ProjectSDGGoals.objects.filter(project=instance).count():
            self.add_sdg_goals(sdg_goals, project_id)
        tags = request.data.get('tags', [])
        if tags or ProjectTag.objects.filter(project=instance).count():
            self.add_tags(tags, project_id)
        mca_sector = request.data.get('mca_sector', [])
        if mca_sector or ProjectMCA.objects.filter(project=instance).count():
            self.add_mca_sector(mca_sector, project_id)

    def create(self, request, *args, **kwargs):
        """
        This function will create the project for particular user if current user is not sattva admin
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.add_child_data_for_project(request, serializer)
        project=Project.objects.get(id=serializer.data.get('id'))
        client_users=ClientUser.objects.filter(client=project.client,user__application_role__role_name='sattva-user')
        for client in client_users:
            if client.user!=request.user:
                sattva_user_added_as_project_user = ProjectUser.objects.create(user=client.user,
                       project=serializer.instance)
                project_features = ProjectFeature.objects.all()
                user_role = client.user.application_role.role_name
                for project_feature in project_features:
                    if user_role in project_feature.applicable_roles:
                        permissions = project_feature.default_permissions[user_role]
                        project_permissions = ProjectFeaturePermissionModel.objects.filter(project=serializer.instance,
                                                                                       feature=project_feature,
                                                                                       permission__in=permissions)
                        client.user.project_feature_permission.add(*project_permissions)
        if request.user.application_role is not None and request.user.application_role.role_name != 'sattva-admin':
            project_user = ProjectUser.objects.create(user=request.user,
                                                      project=serializer.instance)
            project_features = ProjectFeature.objects.all()
            user_role = request.user.application_role.role_name
            for project_feature in project_features:
                if user_role in project_feature.applicable_roles:
                    permissions = project_feature.default_permissions[user_role]
                    project_permissions = ProjectFeaturePermissionModel.objects.filter(project=serializer.instance,
                                                                                       feature=project_feature,
                                                                                       permission__in=permissions)
                    request.user.project_feature_permission.add(*project_permissions)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the project
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        self.add_child_data_for_project(request, serializer,instance)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

"""
    Viewset for Project details allows read operations
"""
class ProjectModelDetailViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectDetailSerializer
    queryset = Project.objects.all().order_by('client')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectFilter
    permission_classes = [ProjectPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the project details and if current user's application role is not sattva-admin it will fetch the project details of the current project user.
        """
        queryset = Project.objects.all().order_by('client')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(id__in=user_projects)
        return queryset

"""
   Filter for Project Document which will filter document by exact project id.
"""
class ProjectDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectDocument
        fields = {
            'project': ['exact']
        }

"""
    Viewset for Project Document allows CRUD operations
"""
class ProjectDocumentModelViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectDocumentSerializer
    queryset = ProjectDocument.objects.all().order_by('project')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectDocumentFilter
    permission_classes = [ProjectDocumentPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the project document and if current user's application role is not sattva-admin it will fetch the project document of the current project user.
        """
        queryset = ProjectDocument.objects.all().order_by('project')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        return queryset

"""
   Filter for Project tag which will filter tags by exact project id , project__client , project__plant.
"""
class ProjectTagFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectTag
        fields = {
            'project': ['exact'],
            'project__client': ['exact'],
            'project__plant': ['exact']
        }

"""
    Viewset for Project Tags allows read operations
"""
class ProjectTagModelViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectTagSerializer
    queryset = ProjectTag.objects.all().order_by('project')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectTagFilter
    permission_classes = [ProjectTagPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the project tags and if current user's application role is not sattva-admin it will fetch the project tags of the current project user.
        """
        queryset = ProjectTag.objects.distinct('tag')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        return queryset

"""
   Filter for Project Image which will filter images by exact project id.
"""
class ProjectImageFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectImage
        fields = {
            'project': ['exact']
        }

"""
    Viewset for Project Image allows CRUD operations
"""
class ProjectImageModelViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectImageSerializer
    queryset = ProjectImage.objects.all().order_by('project')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectImageFilter
    permission_classes = [ProjectDocumentPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the project images and if current user's application role is not sattva-admin it will fetch the project images of the current project user.
        """
        queryset = ProjectImage.objects.all().order_by('project')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        return queryset

"""
   Filter for Milestone which will filter Milestones by exact project id,project__client,project__plant.
"""
class MilestoneFilter(django_filters.FilterSet):
    class Meta:
        model = Milestone
        fields = {
            'project': ['exact'],
            'project__client': ['exact'],
            'project__plant': ['exact']
        }


"""
    Viewset for Milestone allows read operations
"""
class MilestoneListModelViewSet(viewsets.ModelViewSet):
    serializer_class = MilestoneListSerializer
    queryset = Milestone.objects.all().order_by('position', 'start_date', 'milestone_name')
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filter_class = MilestoneFilter
    search_fields = ['start_date', 'end_date', 'milestone_name', 'status', 'task__task_name']
    permission_classes = [ProjectPlanPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Viewset for Milestone allows CRUD operations
"""
class MilestoneModelViewSet(viewsets.ModelViewSet):
    serializer_class = MilestoneSerializer
    queryset = Milestone.objects.all().order_by('position', 'start_date', 'milestone_name')
    filter_backends = (filters.DjangoFilterBackend, SearchFilter)
    filter_class = MilestoneFilter
    search_fields = ['start_date', 'end_date', 'milestone_name', 'status', 'task__task_name']
    permission_classes = [ProjectPlanPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This function will get the serializer class
        If method is PUT,POST or PATCH then it will return MilestoneWriteSerializer
        else it will return MilestoneSerializer
        """
        method = self.request.method
        if method == 'PUT' or method == 'POST' or method == 'PATCH':
            return MilestoneWriteSerializer
        else:
            return MilestoneSerializer

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the milestones and if current user's application role is not sattva-admin it will fetch the milestone of the current project user.
        """
        queryset = Milestone.objects.all().order_by('position', 'start_date', 'milestone_name')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        return queryset

    def create(self, request, *args, **kwargs):
        """
        This function will create the milestone
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        create_partner_action_alert_on_milestone_creation(serializer.instance, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the milestone
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        create_partner_action_alert_on_milestone_update(serializer.instance, request.user)
        return Response(serializer.data)

"""
    Viewset for Mini Milestone allows CRUD operations
"""
class MiniMilestoneModelViewSet(viewsets.ModelViewSet):
    serializer_class = MilestoneMiniSerializer
    queryset = Milestone.objects.all().order_by('position', 'start_date', 'milestone_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MilestoneFilter
    permission_classes = [ProjectPlanPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the milestones and if current user's application role is not sattva-admin it will fetch the milestone for the current project user.
        """
        queryset = Milestone.objects.all().order_by('position', 'start_date', 'milestone_name')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        return queryset

"""
   Filter for MilestoneNote which will filter Notes by exact milestone id
"""
class MilestoneNoteFilter(django_filters.FilterSet):
    class Meta:
        model = MilestoneNote
        fields = {
            'milestone': ['exact']
        }

"""
    Viewset for Milestone Note allows CRUD operations
"""
class MilestoneNoteModelViewSet(viewsets.ModelViewSet):
    serializer_class = MilestoneNoteSerializer
    queryset = MilestoneNote.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MilestoneNoteFilter
    permission_classes = [MilestoneEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Milestone Document which will filter documents by exact milestone id
"""
class MilestoneDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = MilestoneDocument
        fields = {
            'milestone': ['exact']
        }

"""
    Viewset for Milestone Document allows CRUD operations
"""
class MilestoneDocumentModelViewSet(viewsets.ModelViewSet):
    serializer_class = MilestoneDocumentSerializer
    queryset = MilestoneDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MilestoneDocumentFilter
    permission_classes = [MilestoneEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Task which will filter the task by exact milestone id,project id,client id
"""
class TaskFilter(django_filters.FilterSet):
    class Meta:
        model = Task
        fields = {
            'milestone': ['exact', 'isnull'],
            'project': ['exact'],
            'client': ['exact']
        }
"""
   Pagination class for Task default page size is 5 and maximum page size is 100
"""
class TaskPagination(pagination.PageNumberPagination):
    page_size = 5
    max_page_size = 100
    page_size_query_param = 'records'

    def get_paginated_response(self, data):
        """
        This is the function to get the paginated response
        by default page size will be 5 and max page size will be 100
        """
        return Response({
             'count': self.page.paginator.count,
             'current': self.page.number,
             'next': self.get_next_link(),
             'previous': self.get_previous_link(),
             'results': data
        })

    def paginate_queryset(self, queryset, request, view=None):
        if 'no_page' in request.query_params:
            return None
        return super().paginate_queryset(queryset, request, view)


"""
    Viewset for Task Model allows CRUD operations
"""
class TaskModelViewSet(viewsets.ModelViewSet):
    serializer_class = TaskReadSerializer
    pagination_class = TaskPagination
    queryset = Task.objects.all().order_by('position', 'start_date', 'task_name')
    filter_backends = (filters.DjangoFilterBackend, SearchFilter,)
    filter_class = TaskFilter
    search_fields = ['start_date', 'end_date', 'task_name', 'status', 'priority', 'task_description', 'project__project_name',
    'task_tag', 'tags', 'milestone__milestone_name', 'percentage_completed', 'actual_start_date', 'actual_end_date', 'client__client_name']
    permission_classes = [ProjectPlanPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This function will get the serializer class
        If method is PUT,POST or PATCH then it will return TaskWriteSerializer
        else it will return TaskReadSerializer
        """
        method = self.request.method
        if method == 'PUT' or method == 'POST' or method == 'PATCH':
            return TaskWriteSerializer
        else:
            return TaskReadSerializer

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the task and if current user's application role is not sattva-admin it will fetch the task for the current project user.
        """
        queryset = Task.objects.all().order_by('position', 'start_date', 'task_name')
        if self.request.user.application_role.role_name in ['sattva-user', 'csr-admin', 'csr-user']:
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(Q(project__id__in=user_projects) | Q(project=None))
        elif self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        queryset = self.filter_list(queryset)

        return queryset.order_by('position', 'start_date', 'task_name')

    def filter_list(self, queryset):
        """
        This function will return the queryset
        queryset will fetch the filter list for Task.
        """
        start_date = check_null_and_replace_to_none(self.request.query_params.get('start_date'))
        end_date = check_null_and_replace_to_none(self.request.query_params.get('end_date'))
        priority = check_null_and_replace_to_none(self.request.query_params.get('priority'))
        status = check_null_and_replace_to_none(self.request.query_params.get('status'))
        assignee = check_null_and_replace_to_none(self.request.query_params.get('assignee'))

        if priority is not None:
            queryset = queryset.filter(priority=priority)
        if status is not None:
            queryset = queryset.filter(status=status)
        if assignee is not None:
            queryset = queryset.filter(Q(client_assignee__user__id=assignee) | Q(assignee__user__id=assignee) | Q(assignee__id=assignee))
        if start_date is not None:
            queryset = queryset.filter(start_date__gte=start_date)
        if end_date is not None:
            queryset = queryset.filter(end_date__lte=end_date)
        return queryset

    def create(self, request, *args, **kwargs):
        """
        This function will create the task
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        create_partner_action_alert_on_task_creation(serializer.instance, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the task
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        create_partner_action_alert_on_task_update(serializer.instance, request.user)
        return Response(serializer.data)

"""
   Filter for TaskComment which will filter the comments by exact task id
"""
class TaskCommentFilter(django_filters.FilterSet):
    class Meta:
        model = TaskComment
        fields = {
            'task': ['exact']
        }


"""
    Viewset for Task comment allows CRUD operations
"""
class TaskCommentModelViewSet(viewsets.ModelViewSet):
    serializer_class = TaskCommentSerializer
    queryset = TaskComment.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TaskCommentFilter
    permission_classes = [TaskEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the task comment
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        create_partner_action_alert_on_task_update(serializer.instance.task, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the task comment
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        create_partner_action_alert_on_task_update(serializer.instance.task, request.user)
        return Response(serializer.data)

"""
   Filter for Task document which will filter the documents by exact task id
"""
class TaskDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = TaskDocument
        fields = {
            'task': ['exact']
        }


"""
    Viewset for Task document allows CRUD operations
"""
class TaskDocumentModelViewSet(viewsets.ModelViewSet):
    serializer_class = TaskDocumentSerializer
    queryset = TaskDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TaskDocumentFilter
    permission_classes = [TaskEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the task document
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        create_partner_action_alert_on_task_update(serializer.instance.task, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the task document
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        create_partner_action_alert_on_task_update(serializer.instance.task, request.user)
        return Response(serializer.data)


"""
   Filter for SubTask which will filter the SubTasks by exact task id
"""
class SubTaskFilter(django_filters.FilterSet):
    class Meta:
        model = SubTask
        fields = {
            'task': ['exact']
        }


"""
    Viewset for SubTask allows CRUD operations
"""
class SubTaskModelViewSet(viewsets.ModelViewSet):
    serializer_class = SubTaskSerializer
    queryset = SubTask.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = SubTaskFilter
    permission_classes = [TaskEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for TaskActivityLog which will filter the ActivityLog by exact task id
"""
class TaskActivityLogFilter(django_filters.FilterSet):
    class Meta:
        model = TaskActivityLog
        fields = {
            'task': ['exact']
        }

"""
    Viewset for TaskActivityLog allows read operations
"""
class TaskActivityLogViewSet(viewsets.ModelViewSet):
    serializer_class = TaskActivityLogSerializer
    queryset = TaskActivityLog.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TaskActivityLogFilter
    permission_classes = [TaskEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for MilestoneActivityLog which will filter the ActivityLog by exact milestone id
"""
class MilestoneActivityLogFilter(django_filters.FilterSet):
    class Meta:
        model = MilestoneActivityLog
        fields = {
            'milestone': ['exact']
        }
"""
    Viewset for MilestoneActivityLog allows read operations
"""
class MilestoneActivityLogViewSet(viewsets.ModelViewSet):
    serializer_class = MilestoneActivityLogSerializer
    queryset = MilestoneActivityLog.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MilestoneActivityLogFilter
    permission_classes = [MilestoneEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Disbursement which will filter the Disbursement by exact id,project,ststus,expected date
"""
class DisbursementFilter(django_filters.FilterSet):
    class Meta:
        model = Disbursement
        fields = {
            'id': ['exact'],
            'project': ['exact'],
            'status': ['exact'],
            'expected_date': ['exact'],
        }

"""
    Viewset for disbursement allows CRUD operations
"""
class DisbursementModelViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementSerializer
    queryset = Disbursement.objects.all().order_by('position', 'start_date', 'disbursement_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementFilter
    permission_classes = [FinancialPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This function will get the serializer class
        If method is PUT,POST or PATCH then it will return DisbursementWriteSerializer
        else it will return DisbursementSerializer
        """
        method = self.request.method
        if method == 'PUT' or method == 'POST' or method == 'PATCH':
            return DisbursementWriteSerializer
        else:
            return DisbursementSerializer

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the disbursement and if current user's application role is not sattva-admin it will fetch the disbursement for the current project user.
        """
        queryset = Disbursement.objects.all().order_by('position', 'start_date', 'disbursement_name')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        return queryset

    def create(self, request, *args, **kwargs):
        """
        This function will create the Disbursement
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        action_by = ProjectUser.objects.get(user=request.user, project=serializer.instance.project) \
            if request.user.application_role.role_name != 'sattva-admin' else None
        DisbursementStatus.objects.create(
            disbursement=serializer.instance,
            action_by=action_by,
            status='SCHEDULED'
        )
        # create_partner_action_alert_on_disbursement_creation(serializer.instance, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Disbursement
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_disbursement_update(serializer.instance, request.user)
        return Response(DisbursementSerializer(serializer.instance).data)


"""
    Viewset for Mini Disbursement Model allows CRUD operations
"""
class MiniDisbursementModelViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementMiniSerializer
    queryset = Disbursement.objects.all().order_by('position', 'start_date', 'disbursement_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementFilter
    permission_classes = [ProjectPlanPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch disbursement and if current user's application role is not sattva-admin it will fetch the disbursement for the current project user.
        """
        queryset = Disbursement.objects.all().order_by('position', 'start_date', 'disbursement_name')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        return queryset

"""
   Filter for DisbursementComment which will filter the comment by exact disbursement id
"""
class DisbursementCommentFilter(django_filters.FilterSet):
    class Meta:
        model = DisbursementComment
        fields = {
            'disbursement': ['exact']
        }


"""
    Viewset for Disbursement Comment Model allows CRUD operations
"""
class DisbursementCommentViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementCommentSerializer
    queryset = DisbursementComment.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementCommentFilter
    permission_classes = [DisbursementEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Disbursement Comment
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_disbursement_update(serializer.instance.disbursement, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Disbursement Comment
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_disbursement_update(serializer.instance.disbursement, request.user)
        return Response(serializer.data)

"""
   Filter for Disbursement Status which will filter the status by exact disbursement id
"""
class DisbursementStatusFilter(django_filters.FilterSet):
    class Meta:
        model = DisbursementStatus
        fields = {
            'disbursement': ['exact']
        }

"""
    Viewset for Disbursement Status Model allows CRUD operations
"""
class DisbursementStatusViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementStatusSerializer
    queryset = DisbursementStatus.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementStatusFilter
    permission_classes = [DisbursementEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Disbursement Status
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_disbursement_update(serializer.instance.disbursement, request.user)
        instance = serializer.instance
        instance.action_by = ProjectUser.objects.get(user=request.user,
                                                     project=serializer.instance.disbursement.project)
        instance.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

"""
   Filter for Disbursement ActivityLog which will filter the activitylogs by exact disbursement id
"""
class DisbursementActivityLogFilter(django_filters.FilterSet):
    class Meta:
        model = DisbursementActivityLog
        fields = {
            'disbursement': ['exact']
        }

"""
    Viewset for Disbursement Activity Log Model allows read operations
"""
class DisbursementActivityLogViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementActivityLogSerializer
    queryset = DisbursementActivityLog.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementActivityLogFilter
    permission_classes = [DisbursementEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Disbursement Document which will filter the document by exact disbursement id
"""
class DisbursementDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = DisbursementDocument
        fields = {
            'disbursement': ['exact']
        }


"""
    Viewset for Disbursement Document Model allows CRUD operations
"""
class DisbursementDocumentViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementDocumentSerializer
    queryset = DisbursementDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementDocumentFilter
    permission_classes = [DisbursementEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the DisbursementDocument
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_disbursement_update(serializer.instance.disbursement, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the DisbursementDocument
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_disbursement_update(serializer.instance.disbursement, request.user)
        return Response(serializer.data)

"""
   Filter for Utilisation which will filter the Utilisation by exact disbursement id,start_date,end_date,expense_category,project
"""
class UtilisationFilter(django_filters.FilterSet):
    class Meta:
        model = Utilisation
        fields = {
            'disbursement': ['exact', 'isnull'],
            'start_date': ['lte', 'exact', 'gte'],
            'end_date': ['lte', 'exact', 'gte'],
            'expense_category': ['exact'],
            'project': ['exact']
        }

"""
    Viewset for Utilisation Model allows CRUD operations
"""
class UtilisationViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationSerializer
    queryset = Utilisation.objects.all().order_by('position', 'start_date', 'particulars')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationFilter
    permission_classes = [FinancialPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the Utilisation and if current user's application role is not sattva-admin it will fetch the Utilisation for the current project user.
        """
        queryset = Utilisation.objects.all().order_by('position', 'start_date', 'particulars')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=self.request.user).values_list('project__id', flat=True)
            queryset = queryset.filter(project__id__in=user_projects)
        return queryset

    def create(self, request, *args, **kwargs):
        """
        This function will create the Utilisation
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        action_by = ProjectUser.objects.get(user=request.user, project=serializer.instance.project) \
            if request.user.application_role.role_name != 'sattva-admin' else None
        if serializer.instance.is_custom_workflow:
            UtilisationStatus.objects.create(
                utilisation=serializer.instance,
                action_by=action_by,
                status='NEW_REQUEST',
                rule=UtilisationWorkflowRule.objects.get(rule_type='REQUESTER',
                                                         ruleset__projects__in=[serializer.instance.project])
            )
        # create_partner_action_alert_on_utilisation_creation(serializer.instance, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Utilisation
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_utilisation_update(serializer.instance, request.user)
        return Response(serializer.data)

"""
   Filter for Utilisation Comment which will filter the comment by exact Utilisation id
"""
class UtilisationCommentFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationComment
        fields = {
            'utilisation': ['exact']
        }

"""
    Viewset for Utilisation Comment allows CRUD operations
"""
class UtilisationCommentViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationCommentSerializer
    queryset = UtilisationComment.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationCommentFilter
    permission_classes = [UtilisationEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Utilisation comment
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Utilisation comment
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data)

"""
   Filter for Utilisation status which will filter the status by exact Utilisation id
"""
class UtilisationStatusFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationStatus
        fields = {
            'utilisation': ['exact']
        }


"""
    Viewset for Utilisation status allows CRUD operations
"""
class UtilisationStatusViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationStatusSerializer
    queryset = UtilisationStatus.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationStatusFilter
    permission_classes = [UtilisationEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Utilisation status
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        instance = serializer.instance
        instance.action_by = ProjectUser.objects.get(user=request.user,
                                                     project=serializer.instance.utilisation.project)
        instance.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


"""
   Filter for ProjectOutput status which will filter the status by exact projectOutput id
"""
class ProjectOutputStatusFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectOutputStatus
        fields = {
            'projectOutput': ['exact']
        }
"""
    Viewset for Project Output Status allows CRUD operations
"""
class ProjectOutputStatusViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectOutputStatusSerializer
    queryset = ProjectOutputStatus.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectOutputStatusFilter
    # permission_classes = [ProjectOutputEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Project Output Status
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        instance = serializer.instance
        instance.action_by = ProjectUser.objects.get(user=request.user,
                                                     project=serializer.instance.projectOutput.project)
        instance.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

"""
   Filter for ProjectOutcome status which will filter the status by exact projectOutcome id
"""
class ProjectOutcomeStatusFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectOutcomeStatus
        fields = {
            'projectOutcome': ['exact']
        }
"""
    Viewset for Project Outcome Status allows CRUD operations
"""
class ProjectOutcomeStatusViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectOutcomeStatusSerializer
    queryset = ProjectOutcomeStatus.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectOutcomeStatusFilter
    # permission_classes = [ProjectOutEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Project Outcome Status
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        instance = serializer.instance
        instance.action_by = ProjectUser.objects.get(user=request.user,
                                                     project=serializer.instance.projectOutcome.project)
        instance.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

"""
   Filter for UtilisationActivityLog which will filter the ActivityLog  by exact utilisation id
"""
class UtilisationActivityLogFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationActivityLog
        fields = {
            'utilisation': ['exact']
        }

"""
    Viewset for Utilisation Activity Log allows read operations
"""
class UtilisationActivityLogViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationActivityLogSerializer
    queryset = UtilisationActivityLog.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationActivityLogFilter
    permission_classes = [UtilisationEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for UtilisationDocument which will filter the document by exact utilisation id
"""
class UtilisationDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationDocument
        fields = {
            'utilisation': ['exact']
        }

"""
    Viewset for Utilisation Document allows CRUD operations
"""
class UtilisationDocumentViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationDocumentSerializer
    queryset = UtilisationDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationDocumentFilter
    permission_classes = [UtilisationEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Utilisation document
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Utilisation document
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data)

"""
    Viewset for UtilisationUpload allows CRUD operations
"""
class UtilisationUploadViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationUploadSerializer
    queryset = UtilisationUpload.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [UtilisationUploadVerificationPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query will fetch all Utilisation Upload
        """
        queryset = UtilisationUpload.objects.all()

        project_id = self.request.query_params.get("project_id")
        if project_id:
            queryset = queryset.filter(project__id=project_id)

        return queryset

    def create(self, request, *args, **kwargs):
        """
        This function will create the Utilisation upload
        """
        try:
            with transaction.atomic():
                file_name = request.data.get("csv_file")

                csv_file = request.FILES.get("csv_file")

                decoded_file = csv_file.read().decode('utf-8').splitlines()

                file_data = BulkUtilisation().read_csv(decoded_file)

                serializer = self.get_serializer(data=request.data)
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer)

                upload_type = self.request.data.get('upload_type', "document")
                project_id = self.request.data.get('project_id')

                full_filename = serializer.data.get('csv_file').split('/')[-1].split('?')[0]
                print(full_filename)

                project = Project.objects.get(id=project_id)

                instance = serializer.instance
                instance.csv_file = full_filename
                instance.file_name = file_name
                instance.upload_type = upload_type
                instance.project = project
                instance.save()

                BulkUtilisation().process(file_data, instance, project)

                serializer = self.get_serializer(instance)

                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        except BulkUtilisationException as e:
            return Response(
                {
                    "error": f"CSV Error: {len(e.errors)} error(s) found",
                    "errors": e.errors
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        except Exception as e:
            return Response({"error": f"Something went wrong: {str(e)}"}, status=status.HTTP_400_BAD_REQUEST)

"""
   Filter for Utilisation Upload Document which will filter the document by exact utilisation_upload id
"""
class UtilisationUploadDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationUploadDocument
        fields = {
            'utilisation_upload': ['exact']
        }

"""
    Viewset for Utilisation upload Document allows CRUD operations
"""
class UtilisationUploadDocumentViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationUploadDocumentSerializer
    queryset = UtilisationUploadDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationUploadDocumentFilter
    permission_classes = [UtilisationUploadEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Utilisation upload document
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # Crete a new folder if not existing with the name of the bulk upload inside the project

        # Move the document to the new folder

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Utilisation upload document
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data)

"""
    Api view for Bulk Utilisation Template
"""
class BulkUtilisationTemplateAPIView(APIView):
    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        template_name = "bulk_utilisation_template"
        template_attributes = [
            "project_id",
            "disbursement_name",
            "expense_category",
            "particulars",
            "utilisation_description",
            "planned_cost",
            "avg_unit",
            "avg_unit_cost",
            "from_date",
            "to_date",
            "total_estimate_cost",
            "total_actual_cost",
            "tags",
            "split_frequency",
            "period_start_date",
            "period_end_date",
            "estimate_cost",
            "actual_cost",
            "unit_cost",
            "number_of_units",
            "remarks",
            "comments"
        ]

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="download-' + template_name + '.csv"'
        writer = csv.writer(response)
        writer.writerow(list(template_attributes))
        return response

"""
    Viewset for Utilisation Expense allows CRUD operations
"""
class UtilisationExpenseViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationExpenseSerializer
    queryset = UtilisationExpense.objects.all().order_by('period_start_date')
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [UtilisationEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        query will fetch all Utilisation Expense
        """
        queryset = UtilisationExpense.objects.all().order_by('period_start_date')

        utilisation_id = self.request.query_params.get("utilisation_id")
        if utilisation_id:
            queryset = queryset.filter(utilisation__id=utilisation_id).order_by('period_start_date')

        return queryset

"""
   Filter for Utilisation ExpenseDocument which will filter the document by exact utilisation_expense id
"""
class UtilisationExpenseDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationExpenseDocument
        fields = {
            'utilisation_expense': ['exact']
        }

"""
    Viewset for Utilisation Expense Document allows CRUD operations
"""
class UtilisationExpenseDocumentViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationExpenseDocumentSerializer
    queryset = UtilisationExpenseDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationExpenseDocumentFilter
    permission_classes = [UtilisationExpenseEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Utilisation expense document
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Utilisation expense document
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data)

"""
   Filter for ProjectOutputDocument which will filter the document by exact project_output id
"""
class ProjectOutputDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectOutputDocument
        fields = {
            'project_output': ['exact']
        }
"""
    Viewset for Project Output Document allows CRUD operations
"""
class ProjectOutputDocumentViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectOutputDocumentSerializer
    queryset = ProjectOutputDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectOutputDocumentFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Project Output Document
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        # create_partner_action_alert_on_utilisation_update(serializer.instance.utilisation, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Project Output Document
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)

"""
   Filter for ProjectOutcomeDocument which will filter the document by exact project_outcome id
"""
class ProjectOutcomeDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectOutcomeDocument
        fields = {
            'project_outcome': ['exact']
        }

"""
    Viewset for ProjectOutcomeDocument allows CRUD operations
"""
class ProjectOutcomeDocumentViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectOutcomeDocumentSerializer
    queryset = ProjectOutcomeDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectOutcomeDocumentFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Project Outcome Document
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Project Outcome Document
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)

"""
    Api view for User UnAdded Projects
"""
class UserUnAddedProjectsAPIView(APIView):
    permission_classes = [ProjectManageUserPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        """
        This function will fetch the User UnAdded Projects
        """
        plant = request.GET.get('plant')
        user = request.GET.get('user')
        client = request.GET.get('client')
        user_projects = ProjectUser.objects.filter(user__id=user).values_list('project__id', flat=True)
        projects = Project.objects.filter(plant=plant).exclude(id__in=
                                                               user_projects)
        if client:
            projects = projects.filter(client=client)
        serializer = ProjectSerializer(projects, many=True)
        return Response(serializer.data)


"""
    Api view for User UnAdded Plant
"""
class UserUnAddedPlantAPIView(APIView):
    permission_classes = [ProjectManageUserPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        client = request.GET.get('client')
        user = request.GET.get('user')
        user_plants = PlantUser.objects.filter(user__id=user).values_list('plant__id', flat=True)
        plants = Plant.objects.filter(client=client).exclude(id__in=user_plants)
        serializer = PlantSerializer(plants, many=True)
        return Response(serializer.data)

"""
    Api view for Delete User Project
"""
class DeleteUserProjectAPIView(APIView):
    permission_classes = [DeleteProjectUserPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def delete(request):
        project = request.GET.get('project')
        project_obj = Project.objects.get(id=project)
        user = request.GET.get('user')
        user_obj = User.objects.get(id=user)
        project_user = ProjectUser.objects.get(user__id=user, project__id=project)
        UserActivityLog.objects.create(
            object_id=user,
            user=user_obj,
            action='user_entities_removed',
            projects_changed = ProjectListSerializer(project_obj).data
        )
        project_user.delete()
        return Response({'success': True}, status=status.HTTP_200_OK)

"""
    Viewset for User Project allows CRUD operations
"""
class UserProjectViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectUserSerializer
    queryset = ProjectUser.objects.all().order_by('project')
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [ProjectUserPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def create_permission_for_user(project_user):
        """
        This function will create a permission for project user
        """
        user = project_user.user
        project = project_user.project
        project_permissions = ProjectFeaturePermissionModel.objects.filter(project=project)
        UserActivityLog.objects.create(
            user=user,
            action='user_entities_added',
            projects_changed = ProjectListSerializer(project).data
        )
        user.project_feature_permission.add(*list(project_permissions))
        user.save()

    def create(self, request, *args, **kwargs):
        """
        This function will create the project user
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.create_permission_for_user(serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

"""
   Filter for ProjectIndicator which will filter the Indicator by exact project id,milestone id.
"""
class ProjectIndicatorFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectIndicator
        fields = {
            'project': ['exact'],
            'milestone': ['exact', 'isnull']
        }
"""
    Viewset for ProjectIndicator allows delete operations
"""
class ProjectIndicatorViewSet(BulkModelViewSet):
    queryset = ProjectIndicator.objects.all().order_by('position', 'milestone')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectIndicatorFilter
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        method = self.request.method
        if method == 'PUT' or method == 'POST' or method == 'PATCH':
            return ProjectIndicatorSerializer
        elif self.action == 'list':
            return ProjectIndicatorReadSerializer
        else:
            return ProjectDetailIndicatorSerializer

    @staticmethod
    def add_locations(locations, indicator_id):
        """
        This function will fetch the existing locations and delete them and create new location for particular indicator_location.
        """
        existing_locations = ProjectIndicatorLocation.objects.filter(indicator=indicator_id)
        existing_locations.delete()
        location_objects = []
        for location in locations:
            location_objects.append(ProjectIndicatorLocation(
                location=location,
                area=location.get('area', ''),
                city=location.get('city', ''),
                state=location.get('state', ''),
                country=location.get('country', ''),
                latitude=location.get('latitude', 0),
                longitude=location.get('longitude', 0),
                indicator=ProjectIndicator.objects.get(id=indicator_id)))
        ProjectIndicatorLocation.objects.bulk_create(location_objects)

    def add_child_data_for_indicator_location(self, request, serializer, instance=None):
        """
        This function will create the child data for particular indicator_location
        """
        project_indicator = serializer.data['id']
        locations = request.data.get('location', [])
        if locations or ProjectIndicatorLocation.objects.filter(indicator=instance).count():
            self.add_locations(locations, project_indicator)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.add_child_data_for_indicator_location(request, serializer)
        # bulk = isinstance(request.data, list)
        # if not bulk:
        #     return super(BulkCreateModelMixin, self).create(request, *args, **kwargs)
        #
        # else:
        #     serializer = self.get_serializer(data=request.data, many=True)
        #     serializer.is_valid(raise_exception=True)
        #     self.perform_bulk_create(serializer)
        #     if len(serializer.instance):
        #         create_partner_action_alert_on_indicator_creation(serializer.instance[0], request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    # def bulk_update(self, request, *args, **kwargs):
    #     batch_id = request.data[0].get('indicator_batch')
    #     qs = ProjectIndicator.objects.filter(indicator_batch=batch_id)
    #     filtered = self.filter_queryset(qs)
    #     for obj in filtered:
    #         self.perform_destroy(obj)
    #     serializer = self.get_serializer(data=request.data, many=True)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_bulk_create(serializer)
    #     if len(serializer.instance):
    #         create_partner_action_alert_on_indicator_update(serializer.instance[0], request.user)
    #     return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        """
        This function will update the project
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        self.add_child_data_for_indicator_location(request, serializer,instance)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
"""
    Viewset for ProjectOutput allows CRUD operations
"""
class ProjectOutputViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectOutputMiniSerializer
    queryset = ProjectOutput.objects.all().order_by('project_indicator')
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the project output
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        project = serializer.instance.project
        action_by = ProjectUser.objects.get(user=request.user, project=project) \
            if request.user.application_role.role_name != 'sattva-admin' else None
        client_settings = ClientSettings.objects.get(client=project.client)
        if client_settings.impact_workflow:
            rule_set = ImpactWorkflowRuleSet.objects.filter(projects__in=[project])
            if len(rule_set) > 0:
                ProjectOutputStatus.objects.create(
                    projectOutput=serializer.instance,
                    action_by=action_by,
                    status='UNVERIFIED',
                    rule=ImpactWorkflowRule.objects.get(rule_type='REQUESTER',
                                                            ruleset__projects__in=[project])
                )
        # create_partner_action_alert_on_utilisation_creation(serializer.instance, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the project output
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        output_status_list = ProjectOutputStatus.objects.filter(projectOutput=serializer.instance)
        if len(output_status_list) > 0:
            status = output_status_list.order_by('id').reverse()[0]
            if status.status:
                status.status='UNVERIFIED'
                status.rule=ImpactWorkflowRule.objects.get(rule_type='REQUESTER', ruleset__projects__in=[serializer.instance.project])
                status.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_utilisation_update(serializer.instance, request.user)
        return Response(serializer.data)

"""
    Viewset for ProjectOutcome allows CRUD operations
"""
class ProjectOutcomeViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectOutcomeMiniSerializer
    queryset = ProjectOutcome.objects.all().order_by('project_indicator')
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Project Outcome.
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        project = serializer.instance.project
        action_by = ProjectUser.objects.get(user=request.user, project=serializer.instance.project) \
            if request.user.application_role.role_name != 'sattva-admin' else None
        client_settings = ClientSettings.objects.get(client=project.client)
        if client_settings.impact_workflow:
            rule_set = ImpactWorkflowRuleSet.objects.filter(projects__in=[project])
            if len(rule_set) > 0:
                ProjectOutcomeStatus.objects.create(
                    projectOutcome=serializer.instance,
                    action_by=action_by,
                    status='UNVERIFIED',
                    rule=ImpactWorkflowRule.objects.get(rule_type='REQUESTER',
                                                                ruleset__projects__in=[project])
                )
        # create_partner_action_alert_on_utilisation_creation(serializer.instance, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Project Outcome.
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        outcome_status_list = ProjectOutcomeStatus.objects.filter(projectOutcome=serializer.instance)
        if len(outcome_status_list) > 0:
            status = outcome_status_list.order_by('id').reverse()[0]
            if status.status:
                status.status='UNVERIFIED'
                status.rule=ImpactWorkflowRule.objects.get(rule_type='REQUESTER', ruleset__projects__in=[serializer.instance.project])
                status.save()

        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        # create_partner_action_alert_on_utilisation_update(serializer.instance, request.user)
        return Response(serializer.data)

"""
   Filter for ProjectOutput which will filter the output by exact project_indicator id
"""
class ProjectOutputFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectOutput
        fields = {
            'project_indicator': ['exact']
        }

"""
    Viewset for Project Output Mini model allows read operations
"""
class ProjectOutputMiniViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectOutputMicroSerializer
    queryset = ProjectOutput.objects.all().order_by('project_indicator')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectOutputFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for ProjectOutcome which will filter the outcome by exact project_indicator id
"""
class ProjectOutcomeFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectOutcome
        fields = {
            'project_indicator': ['exact']
        }

"""
    Viewset for ProjectOutcome allows read operations
"""
class ProjectOutcomeMiniViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectOutcomeMicroSerializer
    queryset = ProjectOutcome.objects.all().order_by('project_indicator')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectOutcomeFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Indicator Outcome which will filter the outcome by exact indicator_batch id
"""
class IndicatorOutcomeFilter(django_filters.FilterSet):
    class Meta:
        model = IndicatorOutcome
        fields = {
            'indicator_batch': ['exact']
        }


"""
   Filter for Indicator Document which will filter the document by exact indicator id
"""
class IndicatorDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = IndicatorDocument
        fields = {
            'indicator': ['exact']
        }

"""
    Viewset for Indicator Document allows CRUD operations
"""
class IndicatorDocumentViewSet(viewsets.ModelViewSet):
    serializer_class = IndicatorDocumentSerializer
    queryset = IndicatorDocument.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = IndicatorDocumentFilter
    permission_classes = [ImpactIndicatorPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the indicator document
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the indicator document
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)

"""
   Filter for IndicatorComment which will filter the comment by exact indicator id
"""
class IndicatorCommentFilter(django_filters.FilterSet):
    class Meta:
        model = IndicatorComment
        fields = {
            'indicator': ['exact']
        }

"""
    Viewset for Indicator Comment allows CRUD operations
"""
class IndicatorCommentViewSet(viewsets.ModelViewSet):
    serializer_class = IndicatorCommentSerializer
    queryset = IndicatorComment.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = IndicatorCommentFilter
    permission_classes = [ImpactIndicatorPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the Indicator Comment
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Indicator Comment
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)


"""
   Filter for Indicator ActivityLog which will filter the ActivityLog by exact indicator id
"""
class IndicatorActivityLogFilter(django_filters.FilterSet):
    class Meta:
        model = IndicatorActivityLog
        fields = {
            'indicator': ['exact']
        }

"""
    Viewset for Indicator Activity Log allows read operations
"""
class IndicatorActivityLogViewSet(viewsets.ModelViewSet):
    serializer_class = IndicatorActivityLogSerializer
    queryset = IndicatorActivityLog.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = IndicatorActivityLogFilter
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Impact Data And Template ActivityLog which will filter the ActivityLog  by exact template id
"""
class TemplateActivityLogFilter(django_filters.FilterSet):
    class Meta:
        model = ImpactDataAndTemplateActivityLog
        fields = {
            'template': ['exact']
        }
"""
    Viewset for TemplateActivityLog allows read operations
"""
class TemplateActivityLogViewSet(viewsets.ModelViewSet):
    serializer_class = ImpactDataAndTemplateActivityLogSerializer
    queryset = ImpactDataAndTemplateActivityLog.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TemplateActivityLogFilter
    permission_classes = [TemplateActivityLogPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Viewset for IndicatorOutcome allows CRUD operations
"""
class IndicatorOutcomeViewSet(viewsets.ModelViewSet):
    serializer_class = IndicatorOutcomeSerializer
    queryset = IndicatorOutcome.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = IndicatorOutcomeFilter
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Project Indicator Outcome which will filter the Indicator by exact project_indicator id
"""
class ProjectIndicatorOutcomeFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectIndicatorOutcome
        fields = {
            'project_indicator': ['exact']
        }

"""
    Viewset for Project Indicator Outcome allows CRUD operations
"""
class ProjectIndicatorOutcomeViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectIndicatorOutcomeSerializer
    queryset = ProjectIndicatorOutcome.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectIndicatorOutcomeFilter
    permission_classes = [ImpactIndicatorPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for ProjectRemark which will filter the remark by exact project id
"""
class ProjectRemarkFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectRemark
        fields = {
            'project': ['exact']
        }

"""
    Viewset for Project Remark allows CRUD operations
"""
class ProjectRemarkViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectRemarkSerializer
    queryset = ProjectRemark.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectRemarkFilter
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Viewset for Template Type allows read operations
"""
class TemplateTypeViewSet(viewsets.ModelViewSet):
    serializer_class = TemplateTypeSerializer
    queryset = TemplateType.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Template which will filter the Template by exact project id,client id,template_type ,focus_area,is_added,is_standard.
"""
class TemplateFilter(django_filters.FilterSet):
    class Meta:
        model = Template
        fields = {
            'project': ['exact', 'isnull'],
            'client': ['exact'],
            'template_type': ['exact'],
            'template_type__template_type': ['exact'],
            'focus_area': ['exact'],
            'is_added': ['exact'],
            'is_standard': ['exact']
        }

"""
    Viewset for Template allows CRUD operations
"""
class TemplateViewSet(viewsets.ModelViewSet):
    serializer_class = TemplateSerializer
    queryset = Template.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TemplateFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def list(self, request, *args, **kwargs):
        """
        This function will list the Template
        """
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        """
        This function will create the Template
        """
        data = request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        create_partner_action_alert_on_template_creation(serializer.instance, request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        """
        This function will update the Template
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        create_partner_action_alert_on_template_update(serializer.instance, request.user)
        return Response(serializer.data)

"""
    Api view for Un added Project Template
"""
class UnaddedProjectTemplateAPIView(APIView):
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request, project_id):
        """
        This function will fetch the Un added Project Template
        """
        template_type = request.GET.get('template_type')
        focus_area = request.GET.get('focus_area')
        templates = Template.objects.filter(focus_area_id=focus_area, template_type_id=template_type)
        templates = templates.filter(Q(project__id=project_id, is_added=False) | Q(is_standard=True, project=None))
        serializer = TemplateSerializer(templates, many=True)
        return Response(serializer.data)


def get_parent_table_name(client_id, project_id, template_id):
    """
        This function will fetch the parent table name
    """
    table_name = 'C' + str(client_id) + '_' + 'P' + str(project_id) + '_' \
                 + 'T' + str(template_id)
    return table_name.lower()


def alter_column_in_table(table_name, old_name, new_name):
    """
        This function will alter the column in table
    """
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            cursor.execute('ALTER TABLE ' + table_name + ' RENAME COLUMN "' + old_name + '" TO "' + new_name + '";')
            altered = True
        else:
            altered = False
    return altered


def check_if_table_exists(table_name):
    """
        This function will check if the table exists
    """
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            return True
        else:
            return False
    return False


def create_column_in_table(table_name, column_name, data_type):
    """
        This function will create column in table
    """
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            if data_type == 'varchar':
                sql = 'ALTER TABLE ' + table_name + ' ADD COLUMN "' + column_name + '" ' + data_type + '(100);'
            else:
                sql = 'ALTER TABLE ' + table_name + ' ADD COLUMN "' + column_name + '" ' + data_type + ';'
            response = cursor.execute(sql)
            created = True
        else:
            created = False
    return created


def delete_column_in_table(table_name, column_name):
    """
        This function will delete column in table
    """
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            print(table_name)
            print(column_name)
            print('ALTER TABLE ' + table_name + ' DROP COLUMN "' + column_name + '";')
            response = cursor.execute('ALTER TABLE ' + table_name + ' DROP COLUMN "' + column_name + '";')
            dropped = True
        else:
            dropped = False
    return dropped

"""
   Filter for Template Attribute which will filter the attribute by exact template id
"""
class TemplateAttributeFilter(django_filters.FilterSet):
    class Meta:
        model = TemplateAttribute
        fields = {
            'template': ['exact']
        }

"""
    Viewset for Template Attribute allows CRUD operations
"""
class TemplateAttributeViewSet(BulkModelViewSet):
    serializer_class = TemplateAttributeSerializer
    queryset = TemplateAttribute.objects.all().order_by('attribute_order')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = TemplateAttributeFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the template attribute
        """
        bulk = isinstance(request.data, list)

        if not bulk:
            template_id = request.data.get('template')
            if not template_id:
                return Response('Invalid attribute parameters', status.HTTP_400_BAD_REQUEST)
            template = Template.objects.get(id=template_id)
            project_id = template.project.id
            client_id = template.project.client.id
            table_name = get_parent_table_name(client_id, project_id, template.id)
            if template.table_created or check_if_table_exists(table_name):
                try:
                    create_column_in_table(table_name, request.data.get('attribute_name'),
                                           request.data.get('attribute_type'))
                except:
                    return Response('Invalid attribute parameters', status.HTTP_400_BAD_REQUEST)
            return super(BulkCreateModelMixin, self).create(request, *args, **kwargs)

        else:
            serializer = self.get_serializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            self.perform_bulk_create(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    def bulk_update(self, request, *args, **kwargs):
        """
        This function will update the template attribute in bulk
        """
        template_id = request.query_params.get('template_id')

        qs = TemplateAttribute.objects.filter(template__id=template_id)
        filtered = self.filter_queryset(qs)
        for obj in filtered:
            self.perform_destroy(obj)
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_bulk_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        """
        This function will destroy the template attribute
        """
        instance = self.get_object()
        template = instance.template
        project_id = template.project.id
        client_id = template.project.client.id
        table_name = get_parent_table_name(client_id, project_id, template.id)
        if template.table_created or check_if_table_exists(table_name):
            column_name = instance.attribute_name
            try:
                delete_column_in_table(table_name, column_name)
            except Exception as ex:
                print(ex)
                return Response("Template cannot be updated, please try again!", status=status.HTTP_400_BAD_REQUEST)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        """
        This function will update the template attribute
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        template = instance.template
        project_id = template.project.id
        client_id = template.project.client.id
        table_name = get_parent_table_name(client_id, project_id, template.id)
        if template.table_created or check_if_table_exists(table_name):
            if instance.attribute_type.data_type != request.data.get('attribute_type'):
                return Response("Data type of attribute cannot be changed once table is created!",
                                status=status.HTTP_400_BAD_REQUEST)
            old_name = instance.attribute_name
            new_name = request.data.get('attribute_name')
            try:
                alter_column_in_table(table_name, old_name, new_name)
            except Exception as ex:
                print(ex)
                return Response("Template cannot be updated, please try again!", status=status.HTTP_400_BAD_REQUEST)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

"""
   Api view for download template
"""
class DownloadTemplateAPIView(APIView):
    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request, project_id, template_id):
        template = Template.objects.get(id=template_id)
        template_attribute_objects = TemplateAttribute.objects.filter(template=template, attribute_required=True).order_by(
            'attribute_order', 'id')
        template_attributes = template_attribute_objects.values_list('attribute_name', flat=True)
        template_type = template_attribute_objects.values_list('attribute_type', flat=True)
        response = HttpResponse(content_type='text/csv')
        response[
            'Content-Disposition'] = 'attachment; filename="download-' + template.template_name + ' - ' + template_id + '.csv"'
        writer = csv.writer(response)
        caution = ['Caution: Add Data from 4th row. Do not delete first three rows.']
        writer.writerow(list(caution))
        writer.writerow(list(template_attributes))
        description = self.getDetailsForEachRow(template_type)
        writer.writerow(list(description))
        return response

    def getDetailsForEachRow(self, template_type):
        """
        This function will fetch the details for each row
        """
        mapDescWithType = {
            'numeric': 'Only numbers are allowed without any other commas, dots, symbols',
            'varchar': 'Text field can have any value.',
            'date': 'Only Date formats dd/mm/yyyy or dd-mm-yyyy supported.',
            'timestamp': 'The data can be only timestamp here. Only Date formats yyyy/mm/dd HH24:mm:ss or yyyy-mm-dd HH24:mm:ss are supported.',
            'boolean': 'True/False text is only allowed here'
        }
        description = []
        for attribute in template_type:
            description.append(mapDescWithType[attribute])
        return description

"""
   Filter for CSVUpload which will filter the uploads by exact project id,client,template,status if contained.
"""
class CSVUploadFilter(django_filters.FilterSet):
    class Meta:
        model = CSVUpload
        fields = {
            'project': ['exact'],
            'client': ['exact'],
            'template': ['exact'],
            'status': ['icontains']
        }

"""
   Filter for Impact CaseStudy which will filter the casestudy by exact project id.
"""
class CaseStudyFilter(django_filters.FilterSet):
    class Meta:
        model = ImpactCaseStudy
        fields = {
            'project': ['exact']
        }

"""
    Viewset for Impact CaseStudy allows CRUD operations
"""
class ImpactCaseStudyViewSet(viewsets.ModelViewSet):
    serializer_class = ImpactCaseStudySerializer
    queryset = ImpactCaseStudy.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CaseStudyFilter
    permission_classes = [ImpactCaseStudyPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for CaseStudy Documents which will filter the document by exact case_study id
"""
class CaseStudyDocumentsFilter(django_filters.FilterSet):
    class Meta:
        model = CaseStudyDocuments
        fields = {
            'case_study': ['exact']
        }
"""
    Viewset for CaseStudy Documents allows CRUD operations
"""
class CaseStudyDocumentsViewSet(viewsets.ModelViewSet):
    serializer_class = CaseStudyDocumentsSerializer
    queryset = CaseStudyDocuments.objects.all().order_by('case_study')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CaseStudyDocumentsFilter
    permission_classes = [ImpactCaseStudyPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for CaseStudy ActivityLog which will filter the activity logs by exact case_study id
"""
class CaseStudyActivityLogFilter(django_filters.FilterSet):
    class Meta:
        model = CaseStudyActivityLog
        fields = {
            'case_study': ['exact']
        }

"""
    Viewset for CaseStudy ActivityLog allows read operations
"""
class CaseStudyActivityLogViewSet(viewsets.ModelViewSet):
    serializer_class = CaseStudyActivityLogSerializer
    queryset = CaseStudyActivityLog.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CaseStudyActivityLogFilter
    permission_classes = [ImpactCaseStudyPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for ImpactUpload Status which will filter the uploads by exact impact_upload id
"""
class ImpactUploadStatusFilter(django_filters.FilterSet):
    class Meta:
        model = ImpactUploadStatus
        fields = {
            'impact_upload': ['exact']
        }
"""
    Viewset for Impact Upload Status allows CRUD operations
"""
class ImpactUploadStatusViewSet(viewsets.ModelViewSet):
    serializer_class = ImpactUploadStatusSerializer
    queryset = ImpactUploadStatus.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImpactUploadStatusFilter
    # permission_classes = [ProjectOutputEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the ImpactUploadStatus
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        instance = serializer.instance
        instance.action_by = ProjectUser.objects.get(user=request.user,
                                                     project=serializer.instance.impact_upload.project)
        instance.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

def create_parent_table_definition(client_id, project_id, template_id):
    """
        This function will create the parent table definition
    """
    attribute_list = TemplateAttribute.objects.filter(template__id=template_id, attribute_required=True).order_by('attribute_order', 'id')
    parent_table_name = get_parent_table_name(client_id, project_id, template_id)
    table_definition = 'CREATE TABLE IF NOT EXISTS ' + parent_table_name + ' ('
    table_definition += 'id serial, ' \
                        'batch_id bigint, ' \
                        'created_date timestamp default current_timestamp, ' \
                        'deleted boolean default false, '
    column_list = []
    for attribute in attribute_list:
        col_name = attribute.attribute_name.strip()
        data_type = attribute.attribute_type.data_type
        data_length = 100
        if data_type == 'varchar':
            table_definition += '"' + col_name + '" ' + data_type + '(' + str(data_length) + '),'
            column_list.append(col_name)
        else:
            table_definition += '"' + col_name + '" ' + data_type + ','
            column_list.append(col_name)
    table_definition = table_definition[:-1] + ')'
    return table_definition, column_list


def create_parent_table(client_id, project_id, template_id):
    """
        This function will create the parent table
    """
    parent_table_name = get_parent_table_name(client_id, project_id, template_id)
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" \
                           + str(parent_table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            sql, column_list = create_parent_table_definition(client_id, project_id, template_id)
        else:
            sql, column_list = create_parent_table_definition(client_id, project_id, template_id)
            cursor.execute(sql)
    return column_list


def read_csv_header_column_list(file):
    with default_storage.open(file) as csvfile:
        fl = csvfile.read().decode("utf-8")
        f = StringIO(fl)
        reader = csv.reader(f, delimiter=',')
        next(reader)
        header = next(reader)
    return header


def confirm_csv_header_vs_model_correctness(column_list, header_list):
    """
        This function will confirm csv header vs_model correctness
    """
    diff_in_cols = []
    matched = True
    for i, (col_name, csv_name) in enumerate(zip(column_list, header_list)):
        if col_name.strip().lower() == csv_name.strip().lower():
            diff_in_cols.append(col_name.strip() + ',' + csv_name.strip() + ',Y')
        else:
            matched = False
            diff_in_cols.append(col_name.strip() + ',' + csv_name.strip() + ',N')
    if len(column_list) == len(header_list):
        diff_in_cols.append('DBColCount:' + str(len(column_list)) + ',CSVColCount:' + str(len(header_list)) + ',Y')
    else:
        diff_in_cols.append('DBColCount:' + str(len(column_list)) + ',CSVColCount:' + str(len(header_list)) + ',N')
        matched = False
    return diff_in_cols, matched


def try_parsing_date(text):
    for fmt in ('%Y-%m-%d', '%d/%m/%Y', '%Y/%m/%d', '%d-%m-%Y'):
        try:
            return datetime.datetime.strptime(text, fmt)
        except ValueError:
            pass
    raise ValueError('no valid date format found')

def try_parsing_timestamp(text):
    for fmt in ('%Y-%m-%d %H:%M:%S', '%Y/%m/%d %H:%M:%S'):
        try:
            return datetime.datetime.strptime(text, fmt)
        except ValueError:
            pass
    raise ValueError('no valid time format found')


def boolean_validation(value_list, column):
    """
        This function is responsible for boolean validation
    """
    results = []
    for index, value in enumerate(value_list):
        if value is None or value == '':
            continue
        if value.lower() not in ('true', 'false'):
            dictionary = {'index': index + 1,
                          'value': value,
                          'column': column,
                          'error': 'Value should be either TRUE or FALSE; Change - ' + value}
            results.append(dictionary)
    return results


def timestamp_validation(value_list, column):
    """
        This function responsible for timestamp validation
    """
    results = []
    for index, value in enumerate(value_list):
        if value is None or value == '':
            continue
        try:
            try_parsing_timestamp(value)
        except ValueError:
            try:
                try_parsing_date(value)
            except ValueError:
                dictionary = {'index': index + 1,
                              'value': value,
                              'column': column,
                              'error': 'Incorrect Time format, should be YYYY-MM-DD or YYYY-MM-DD HH24:MI:SS; Change - ' + value}
                results.append(dictionary)
    return results


def varchar_validation(value_list, length, column):
    """
        This function responsible for varchar validation
    """
    results = []
    for index, value in enumerate(value_list):
        if len(value) > length:
            dictionary = {'index': index + 1,
                          'value': value,
                          'column': column,
                          'error': 'Value should be less than ' + str(length) + ' characters; Change - ' + value}
            results.append(dictionary)
    return results


def numeric_validation(value_list, column):
    """
        This function responsible for numeric validation
    """
    results = []
    for index, value in enumerate(value_list):
        try:
            value = value.replace(',','')
            if value is None or value == '':
                continue
            else:
                float(value)
        except ValueError:
            dictionary = {'index': index + 1,
                          'value': value,
                          'column': column,
                          'error': 'Value is not a number; Change - ' + value}
            results.append(dictionary)
    return results


def date_validation(value_list, column):
    """
        This function responsible for date validation
    """
    results = []
    for index, value in enumerate(value_list):
        try:
            if value is None or value == '':
                continue
            else:
                try_parsing_date(value)
        except ValueError:
            dictionary = {'index': index + 1,
                          'value': value,
                          'column': column,
                          'error': 'Incorrect Date format, should be YYYY-MM-DD or DD-MM-YYYY'
                                   ' or YYYY/MM/DD or DD/MM/YYYY; Change - ' + value}
            results.append(dictionary)
    return results


def db_column_details(table_name):
    col_details = []
    sql = """
    select column_name,
        case when data_type = 'character varying' then 'varchar'
            when data_type = 'integer' then 'numeric'
            when data_type = 'date' then 'date'
            when data_type = 'timestamp without time zone' then 'timestamp'
            when data_type = 'timestamp without time zone' then 'timestamp'
            when data_type = 'boolean' then 'boolean'
            when data_type = 'text' then 'varchar'
            else data_type end as data_type,
        case when data_type = 'character varying' then character_maximum_length
            when data_type = 'text' then '1000'
            when data_type = 'varchar' then '1000' end as length
    from information_schema.columns
    where table_name = '""" + table_name + """'
    and column_name not in ('id', 'batch_id', 'created_date', 'deleted')
    and lower(column_name) not like lower('OC % Date Captured')
    and lower(column_name) not like lower('OP % Date Captured')
    order by ordinal_position"""
    with connection.cursor() as cursor:
        cursor.execute(sql)
        results = cursor.fetchall()
    for row in results:
        dictionary = {'col_name': row[0],
                      'data_type': row[1],
                      'length': row[2]}
        col_details.append(dictionary)
    return col_details


def validate_data(table, file):
    """
        This function will validate_data
    """
    column_count = 0
    columns = defaultdict(list)
    db_columns = db_column_details(table.lower())
    with default_storage.open(file) as f:
        fl = f.read().decode("utf-8")
        csvfile = StringIO(fl)
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        row_number = 1
        for row in reader:
            if row_number == 2:
                row_number += 1
                continue
            row_number += 1
            for (i, v) in enumerate(row):
                columns[i].append(v)
                column_count = i + 1

    column_headers = []
    for i in range(0, column_count):
        column_header = columns[i].pop(0)
        column_headers.append(column_header)
    temp_errors = []
    # error_results_all = defaultdict(dict)
    for (i, v) in enumerate(column_headers):
        for dictionary in db_columns:
            # print('Inside - ' + dictionary['col_name'])
            if v == dictionary['col_name']:
                error_results = []
                if dictionary['data_type'] == 'varchar':
                    length = dictionary['length']
                    error_results = varchar_validation(columns[i], length, v)
                    # print(columns[i])
                    # print('varchar_validation(columns[i], ' + str(length) + ', ' + v + ')')
                elif dictionary['data_type'] == 'numeric':
                    error_results = numeric_validation(columns[i], v)
                    # print(columns[i])
                    # print('numeric_validation(columns[i], ' + v + ')')
                elif dictionary['data_type'] == 'date':
                    error_results = date_validation(columns[i], v)
                elif dictionary['data_type'] == 'timestamp':
                    error_results = timestamp_validation(columns[i], v)
                elif dictionary['data_type'] == 'boolean':
                    error_results = boolean_validation(columns[i], v)
            else:
                error_results = []
            if error_results:
                temp_errors.append(error_results)
    dict_list = []
    for x in temp_errors:
        for v in x:
            dict_list.append(v)
    dict_list = sorted(dict_list, key=itemgetter('index'))
    outer_dictionary = {}
    dictionary_list = []
    for key, value in itertools.groupby(dict_list, key=itemgetter('index')):
        outer_dictionary.update({'Record': str(key)})
        inner_dictionary_list = []
        for i in value:
            inner_dictionary = {}
            inner_dictionary.update({i.get('column'): i.get('error')})
            inner_dictionary_list.append(inner_dictionary)
        outer_dictionary.update({'Details': inner_dictionary_list})
        dictionary_list.append(outer_dictionary.copy())
    return dictionary_list


def get_columns_from_attribute(template_attributes):
    """
        This function will fetch columns from attribute
    """
    col_names = []
    for attribute in template_attributes:
        attribute = '"' + attribute + '"'
        col_names.append(attribute)
    return ','.join(col_names)


def read_table_data(client_id, project_id, template_id, batch_id=None, template_attributes=None):
    table_name = get_parent_table_name(client_id, project_id, template_id)
    if not template_attributes:
        query = 'select * from ' + table_name + ' where deleted = false'
    else:
        query = 'select ' + get_columns_from_attribute(
            template_attributes) + ' from ' + table_name + ' where deleted = false'
    if batch_id:
        query += ' and batch_id=' + str(batch_id)
    query += ' order by 1, 2'
    try:
        with connection.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
            data_inserted = [dict(zip([key[0] for key in cursor.description], row)) for row in result]
            inserted_data = [{k: v for k, v in d.items() if k not in ('deleted', 'id', 'batch_id', 'created_date')} for
                             d in data_inserted]
        return inserted_data
    except:
        return []


def get_new_batch_id_for_table(table_name):
    """
        This function will fetch new batch id for table
    """
    if check_if_table_exists(table_name):
        sql_query = 'select max(batch_id) from ' + table_name
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql_query)
                value = -1
                for row in cursor.fetchall():
                    print(row)
                    value = row[0]
                    if value:
                        return value + 1
                    else:
                        return 1
        except Exception as error:
            print(error)
            return -1
    return 1


def insert_data(file, column_list, client_id, project_id, template_id, batch_id):
    table_name = get_parent_table_name(client_id, project_id, template_id)
    db_columns = db_column_details(table_name.lower())
    with default_storage.open(file) as f:
        fl = f.read().decode("utf-8")
        csvfile = StringIO(fl)
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        records = []
        row_count = 1
        for row in reader:
            if row_count == 1:
                db_columns = sorted(db_columns, key=lambda x: row.index(x['col_name']))
            if row_count == 2:
                row_count += 1
                continue
            col_values = ''
            col_count = 0
            for col in row:
                if col == '':
                    col_values += """null, """
                elif row_count > 1 and db_columns[col_count].get('data_type') == 'date':
                    date_value = dateutil.parser.parse(col, dayfirst=True)
                    date_value = datetime.datetime.strftime(date_value, '%Y-%m-%d')
                    col_values += """'""" + date_value + """', """
                elif row_count > 1 and db_columns[col_count].get('data_type') == 'varchar':
                    col = col.replace("'", "")
                    col_values += "'" + col + "', "
                elif row_count > 1 and db_columns[col_count].get('data_type') == 'numeric':
                    col = col.replace(",", "")
                    col_values += "'" + col + "', "
                else:
                    if "'" in col:
                        col = col.replace("'", "''")
                    col_values += """'""" + col + """', """
                col_count = col_count + 1
            col_values = col_values[:-2]
            col_values = """'""" + str(batch_id) + """', """ + col_values
            row_item = col_values
            records.append(row_item)
            row_count += 1
    records.pop(0)
    # new_column_list = column_list.split(",")
    new_column_list = column_list
    new_column_list.insert(0, 'batch_id')
    values_sql_list = ''
    columns = '", "'.join(new_column_list)
    columns = '"' + columns + '"'
    insert_sql = """insert into """ + table_name + """(""" + columns + """) values """
    for record in records:
        values_sql_list += """(""" + record + """), """
    insert_sql += values_sql_list[:-2]
    query = """select * from """ + table_name + """ where deleted = false order by 1, 2"""
    try:
        with connection.cursor() as cursor:
            cursor.execute(insert_sql)
            recnum = cursor.rowcount
            cursor.execute(query)
            result = cursor.fetchall()
            data_inserted = [dict(zip([key[0] for key in cursor.description], row)) for row in result]
            inserted_data = [{k: v for k, v in d.items() if k not in ('deleted', 'id')} for d in data_inserted]
    except Exception as error:
        return -1, -1, -1, str(error)
    return batch_id, inserted_data, -1, {}


def validate_file(file):
    """
        This function will validate the file
    """
    with default_storage.open(file) as csvfile:
        line_number = 1
        all_errors = []
        csvfile.readline()
        while True:
            if line_number == 2:
                line = csvfile.readline()
                line_number += 1
                continue

            line = csvfile.readline()
            line_number += 1
            try:
                decoded_line = line.decode("utf-8")
            except Exception as ex:
                error = {'index': line_number,
                         'value': None,
                         'column': None,
                         'error': 'Unsupported special character in line number ' + str(line_number)}
                all_errors.append(error)
            if line == '' or not line:
                break
    return all_errors


def upload_data(client_id, project_id, template_id, file, batch_id=None):
    column_list = create_parent_table(client_id, project_id, template_id)
    errors = validate_file(file)
    if len(errors):
        return -1, -1, -1, {'Failed': True, 'UnsupportedCharacters': True, 'AllErrors': errors}
    header_list = read_csv_header_column_list(file)
    diff_in_expected_cols, matched = confirm_csv_header_vs_model_correctness(column_list, header_list)
    if not matched:
        return -1, -1, -1, {'Failed': True, 'SchemaMismatch': True, 'SchemaDiff': diff_in_expected_cols}
    table = get_parent_table_name(client_id, project_id, template_id)
    if not batch_id:
        batch_id = get_new_batch_id_for_table(table)
    error_list = validate_data(table, file)
    if error_list:
        return -1, -1, -1, {'Failed': True, 'DataValidationIssue': True, 'AllErrors': error_list}
    batch_id, response, data_inserted, errors = insert_data(file, column_list, client_id, project_id, template_id,
                                                            batch_id)
    if batch_id == -1:
        return batch_id, response, data_inserted, {'Failed': True, 'DataIssue': True, 'FirstError': errors}
    else:
        return batch_id, response, data_inserted, {'Failed': False}


def delete_csv_data(project_id, template_id, batch_id):
    template = Template.objects.get(id=template_id)
    table_name = get_parent_table_name(client_id=template.project.client.id, project_id=project_id,
                                       template_id=template_id)
    if template.table_created or check_if_table_exists(table_name):
        sql_to_delete_columns = 'DELETE from ' + table_name
        csv_uploads = CSVUpload.objects.filter(template=template)
        if batch_id:
            sql_to_delete_columns += ' where batch_id=' + batch_id
            csv_uploads = csv_uploads.filter(batch_id=batch_id)
        with connection.cursor() as cursor:
            cursor.execute(sql_to_delete_columns, (1,))
            # logger.info("Delete records from Table : " + table_name)
            # logger.info(sql_to_delete_columns)
            rowcount = cursor.rowcount
            csv_uploads.update(is_deleted=True)

"""
    Viewset for CSVUpload allows CRUD operations
"""
class CSVUploadViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'csvupload:view',
        'POST': 'csvupload:add'
    }
    serializer_class = CSVUploadSerializer
    queryset = CSVUpload.objects.all().order_by('created_date')
    parser_classes = (MultiPartParser, FormParser,)
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CSVUploadFilter
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the csv uploads
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        client_id = self.request.data.get('client')
        project_id = self.request.data.get('project')
        template_id = self.request.data.get('template')
        batch_id = self.request.data.get('batch_id')
        if batch_id:
            # First delete the data then upload
            delete_csv_data(project_id, template_id, batch_id)
        file = serializer.data.get('csv_file').split('/')[-1].split('?')[0]
        batch_id, response, data_inserted, errors = upload_data(client_id, project_id, template_id, file, batch_id)
        if errors['Failed']:
            instance = serializer.instance
            instance.upload_successful = False
            instance.save()
            data = {'Upload': 'Failed', 'Error': errors}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
        else:
            data = {'InsertCount': response, 'DataInserted': data_inserted, 'Errors': errors}
            instance = serializer.instance
            instance.batch_id = batch_id
            instance.csv_file = file
            instance.upload_successful = True
            instance.table_name = get_parent_table_name(client_id, project_id, template_id)
            instance.save()
            template = Template.objects.get(id=template_id)
            template.table_created = True
            template.save(activity_log=True)
            create_partner_action_alert_on_impact_data_upload(template, request.user)
            headers = self.get_success_headers(serializer.data)
            action_by = ProjectUser.objects.get(user=request.user, project=serializer.instance.project) \
                if request.user.application_role.role_name != 'sattva-admin' else None
            client_settings = ClientSettings.objects.get(client=serializer.instance.project.client)
            if client_settings.impact_workflow:
                rule_set = ImpactWorkflowRuleSet.objects.filter(projects__in=[serializer.instance.project])
                if len(rule_set) > 0:
                    ImpactUploadStatus.objects.create(
                        impact_upload=serializer.instance,
                        action_by=action_by,
                        status='UNVERIFIED',
                        rule=ImpactWorkflowRule.objects.get(rule_type='REQUESTER',
                                                                ruleset__projects__in=[serializer.instance.project])
                    )
            return Response(data, status=status.HTTP_201_CREATED, headers=headers)

"""
    Viewset for CSVData allows CRUD operations
"""
class CSVDataViewSet(APIView):
    # TODO: Unused, Should it be removed?
    def get(self, request):
        try:
            client_id = int(self.request.query_params.get('client_id'))
            project_id = int(self.request.query_params.get('project_id'))
            template_id = int(self.request.query_params.get('template_id'))
            attribute_list = TemplateAttribute.objects.filter(template__id=template_id).order_by('attribute_order',
                                                                                                 'id').values_list(
                'attribute_name', flat=True)
            data = read_table_data(client_id, project_id, template_id)
            return Response({'columns': attribute_list, 'data': data}, status=status.HTTP_200_OK)
        except:
            return Response({"error": "Wrong Input details"}, status=status.HTTP_400_BAD_REQUEST)

"""
    Viewset for FilesModel allows CRUD operations
"""
class FilesModelViewset(APIView):
    # TODO: Unused, Should it be removed?

    def get(self, request, pk=None):
        try:
            project = Project.objects.get(pk=pk)
            file_list = []
            project_file = project.get_all_project_file()
            sub_file_list = []
            project_images = project.get_all_images()
            sub_file_list.append(
                [{"name": "Images", "files": FilesCommonProjectImageSerializer(project_images, many=True).data}])
            disbursement_docs = project.get_all_disbursements_documents()
            sub_file_list.append([{"name": "Disbursement",
                                   "files": FilesCommonDisbursementSerializer(disbursement_docs, many=True).data}])
            file_list.append(
                {"files": FilesCommonProjectSerializer(project_file, many=True).data, "folders": sub_file_list})
            return Response(file_list, status=status.HTTP_200_OK)
        except:
            error_message = {'Error': 'Project PK not valid', }
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk=None):
        try:
            client_id = get_value_or_default(request.data, 'client_id', None)
            upload_file = request.FILES['upload_file']
            if not pk is None:
                project = Project.objects.get(pk=pk)
                project_doc = ProjectDocument.objects.create(
                    project=project,
                    document_file=upload_file
                )
                data_message = {"Success": True}
            elif not client_id is None:
                client = Client.objects.get(pk=pk)
                client_doc = ClientDocument.objects.create(
                    client=client,
                    document_file=upload_file
                )
                data_message = {"Success": True}
            else:
                data_message = {'Error': 'Client ID not given'}
            return Response(data_message, status=status.HTTP_200_OK)
        except:
            error_message = {'Error': 'Project ID/Client ID not valid', }
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

"""
    Api view for Create Project Template
"""
class CreateProjectTemplateAPIView(APIView):
    permission_classes = [ImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def post(request, project_id, template_id):
        template = Template.objects.get(id=template_id)
        if template.project and template.project.id == project_id and not template.is_added:
            template.is_added = True
            template.save()
            serializer = TemplateSerializer(template)
            data = serializer.data
            return Response(data, status=status.HTTP_201_CREATED)
        template_attributes = TemplateAttribute.objects.filter(template=template, attribute_required=True).order_by(
            'attribute_order', 'id')

        try:
            project_template = Template.objects.create(
                template_name=template.template_name,
                focus_area=template.focus_area,
                template_type=template.template_type,
                project=Project.objects.get(id=project_id),
                is_added=True
            )
        except IntegrityError as ex:
            return Response("Template with same name already exists for the project!",
                            status=status.HTTP_400_BAD_REQUEST)
        template_attribute_list = []
        for template_attribute in template_attributes:
            template_attribute_list.append(
                TemplateAttribute(template=project_template,
                                  attribute_name=template_attribute.attribute_name,
                                  attribute_type=template_attribute.attribute_type,
                                  attribute_order=template_attribute.attribute_order,
                                  attribute_required=template_attribute.attribute_required,
                                  attribute_grouping=template_attribute.attribute_grouping))
        TemplateAttribute.objects.bulk_create(template_attribute_list)
        serializer = TemplateSerializer(template)
        data = serializer.data
        return Response(data, status=status.HTTP_201_CREATED)

"""
    Api view for delete Project Template
"""
class DeleteProjectTemplateAPIView(APIView):
    permission_classes = [DeleteImpactPermission, DeleteImpactTemplatePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def delete(request, project_id, template_id):
        """
        This function will delete the Project Template
        """
        template = Template.objects.get(id=template_id)
        table_name = get_parent_table_name(client_id=template.project.client.id, project_id=project_id,
                                           template_id=template_id)
        csv_uploads = CSVUpload.objects.filter(template=template)
        if template.table_created or check_if_table_exists(table_name):
            sql_to_delete_columns = 'DELETE from ' + table_name + ';'
            with connection.cursor() as cursor:
                cursor.execute(sql_to_delete_columns, (1,))
                # logger.info("Delete records from Table : " + table_name)
                # logger.info(sql_to_delete_columns)
                rowcount = cursor.rowcount
                csv_uploads.update(is_deleted=True)
        # template.is_added = False
        # template.save()
        template.delete()
        serializer = TemplateSerializer(template)
        data = serializer.data
        return Response(data, status=status.HTTP_200_OK)

"""
    Api view for delete Project Template data
"""
class DeleteProjectTemplateDataAPIView(APIView):
    permission_classes = [DeleteImpactPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def delete(request, project_id, template_id):
        """
        This function will delete the Project Template data
        """
        template = Template.objects.get(id=template_id)
        batch_id = request.GET.get('batch_id')
        table_name = get_parent_table_name(client_id=template.project.client.id, project_id=project_id,
                                           template_id=template_id)
        if template.table_created or check_if_table_exists(table_name):
            sql_to_delete_columns = 'DELETE from ' + table_name
            csv_uploads = CSVUpload.objects.filter(template=template)
            if batch_id:
                sql_to_delete_columns += ' where batch_id=' + batch_id
                csv_uploads = csv_uploads.filter(batch_id=batch_id)
            ImpactDataAndTemplateActivityLog.objects.create(
                object_id=batch_id,
                template=template,
                action='data_deleted',
                new_value={},
                old_value=serializers.serialize('json', csv_uploads)
            )
            with connection.cursor() as cursor:
                cursor.execute(sql_to_delete_columns, (1,))
                # logger.info("Delete records from Table : " + table_name)
                # logger.info(sql_to_delete_columns)
                rowcount = cursor.rowcount
                csv_uploads.update(is_deleted=True)
        serializer = TemplateSerializer(template)
        data = serializer.data
        return Response(data, status=status.HTTP_200_OK)

"""
    Api view for Download CSVData
"""
class DownloadCSVDataAPIView(APIView):
    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request, project_id, template_id):
        template = Template.objects.get(id=template_id)
        template_attributes = TemplateAttribute.objects.filter(template=template, attribute_required=True).order_by(
            'attribute_order', 'id').values_list('attribute_name', flat=True)
        response = HttpResponse(content_type='text/csv')
        response[
            'Content-Disposition'] = 'attachment; filename="download-' + template.template_name + ' - ' + template_id + '.csv"'
        writer = csv.writer(response)
        writer.writerow(list(template_attributes))
        table_name = get_parent_table_name(template.project.client.id, template.project.id, template.id)
        batch_id = request.GET.get('batch_id')
        if check_if_table_exists(table_name):
            data = read_table_data(template.project.client.id, project_id, template_id, batch_id, template_attributes)
            for row in data:
                writer.writerow(row.values())
        return response

"""
    Api view for Impact Data Batches
"""
class ImpactDataBatchesAPIView(APIView):
    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request, project_id, template_id):
        template = Template.objects.get(id=template_id)
        batches = {}
        csv_uploads = CSVUpload.objects.filter(template=template, is_deleted=False).order_by('created_date')
        csv_uploads_data = CSVUploadSerializer(csv_uploads, many=True).data
        for csv_upload in csv_uploads_data:
            csv_upload = dict(csv_upload)
            if csv_upload['batch_id'] in batches:
                old_batch = batches[csv_upload['batch_id']]
                old_batch['updated_date'] = csv_upload['updated_date']
                old_batch['updated_by'] = csv_upload['updated_by']
            else:
                batches[csv_upload['batch_id']] = csv_upload
        return Response(batches.values(), status=status.HTTP_200_OK)

"""
   Filter for ProjectDashboard which will filter the dashboard by exact project id
"""
class ProjectDashboardFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectDashboard
        fields = {
            'project': ['exact'],
        }

"""
    Viewset for Project Dashboard allows CRUD operations
"""
class ProjectDashboardModelViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectDashboardSerializer
    queryset = ProjectDashboard.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectDashboardFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Api view for Project Location Extract
"""
class ProjectLocationExtract(APIView):

    def get(self, request, pk=None):
        if pk is not None:
            try:
                project = Project.objects.get(id=pk)
                location_json = project.location
                if (len(location_json) > 0):
                    location_details = location_json[0]['details'] if 'details' in location_json[0] else None
                    if location_details is not None:
                        if 'address_components' in location_details:
                            address_components = location_details['address_components']
                            locality_name = None
                            city_name = None
                            state_name = None
                            country_name = None
                            if len(address_components):
                                for loc in address_components:
                                    if loc['types'][0] == 'locality':
                                        locality_name = loc['long_name']
                                    elif loc['types'][0] == 'administrative_area_level_1':
                                        city_name = loc['long_name']
                                    elif loc['types'][0] == 'administrative_area_level_2':
                                        state_name = loc['long_name']
                                    elif loc['types'][0] == 'country':
                                        country_name = loc['long_name']
                        project.area = locality_name if locality_name is not None else ''
                        project.city = city_name if city_name is not None else ''
                        city_name = city_name if city_name is not None else locality_name
                        state_name = state_name if state_name is not None else city_name
                        project.state = state_name if state_name is not None else ''
                        project.country = country_name if country_name is not None else ''
                        project.save()
                data = {"success": "true"}
            except:
                data = {"error": "Project ID invalid or Error parsing location json"}
        else:
            data = {"error": "Project ID not given"}
        return Response(data, status=status.HTTP_200_OK)

"""
   Filter for ProjectScheduleVII which will filter the ScheduleVII by exact project id
"""
class ProjectScheduleVIIActivityFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectScheduleVII
        fields = {
            'project': ['exact']
        }

"""
    Viewset for Project ScheduleVII allows CRUD operations
"""
class ProjectScheduleVIIActivityViewSet(BulkModelViewSet):
    serializer_class = ProjectScheduleVIIActivitySerializer
    queryset = ProjectScheduleVII.objects.all().order_by('created_date')
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectScheduleVIIActivityFilter
    lookup_field = 'project'

    def bulk_update(self, request, *args, **kwargs):
        """
        This function will update ScheduleVII in bulk
        """
        project = request.data[0].get('project')
        qs = ProjectScheduleVII.objects.filter(project__id=project)
        filtered = self.filter_queryset(qs)
        for obj in filtered:
            self.perform_destroy(obj)
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_bulk_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

"""
    Api view for Reminding Users For Action On Disbursement
"""
class RemindUsersForActionOnDisbursement(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def post(self, request, disbursement_id):
        disbursement = Disbursement.objects.get(id=disbursement_id)
        if disbursement.is_custom_workflow:
            next_rule = disbursement.get_next_rule()
            if next_rule.get('id'):
                assignees = DisbursementWorkflowRuleAssignee.objects.filter(rule__id=next_rule.get('id'),
                                                                            project=disbursement.project)
            else:
                return Response({"result": 'No Action Required!'}, status=status.HTTP_200_OK)
        else:
            return Response({"result": 'No Action Required!'}, status=status.HTTP_200_OK)
        create_action_alert_on_disbursement_action_required(disbursement, assignees)
        return Response({"result": 'A reminder has been sent successfully!'}, status=status.HTTP_200_OK)


"""
   Filter for Disbursement Workflow RuleSet which will filter the RuleSet by exact client id,project id.
"""
class DisbursementWorkflowRuleSetFilter(django_filters.FilterSet):
    class Meta:
        model = DisbursementWorkflowRuleSet
        fields = {
            'client': ['exact'],
            'projects': ['exact']
        }

"""
    Viewset for Disbursement Workflow RuleSet allows CRUD operations
"""
class DisbursementWorkflowRuleSetViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementWorkflowRuleSetSerializer
    queryset = DisbursementWorkflowRuleSet.objects.all().order_by('id')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementWorkflowRuleSetFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        queryset = DisbursementWorkflowRuleSet.objects.all().order_by('id')
        if 'project' in self.request.query_params:
            project = Project.objects.get(id=self.request.query_params['project'])
            client_settings = ClientSettings.objects.get(client=project.client)
            if client_settings.disbursement_workflow:
                queryset = queryset.filter(projects__in=[self.request.query_params['project']])
            else:
                queryset = DisbursementWorkflowRuleSet.objects.none()
        return queryset

    @staticmethod
    def create_initial_rules(instance):
        DisbursementWorkflowRule.objects.create(ruleset=instance, rule_type='REQUESTER', level=100)
        DisbursementWorkflowRule.objects.create(ruleset=instance, rule_type='CHECKER', level=200)
        DisbursementWorkflowRule.objects.create(ruleset=instance, rule_type='APPROVER', level=300)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.create_initial_rules(serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

"""
   Filter for Disbursement Workflow Rule which will filter the workflow by exact ruleset
"""
class DisbursementWorkflowRuleFilter(django_filters.FilterSet):
    class Meta:
        model = DisbursementWorkflowRule
        fields = {
            'ruleset': ['exact']
        }

"""
    Viewset for Disbursement Workflow Rule allows CRUD operations
"""
class DisbursementWorkflowRuleViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementWorkflowRuleSerializer
    queryset = DisbursementWorkflowRule.objects.all().order_by('level')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementWorkflowRuleFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Disbursement Workflow Rule Assignee which will filter the assignee by exact rule
"""
class DisbursementWorkflowAssigneeFilter(django_filters.FilterSet):
    class Meta:
        model = DisbursementWorkflowRuleAssignee
        fields = {
            'rule': ['exact']
        }

"""
    Viewset for Disbursement Workflow Assignee allows CRUD operations
"""
class DisbursementWorkflowAssigneeViewSet(viewsets.ModelViewSet):
    serializer_class = DisbursementWorkflowAssigneeSerializer
    queryset = DisbursementWorkflowRuleAssignee.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DisbursementWorkflowAssigneeFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Api view for Disbursement Workflow Project
"""
class DisbursementWorkflowProjectAPIView(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request, client):
        """
        This function will fetch the DisbursementWorkflow for project
        """
        disbursement_projects = DisbursementWorkflowRuleSet.objects.filter(client=client).values_list('projects',
                                                                                                      flat=True)
        disbursement_projects = list(disbursement_projects)
        while None in disbursement_projects:
            disbursement_projects.remove(None)
        projects = Project.objects.filter(client=client).exclude(id__in=disbursement_projects)
        data = ProjectMicroSerializer(projects, many=True).data
        return Response(data)

    def patch(self, request, client):
        """
        This function will update the DisbursementWorkflow for project
        """
        ruleset = request.data.get('ruleset')
        projects = request.data.get('projects')
        ruleset_instance = DisbursementWorkflowRuleSet.objects.get(id=ruleset)
        ruleset_instance.projects.add(*projects)
        return Response(status=204)

    def post(self, request, client):
        """
        This function will remove the DisbursementWorkflow for project
        """
        ruleset = request.data.get('ruleset')
        projects = request.data.get('projects')
        ruleset_instance = DisbursementWorkflowRuleSet.objects.get(id=ruleset)
        ruleset_instance.projects.remove(*projects)
        return Response(status=204)

"""
    Api view for Remove Disbursement Workflow Assignee
"""
class RemoveDisbursementWorkflowAssigneeAPIView(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def post(self, request):
        user = request.data.get('user')
        project = request.data.get('project')
        rule = request.data.get('rule')
        assignee = DisbursementWorkflowRuleAssignee.objects.get(
            project=project,
            rule=rule,
            user=user
        )
        assignee.delete()
        return Response(status=204)

"""
   Filter for UtilisationWorkflowRuleSet which will filter the ruleset by exact client,project id.
"""
class UtilisationWorkflowRuleSetFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationWorkflowRuleSet
        fields = {
            'client': ['exact'],
            'projects': ['exact']
        }

"""
    Viewset for Utilisation Workflow RuleSet allows CRUD operations
"""
class UtilisationWorkflowRuleSetViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationWorkflowRuleSetSerializer
    queryset = UtilisationWorkflowRuleSet.objects.all().order_by('id')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationWorkflowRuleSetFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the query set
        """
        queryset = UtilisationWorkflowRuleSet.objects.all().order_by('id')
        if 'project' in self.request.query_params:
            project = Project.objects.get(id=self.request.query_params['project'])
            client_settings = ClientSettings.objects.get(client=project.client)
            if client_settings.utilisation_workflow:
                queryset = queryset.filter(projects__in=[self.request.query_params['project']])
            else:
                queryset = UtilisationWorkflowRuleSet.objects.none()
        return queryset

    @staticmethod
    def create_initial_rules(instance):
        UtilisationWorkflowRule.objects.create(ruleset=instance, rule_type='REQUESTER', level=100)
        UtilisationWorkflowRule.objects.create(ruleset=instance, rule_type='CHECKER', level=200)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.create_initial_rules(serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

"""
   Filter for ImpactWorkflowRuleSet which will filter the ruleset by exact client,project id.
"""
class ImpactWorkflowRuleSetFilter(django_filters.FilterSet):
    class Meta:
        model = ImpactWorkflowRuleSet
        fields = {
            'client': ['exact'],
            'projects': ['exact']
        }

"""
    Viewset for Impact Workflow RuleSet allows CRUD operations
"""
class ImpactWorkflowRuleSetViewSet(viewsets.ModelViewSet):
    serializer_class = ImpactWorkflowRuleSetSerializer
    queryset = ImpactWorkflowRuleSet.objects.all().order_by('id')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImpactWorkflowRuleSetFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        queryset = ImpactWorkflowRuleSet.objects.all().order_by('id')
        if 'project' in self.request.query_params:
            project = Project.objects.get(id=self.request.query_params['project'])
            client_settings = ClientSettings.objects.get(client=project.client)
            if client_settings.impact_workflow:
                queryset = queryset.filter(projects__in=[self.request.query_params['project']])
            else:
                queryset = ImpactWorkflowRuleSet.objects.none()
            if len(queryset) > 0:
                # If workflow is on
                indicator_update_on_workflow.apply_async(kwargs={"project_id": project.id})
        return queryset

    @staticmethod
    def create_initial_rules(instance):
        ImpactWorkflowRule.objects.create(ruleset=instance, rule_type='REQUESTER', level=100)
        ImpactWorkflowRule.objects.create(ruleset=instance, rule_type='CHECKER', level=200)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.create_initial_rules(serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

"""
   Filter for Impact Workflow Rule which will filter the rule by exact ruleset id
"""
class ImpactWorkflowRuleFilter(django_filters.FilterSet):
    class Meta:
        model = ImpactWorkflowRule
        fields = {
            'ruleset': ['exact']
        }

"""
    Viewset for Impact WorkflowRule allows CRUD operations
"""
class ImpactWorkflowRuleViewSet(viewsets.ModelViewSet):
    serializer_class = ImpactWorkflowRuleSerializer
    queryset = ImpactWorkflowRule.objects.all().order_by('level')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImpactWorkflowRuleFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Impact Workflow Assignee which will filter the assignee by exact rule id
"""
class ImpactWorkflowAssigneeFilter(django_filters.FilterSet):
    class Meta:
        model = ImpactWorkflowRuleAssignee
        fields = {
            'rule': ['exact']
        }
"""
    Viewset for Impact Workflow Assignee allows CRUD operations
"""
class ImpactWorkflowAssigneeViewSet(viewsets.ModelViewSet):
    serializer_class = ImpactWorkflowAssigneeSerializer
    queryset = ImpactWorkflowRuleAssignee.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ImpactWorkflowAssigneeFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Api for Impact Workflow Project
"""
class ImpactWorkflowProjectAPIView(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request, client):
        """
        This function will fetch the Impact Workflow RuleSet
        """
        impact_projects = ImpactWorkflowRuleSet.objects.filter(client=client).values_list('projects',
                                                                                                    flat=True)
        impact_projects = list(impact_projects)
        while None in impact_projects:
            impact_projects.remove(None)
        projects = Project.objects.filter(client=client).exclude(id__in=impact_projects)
        data = ProjectMicroSerializer(projects, many=True).data
        return Response(data)

    def patch(self, request, client):
        """
        This function will fetch the Impact Workflow RuleSet
        """
        ruleset = request.data.get('ruleset')
        projects = request.data.get('projects')
        ruleset_instance = ImpactWorkflowRuleSet.objects.get(id=ruleset)
        ruleset_instance.projects.add(*projects)
        return Response(status=204)

    def post(self, request, client):
        """
        This function will create and remove the Impact Workflow RuleSet
        """
        ruleset = request.data.get('ruleset')
        projects = request.data.get('projects')
        ruleset_instance = ImpactWorkflowRuleSet.objects.get(id=ruleset)
        ruleset_instance.projects.remove(*projects)
        return Response(status=204)

"""
    Api for Remove ImpactWork flow Assignee
"""
class RemoveImpactWorkflowAssigneeAPIView(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def post(self, request):
        user = request.data.get('user')
        project = request.data.get('project')
        rule = request.data.get('rule')
        assignee = ImpactWorkflowRuleAssignee.objects.get(
            project=project,
            rule=rule,
            user=user
        )
        assignee.delete()
        return Response(status=204)

"""
   Filter for Utilisation Workflow Rule which will filter the rule by exact ruleset id
"""
class UtilisationWorkflowRuleFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationWorkflowRule
        fields = {
            'ruleset': ['exact']
        }


"""
    Viewset for Utilisation Workflow Rule allows CRUD operations
"""
class UtilisationWorkflowRuleViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationWorkflowRuleSerializer
    queryset = UtilisationWorkflowRule.objects.all().order_by('level')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationWorkflowRuleFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Utilisation Workflow Rule Assignee which will filter the Assignee by exact rule id
"""
class UtilisationWorkflowAssigneeFilter(django_filters.FilterSet):
    class Meta:
        model = UtilisationWorkflowRuleAssignee
        fields = {
            'rule': ['exact']
        }

"""
    Viewset for Utilisation Workflow Assignee allows CRUD operations
"""
class UtilisationWorkflowAssigneeViewSet(viewsets.ModelViewSet):
    serializer_class = UtilisationWorkflowAssigneeSerializer
    queryset = UtilisationWorkflowRuleAssignee.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UtilisationWorkflowAssigneeFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Api for Utilisation Workflow Project
"""
class UtilisationWorkflowProjectAPIView(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request, client):
        """
        This function will fetch the Utilisation Workflow Project
        """
        utilisation_projects = UtilisationWorkflowRuleSet.objects.filter(client=client).values_list('projects',
                                                                                                    flat=True)
        utilisation_projects = list(utilisation_projects)
        while None in utilisation_projects:
            utilisation_projects.remove(None)
        projects = Project.objects.filter(client=client).exclude(id__in=utilisation_projects)
        data = ProjectMicroSerializer(projects, many=True).data
        return Response(data)

    def patch(self, request, client):
        """
        This function will update the Utilisation Workflow Project
        """
        ruleset = request.data.get('ruleset')
        projects = request.data.get('projects')
        ruleset_instance = UtilisationWorkflowRuleSet.objects.get(id=ruleset)
        ruleset_instance.projects.add(*projects)
        return Response(status=204)

    def post(self, request, client):
        """
        This function will remove the Utilisation Workflow Project
        """
        ruleset = request.data.get('ruleset')
        projects = request.data.get('projects')
        ruleset_instance = UtilisationWorkflowRuleSet.objects.get(id=ruleset)
        ruleset_instance.projects.remove(*projects)
        return Response(status=204)

"""
    Api for Removing Utilisation Workflow Assignee
"""
class RemoveUtilisationWorkflowAssigneeAPIView(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def post(self, request):
        user = request.data.get('user')
        project = request.data.get('project')
        rule = request.data.get('rule')
        assignee = UtilisationWorkflowRuleAssignee.objects.get(
            project=project,
            rule=rule,
            user=user
        )
        assignee.delete()
        return Response(status=204)

"""
    Api for Checking Disbursement Unassigned Rule
"""
class CheckDisbursementUnassignedRuleAPIView(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request):
        project_id = request.GET.get('project')
        rules = DisbursementWorkflowRule.objects.filter(ruleset__projects__in=[project_id])
        assigned_rules = DisbursementWorkflowRuleAssignee.objects.values('rule').filter(
            project__id=project_id).distinct()
        if rules.count() == assigned_rules.count():
            return Response({'is_unassigned': False}, status=200)
        return Response({'is_unassigned': True}, status=200)

"""
    Api for Checking Previous Utilisation For Disbursement
"""
class CheckPreviousUtilisationForDisbursementAPIView(APIView):
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request):
        disbursement_id = request.GET.get('disbursement')
        disbursement = Disbursement.objects.get(id=disbursement_id)
        disbursements_to_check = Disbursement.objects.filter(expected_date__lte=disbursement.expected_date,
                                                             project=disbursement.project).exclude(id=disbursement.id)
        can_proceed = True
        for disb in disbursements_to_check:
            utilisations = disb.get_all_utilisations()
            if not len(utilisations):
                can_proceed = False
                break
            else:
                utilisations = utilisations.filter(is_custom_workflow=True)
                for utilisation in utilisations:
                    current_status = utilisation.get_current_status()
                    if current_status['status'] != 'APPROVED':
                        can_proceed = False
                        break
                if not can_proceed:
                    break
        return Response({'can_proceed': can_proceed}, status=200)

"""
   Filter for Project Setting which will filter the setting by exact project id
"""
class ProjectSettingFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectSetting
        fields = {
            'project': ['exact']
        }


"""
    Viewset for Project Setting allows CRUD operations
"""
class ProjectSettingModelViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectSettingSerializer
    queryset = ProjectSetting.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectSettingFilter
    permission_classes = [ProjectSettingsPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

class BulkUploadFilter(django_filters.FilterSet):
    class Meta:
        model = BulkUpload
        fields = {
            'batch_id': ['exact'],
            'client':['exact']
        }

class BulkUploadModelViewSet(viewsets.ModelViewSet):
    serializer_class = BulkUploadSerializer
    queryset = BulkUpload.objects.all().order_by('batch_id')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = BulkUploadFilter
    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):

            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            client_id = self.request.data.get('client')
            template_name = self.request.data.get('template_name')
            batch_id = self.request.data.get('batch_id')
            headers = self.get_success_headers(serializer.data)
            file=serializer.instance.file_name.name
            flag=0
            if template_name=='Task Upload':
                result,errors=upload_taskss(file,client_id)
                if result==False:
                        serializer.instance.errors=errors
                        serializer.instance.save()
                        return Response(
                            {   "error": f"CSV Error: {len(errors)} error(s) found",
                                "errors": errors
                            },status=status.HTTP_400_BAD_REQUEST)
                else :
                    flag=1
                    return Response(status=status.HTTP_201_CREATED)

            elif template_name=='Project Upload':
                result,errors=upload_projects(file)
                if result==False:
                    serializer.instance.errors=errors
                    serializer.instance.save()
                    return Response(
                        {   "error": f"CSV Error: {len(errors)} error(s) found",
                            "errors": errors
                        },status=status.HTTP_400_BAD_REQUEST)
                else :
                    flag=1
                    return Response(status=status.HTTP_201_CREATED)

            elif template_name=='Milestone Upload':
                result,errors=upload_milestones(file, client_id)
                if result==False:
                    serializer.instance.errors=errors
                    serializer.instance.save()
                    return Response(
                        {   "error": f"CSV Error: {len(errors)} error(s) found",
                            "errors": errors
                        },status=status.HTTP_400_BAD_REQUEST)
                else :
                    flag=1
                    return Response(status=status.HTTP_201_CREATED)

            elif template_name=='Disbursement Upload':
                result,errors=upload_disbursement(file)
                if result==False:
                    serializer.instance.errors=errors
                    serializer.instance.save()
                    return Response(
                        {   "error": f"CSV Error: {len(errors)} error(s) found",
                            "errors": errors
                        },status=status.HTTP_400_BAD_REQUEST)
                else :
                    flag=1
                    return Response(status=status.HTTP_201_CREATED)

            elif template_name=='Milestone Notes Upload':
                result,errors=upload_milestone_notes(file)
                if result==False:
                    serializer.instance.errors=errors
                    serializer.instance.save()
                    return Response(
                        {   "error": f"CSV Error: {len(errors)} error(s) found",
                            "errors": errors
                        },status=status.HTTP_400_BAD_REQUEST)
                else :
                    flag=1
                    return Response(status=status.HTTP_201_CREATED)

            elif template_name=='Task Comments Upload':
                result,errors=upload_task_comment(file, client_id)
                if result==False:
                    serializer.instance.errors=errors
                    serializer.instance.save()
                    return Response(
                        {   "error": f"CSV Error: {len(errors)} error(s) found",
                            "errors": errors
                        },status=status.HTTP_400_BAD_REQUEST)
                else :
                    flag=1
                    return Response(status=status.HTTP_201_CREATED)

            elif template_name=='Indicator Upload':
                result,errors=upload_impact_indicator(file)
                if result==False:
                    serializer.instance.errors=errors
                    serializer.instance.save()
                    return Response(
                        {   "error": f"CSV Error: {len(errors)} error(s) found",
                            "errors": errors
                        },status=status.HTTP_400_BAD_REQUEST)
                else :
                    flag=1
                    return Response(status=status.HTTP_201_CREATED)

            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

"""
    Api for Create Milestone and Task .xlsx File
"""


class ProjectPlanExcelView(APIView):
    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        try:
            project_id = request.GET.get('project')
            milestone_objects = Milestone.objects.filter(project=project_id).values('id', 'project__project_name',
                                                                                    'milestone_name',
                                                                                    'milestone_description',
                                                                                    'start_date__date',
                                                                                    'end_date__date',
                                                                                    'actual_start_date__date',
                                                                                    'actual_end_date__date', 'tags',
                                                                                    'status', 'location',
                                                                                    'sub_focus_area',
                                                                                    'sub_target_segment').order_by('position')
            milestone_ids = [obj['id'] for obj in milestone_objects if milestone_objects]
            milestone_comment_objects = MilestoneNote.objects.filter(milestone__in=milestone_ids).order_by(
                                        'updated_date', 'created_date')
            milestone_comments = {}
            for num, mil in enumerate(milestone_objects):
                loc_list = None
                if isinstance(mil.get('location'), list):
                    loc_list = '; '.join(i.get('area') for i in mil.get('location') if i.get('area'))
                if isinstance(mil.get('location'), dict):
                    loc_list = mil.get('location').get('area') if mil.get('location').get('area') else mil.get('location').get('city')
                milestone_objects[num]['location']=loc_list if loc_list else None
            for i in milestone_comment_objects:
                if milestone_comments.get(i.milestone.id):
                    if len(milestone_comments[i.milestone.id]) >= 10:
                        continue
                    else:
                        milestone_comments[i.milestone.id] = milestone_comments[i.milestone.id] + \
                                                    ["/".join((str(i.created_date.date()), i.created_by, i.note))]
                else:
                    milestone_comments[i.milestone.id] = ["/".join((str(i.created_date.date()), i.created_by, i.note))]
            task_objects = Task.objects.filter(project=project_id).values('id', 'project__project_name',
                                                                          'milestone__milestone_name',
                                                                          'task_name',
                                                                          'task_description',
                                                                          'start_date__date',
                                                                          'end_date__date',
                                                                          'priority', 'status',
                                                                          'actual_start_date__date',
                                                                          'actual_end_date__date',
                                                                          'assignee__user__first_name').order_by(
                                                                         'milestone__position', 'position')
            task_ids = [obj['id'] for obj in task_objects if task_objects]
            task_comment_object = TaskComment.objects.filter(task__in=task_ids).order_by('updated_date', 'created_date')
            comments = {}
            for obj in task_comment_object:
                if comments.get(obj.task.id):
                    if len(comments[obj.task.id]) >= 10:
                        continue
                    else:
                        comments[obj.task.id] = comments[obj.task.id] + \
                                              ["/".join((str(obj.created_date.date()), obj.created_by, obj.comment))]
                else:
                    comments[obj.task.id] = ["/".join((str(obj.created_date.date()), obj.created_by, obj.comment))]
            milestone_header = ['Project Name', 'Milestone Name', 'Milestone Description', 'Planned Start Date',
                                'Planned End Date', 'Actual Start Date', 'Actual End Date',
                                'Tags', 'Status', 'Location', 'Sub-Focus Area', 'Sub-Target Segment']
            task_header = ['Project Name', 'Milestone Name', 'Task Name', 'Task Description',
                           'Planned Start Date', 'Planned End Date', 'Priority', 'Status', 'Actual Start Date',
                           'Actual End Date', 'Assignee User']
            return create_xlsx_file(['Milestone', 'Task'], [milestone_header, task_header],
                                    [milestone_objects, task_objects], [milestone_comments, comments])
        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)


"""
    Api for Create Disbursement and Utilisation .xlsx FileFile
"""


class FinancialExcelView(APIView):
    @staticmethod
    def get(request):
        try:
            project_id = request.GET.get('project')
            disbursement_objects = Disbursement.objects.filter(project=project_id).values('id', 'project__project_name',
                                                                                          'disbursement_name',
                                                                                          'milestone__milestone_name',
                                                                                          'disbursement_description',
                                                                                          'expected_date__date',
                                                                                          'expected_amount',
                                                                                          'expense_category',
                                                                                          'start_date__date',
                                                                                          'end_date__date',
                                                                                          'invoice_request_date__date',
                                                                                          'actual_date__date',
                                                                                          'actual_amount',
                                                                                          'approval_date__date', 'tags',
                                                                                          'status').order_by('position')

            disbursement_ids = [obj['id'] for obj in disbursement_objects if disbursement_objects]
            disbursement_comment_objects = DisbursementComment.objects.filter(disbursement__in=disbursement_ids).order_by('updated_date',
                                                                                                   'created_date')
            disbursement_comments = {}
            for i in disbursement_comment_objects:
                if disbursement_comments.get(i.disbursement.id):
                    if len(disbursement_comments[i.disbursement.id]) >= 10:
                        continue
                    else:
                        disbursement_comments[i.disbursement.id] = disbursement_comments[i.disbursement.id] + \
                                                             ["/".join((str(i.created_date.date()), i.created_by, i.comment))]
                else:
                    disbursement_comments[i.disbursement.id] = ["/".join((str(i.created_date.date()), i.created_by, i.comment))]
            utilisation_objects = Utilisation.objects.filter(project=project_id).values('id', 'project__project_name',
                                                                                        'disbursement__disbursement_name',
                                                                                        'particulars',
                                                                                        'unit_cost',
                                                                                        'actual_cost',
                                                                                        'start_date__date',
                                                                                        'end_date__date',
                                                                                        'description', 'tags',
                                                                                        'position',
                                                                                        'planned_cost',
                                                                                        'total_estimate_cost',
                                                                                        'expense_category',
                                                                                        'number_of_units',
                                                                                        'updated_date__date').order_by(
                'disbursement__position', 'position')

            utilisation_ids = [obj['id'] for obj in utilisation_objects if utilisation_objects]
            utilisation_comment_objects = UtilisationComment.objects.filter(utilisation__in=utilisation_ids).order_by(
                'updated_date', 'created_date')
            utilisation_comments = {}
            for i in utilisation_comment_objects:
                if utilisation_comments.get(i.utilisation.id):
                    if len(utilisation_comments[i.utilisation.id]) >= 10:
                        continue
                    else:
                        utilisation_comments[i.utilisation.id] = utilisation_comments[i.utilisation.id] + \
                                                                ["/".join((str(i.created_date.date()), i.created_by, i.comment))]
                else:
                    utilisation_comments[i.utilisation.id] = ["/".join((str(i.created_date.date()), i.created_by, i.comment))]
            disbursement_header = ['Project Name', 'Disbursement Name', 'Milestone Name',  'Disbursement Description',
                                   'Expected Date',  'Expected Amount', 'Expense Category', 'Start Date',
                                    'End Date', 'Fund Request Date' 'Disbursement Date', 'Approved Amount',
                                   'Approval Date', 'Tags', 'Status']
            utilisation_header = ['Project Name', 'Disbursement Name', 'Particulars', 'Unit Cost', 'Total Actual Cost',
                                  'Start Date', 'End Date', 'Description', 'Tags', 'Position',
                                  'Total Planned Cost', 'Total Estimate Cost', 'Expense Category', 'Number Of Units',
                                  'Last Modified Date']

            return create_xlsx_file(['Disbursement', 'Utilisation'], [disbursement_header, utilisation_header],
                                    [disbursement_objects, utilisation_objects], [disbursement_comments, utilisation_comments])
        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)



class AllTaskExcelView(APIView):
    @staticmethod
    def get(request):
        try:
            client_id = request.GET.get('client')
            task_objects = Task.objects.filter(client=client_id).values('id', 'project__project_name',
                                                      'milestone__milestone_name',
                                                      'task_name',
                                                      'task_description',
                                                      'start_date__date',
                                                      'end_date__date',
                                                      'actual_start_date__date',
                                                      'actual_end_date__date', 'tags',
                                                      'priority', 'status', 'percentage_completed').order_by(
                                                     'milestone__position', 'position')
            task_ids = [obj['id'] for obj in task_objects if task_objects]
            task_comment_object = TaskComment.objects.filter(task__in=task_ids).order_by('updated_date', 'created_date')
            comments = {}
            for obj in task_comment_object:
                if comments.get(obj.task.id):
                    if len(comments[obj.task.id]) >= 10:
                        continue
                    else:
                        comments[obj.task.id] = comments[obj.task.id] + \
                                              ["/".join((str(obj.created_date.date()), obj.created_by, obj.comment))]
                else:
                    comments[obj.task.id] = ["/".join((str(obj.created_date.date()), obj.created_by, obj.comment))]
            task_header = ['Project Name', 'Milestone Name', 'Task Name', 'Task Description',
                           'Planned Start Date', 'Planned End Date', 'Actual Start Date', 'Actual End Date', 'Tags',
                           'Priority', 'status', 'Task Completion Status']
            return create_xlsx_file(['All Task'], [task_header],
                                    [task_objects], [comments])
        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)


class AllProgramsExcelView(APIView):
    @staticmethod
    def get(request):
        try:
            client_id = request.GET.get('client')
            project_objects = Project.objects.filter(client=client_id).values('id', 'plant__plant_name',
                                                                                    'project_name',
                                                                                    'start_date__date',
                                                                                    'end_date__date',
                                                                                    'ngo_partner__ngo_name',
                                                                                    'project_description'
                                                                            ).order_by('plant__plant_name', 'project_name')


            members = {}
            for project in project_objects:
                loc = ProjectLocation.objects.filter(project=project.get('id')).values('city', 'area')
                loc_list = '; '.join([i.get('area') for i in loc])
                target_segment = ProjectTargetSegment.objects.filter(project=project.get('id')).values_list('target_segment',
                                                                                               flat=True).order_by('target_segment')
                project['tags'] = target_segment
                project_users = ProjectUser.objects.filter(project=project.get('id')).values_list('user', flat=True)
                project_user = UserSerializerForProject(User.objects.filter(id__in=project_users), many=True).data
                project_user_list = [dict(i) for i in project_user]
                for obj in project_user_list:
                    if members.get(project.get('id')):
                        members[project.get('id')] = members[project.get('id')] + \
                                                    [" ".join((obj.get('first_name'), obj.get('last_name')))]
                    else:
                        members[project.get('id')] = [" ".join((obj.get('first_name'), obj.get('last_name')))]
                project['location'] = loc_list
            project_header = ['Plant Name', 'Project Name', 'Start Date', 'End Date', 'Partner', 'Project Objective', 'Tags',  'Location']
            return create_xlsx_file(titles=['Program'], headers=[project_header],
                                    objects=[project_objects], comments=None, members=[members])
        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)



class IndicatorExcelView(APIView):
    @staticmethod
    def get(request):
        try:
            project_id = request.GET.get('project')


            project_objects = ProjectIndicator.objects.filter(project=project_id).values('id', 'milestone__milestone_name',
                                                                                        'is_beneficiary',
                                                                                        'output_indicator',
                                                                                        'outcome_indicator').order_by('position', 'project')

            indicator_object = []
            comments = {}
            for indicator in project_objects:
                out_come = {}
                out_come['id'] = indicator.get('id')
                out_come['milestone'] = indicator.get('milestone__milestone_name')
                out_come['Beneficiary']='ON' if indicator.get('is_beneficiary') == True else 'OFF'
                out_come['output_indicator'] = indicator.get('output_indicator')
                objects_output = ProjectOutput.objects.filter(project_indicator=indicator.get('id')).aggregate(Sum('planned_output'), Sum('actual_output'))
                objects_output['outcome_indicator'] = indicator.get('outcome_indicator')
                objects_outcome = ProjectOutcome.objects.filter(project_indicator=indicator.get('id')).aggregate(Sum('planned_outcome'), Sum('actual_outcome'))
                outcome_type = ProjectIndicatorOutcome.objects.filter(project_indicator=indicator.get('id')).values('outcome', 'outcome_type')
                objects_output.update(objects_outcome)
                out_come.update(objects_output)
                out_come['short'] = None
                out_come['long'] = None

                for i in outcome_type:
                    if i.get('outcome_type') == 'SHORT':
                        out_come['short'] = out_come.get('short')+", "+i.get('outcome') if out_come.get('short') else i.get('outcome')
                    elif i.get('outcome_type') == 'LONG':
                        out_come['long'] = out_come.get('long')+", "+i.get('outcome') if out_come.get('long') else i.get('outcome')
                indicator_object.append(out_come)

                comment_object = IndicatorComment.objects.filter(indicator=indicator.get('id')).order_by('updated_date',
                                                                                             'created_date')
                for obj in comment_object:
                    if comments.get(obj.indicator.id):
                        if len(comments[obj.indicator.id]) >= 10:
                            continue
                        else:
                            comments[obj.indicator.id] = comments[obj.indicator.id] + \
                                                    ["/".join(
                                                        (str(obj.created_date.date()), obj.created_by, obj.comment))]
                    else:
                        comments[obj.indicator.id] = ["/".join((str(obj.created_date.date()), obj.created_by, obj.comment))]

            indicator_header = ['Milestone Name', 'Beneficiary', 'Output Indicator', 'Planned Output', 'Actual Output',
                                'Outcome Indicator', 'Planned outcome', 'Actual Outcome', 'Short Term Outcome',
                                'Long Term Outcome']
            return create_xlsx_file(titles=['Indicator'], headers=[indicator_header],
                                    objects=[indicator_object], comments=[comments], members=None)
        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)


class LocationPortfolioExcelView(APIView):
    @staticmethod
    def get(request):
        try:
            client_id = request.GET.get('client')
            plant_id = request.GET.get('plant')
            project_id = request.GET.get('project')
            if client_id:
                locations = ProjectLocation.objects.filter(project__client=client_id).values('project__plant__plant_name',
                                                                                            'project__project_name',
                                                                                            'country', 'state', 'city',
                                                                                             'area' )
            elif plant_id:
                locations = ProjectLocation.objects.filter(project__plant=plant_id).values('project__plant__plant_name',
                                                                                             'project__project_name',
                                                                                             'country', 'state', 'city',
                                                                                             'area').order_by(
                    'updated_date', 'created_date')
            elif project_id:
                locations = ProjectLocation.objects.filter(project=project_id).values('project__plant__plant_name',
                                                                                      'project__project_name',
                                                                                      'country', 'state', 'city',
                                                                                      'area').order_by('updated_date', 'created_date')
            locations_header = ['Program Name', 'Project Name', 'Country', 'State', 'District', 'City']

            return create_xlsx_file(titles=['Location'], headers=[locations_header],
                                    objects=[locations], comments=None, members=None)
        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)


class LocationPortfolioViewSet(APIView):
    @staticmethod
    def get(request):
        try:
            client_id = request.GET.get('client')
            plant_id = request.GET.get('plant')
            project_id = request.GET.get('project')
            from django.http import JsonResponse
            if client_id:
                locations = ProjectLocation.objects.filter(project__client=client_id).values('project__plant__plant_name',
                                                                                            'project__project_name',
                                                                                            'country', 'state', 'city',
                                                                                             'area' )
            elif plant_id:
                locations = ProjectLocation.objects.filter(project__plant=plant_id).values('project__plant__plant_name',
                                                                                           'project__project_name',
                                                                                           'country', 'state', 'city',
                                                                                           'area').order_by('updated_date', 'created_date')
            elif project_id:
                locations = ProjectLocation.objects.filter(project=project_id).values('project__plant__plant_name',
                                                                                      'project__project_name',
                                                                                      'country', 'state', 'city',
                                                                                      'area').order_by('updated_date', 'created_date')
            rows = []
            for row in locations:
                new_key_1 = "Program_Name"
                new_key_2 = "Project_Name"
                old_key_1 = "project__plant__plant_name"
                old_key_2 = "project__project_name"
                row[new_key_1] = row.pop(old_key_1)
                row[new_key_2] = row.pop(old_key_2)
                rows.append(row)
            return JsonResponse(rows, safe=False)
        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)


class BulkfileFilter(django_filters.FilterSet):
    class Meta:
        model = Bulkfiles
        fields = {
            'template_name': ['exact']
        }


class BulkfileModelViewSet(viewsets.ModelViewSet):
    serializer_class = BulkfileSerializer
    queryset = Bulkfiles.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = BulkfileFilter
    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ProjectImageDownload(APIView):
    @staticmethod
    def get(request):
        try:
            project_id = request.GET.get("project")
            image_objects = ProjectImage.objects.filter(project=project_id)
            url_list = image_objects[:]
            f = BytesIO()
            # zip = zipfile.ZipFile(f, 'w')
            with zipfile.ZipFile(f, mode='w') as zf:
                for image in url_list:
                    url_path = image.image_file.url
                    filename = url_path.split('/')[-1].split('.png')[0]
                    url = urllib.request.urlopen(url_path)
                    zf.writestr(filename+'.jpge', url.read())
            # zip.close()
            response = HttpResponse(f.getvalue(), content_type="application/zip")
            response['Content-Disposition'] = 'attachment; filename=plant4bar.zip'
            return response
        except Exception as err:
             return Response({'error': 'Failed to Download file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)


class GeneratePDF(APIView):
    @staticmethod
    def get(request):
        try:
            project_id = request.GET.get("project")
            case_study_id = request.GET.get("id")
            cases = ImpactCaseStudy.objects.get(id=case_study_id, project=project_id)
            location = get_location(cases.location)
            template_path = './pdf.html'
            context = {'title': cases.title, 'image': cases.logo.url,
                       'description': cases.description}
            if cases.focus_area:
                context = append_in_context(cases.focus_area, context, 'focus_area')
            if cases.sub_focus_area:
                context = append_in_context(cases.sub_focus_area, context, 'sub_focus_area')
            if cases.target_segment:
                context = append_in_context(cases.target_segment, context, 'target_segment')
            if cases.sub_target_segment:
                context = append_in_context(cases.sub_target_segment, context, 'sub_target_segment')
            if cases.location:
                context = append_in_context(location, context, 'location')
            if cases.tags:
                context = append_in_context(cases.tags, context, 'tags')

            # Create a Django response object, and specify content_type as pdf
            response = HttpResponse(content_type='application/pdf')
            response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            # find the template and render it.
            template = get_template(template_path)
            html = template.render(context)

            # create a pdf
            pisa_status = pisa.CreatePDF(
               # html, dest=response, link_callback=link_callback)
               html, dest=response)
            # if error then show some funy view
            if pisa_status.err:
               return HttpResponse('We had some errors <pre>' + html + '</pre>')
            return response
        except Exception as e:
            return Response({"error": "Something went wrong:"+str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)



class DatabaseQuery(APIView):
    @staticmethod
    def get(request):
        import time
        from v2.client.models import Client
        from v2.data_scripts.client_tables import ClientTableHelper
        from django.db import connection
        from v2.data_scripts.constants import CREATE_TABLE_PROJECT_MCA, CREATE_TABLE_PLANT_MCA, CREATE_PROJECT_INDICATOR_LOCATION
        clients = Client.objects.all()
        for client in clients:
            querys = [CREATE_TABLE_PROJECT_MCA, CREATE_TABLE_PLANT_MCA, CREATE_PROJECT_INDICATOR_LOCATION]
            try:
                cl_helper = ClientTableHelper(client)
                for query in querys:
                    print("query===", query)
                    query = query.format(cl_helper.client.id, cl_helper.schema_name)
                    cursor = connection.cursor()
                    cursor.execute(query)
                    cursor.close()
            except Exception as err:
                print("error=" + str(err) + str(cl_helper.schema_name))
                continue
        return Response({"status": "successful"}, status=status.HTTP_200_OK)



class DatabaseQuery2(APIView):
    @staticmethod
    def get(request):
        import time
        from v2.client.models import Client
        from v2.data_scripts.client_tables import ClientTableHelper
        from django.db import connection
        clients = Client.objects.all()
        set_primary_key="""
        ALTER TABLE {1}.project_projectindicatorlocation ADD PRIMARY KEY (id);
        ALTER TABLE {1}.plant_plantmca ADD PRIMARY KEY (id);
        ALTER TABLE {1}.project_projectmca ADD PRIMARY KEY (id);
        ALTER TABLE {1}.project_projectoutputoutcome ADD PRIMARY KEY (id);

        """

        set_foregin_key = """
        ALTER TABLE {1}.plant_plantmca ADD FOREIGN KEY (plant_id) REFERENCES {1}.plant_plant(id);
        ALTER TABLE {1}.project_projectmca ADD FOREIGN KEY (project_id) REFERENCES {1}.project_project(id);
        ALTER TABLE {1}.project_projectindicatorlocation ADD FOREIGN KEY (indicator_id) REFERENCES {1}.project_projectoutputoutcome(id);
        """
        drow_column = """
        ALTER TABLE {1}.project_projectoutputoutcome DROP "location";
        """
        for client in clients:
            querys = [set_primary_key, set_foregin_key, drow_column]
            try:
                cl_helper = ClientTableHelper(client)
                for query in querys:
                    print("query===", query)
                    query = query.format(cl_helper.client.id, cl_helper.schema_name)
                    cursor = connection.cursor()
                    cursor.execute(query)
                    cursor.close()
            except Exception as err:
                print("error=" + str(err) + str(cl_helper.schema_name))
                continue
        return Response({"status": "successful"}, status=status.HTTP_200_OK)
