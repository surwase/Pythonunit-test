# Generated by Django 2.0.5 on 2020-03-05 13:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0081_auto_20200220_1849'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='projectoutputoutcome',
            unique_together=set(),
        ),
    ]
