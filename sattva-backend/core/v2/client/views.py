import datetime
import random
import urllib.request
from io import BytesIO
import zipfile
from django.utils.translation import activate

import django_filters
from dateutil.relativedelta import relativedelta
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import JsonResponse
from django.http import HttpResponse
from django_filters import rest_framework as filters
from rest_framework import status, viewsets
from rest_framework.filters import SearchFilter
from django.db.models import Q
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import pagination
from rest_framework_bulk import BulkModelViewSet

from core.keycloak.helper import KeyclockHelper
from core.users.models import User, UserActivityLog
from core.utils import check_null_and_replace_to_none
from domainlibraries.models import ApplicationRole
from domainlibraries.views import v2_send_welcome_email_to_user
from v2.authentication import KeycloakUserUpdateAuthentication
from v2.client.models import (
    Client,
    ClientComplianceCheckList,
    ClientComplianceEvaluation,
    ClientDashboard,
    ClientDetail,
    ClientDocument,
    ClientImage,
    ClientOverview,
    ClientUser,
    CustomFolder,
    ClientSettings,
    ClientDatabaseDetail,
)
from v2.client.permissions import (
    ClientPermission,
    CompliancePermission,
    ComplianceChecklistPermission,
    ClientCreatePermission,
    ClientFilesPermission,
    ConsultantPermission,
    ClientEntityPermission,
    ClientFolderPermission,
    ClientOverviewPermission,
    ClientInsightsPermission,
    SuperAdminPermission,
)
from v2.client.tasks import sync_data_for_client_tables
from v2.client.serializers import (
    ClientComplianceCheckListSerializer,
    ClientComplianceEvaluationReadSerializer,
    ClientComplianceEvaluationSerializer,
    ClientDashboardSerializer,
    ClientDetailReadSerializer,
    ClientDetailSerializer,
    ClientFilesCommonDisbursementSerializer,
    ClientImageSerializer,
    ClientImagesCommonDisbursementSerializer,
    ClientListReadSerializer,
    ClientOptionReadSerializer,
    ClientOverviewSerializer,
    ClientPlantListSerializer,
    ClientPlantProjectListSerializer,
    ClientProjectListSerializer,
    ClientReadSerializer,
    ClientUserReadSerializer,
    ClientUserWriteSerializer,
    ClientWriteSerializer,
    MiniClientUserReadSerializer,
    ClientMicroListReadSerializer,
    CustomFolderSerializer,
    ClientSettingsSerializer,
    ClientMiniListReadSerializer,
    ClientRiskStatusSerializer,
    ClientProjectStatusSerializer,
    ClientMicroSerializer,
    ClientDatabaseDetailSerializer,
)
from v2.data_scripts.client_tables import ClientTableHelper
from v2.ngo.models import NGOPartner
from v2.plant.models import Plant, PlantUser
from v2.plant.serializers import PlantImagesSerializer, PlantListSerializer
from v2.project.models import Project, ProjectUser, Task, TaskDocument
from v2.project.serializers import (
    FilesCommonDisbursementSerializer,
    FilesCommonProjectImageSerializer,
    FilesCommonProjectSerializer,
    ProjectListSerializer,
)
from v2.kobo.kobo_helper import create_user_in_kobo
from v2.user_management.models import (
    ClientFeaturePermission as ClientFeaturePermissionModel,
    ProjectFeaturePermission,
)
from v2.user_management.permissions import (
    ClientFeaturePermission,
    InviteUserPermission,
    DeleteUserPermission,
    EditUserPermission,
)
from v2.user_management.serializers import (
    ClientFeaturePermissionMicroSerializer,
    ProjectFeaturePermissionMicroSerializer,
)


class ClientFilter(django_filters.FilterSet):
    """
    Filter for client model where we filter client by exact id or by list of ids
    """

    class Meta:
        model = Client
        fields = {"id": ["exact", "in"]}


class ClientModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for client model, allows CRUD operations.
    """

    serializer_class = ClientReadSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientCreatePermission, ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This is the function to get the serializer class
        If the method is GET it will return ClientListReadSerializer
        If the method is Post it will return ClientWriteSerializer
        else it will return ClientReadSerializer
        """
        method = self.request.method
        if method == "GET" and self.action == "list":
            return ClientListReadSerializer
        if method == "PUT" or method == "POST" or method == "PATCH":
            return ClientWriteSerializer
        else:
            return ClientReadSerializer

    def get_queryset(self):
        """
        This is the function to get the queryset
        the query set will fetch all the users. If current user's application role is not sattva-admin it will fetch users of the current client
        """
        queryset = Client.objects.all().order_by("client_name")
        if (
            self.request.user.application_role is not None
            and self.request.user.application_role.role_name != "sattva-admin"
        ):
            user_clients = ClientUser.objects.filter(
                user=self.request.user
            ).values_list("client__id", flat=True)
            queryset = queryset.filter(id__in=user_clients)
        return queryset

    def create(self, request, *args, **kwargs):
        """
        This is the function to create client schema and views
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        client = Client.objects.get(id=serializer.data.get("id"))
        if (
            request.user.application_role is not None
            and request.user.application_role.role_name == "sattva-user"
        ):
            client_user = ClientUser.objects.create(user=request.user, client=client)
            client_permissions = ClientFeaturePermissionModel.objects.filter(
                client=serializer.instance
            )
            request.user.client_feature_permission.add(*list(client_permissions))
        cl_helper = ClientTableHelper(client)
        cl_helper.create_tables()
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )


class ClientMiniModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Mini Model allows write operations
    """

    serializer_class = ClientWriteSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientCreatePermission, ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This is the function to get the queryset
         the query set will fetch all the users. If current user's application role is not sattva-admin it will fetch users of the current client
        """
        queryset = Client.objects.all().order_by("client_name")
        if (
            self.request.user.application_role is not None
            and self.request.user.application_role.role_name != "sattva-admin"
        ):
            user_clients = ClientUser.objects.filter(
                user=self.request.user
            ).values_list("client__id", flat=True)
            queryset = queryset.filter(id__in=user_clients)
        return queryset


class ClientMicroModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Micro Model allows read operations
    """

    serializer_class = ClientMiniListReadSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientCreatePermission, ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This is the function to get the queryset
        the query set will fetch all the users of the current client
        """
        queryset = Client.objects.all().order_by("client_name")
        if (
            self.request.user.application_role is not None
            and self.request.user.application_role.role_name != "sattva-admin"
        ):
            user_clients = ClientUser.objects.filter(
                user=self.request.user
            ).values_list("client__id", flat=True)
            queryset = queryset.filter(id__in=user_clients)
        return queryset


class ClientModelDetailViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Detail allows read operation
    """

    serializer_class = ClientDetailReadSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientModelRiskStatusViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Risk Status allows CRUD operations
    """

    serializer_class = ClientRiskStatusSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientModelProjectStatusViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Project Status allows CRUD operations
    """

    serializer_class = ClientProjectStatusSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientModelProjectViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Project allows read operation
    """

    serializer_class = ClientProjectListSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientModelPlantViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Plant allows read operation
    """

    serializer_class = ClientPlantListSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientModelPlantAndProjectViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Plant and Project allows read operation
    """

    serializer_class = ClientPlantProjectListSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientImageFilter(django_filters.FilterSet):
    """
    Filter for Client Images where we filter clientimage by exact clientid.
    """

    class Meta:
        model = ClientImage
        fields = {"client": ["exact"]}


class ClientImageModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client Image allows CRUD operations
    """

    serializer_class = ClientImageSerializer
    queryset = ClientImage.objects.all().order_by("client")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientImageFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientOptionViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client option allows read operation.
    """

    serializer_class = ClientOptionReadSerializer
    queryset = Client.objects.all().order_by("client_name")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFilter
    permission_classes = [ClientFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientUserFilter(django_filters.FilterSet):
    """
    Filter for Client User where we filter clientuser by exact clientid.
    """

    class Meta:
        model = ClientUser
        fields = {"client": ["exact"]}


class ClientUserPagination(pagination.PageNumberPagination):
    """
    Pagination class for client user default page size is 5 and maximum page size is 100
    """

    page_size = 5
    max_page_size = 100
    page_size_query_param = "records"

    def get_paginated_response(self, data):
        """
        This is the function to get the paginated response
        by default page size will be 5 and max page size will be 100
        """
        return Response(
            {
                "count": self.page.paginator.count,
                "current": self.page.number,
                "next": self.get_next_link(),
                "previous": self.get_previous_link(),
                "results": data,
            }
        )


class ClientUserModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client User allows write operations
    """

    serializer_class = ClientUserWriteSerializer
    pagination_class = ClientUserPagination
    queryset = ClientUser.objects.all().order_by("client")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientUserFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def create_permission_for_user(client_user):
        """
        This is the function to create the permission for client user
        """
        user = client_user.user
        client = client_user.client
        client_permissions = ClientFeaturePermissionModel.objects.filter(client=client)
        user.client_feature_permission.add(*list(client_permissions))
        user.save()

    def create(self, request, *args, **kwargs):
        """
        This is the function to create the client user
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.create_permission_for_user(serializer.instance)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def get_serializer_class(self):
        """
        This is the function to return the serializer class
        if the method is put or post or patch it will return ClientUserWriteSerializer else it will return ClientUserReadSerializer
        """
        method = self.request.method
        if method == "PUT" or method == "POST" or method == "PATCH":
            return ClientUserWriteSerializer
        else:
            return ClientUserReadSerializer

    def get_queryset(self):
        """
        This is the function to return the queryset
        queryset will fetch the client user by search_results and filter_list
        """
        queryset = ClientUser.objects.all().order_by("client")
        queryset = self.search_results(queryset)
        queryset = self.filter_list(queryset)

        return queryset

    def filter_list(self, queryset):
        """
        This is the function to return the queryset
        queryset will fetch the filter list for plant,project,role,client.
        """
        project = check_null_and_replace_to_none(
            self.request.query_params.get("project")
        )
        plant = check_null_and_replace_to_none(self.request.query_params.get("plant"))
        role = check_null_and_replace_to_none(self.request.query_params.get("role"))
        client = check_null_and_replace_to_none(self.request.query_params.get("client"))

        if project is not None:
            project = project.split(",")
            projectUser = ProjectUser.objects.filter(project__id__in=project)
            projectUser = projectUser.filter(project__client__id=client)
            projectUser = projectUser.values_list("user", flat=True)
            queryset = queryset.filter(user__in=projectUser)

        if plant is not None:
            plant = plant.split(",")
            plantUser = PlantUser.objects.filter(plant__id__in=plant)
            plantUser = plantUser.filter(plant__client__id=client)
            plantUser = plantUser.values_list("user", flat=True)
            queryset = queryset.filter(user__in=plantUser)

        if role is not None:
            queryset = queryset.filter(user__application_role__role_name=role)
        return queryset

    def search_results(self, queryset):
        """
        This is the function to return the queryset
        queryset will fetch the search result for projectUser,plantUser.
        """
        search = check_null_and_replace_to_none(self.request.query_params.get("search"))
        client = check_null_and_replace_to_none(self.request.query_params.get("client"))

        if search is not None:

            projectUser = ProjectUser.objects.filter(project__client__id=client)
            projectUser = projectUser.filter(project__project_name__icontains=search)
            projectUser = projectUser.values_list("user", flat=True)

            plantUser = PlantUser.objects.filter(plant__client__id=client)
            plantUser = plantUser.filter(plant__plant_name__icontains=search)
            plantUser = plantUser.values_list("user", flat=True)

            application_roles = [
                "partner",
                "client admin",
                "collector",
                "consultant",
                "client user",
            ]
            application_roles_maps = {
                "partner": "ngo-user",
                "client admin": "csr-admin",
                "collector": "data-collector",
                "client user": "csr-user",
                "consultant": "sattva-user",
            }
            searched_roles = []
            for role in application_roles:
                if role.find(search.lower()) != -1:
                    searched_roles.append(application_roles_maps[role])

            queryset = queryset.filter(
                Q(user__in=projectUser)
                | Q(user__in=plantUser)
                | Q(user__email__icontains=search)
                | Q(user__application_role__role_name__in=searched_roles)
                | Q(user__name__icontains=search)
            )

        return queryset

    def destroy(self, request, *args, **kwargs):
        """
        This is the function to destroy the instance
        """
        instance = self.get_object()
        client = instance.client
        user = instance.user
        self.perform_destroy(instance)
        plant_users = PlantUser.objects.filter(plant__client=client, user=user)
        plant_users.delete()
        project_users = ProjectUser.objects.filter(project__client=client, user=user)
        project_users.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class MiniClientUserModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for mini Client User allows read operation
    """

    serializer_class = MiniClientUserReadSerializer
    queryset = ClientUser.objects.all().order_by("client")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientUserFilter
    permission_classes = [ClientPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientOverviewModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client overview allows CRUD operations
    """

    serializer_class = ClientOverviewSerializer
    queryset = ClientOverview.objects.all().order_by("client")
    lookup_field = "client"
    permission_classes = [ClientCreatePermission, ClientOverviewPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientDetailModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client detail allows CRUD operations
    """

    serializer_class = ClientDetailSerializer
    queryset = ClientDetail.objects.all().order_by("client")
    lookup_field = "client"
    permission_classes = [ClientCreatePermission, ClientOverviewPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientSettingsModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for Client settings allows CRUD operations
    """

    serializer_class = ClientSettingsSerializer
    queryset = ClientSettings.objects.all().order_by("client")
    lookup_field = "client"
    permission_classes = [ClientEntityPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class ClientComplianceCheckListFilter(django_filters.FilterSet):
    """
    Filter for Client Compliance CheckList where we filter ClientComplianceCheckList by exact clientid , area and indicator if contained and compulsion by exact id.
    """

    class Meta:
        model = ClientComplianceCheckList
        fields = {
            "client": ["exact"],
            "area": ["icontains"],
            "indicator": ["icontains"],
            "compulsion": ["exact"],
        }


class ClientComplianceCheckListModelViewSet(BulkModelViewSet):
    """
    Viewset for Client Compliance CheckList
    """

    serializer_class = ClientComplianceCheckListSerializer
    queryset = ClientComplianceCheckList.objects.all().order_by("indicator")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientComplianceCheckListFilter
    permission_classes = [ComplianceChecklistPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]
    lookup_field = "client"

    def get_queryset(self):
        """
        This is the function to get the query set
        Queryset will fetch Client Compliance CheckList and order it by indicator and created date
        """
        queryset = ClientComplianceCheckList.objects.order_by(
            "indicator", "-created_date"
        ).distinct("indicator")
        return queryset

    def bulk_update(self, request, *args, **kwargs):
        """
        This is the function to fetch Client Compliance CheckList of particulars client and create the bulk upload
        """
        qs = ClientComplianceCheckList.objects.filter(
            client__id=request.GET.get("client")
        )
        filtered = self.filter_queryset(qs)
        for obj in filtered:
            self.perform_destroy(obj)
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_bulk_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ClientComplianceEvaluationFilter(django_filters.FilterSet):
    """
    filter for Client Compliance Evaluation which filters the ClientComplianceEvaluation by exact clientid and exact date captured.
    """

    class Meta:
        model = ClientComplianceEvaluation
        fields = {
            "client": ["exact"],
            "date_captured": ["exact"],
        }


class ClientComplianceEvaluationModelViewSet(BulkModelViewSet):
    """
    Viewset for Client Compliance Evaluation
    """

    queryset = ClientComplianceEvaluation.objects.all()
    serializer_class = ClientComplianceEvaluationSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientComplianceEvaluationFilter
    permission_classes = [ComplianceChecklistPermission, CompliancePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This is the function to get serializer class
        If method is PUT or POST or PATCH then it will return ClientComplianceEvaluationSerializer
        else ClientComplianceEvaluationReadSerializer
        """
        method = self.request.method
        if method == "PUT" or method == "POST" or method == "PATCH":
            return ClientComplianceEvaluationSerializer
        else:
            return ClientComplianceEvaluationReadSerializer


def create_permissions_for_user(user, clients, projects, application_role):
    """
    Function for creating permissions for user
    """
    for client in clients:
        user_permissions = []
        client_permissions = ClientFeaturePermissionModel.objects.filter(client=client)
        for permission in client_permissions:
            allowed_permissions = permission.feature.default_permissions.get(
                application_role, []
            )
            if permission.permission in allowed_permissions:
                user_permissions.append(permission)
        user.client_feature_permission.add(*list(user_permissions))
    for project in projects:
        user_permissions = []
        project_permissions = ProjectFeaturePermission.objects.filter(project=project)
        for permission in project_permissions:
            allowed_permissions = permission.feature.default_permissions.get(
                application_role, []
            )
            if permission.permission in allowed_permissions:
                user_permissions.append(permission)
        user.project_feature_permission.add(*list(user_permissions))
    user.save(activity="user_permissions_edited")


def create_password_for_user():
    """
    Function for creating password for user
    """
    small_case = "abcdefghijklmnopqrstuvwxyz"
    capital_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    digits = "01234567890"
    special_characters = "!@#$%^&*()?"
    small_case_length = 5
    capital_case_length = 1
    digits_length = 1
    special_characters_length = 1
    p = "".join(random.sample(small_case, small_case_length))
    p = p + "".join(random.sample(capital_case, capital_case_length))
    p = p + "".join(random.sample(digits, digits_length))
    p = p + "".join(random.sample(special_characters, special_characters_length))
    return p


class InviteUserAPIView(APIView):
    """
    API view for Invite User
    """

    permission_classes = [
        ClientFeaturePermission,
        InviteUserPermission,
        EditUserPermission,
    ]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def validate_data(email, application_role, first_name, last_name):
        """
        This is a function to validate the email, application_role, first_name, last_name
        """
        errors = []
        if not email:
            errors.append("Please enter a valid email!")
        if not application_role:
            errors.append("Please select a valid application role!")
        if not first_name:
            errors.append("Please enter a valid first name!")
        if not last_name:
            errors.append("Please enter a valid last name!")
        return errors
        # TODO: Validate all data here.

    @staticmethod
    def validate_users_count(clients, application_role):
        """
        This is a function to validate the users count of client user
        if  application_role is sattva-user it will return an error message or it will fetch every single client from clients
        list ,get the max count and role count if role count is greater than max count it will return an error message
        """
        choices = {
            "csr-admin": "csr_admin",
            "ngo-user": "ngo_user",
            "csr-user": "csr_user",
            "data-collector": "data_collector",
            "sattva-user": "sattva_user",
            "csr-watcher": "csr_watcher",
        }
        errors = []
        role_name = choices.get(application_role)
        if application_role == "sattva-user":
            return errors
        for client_item in clients:
            client = Client.objects.get(id=client_item)
            max_count = getattr(client, role_name)
            role_count = ClientUser.objects.filter(
                client=client, user__application_role__role_name=application_role
            ).count()
            if role_count >= max_count:
                errors.append(
                    "Max Limit of {} for {} has reached. Please contact the admin to know more about this.".format(
                        max_count, application_role
                    )
                )
        return errors

    def post(self, request):
        """
        This is a function to create a post request for invite user
        """
        email = request.data.get("email", "").lower()
        application_role = request.data.get("application_role")
        first_name = request.data.get("first_name")
        last_name = request.data.get("last_name")
        clients = request.data.get("clients", [])
        # TODO: allocate plants and projects to the user
        plants = request.data.get("plants", [])
        projects = request.data.get("projects", [])
        errors = self.validate_data(email, application_role, first_name, last_name)
        if len(errors):
            return JsonResponse(errors, status=400)
        errors = self.validate_users_count(clients, application_role)
        if len(errors):
            return JsonResponse(errors, status=400, safe=False)
        user = User.objects.filter(email=email)
        if user:
            errors.append("User already exists with the email id:{0}, Please check in User Management".format(email))
            return JsonResponse(errors, status=400, safe=False)
        if len(clients) > 1 and application_role != "sattva-user":
            errors.append("User cannot be added to more than one client!")
            return JsonResponse(errors, status=400, safe=False)
        keycloak_helper = KeyclockHelper()
        password = create_password_for_user()
        keycloak_user = keycloak_helper.create_user(
            {
                "email": email,
                "username": email,
                "enabled": True,
                "firstName": first_name,
                "lastName": last_name,
                "requiredActions": ["UPDATE_PASSWORD", "CONFIGURE_TOTP"],
                "attributes": {
                    "application_role": application_role,
                    "clients": clients,
                },
                "credentials": [
                    {
                        "value": password,
                        "type": "password",
                        "temporary": True,
                    }
                ],
            }
        )
        keycloak_helper.assign_role(email, application_role)
        if not user:
            user = User.objects.create_user(
                username=email,
                email=email,
                name=first_name + " " + last_name,
                first_name=first_name,
                last_name=last_name,
                password=password,
            )
            user.application_role = ApplicationRole.objects.get(
                role_name=application_role
            )
            user.save()
            # keycloak_helper.send_account_update(email)
            v2_send_welcome_email_to_user(email, password, first_name)
        else:
            user = user[0]
        for client in clients:
            # TODO: Do error handling here
            client_user = ClientUser.objects.create(
                client=Client.objects.get(id=client), user=user
            )
        for project in projects:
            project_user = ProjectUser.objects.create(
                project=Project.objects.get(id=project), user=user
            )
        for plant in plants:
            plant_user = PlantUser.objects.create(
                plant=Plant.objects.get(id=plant), user=user
            )
        create_permissions_for_user(
            user, clients, projects, user.application_role.role_name
        )
        if application_role == "ngo-user" and request.data.get("ngo"):
            ngo = request.data.get("ngo")
            ngo_partner = NGOPartner.objects.get(id=ngo)
            user.ngo = ngo_partner
            user.save()
        create_user_in_kobo(email, password)
        return Response("User has been sent an invitation!", status=201)

    def patch(self, request):
        """
        This is a function to make a patch request for invite user
        """
        new_clients = []
        new_plants = []
        new_projects = []
        email = request.data.get("email")
        application_role = request.data.get("application_role")
        first_name = request.data.get("first_name")
        last_name = request.data.get("last_name")
        try:
            user = User.objects.get(email=email)
        except ObjectDoesNotExist as ex:
            return Response("User does not exists", status=400)
        errors = self.validate_data(email, application_role, first_name, last_name)
        if len(errors):
            return JsonResponse(errors, status=400)
        user_clients = ClientUser.objects.filter(user=user).values_list(
            "client", flat=True
        )
        user_plants = PlantUser.objects.filter(user=user).values_list(
            "plant", flat=True
        )
        user_projects = ProjectUser.objects.filter(user=user).values_list(
            "project", flat=True
        )
        user.first_name = first_name
        user.last_name = last_name
        clients = request.data.get("clients", [])
        plants = request.data.get("plants", [])
        projects = request.data.get("projects", [])
        clients_removed = []
        plants_removed = []
        projects_removed = []
        for client in user_clients:
            if client not in clients:
                clients_removed.append(client)
                ClientUser.objects.get(client=client, user=user).delete()
        for plant in user_plants:
            if plant not in plants:
                plants_removed.append(plant)
                PlantUser.objects.get(plant=plant, user=user).delete()
        for project in user_projects:
            if project not in projects:
                projects_removed.append(project)
                ProjectUser.objects.get(project=project, user=user).delete()
        if (
            len(projects_removed) > 0
            or len(plants_removed) > 0
            or len(clients_removed) > 0
        ):
            projects_changed = Project.objects.filter(id__in=projects_removed)
            plants_changed = Plant.objects.filter(id__in=plants_removed)
            clients_changed = Client.objects.filter(id__in=clients_removed)
            UserActivityLog.objects.create(
                object_id=user.id,
                user=user,
                action="user_entities_removed",
                projects_changed=ProjectListSerializer(
                    projects_changed, many=True
                ).data,
                plants_changed=PlantListSerializer(plants_changed, many=True).data,
                clients_changed=ClientMicroSerializer(clients_changed, many=True).data,
            )
        for client in clients:
            if client not in user_clients:
                new_clients.append(client)
                client_user, created = ClientUser.objects.get_or_create(
                    client=Client.objects.get(id=client), user=user
                )
        for project in projects:
            if project not in user_projects:
                new_projects.append(project)
                project_user, created = ProjectUser.objects.get_or_create(
                    project=Project.objects.get(id=project), user=user
                )
        for plant in plants:
            if plant not in user_plants:
                new_plants.append(plant)
                plant_user, created = PlantUser.objects.get_or_create(
                    plant=Plant.objects.get(id=plant), user=user
                )
        if len(new_projects) > 0 or len(new_clients) > 0 or len(new_plants) > 0:
            projects_changed = Project.objects.filter(id__in=new_projects)
            plants_changed = Plant.objects.filter(id__in=new_plants)
            clients_changed = Client.objects.filter(id__in=new_clients)
            UserActivityLog.objects.create(
                user=user,
                action="user_entities_added",
                projects_changed=ProjectListSerializer(
                    projects_changed, many=True
                ).data,
                plants_changed=PlantListSerializer(plants_changed, many=True).data,
                clients_changed=ClientMicroSerializer(clients_changed, many=True).data,
            )
            create_permissions_for_user(
                user, new_clients, new_projects, user.application_role.role_name
            )
        if application_role == "ngo-user" and request.data.get("ngo"):
            ngo = request.data.get("ngo")
            ngo_partner = NGOPartner.objects.get(id=ngo)
            user.ngo = ngo_partner
        user.save()
        return Response("User has been updated Successfully!", status=200)


class DeleteUserAPIView(APIView):
    """
    API view for delete User
    """

    authentication_classes = [KeycloakUserUpdateAuthentication]
    permission_classes = [
        ClientFeaturePermission,
        InviteUserPermission,
        DeleteUserPermission,
    ]

    def delete(self, request, user_id):
        """
        This is a function to delete the user
        """
        try:
            user = User.objects.get(id=user_id)
            username = user.email
            # DELETE user from keycloak
            keycloak_helper = KeyclockHelper()
            keycloak_helper.delete_user(username)
            # DELETE User from user table
            user.delete()
        except ObjectDoesNotExist as ex:
            # Return object does not exists
            pass
        return Response("User has been removed successfully!", status=200)


class CheckUserEditPermissionAPIView(APIView):
    """
    API view for Checking User Edit Permission
    """

    permission_classes = [ClientFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request):
        """
        This is a function to make get request it will check if current users application role is sattva-admin, sattva-user
        it will return True.If current user is csr-admin it will get user by email and check if the users application role is
        sattva-admin, sattva-user it will return true else return false.
        """
        if request.user.application_role.role_name in ["sattva-admin", "sattva-user"]:
            return Response("True", status=200)
        if request.user.application_role.role_name == "csr-admin":
            user_email = request.GET.get("email")
            user = User.objects.get(email=user_email)
            if user.application_role.role_name not in ["sattva-admin", "sattva-user"]:
                return Response("True", status=200)
        return Response("False", status=403)


def get_project_document_object(project, doc_type, file_id):
    """
    This is a function to fetch the project document object
    It will check if the doc type is img,dis,mil,task,util,ind,output,outcome,case,rf it will return the corresponding document.
    """
    if doc_type == "IMG" and file_id is not None:
        from v2.project.models import ProjectImage

        try:
            project_image = ProjectImage.objects.get(id=file_id)
            return project_image
        except ObjectDoesNotExist as ex:
            return None
    elif doc_type == "DIS" and file_id is not None:
        from v2.project.models import DisbursementDocument

        try:
            document = DisbursementDocument.objects.get(id=file_id)
            return document
        except ObjectDoesNotExist:
            return document
    elif doc_type == "MIL" and file_id is not None:
        from v2.project.models import MilestoneDocument

        try:
            document = MilestoneDocument.objects.get(id=file_id)
            return document
        except ObjectDoesNotExist as ex:
            return None
    elif doc_type == "TASK" and file_id is not None:
        from v2.project.models import TaskDocument

        try:
            document = TaskDocument.objects.get(id=file_id)
            return document
        except ObjectDoesNotExist as ex:
            return None
    elif doc_type == "UTIL" and file_id is not None:
        from v2.project.models import UtilisationDocument

        try:
            util_doc = UtilisationDocument.objects.get(id=file_id)
            return util_doc
        except ObjectDoesNotExist as ex:
            return None
    elif doc_type == "IND" and file_id is not None:
        from v2.project.models import IndicatorDocument

        try:
            document = IndicatorDocument.objects.get(id=file_id)
            return document
        except ObjectDoesNotExist as ex:
            return None
    elif doc_type == "OUTPUT" and file_id is not None:
        from v2.project.models import ProjectOutputDocument

        try:
            document = ProjectOutputDocument.objects.get(id=file_id)
            return document
        except ObjectDoesNotExist as ex:
            return None
    elif doc_type == "OUTCOME" and file_id is not None:
        from v2.project.models import ProjectOutcomeDocument

        try:
            document = ProjectOutcomeDocument.objects.get(id=file_id)
            return document
        except ObjectDoesNotExist as ex:
            return None
    elif doc_type == "CASE" and file_id is not None:
        from v2.project.models import CaseStudyDocuments

        try:
            document = CaseStudyDocuments.objects.get(id=file_id)
            return document
        except ObjectDoesNotExist as ex:
            return None
    elif doc_type == "RF" and file_id is not None:
        from v2.project.models import ProjectDocument

        try:
            document = ProjectDocument.objects.get(id=file_id)
            return document
        except ObjectDoesNotExist as ex:
            return None
    else:
        return None


def type_id_split(s):
    head = s.rstrip("0123456789")
    tail = s[len(head) :]
    return head, int(tail)


def get_plant_project_document_type(file_pk):
    """
    This is a function to fetch the the plant project document type
    """
    try:
        project_id = int((file_pk.split("PR")[1]).split("_")[0])
        project = Project.objects.get(id=project_id) if project_id > 0 else None
    except:
        project = None
    try:
        plant_id = int((file_pk.split("PL")[1]).split("_")[0])
        plant = Plant.objects.get(id=plant_id) if plant_id > 0 else None
    except:
        plant = None
    try:
        doc_type, file_id = type_id_split(file_pk.split("_")[-1])
    except Exception as ex:
        print(ex)
        doc_type, file_id = None

    return project, plant, doc_type, file_id


class ClientFilesModelViewset(APIView):
    """
    API view for Client Files
    """

    authentication_classes = [KeycloakUserUpdateAuthentication]
    permission_classes = [ClientFilesPermission]
    client = None
    client_projects = None
    client_plants = None
    client_all_files = None
    folders_list = []
    client_all_image_files = None
    user_plants = []
    user_projects = []

    def get_plant_and_projects(self, request, pk):
        """
        This is a function to fetch the the plant and projects.
        """
        self.client = Client.objects.get(pk=pk)
        if request.user.application_role.role_name == "sattva-admin":
            client_projects = self.client.get_all_standalone_projects()
            client_plants = self.client.get_all_plants()
            client_all_files = self.client.get_all_client_files()
            client_all_image_files = self.client.get_all_client_images()
            user_projects = Project.objects.all().values_list("id", flat=True)
            user_plants = Plant.objects.all().values_list("id", flat=True)
            client_folders = CustomFolder.objects.filter(client=self.client)
            compliance_documents = ClientComplianceEvaluation.objects.filter(
                client=self.client
            )
            client_task_files = TaskDocument.objects.filter(
                task__in=Task.objects.filter(client=self.client, project=None)
            )
            return (
                client_projects,
                client_plants,
                client_all_files,
                client_folders,
                client_all_image_files,
                user_projects,
                user_plants,
                client_task_files,
                compliance_documents,
            )
        elif (
            ClientUser.objects.filter(user=request.user, client=self.client).count() > 0
        ):
            client_projects = self.client.get_all_standalone_projects()
            user_projects = ProjectUser.objects.filter(user=request.user).values_list(
                "project__id", flat=True
            )
            client_projects = client_projects.filter(id__in=user_projects)
            client_plants = self.client.get_all_plants()
            user_plants = PlantUser.objects.filter(user=request.user).values_list(
                "plant__id", flat=True
            )
            client_plants = client_plants.filter(id__in=user_plants)
            client_folders = CustomFolder.objects.filter(client=self.client)
            client_task_files = TaskDocument.objects.filter(
                task__in=Task.objects.filter(client=self.client, project=None)
            )
            client_all_files = self.client.get_all_client_files()
            client_all_image_files = self.client.get_all_client_images()
            if request.user.application_role.role_name == "ngo-user":
                compliance_documents = []
            else:
                compliance_documents = ClientComplianceEvaluation.objects.filter(
                    client=self.client
                )
            return (
                client_projects,
                client_plants,
                client_all_files,
                client_folders,
                client_all_image_files,
                user_projects,
                user_plants,
                client_task_files,
                compliance_documents,
            )
        else:
            raise PermissionError("No Permission Found")

    def get(self, request, pk=None, file_pk=None):
        """
        This is a function to make get request for plant and projects.
        """
        if file_pk:
            return self.get_document_link(pk, file_pk)
        try:
            (
                client_projects,
                client_plants,
                client_all_files,
                client_folders,
                client_all_image_files,
                user_projects,
                user_plants,
                client_task_files,
                compliance_documents,
            ) = self.get_plant_and_projects(request, pk)
            client_id = "C" + str(self.client.id) if self.client else "C0"
            client_file_list = []
            client_project_files = []
            self.folders_list = []
            self.folders_list.append(
                {
                    "name": "Images",
                    "isFolder": True,
                    "parent": "root",
                    "isDefault": True,
                    "id": client_id,
                }
            )
            self.folders_list.append(
                {
                    "name": "Compliance",
                    "isFolder": True,
                    "parent": "root",
                    "isDefault": True,
                    "id": "COMP" + str(client_id),
                }
            )
            self.folders_list.append(
                {
                    "name": "Tasks",
                    "isFolder": True,
                    "parent": "root",
                    "isDefault": True,
                    "id": "TASK" + str(client_id),
                }
            )
            for project_f in client_all_files:
                self.folders_list.append(
                    {
                        "name": project_f.document_file.name.split("/")[-1]
                        if project_f.document_file
                        else "",
                        "tag": project_f.document_tag,
                        "uploadBy": project_f.created_by,
                        "isFolder": False,
                        "parent": project_f.parent if project_f.parent else "root",
                        "id": client_id + "_PL0_PR0_RF" + str(project_f.id),
                        # "path": project_f.document_file.url if project_f.document_file else '',
                        "created_date": str(project_f.created_date),
                    }
                )
            for project_f in compliance_documents:
                if project_f.support_document:
                    self.folders_list.append(
                        {
                            "name": project_f.support_document.name.split("/")[-1]
                            if project_f.support_document
                            else "",
                            "tag": [],
                            "uploadBy": project_f.created_by,
                            "isFolder": False,
                            "parent": "COMP" + str(client_id),
                            "id": client_id + "_PL0_PR0_CM" + str(project_f.id),
                            # "path": project_f.support_document.url if project_f.support_document else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
            for folder in client_folders:
                self.folders_list.append(
                    {
                        "name": folder.folder_name,
                        "tag": folder.document_tag,
                        "uploadBy": folder.created_by,
                        "isFolder": True,
                        "isDefault": False,
                        "parent": folder.parent if folder.parent else "root",
                        "id": folder.parent + "_FL" + str(folder.id),
                        "created_date": str(folder.created_date),
                    }
                )
            for client_img in client_all_image_files:
                self.folders_list.append(
                    {
                        "name": client_img.image_file.name.split("/")[-1]
                        if client_img.image_file
                        else "",
                        "tag": client_img.document_tag,
                        "uploadBy": client_img.created_by,
                        "isFolder": False,
                        "parent": client_id,
                        "id": client_id + "_PL0_PR0_" + "IMG" + str(client_img.id),
                        # "path": client_img.image_file.url if client_img.image_file else '',
                        "created_date": str(client_img.created_date),
                    }
                )
            for task_file in client_task_files:
                self.folders_list.append(
                    {
                        "name": task_file.document_file.name.split("/")[-1]
                        if task_file.document_file
                        else "",
                        "isFolder": False,
                        "tag": task_file.document_tag,
                        "uploadBy": task_file.created_by,
                        "parent": "TASK" + str(client_id),
                        "id": client_id + "_PL0_PR0_" + "TASK" + str(task_file.id),
                        # "path": task_file.document_file.url if task_file.document_file else '',
                        "created_date": str(task_file.created_date),
                    }
                )
            for plant in client_plants:
                self.folders_list.append(
                    {
                        "name": plant.get_name(),
                        "isFolder": True,
                        "isDefault": True,
                        "parent": "root",
                        "id": client_id + "_PL" + str(plant.id),
                    }
                )
                self.folders_list.append(
                    {
                        "name": "images",
                        "isFolder": True,
                        "isDefault": True,
                        "parent": client_id + "_PL" + str(plant.id),
                        "id": client_id + "_PL" + str(plant.id) + "_PR0_IMG",
                    }
                )
                plant_project_file_list = []
                plant_all_files = plant.get_files()
                for project_f in plant_all_files:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "isFolder": False,
                            "parent": client_id + "_PL" + str(plant.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR0_RF"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                plant_images = plant.get_images()
                for plant_img in plant_images:
                    self.folders_list.append(
                        {
                            "name": plant_img.image_file.name.split("/")[-1]
                            if plant_img.image_file
                            else "",
                            "isFolder": False,
                            "tag": plant_img.document_tag,
                            "uploadBy": plant_img.created_by,
                            "parent": client_id + "_PL" + str(plant.id) + "_IMG",
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR0_IMG"
                            + str(plant_img.id),
                            # "path": plant_img.image_file.url if plant_img.image_file else '',
                            "created_date": str(plant_img.created_date),
                        }
                    )
                projects = plant.get_projects().filter(id__in=user_projects)
                for project in projects:
                    self.folders_list.append(
                        {
                            "name": project.get_name(),
                            "isFolder": True,
                            "isDefault": True,
                            "parent": client_id + "_PL" + str(plant.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id),
                        }
                    )
                    self.folders_list.append(
                        {
                            "name": "Images",
                            "isFolder": True,
                            "isDefault": True,
                            "parent": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id)
                            + "_IMG",
                        }
                    )
                    self.folders_list.append(
                        {
                            "name": "Tasks",
                            "isFolder": True,
                            "isDefault": True,
                            "parent": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id)
                            + "_TASK",
                        }
                    )
                    self.folders_list.append(
                        {
                            "name": "Disbursements",
                            "isFolder": True,
                            "isDefault": True,
                            "parent": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id)
                            + "_DIS",
                        }
                    )
                    self.folders_list.append(
                        {
                            "name": "Milestones",
                            "isFolder": True,
                            "isDefault": True,
                            "parent": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id)
                            + "_MIL",
                        }
                    )
                    self.folders_list.append(
                        {
                            "name": "Utilisations",
                            "isFolder": True,
                            "isDefault": True,
                            "parent": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id)
                            + "_UTIL",
                        }
                    )
                    self.folders_list.append(
                        {
                            "name": "Indicators",
                            "isFolder": True,
                            "isDefault": True,
                            "parent": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id)
                            + "_IND",
                        }
                    )
                    self.folders_list.append(
                        {
                            "name": "Case Studies",
                            "isFolder": True,
                            "isDefault": True,
                            "parent": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id),
                            "id": client_id
                            + "_PL"
                            + str(plant.id)
                            + "_PR"
                            + str(project.id)
                            + "_CASE",
                        }
                    )
                    file_list = []
                    project_file = project.get_all_project_file()
                    project_disbursement_file_list = (
                        project.get_all_disbursement_project_file()
                    )
                    project_milestone_file_list = project.get_all_milestones_file()
                    project_utilisation_file_list = project.get_all_utilisations_file()
                    project_utilisation_upload_file_list = (
                        project.get_all_utilisation_upload_file()
                    )
                    project_utilisation_expense_file_list = (
                        project.get_all_utilisation_expense_file()
                    )
                    project_tasks_file_list = project.get_all_tasks_file()
                    project_indicators_file_list = project.get_all_indicators_file()
                    project_outputs_file_list = project.get_all_output_file()
                    project_outcomes_file_list = project.get_all_outcome_file()
                    project_case_studies_file_list = project.get_all_casestudies_file()
                    sub_file_list = []
                    project_images = project.get_all_images()
                    for project_f in project_file:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id),
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_RF"
                                + str(project_f.id),
                                # "path": project_f.document_file.url if project_f.document_file else '',
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_img in project_images:
                        self.folders_list.append(
                            {
                                "name": project_img.image_file.name.split("/")[-1]
                                if project_img.image_file
                                else "",
                                "isFolder": False,
                                "tag": project_img.document_tag,
                                "uploadBy": project_img.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_IMG",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_IMG"
                                + str(project_img.id),
                                # "path": project_img.image_file.url if project_img.image_file else '',
                                "created_date": str(project_img.created_date),
                            }
                        )
                    for project_f in project_disbursement_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_DIS",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_DIS"
                                + str(project_f.id),
                                # "path": project_f.document_file.url if project_f.document_file else '',
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_milestone_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_MIL",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_MIL"
                                + str(project_f.id),
                                # "path": project_f.document_file.url if project_f.document_file else '',
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_utilisation_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_UTIL",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_UTIL"
                                + str(project_f.id),
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_utilisation_upload_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": project_f.parent
                                if project_f.parent
                                else client_id
                                + "_PL0"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_UTIL",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_UTIL"
                                + str(project_f.id),
                                # "path": project_f.document_file.url if project_f.document_file else '',
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_utilisation_expense_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": project_f.parent
                                if project_f.parent
                                else client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_UTIL",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_UTIL"
                                + str(project_f.id),
                                # "path": project_f.document_file.url if project_f.document_file else '',
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_tasks_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_TASK",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_TASK"
                                + str(project_f.id),
                                # "path": project_f.document_file.url if project_f.document_file else '',
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_indicators_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_IND",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_IND"
                                + str(project_f.id),
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_outputs_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_IND",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_IND"
                                + "_OUTPUT"
                                + str(project_f.id),
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_outcomes_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_IND",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_IND"
                                + "_OUTCOME"
                                + str(project_f.id),
                                "created_date": str(project_f.created_date),
                            }
                        )
                    for project_f in project_case_studies_file_list:
                        self.folders_list.append(
                            {
                                "name": project_f.document_file.name.split("/")[-1]
                                if project_f.document_file
                                else "",
                                "isFolder": False,
                                "tag": project_f.document_tag,
                                "uploadBy": project_f.created_by,
                                "parent": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_CASE",
                                "id": client_id
                                + "_PL"
                                + str(plant.id)
                                + "_PR"
                                + str(project.id)
                                + "_CASE"
                                + str(project_f.id),
                                "created_date": str(project_f.created_date),
                            }
                        )
                    sub_file_list.append(
                        [
                            {
                                "name": "Images",
                                "files": FilesCommonProjectImageSerializer(
                                    project_images, many=True
                                ).data,
                            }
                        ]
                    )
                    disbursement_docs = project.get_all_disbursements_documents()
                    sub_file_list.append(
                        [
                            {
                                "name": "Disbursement",
                                "files": FilesCommonDisbursementSerializer(
                                    disbursement_docs, many=True
                                ).data,
                            }
                        ]
                    )
                    file_list.append(
                        {
                            "files": FilesCommonProjectSerializer(
                                project_file, many=True
                            ).data,
                            "folders": sub_file_list,
                        }
                    )
                    plant_project_file_list.append(
                        {"name": project.get_name(), "folders": file_list}
                    )
                plant_project_file_list.append(
                    {
                        "name": "Images",
                        "files": PlantImagesSerializer(plant_images, many=True).data,
                    }
                )
                client_project_files.append(
                    {"name": plant.get_name(), "folders": plant_project_file_list}
                )
            for project in client_projects:
                self.folders_list.append(
                    {
                        "name": project.get_name(),
                        "isFolder": True,
                        "parent": "root",
                        "isDefault": True,
                        "id": client_id + "_PL0" + "_PR" + str(project.id),
                    }
                )
                self.folders_list.append(
                    {
                        "name": "Images",
                        "isFolder": True,
                        "isDefault": True,
                        "parent": client_id + "_PL0" + "_PR" + str(project.id),
                        "id": client_id + "_PL0" + "_PR" + str(project.id) + "_IMG",
                    }
                )
                self.folders_list.append(
                    {
                        "name": "Tasks",
                        "isFolder": True,
                        "isDefault": True,
                        "parent": client_id + "_PL0" + "_PR" + str(project.id),
                        "id": client_id + "_PL0" + "_PR" + str(project.id) + "_TASK",
                    }
                )
                self.folders_list.append(
                    {
                        "name": "Disbursements",
                        "isFolder": True,
                        "isDefault": True,
                        "parent": client_id + "_PL0" + "_PR" + str(project.id),
                        "id": client_id + "_PL0" + "_PR" + str(project.id) + "_DIS",
                    }
                )
                self.folders_list.append(
                    {
                        "name": "Utilisations",
                        "isFolder": True,
                        "isDefault": True,
                        "parent": client_id + "_PL0" + "_PR" + str(project.id),
                        "id": client_id + "_PL0" + "_PR" + str(project.id) + "_UTIL",
                    }
                )
                self.folders_list.append(
                    {
                        "name": "Milestones",
                        "isFolder": True,
                        "isDefault": True,
                        "parent": client_id + "_PL0" + "_PR" + str(project.id),
                        "id": client_id + "_PL0" + "_PR" + str(project.id) + "_MIL",
                    }
                )
                self.folders_list.append(
                    {
                        "name": "Indicators",
                        "isFolder": True,
                        "isDefault": True,
                        "parent": client_id + "_PL0" + "_PR" + str(project.id),
                        "id": client_id + "_PL0" + "_PR" + str(project.id) + "_IND",
                    }
                )
                self.folders_list.append(
                    {
                        "name": "Case Studies",
                        "isFolder": True,
                        "isDefault": True,
                        "parent": client_id + "_PL0" + "_PR" + str(project.id),
                        "id": client_id + "_PL0" + "_PR" + str(project.id) + "_CASE",
                    }
                )
                file_list = []
                project_file = project.get_all_project_file()
                sub_file_list = []
                project_images = project.get_all_images()
                project_disbursement_file_list = (
                    project.get_all_disbursement_project_file()
                )
                project_utilisation_file_list = project.get_all_utilisations_file()
                project_utilisation_upload_file_list = (
                    project.get_all_utilisation_upload_file()
                )
                project_utilisation_expense_file_list = (
                    project.get_all_utilisation_expense_file()
                )
                project_milestone_file_list = project.get_all_milestones_file()
                project_tasks_file_list = project.get_all_tasks_file()
                project_indicators_file_list = project.get_all_indicators_file()
                project_outputs_file_list = project.get_all_output_file()
                project_outcomes_file_list = project.get_all_outcome_file()
                project_case_studies_file_list = project.get_all_casestudies_file()
                for project_f in project_file:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": None,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id),
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_RF"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_img in project_images:
                    self.folders_list.append(
                        {
                            "name": project_img.image_file.name.split("/")[-1]
                            if project_img.image_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IMG",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IMG"
                            + str(project_img.id),
                            # "path": project_img.image_file.url if project_img.image_file else '',
                            "created_date": str(project_img.created_date),
                        }
                    )
                for project_f in project_disbursement_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_DIS",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_DIS"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_milestone_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_MIL",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_MIL"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_tasks_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_TASK",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_TASK"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_utilisation_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_UTIL",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_UTIL"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_utilisation_upload_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": project_f.parent
                            if project_f.parent
                            else client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_UTIL",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_UTIL"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_utilisation_expense_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": project_f.parent
                            if project_f.parent
                            else client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_UTIL",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_UTIL"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_indicators_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IND",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IND"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_outputs_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IND",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IND"
                            + "_OUTPUT"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_outcomes_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IND",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IND"
                            + "_OUTCOME"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                for project_f in project_case_studies_file_list:
                    self.folders_list.append(
                        {
                            "name": project_f.document_file.name.split("/")[-1]
                            if project_f.document_file
                            else "",
                            "isFolder": False,
                            "tag": project_f.document_tag,
                            "uploadBy": project_f.created_by,
                            "parent": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IND",
                            "id": client_id
                            + "_PL"
                            + str(0)
                            + "_PR"
                            + str(project.id)
                            + "_IND"
                            + str(project_f.id),
                            # "path": project_f.document_file.url if project_f.document_file else '',
                            "created_date": str(project_f.created_date),
                        }
                    )
                sub_file_list.append(
                    [
                        {
                            "name": "Images",
                            "files": FilesCommonProjectImageSerializer(
                                project_images, many=True
                            ).data,
                        }
                    ]
                )
                disbursement_docs = project.get_all_disbursements_documents()
                sub_file_list.append(
                    [
                        {
                            "name": "Disbursement",
                            "files": FilesCommonDisbursementSerializer(
                                disbursement_docs, many=True
                            ).data,
                        }
                    ]
                )
                file_list.append(
                    {
                        "files": FilesCommonProjectSerializer(
                            project_file, many=True
                        ).data,
                        "folders": sub_file_list,
                    }
                )
                client_project_files.append(
                    {"name": project.get_name(), "folders": file_list}
                )
            client_project_files.append(
                {
                    "name": "Images",
                    "files": ClientImagesCommonDisbursementSerializer(
                        client_all_image_files, many=True
                    ).data,
                }
            )
            client_file_list.append(
                {
                    "client_name": self.client.get_name(),
                    "files": ClientFilesCommonDisbursementSerializer(
                        client_all_files, many=True
                    ).data,
                    "folders": client_project_files,
                }
            )
            return Response(self.folders_list, status=status.HTTP_200_OK)
        except Exception as ex:
            error_message = {
                "Error": "Client ID not valid",
            }
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk=None):
        """
        This is the function to make post request.
        """
        try:
            upload_file = request.FILES["upload_file"]
            parent = request.data.get("parent_id", "root")
            tags = request.data.get("document_tag")
            if tags:
                tags = tags.split(",")
            if not pk is None:
                client = Client.objects.get(pk=pk)
                client_doc = ClientDocument(
                    client=client,
                    document_file=upload_file,
                    parent=parent,
                    document_tag=tags,
                )
                try:
                    client_doc.full_clean()
                except ValidationError as ex:
                    error_message = {"Error": dict(ex)}
                    return Response(error_message, status=status.HTTP_400_BAD_REQUEST)
                else:
                    client_doc.save()
                    data_message = {"Success": True}
                    return Response(data_message, status=status.HTTP_200_OK)
            else:
                data_message = {"Error": "Client ID not given"}
                return Response(data_message, status=status.HTTP_400_BAD_REQUEST)
        except:
            error_message = {"Error": "Client ID not valid"}
            return Response(error_message, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk=None, file_pk=None):
        """
        This is the function to make delete request for plant and projects
        """
        try:
            client = Client.objects.get(id=pk)
            project, plant, doc_type, file_id = get_plant_project_document_type(file_pk)
            data = None
            if project:
                document = get_project_document_object(project, doc_type, file_id)
                if document:
                    document.delete()
                else:
                    data = {"error": "Document not found"}
            elif plant is not None:
                if doc_type == "IMG" and file_id is not None:
                    from v2.plant.models import PlantImage

                    try:
                        project_image = PlantImage.objects.get(id=file_id)
                        project_image.delete()
                    except:
                        data = {"error": "Plant Image not found"}
                elif doc_type == "RF" and file_id is not None:
                    from v2.plant.models import PlantDocument

                    try:
                        project_image = PlantDocument.objects.get(id=file_id)
                        project_image.delete()
                    except:
                        data = {"error": "Plant Document not found"}
                else:
                    data = {"error": "Plant Doc Type/ ID not found"}
            else:
                if doc_type == "IMG" and file_id is not None:
                    from v2.client.models import ClientImage

                    try:
                        project_image = ClientImage.objects.get(id=file_id)
                        project_image.delete()
                    except:
                        data = {"error": "Client Image not found"}
                elif doc_type == "TASK" and file_id is not None:
                    from v2.project.models import TaskDocument

                    try:
                        project_image = TaskDocument.objects.get(id=file_id)
                        project_image.delete()
                    except:
                        data = {"error": "Client Task not found"}
                elif doc_type == "RF" and file_id is not None:
                    from v2.client.models import ClientDocument

                    try:
                        project_image = ClientDocument.objects.get(id=file_id)
                        project_image.delete()
                    except:
                        data = {"error": "Client Document not found"}
                else:
                    data = {"error": "Client Doc Type/ID not found"}
            if data is None:
                return Response(status=status.HTTP_204_NO_CONTENT)
            else:
                return Response(data, status=status.HTTP_400_BAD_REQUEST)
        except Exception as ex:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, pk=None, file_pk=None):
        """
        This is the function to make patch request for plant and projects
        """
        try:
            client = Client.objects.get(id=pk)
            new_file_name = request.data.get("file_name")
            tags = request.data.get("tags")
            project, plant, doc_type, file_id = get_plant_project_document_type(file_pk)
            data = None
            if project is not None:
                if doc_type == "IMG" and file_id is not None:
                    from v2.project.models import ProjectImage

                    try:
                        # update project image
                        project_image = ProjectImage.objects.get(id=file_id)
                        file = project_image.document_file.file
                        project_image.image_file.save(
                            "file_system/{0}/{1}/images/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except:
                        data = {"error": "Project Image not found"}
                elif doc_type == "DIS" and file_id is not None:
                    from v2.project.models import DisbursementDocument

                    try:
                        # update DisbursementDocument
                        project_image = DisbursementDocument.objects.get(id=file_id)
                        file = project_image.document_file.file
                        project_image.document_file.save(
                            "file_system/{0}/{1}/disbursements/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except:
                        data = {"error": "Project Disbursement Document not found"}
                elif doc_type == "MIL" and file_id is not None:
                    from v2.project.models import MilestoneDocument

                    try:
                        # update MilestoneDocument
                        project_image = MilestoneDocument.objects.get(id=file_id)
                        file = project_image.document_file.file
                        project_image.document_file.save(
                            "file_system/{0}/{1}/milestones/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except:
                        data = {"error": "Project Milestone Document not found"}
                elif doc_type == "TASK" and file_id is not None:
                    from v2.project.models import TaskDocument

                    try:
                        # update TaskDocument
                        project_image = TaskDocument.objects.get(id=file_id)
                        file = project_image.document_file.file
                        project_image.document_file.save(
                            "file_system/{0}/{1}/tasks/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except:
                        data = {"error": "Project Task not found"}
                elif doc_type == "UTIL" and file_id is not None:
                    from v2.project.models import UtilisationDocument

                    try:
                        # update UtilisationDocument
                        util_doc = UtilisationDocument.objects.get(id=file_id)
                        file = util_doc.document_file.file
                        util_doc.document_file.save(
                            "file_system/{0}/{1}/utilisations/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            util_doc.document_tag = tags
                        util_doc.save()
                    except:
                        data = {"error": "Project Utilisation not found"}

                elif doc_type == "IND" and file_id is not None:
                    from v2.project.models import IndicatorDocument

                    try:
                        # update IndicatorDocument
                        document = IndicatorDocument.objects.get(id=file_id)
                        file = document.document_file.file
                        if tags:
                            document.document_tag = tags
                        document.document_file.save(
                            "file_system/{0}/{1}/indicators/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                            save=True,
                        )
                    except:
                        data = {"error": "Project Indicator not found"}
                elif doc_type == "OUTPUT" and file_id is not None:
                    from v2.project.models import ProjectOutputDocument

                    try:
                        # update ProjectOutputDocument
                        document = ProjectOutputDocument.objects.get(id=file_id)
                        file = document.document_file.file
                        if tags:
                            document.document_tag = tags
                        document.document_file.save(
                            "file_system/{0}/{1}/output_indicators/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                            save=True,
                        )
                    except:
                        data = {"error": "Project Indicator not found"}
                elif doc_type == "OUTCOME" and file_id is not None:
                    from v2.project.models import ProjectOutcomeDocument

                    try:
                        # update ProjectOutcomeDocument
                        document = ProjectOutcomeDocument.objects.get(id=file_id)
                        file = document.document_file.file
                        if tags:
                            document.document_tag = tags
                        document.document_file.save(
                            "file_system/{0}/{1}/outcome_indicators/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                            save=True,
                        )
                    except:
                        data = {"error": "Project Indicator not found"}
                elif doc_type == "CASE" and file_id is not None:
                    from v2.project.models import CaseStudyDocuments

                    try:
                        # update CaseStudyDocuments
                        document = CaseStudyDocuments.objects.get(id=file_id)
                        file = document.document_file.file
                        if tags:
                            document.document_tag = tags
                        document.document_file.save(
                            "file_system/{0}/{1}/case_studies/{2}".format(
                                client.client_name, project.project_name, new_file_name
                            ),
                            content=file,
                            save=True,
                        )
                    except:
                        data = {"error": "Project Case study not found"}
                elif doc_type == "RF" and file_id is not None:
                    from v2.project.models import ProjectDocument

                    try:
                        # update ProjectDocument
                        project_image = ProjectDocument.objects.get(id=file_id)
                        file = project_image.document_file.file
                        if plant:
                            project_image.document_file.save(
                                "file_system/{0}/{1}/{2}/{3}".format(
                                    client.client_name,
                                    project.project_name,
                                    plant.plant_name,
                                    new_file_name,
                                ),
                                content=file,
                            )
                        else:
                            project_image.document_file.save(
                                "file_system/{0}/{1}/{2}".format(
                                    client.client_name,
                                    project.project_name,
                                    new_file_name,
                                ),
                                content=file,
                            )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except Exception as ex:
                        data = {"error": "Project Document not found"}
                else:
                    data = {"error": "Project Doc Type/ ID not found"}

            elif plant is not None:
                if doc_type == "IMG" and file_id is not None:
                    from v2.plant.models import PlantImage

                    try:
                        # update PlantImage
                        project_image = PlantImage.objects.get(id=file_id)
                        file = project_image.image_file.file
                        project_image.image_file.save(
                            "file_system/{0}/{1}/images/{2}".format(
                                client.client_name, plant.plant_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except:
                        data = {"error": "Plant Image not found"}
                elif doc_type == "RF" and file_id is not None:
                    from v2.plant.models import PlantDocument

                    try:
                        # update PlantDocument
                        project_image = PlantDocument.objects.get(id=file_id)
                        file = project_image.document_file.file
                        project_image.document_file.save(
                            "file_system/{0}/{1}/{2}".format(
                                client.client_name, plant.plant_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except:
                        data = {"error": "Plant Document not found"}
                else:
                    data = {"error": "Plant Doc Type/ ID not found"}
            else:
                if doc_type == "IMG" and file_id is not None:
                    from v2.client.models import ClientImage

                    try:
                        # update ClientImage
                        project_image = ClientImage.objects.get(id=file_id)
                        file = project_image.image_file.file
                        project_image.image_file.save(
                            "file_system/{0}/images/{1}".format(
                                client.client_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except Exception as ex:
                        data = {"error": "Client Image not found"}
                elif doc_type == "TASK" and file_id is not None:
                    from v2.project.models import TaskDocument

                    try:
                        # update TaskDocument
                        project_image = TaskDocument.objects.get(id=file_id)
                        project_image.delete()
                        file = project_image.document_file.file
                        project_image.document_file.save(
                            "file_system/{0}/tasks/{1}".format(
                                client.client_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except:
                        data = {"error": "Client Task not found"}
                elif doc_type == "RF" and file_id is not None:
                    from v2.client.models import ClientDocument

                    try:
                        # update ClientDocument
                        project_image = ClientDocument.objects.get(id=file_id)
                        file = project_image.document_file.file
                        project_image.document_file.save(
                            "file_system/{0}/{1}".format(
                                client.client_name, new_file_name
                            ),
                            content=file,
                        )
                        if tags:
                            project_image.document_tag = tags
                        project_image.save()
                    except:
                        data = {"error": "Client Document not found"}
                else:
                    data = {"error": "Client Doc Type/ID not found"}
            if data is None:
                return Response(status=status.HTTP_204_NO_CONTENT)
            else:
                return Response(data, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def get_document_link(self, pk=None, file_pk=None):
        """
        This is the function to fetch the image link
        """
        try:
            url = None
            client = Client.objects.get(id=pk)
            project, plant, doc_type, file_id = get_plant_project_document_type(file_pk)
            data = None
            if project:
                document = get_project_document_object(project, doc_type, file_id)
                if document:
                    url = document.document_file.url
                else:
                    data = {"error": "Document not found"}
            elif plant is not None:
                if doc_type == "IMG" and file_id is not None:
                    from v2.plant.models import PlantImage

                    try:
                        plant_image = PlantImage.objects.get(id=file_id)
                        url = plant_image.image_file.url
                    except:
                        data = {"error": "Plant Image not found"}
                elif doc_type == "RF" and file_id is not None:
                    from v2.plant.models import PlantDocument

                    try:
                        document = PlantDocument.objects.get(id=file_id)
                        url = document.document_file.url
                    except:
                        data = {"error": "Plant Document not found"}
                else:
                    data = {"error": "Plant Doc Type/ ID not found"}
            else:
                if doc_type == "IMG" and file_id is not None:
                    from v2.client.models import ClientImage

                    try:
                        project_image = ClientImage.objects.get(id=file_id)
                        url = project_image.image_file.url
                    except:
                        data = {"error": "Client Image not found"}
                elif doc_type == "TASK" and file_id is not None:
                    from v2.project.models import TaskDocument

                    try:
                        document = TaskDocument.objects.get(id=file_id)
                        url = document.document_file.url
                    except:
                        data = {"error": "Client Task not found"}
                elif doc_type == "RF" and file_id is not None:
                    from v2.client.models import ClientDocument

                    try:
                        document = ClientDocument.objects.get(id=file_id)
                        url = document.document_file.url
                    except:
                        data = {"error": "Client Document not found"}
                else:
                    data = {"error": "Client Doc Type/ID not found"}
            if url:
                return Response(status=status.HTTP_200_OK, data={"url": url})
            else:
                return Response(data, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)


class ClientDashboardFilter(django_filters.FilterSet):
    """
    Filter for ClientDashboard where we filter client by exact id
    """

    class Meta:
        model = ClientDashboard
        fields = {
            "client": ["exact"],
        }


class ClientDashboardModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for ClientDashboard allows CRUD operations
    """

    serializer_class = ClientDashboardSerializer
    queryset = ClientDashboard.objects.all().order_by("client")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientDashboardFilter
    permission_classes = [ClientInsightsPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class CustomFolderModelViewSet(viewsets.ModelViewSet):
    """
    Viewset for CustomFolder allows CRUD operations
    """

    serializer_class = CustomFolderSerializer
    queryset = CustomFolder.objects.all().order_by("client")
    permission_classes = [ClientFolderPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def destroy(self, request, client, folder_id):
        """
        function to destroy the instance
        """
        folder_id = folder_id.split("FL")[-1]
        instance = CustomFolder.objects.get(id=folder_id, client=client)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class UserUnAddedClientsAPIView(APIView):
    """
    Api views for User UnAdded Clients
    """

    permission_classes = [ConsultantPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        """
        function to make get request for user
        """
        user = request.GET.get("user_id")
        user_clients = ClientUser.objects.filter(user__id=user).values_list(
            "client__id", flat=True
        )
        clients = Client.objects.exclude(id__in=user_clients)
        serializer = ClientMicroListReadSerializer(clients, many=True)
        return Response(serializer.data)


class UserClientsAPIView(APIView):
    """
    Api views for User Clients
    """

    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        """
        function to make get request for user clients
        """
        user = request.GET.get("user_id")
        user = User.objects.get(id=user)
        if user.application_role.role_name == "sattva-admin":
            clients = Client.objects.all()
        else:
            user_clients = ClientUser.objects.filter(user=user).values_list(
                "client__id", flat=True
            )
            clients = Client.objects.filter(id__in=user_clients)
        serializer = ClientMicroListReadSerializer(clients, many=True)
        return Response(serializer.data)


class RemoveClientUserAPIView(APIView):
    """
    Api views for Remove Client User
    """

    # TODO: NOT USED, SHOULD BE REMOVED?
    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def post(request):
        """
        function to make post request to remove the client user
        """
        user = request.data.get("user_id")
        client = request.data.get("client_id")
        client_user = ClientUser.objects.get(user__id=user, client__id=client)
        client_user.delete()
        return Response("success")


class ClientComplianceScheduleAPIView(APIView):
    """
    Api views for Client Compliance Schedule
    """

    permission_classes = [CompliancePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]
    FREQUENCY_MONTH_MAP = {
        "Monthly": 1,
        "Quarterly": 3,
        "Half-yearly": 6,
        "Annually": 12,
    }

    def get_next_compliance_date(self, last_compliance_date, frequency):
        """
        function to calculate next compliance date
        """
        months = self.FREQUENCY_MONTH_MAP[frequency]
        next_compliance_date = last_compliance_date + relativedelta(months=+months)
        return next_compliance_date

    def get(self, request, client_id):
        """
        function to fetch next compliance date,last_compliance_date
        """
        client = Client.objects.get(id=client_id)
        last_compliance_date = client.get_last_compliance_date()
        frequency = client.compliance_frequency
        if last_compliance_date:
            next_compliance_date = self.get_next_compliance_date(
                last_compliance_date, frequency
            )
        elif client.evaluation_start_date:
            last_compliance_date = client.evaluation_start_date
            next_compliance_date = self.get_next_compliance_date(
                last_compliance_date, frequency
            )
        else:
            next_compliance_date = None
        return Response(
            {
                "compliance_frequency": frequency,
                "next_compliance_date": next_compliance_date,
            }
        )

    def post(self, request, client_id):
        """
        function to make post request for Client Compliance Schedule
        """
        client = Client.objects.get(id=client_id)
        frequency = request.data.get("compliance_frequency")
        client.compliance_frequency = frequency
        if not client.evaluation_start_date:
            client.evaluation_start_date = datetime.datetime.now()
        client.save()
        last_compliance_date = client.get_last_compliance_date()
        if not last_compliance_date:
            last_compliance_date = client.created_date
        next_compliance_date = self.get_next_compliance_date(
            last_compliance_date, frequency
        )
        return Response(
            {
                "compliance_frequency": frequency,
                "next_compliance_date": next_compliance_date,
            }
        )


class RenameFolderAPIView(APIView):
    """
    Api views for renaming a folder
    """

    permission_classes = [ClientFolderPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]
    client = None

    def post(self, request, folder_id):
        """
        function to make post request for RenameFolder
        """
        folder_id = folder_id.split("FL")[-1]
        new_folder_name = request.data.get("folder_name")
        folder = CustomFolder.objects.get(id=folder_id)
        folder.folder_name = new_folder_name
        folder.save()
        return Response("Folder Name Changed Successfully.")


class CheckClientNameAPIView(APIView):
    """
    Api views for CheckClientName
    """

    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        """
        function to make get request for client name
        """
        client_name = request.GET.get("client_name")
        client_exists = Client.objects.filter(client_name=client_name).exists()
        response_data = {"client_name_exists": client_exists}
        return Response(response_data)


class CheckComplianceListAPIView(APIView):
    """
    Api views for Check Compliance List
    """

    permission_classes = []
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        """
        function to make get request to check if  compliance_checklist_exists
        """
        client_id = request.GET.get("client_id")
        compliance_checklist_exists = ClientComplianceCheckList.objects.filter(
            client=client_id
        ).exists()
        response_data = {"compliance_checklist_exists": compliance_checklist_exists}
        return Response(response_data)


class ClientDatabaseDetailFilter(django_filters.FilterSet):
    """
    filter for ClientDatabaseDetail which will filter ClientDatabaseDetail by exact clientid
    """

    class Meta:
        model = ClientDatabaseDetail
        fields = {"client": ["exact"]}


class ClientDatabaseDetailModelViewset(viewsets.ModelViewSet):
    """
    Viewset for Client database details allows CRUD operations
    """

    serializer_class = ClientDatabaseDetailSerializer
    queryset = ClientDatabaseDetail.objects.all().order_by("client")
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientDatabaseDetailFilter
    permission_classes = [SuperAdminPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


class SyncClientTablesAPIView(APIView):
    """
    Api views for CheckClientName
    """

    permission_classes = [SuperAdminPermission, ConsultantPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request):
        """
        function to make get request for client name
        """
        client_id = request.GET.get("client_id")
        client = Client.objects.get(id=client_id)
        sync_data_for_client_tables.apply_async(kwargs={"client_id": client.id})
        return Response("Sync Triggered!", status=status.HTTP_200_OK)


class ClientImageDownload(APIView):

    @staticmethod
    def get(request):
        try:
            client_id = request.GET.get("client")

            image_objects = ClientImage.objects.filter(client=client_id)
            url_list = image_objects
            f = BytesIO()
            # zip = zipfile.ZipFile(f, 'w')
            with zipfile.ZipFile(f, mode='w') as zf:
                for image in url_list:
                    url_path = image.image_file.url
                    filename = url_path.split('/')[-1].split('.png')[0]
                    url = urllib.request.urlopen(url_path)
                    zf.writestr(filename+'.jpge', url.read())
            # zip.close()
            response = HttpResponse(f.getvalue(), content_type="application/zip")
            response['Content-Disposition'] = 'attachment; filename=plant4bar.zip'
            return response
        except Exception as err:
             return Response({'error': 'Failed to Download file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)

