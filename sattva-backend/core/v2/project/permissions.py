from rest_framework.permissions import BasePermission

from v2.client.permissions import BaseClientPermission, check_if_user_has_client_feature_permission, \
    check_if_user_has_permission_for_client_project_or_plant
from v2.plant.models import Plant
from v2.project.models import Project, Milestone, Task, Disbursement, Utilisation, UtilisationUpload, UtilisationExpense, Template
from v2.user_management.models import ProjectFeaturePermission


def check_if_user_has_project_feature_permission(feature, permission, project, user):
    project_feature_permission = ProjectFeaturePermission.objects.get(feature__feature_code=feature,
                                                                      permission=permission,
                                                                      project=project)
    if user.project_feature_permission.filter(id=project_feature_permission.id).exists():
        return True
    return False

"""
   A base class from which all permission classes should inherit.
"""
class BaseProjectPermission(BasePermission):
    feature = ''

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_project_feature_permission(self.feature, 'Change', obj, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_project_feature_permission(self.feature, 'Delete', obj, request.user)
        if request.method == 'GET':
            return check_if_user_has_project_feature_permission(self.feature, 'View', obj, request.user)
        return False


"""
    permission for project
"""
class ProjectPermission(BaseClientPermission):
    feature = 'PROGRAM_OVERVIEW'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        client = request.GET.get('client')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and client:
            return True
        elif view.action == 'list':
            return False
        elif request.method == 'POST':
            client = request.data.get('client')
            return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT', 'POST'):
            return check_if_user_has_project_feature_permission('PROJECT_OVERVIEW', 'Change', obj, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_project_feature_permission('PROJECT_OVERVIEW', 'Delete', obj, request.user)
        if request.method == 'GET':
            return check_if_user_has_project_feature_permission('PROJECT_OVERVIEW', 'View', obj, request.user)
        return False

"""
    permission for project document
"""
class ProjectDocumentPermission(BasePermission):
    feature = 'PROJECT_OVERVIEW'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        project = request.GET.get('project')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and project:
            return True
        elif view.action == 'list':
            return False
        elif request.method == 'POST':
            project = request.data.get('project')
            client = Project.objects.get(id=project).client
            return check_if_user_has_project_feature_permission(self.feature, 'Change', project, request.user)
        return True

"""
    permission for project tag
"""
class ProjectTagPermission(BasePermission):
    feature = 'CLIENT_OVERVIEW'
    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        client = request.GET.get('project__client')
        plant = request.GET.get('project__plant')
        if request.user.is_anonymous:
            return False
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and plant:
            client = Plant.objects.get(id=plant).client
            return check_if_user_has_client_feature_permission(self.feature, 'View', client, request.user)
        elif view.action == 'list' and client:
            return check_if_user_has_permission_for_client_project_or_plant(client, request.user)
        elif view.action == 'list':
            return False
        return False

"""
    permission for project settings
"""
class ProjectSettingsPermission(BasePermission):
    feature = 'PROJECT_OVERVIEW'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        project = request.GET.get('project')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.user.application_role.role_name == 'ngo-user':
            if request.method in ('POST', 'PATCH'):
                return False
            else:
                return True
        elif view.action == 'list' and project:
            return True
        elif view.action == 'list':
            return False
        elif request.method == 'POST':
            project = request.data.get('project')
            return check_if_user_has_project_feature_permission(self.feature, 'Change', project, request.user)
        return True

"""
    permission for project plan
"""
class ProjectPlanPermission(BaseProjectPermission):
    feature = 'PROJECT_PLAN'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        project = request.GET.get('project')
        client = request.GET.get('client')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and (project or client):
            return True
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.method == 'GET':
            return True
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            if obj.project is not None:
                return check_if_user_has_project_feature_permission(self.feature, 'Change', obj.project, request.user)
            elif obj.client is not None:
                return True
        if request.method == 'DELETE':
            if obj.project is not None:
                return check_if_user_has_project_feature_permission(self.feature, 'Delete', obj.project, request.user)
            elif obj.client is not None:
                return True
        return False

"""
    permission for Milestone Entity
"""
class MilestoneEntityPermission(BaseProjectPermission):
    feature = 'PROJECT_PLAN'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        milestone = request.GET.get('milestone')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and milestone:
            project = Milestone.objects.get(id=milestone).project
            return check_if_user_has_project_feature_permission(self.feature, 'View', project, request.user)
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_project_feature_permission(self.feature, 'Change', obj.milestone.project,
                                                                request.user)
        if request.method == 'DELETE':
            return check_if_user_has_project_feature_permission(self.feature, 'Delete', obj.milestone.project,
                                                                request.user)
        if request.method == 'GET':
            return check_if_user_has_project_feature_permission(self.feature, 'View', obj.milestone.project,
                                                                request.user)
        return False

"""
    permission for Task Entity
"""
class TaskEntityPermission(BaseProjectPermission):
    feature = 'PROJECT_PLAN'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        task = request.GET.get('task')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and task:
            project = Task.objects.get(id=task).project
            if project is not None:
                return check_if_user_has_project_feature_permission(self.feature, 'View', project, request.user)
            else:
                client = Task.objects.get(id=task).client
                if client:
                    return True
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            if obj.task.project is not None:
                return check_if_user_has_project_feature_permission(self.feature, 'Change', obj.task.project,
                                                                request.user)
            elif obj.task.client is not None:
                return True
        if request.method == 'DELETE':
            if obj.task.project is not None:
                return check_if_user_has_project_feature_permission(self.feature, 'Delete', obj.task.project,
                                                                request.user)
            elif obj.task.client is not None:
                return True
        if request.method == 'GET':
            if obj.task.project is not None:
                return check_if_user_has_project_feature_permission(self.feature, 'View', obj.task.project,
                                                                request.user)
            elif obj.task.client is not None:
                return True
        return False

"""
    permission for Financial
"""
class FinancialPermission(ProjectPlanPermission):
    feature = 'FINANCIALS'


"""
    permission for Disbursement Entity
"""
class DisbursementEntityPermission(BaseProjectPermission):
    feature = 'FINANCIALS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        disbursement = request.GET.get('disbursement')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and disbursement:
            project = Disbursement.objects.get(id=disbursement).project
            return check_if_user_has_project_feature_permission(self.feature, 'View', project, request.user)
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_project_feature_permission(self.feature, 'Change', obj.disbursement.project,
                                                                request.user)
        if request.method == 'DELETE':
            return check_if_user_has_project_feature_permission(self.feature, 'Delete', obj.disbursement.project,
                                                                request.user)
        if request.method == 'GET':
            return check_if_user_has_project_feature_permission(self.feature, 'View', obj.disbursement.project,
                                                                request.user)
        return False

"""
    permission for Utilisation Upload Verification
"""
class UtilisationUploadVerificationPermission(BaseProjectPermission):
    feature = 'FINANCIALS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        project_id = request.GET.get('project_id')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.user.application_role.role_name == 'ngo-user' and request.method in ['PATCH']:
            return False
        elif view.action == 'list' and project_id:
            project = Project.objects.get(id=project_id)
            return check_if_user_has_project_feature_permission(self.feature, 'View', project, request.user)
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_project_feature_permission(self.feature, 'Change', obj.project,
                                                                request.user)
        if request.method == 'DELETE':
            return check_if_user_has_project_feature_permission(self.feature, 'Delete', obj.project,
                                                                request.user)
        if request.method == 'GET':
            return check_if_user_has_project_feature_permission(self.feature, 'View', obj.project,
                                                                request.user)
        return False

"""
    permission for Utilisation Entity
"""
class UtilisationEntityPermission(BaseProjectPermission):
    feature = 'FINANCIALS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        utilisation = request.GET.get('utilisation')
        if not utilisation:
            utilisation = request.GET.get('utilisation_id')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and utilisation:
            project = Utilisation.objects.get(id=utilisation).project
            return check_if_user_has_project_feature_permission(self.feature, 'View', project, request.user)
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_project_feature_permission(self.feature, 'Change', obj.utilisation.project,
                                                                request.user)
        if request.method == 'DELETE':
            return check_if_user_has_project_feature_permission(self.feature, 'Delete', obj.utilisation.project,
                                                                request.user)
        if request.method == 'GET':
            return check_if_user_has_project_feature_permission(self.feature, 'View', obj.utilisation.project,
                                                                request.user)
        return False

"""
    permission for Utilisation Expense Entity
"""
class UtilisationExpenseEntityPermission(BaseProjectPermission):
    feature = 'FINANCIALS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        utilisation_expense = request.GET.get('utilisation_expense')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and utilisation_expense:
            project = UtilisationExpense.objects.get(id=utilisation_expense).utilisation.project
            return check_if_user_has_project_feature_permission(self.feature, 'View', project, request.user)
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_project_feature_permission(self.feature, 'Change',
                                                                obj.utilisation_expense.utilisation.project,
                                                                request.user)
        if request.method == 'DELETE':
            return check_if_user_has_project_feature_permission(self.feature, 'Delete',
                                                                obj.utilisation_expense.utilisation.project,
                                                                request.user)
        if request.method == 'GET':
            return check_if_user_has_project_feature_permission(self.feature, 'View',
                                                                obj.utilisation_expense.utilisation.project,
                                                                request.user)
        return False

"""
    permission for Utilisation Upload Entity
"""
class UtilisationUploadEntityPermission(BaseProjectPermission):
    feature = 'FINANCIALS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        utilisation_upload = request.GET.get('utilisation_upload')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and utilisation_upload:
            project = UtilisationUpload.objects.get(id=utilisation_upload).project
            return check_if_user_has_project_feature_permission(self.feature, 'View', project, request.user)
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_project_feature_permission(self.feature, 'Change', obj.utilisation_upload.project,
                                                                request.user)
        if request.method == 'DELETE':
            return check_if_user_has_project_feature_permission(self.feature, 'Delete', obj.utilisation_upload.project,
                                                                request.user)
        if request.method == 'GET':
            return check_if_user_has_project_feature_permission(self.feature, 'View', obj.utilisation_upload.project,
                                                                request.user)
        return False


"""
    permission for Impact
"""
class ImpactPermission(ProjectPlanPermission):
    feature = 'IMPACT'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        indicator_batch = request.GET.get('indicator_batch')
        project = request.GET.get('project')
        indicator = request.GET.get('indicator')
        project_indicator = request.GET.get('project_indicator')
        project_id = view.kwargs.get('project_id')
        id = view.kwargs.get('pk')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.method == 'GET' and (indicator_batch or project or indicator or project_indicator or project_id or id):
            return True
        elif request.method == 'GET':
            return False
        return True


"""
    permission for Impact Indicator
"""
class ImpactIndicatorPermission(ImpactPermission):
    feature = 'IMPACT'

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.method == 'GET':
            return True
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            if hasattr(obj, 'indicator'):
                project = obj.indicator.project
            elif hasattr(obj, 'project_indicator'):
                project = obj.project_indicator.project
            return check_if_user_has_project_feature_permission(self.feature, 'Change', project, request.user)
        if request.method == 'DELETE':
            if hasattr(obj, 'indicator'):
                project = obj.indicator.project
            elif hasattr(obj, 'project_indicator'):
                project = obj.project_indicator.project
            return check_if_user_has_project_feature_permission(self.feature, 'Delete', project, request.user)
        return False


"""
    permission for Template Activity Log
"""
class TemplateActivityLogPermission(ImpactPermission):
    feature = 'IMPACT'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        template = request.GET.get('template')
        if template:
            template_obj = Template.objects.get(id=template)
            if template_obj:
                project = template_obj.project

        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.method == 'GET' and project:
            return True
        elif request.method == 'GET':
            return False
        return True


"""
    permission for Impact Case Study
"""
class ImpactCaseStudyPermission(ProjectPlanPermission):
    feature = 'IMPACT'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        case_study = request.GET.get('case_study')
        project = request.GET.get('project')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and (case_study or project):
            return True
        elif view.action == 'list':
            return False
        return True

"""
    permission for Delete Impact
"""
class DeleteImpactPermission(ProjectPlanPermission):
    feature = 'IMPACT'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        indicator_batch = request.GET.get('indicator_batch')
        project = request.GET.get('project')
        if request.user.is_anonymous:
            return False
        # elif request.user.application_role.role_name == 'sattva-admin':
        #     return True
        # elif view.action == 'list' and (indicator_batch or project):
        #     return True
        # elif view.action == 'list':
        #     return False
        return True

"""
    permission for Delete Impact Template
"""
class DeleteImpactTemplatePermission(DeleteImpactPermission):

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        indicator_batch = request.GET.get('indicator_batch')
        project = request.GET.get('project')
        project_id = view.kwargs.get('project_id')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.method == 'DELETE' and request.user.application_role.role_name == 'ngo-user':
            return False
        elif (request.method == 'GET' or request.method == 'DELETE') and (indicator_batch or project or project_id):
            return True
        elif request.method == 'GET' or request.method == 'DELETE':
            return False
        return True

"""
    permission for Project Manage User
"""
class ProjectManageUserPermission(BaseClientPermission):
    feature = 'MANAGE_USERS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        client = request.GET.get('client')
        if client is None:
            plant = request.GET.get('plant')
            if plant is not None:
                client = Plant.objects.get(id=plant).client
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.method == 'GET' and client:
            return check_if_user_has_client_feature_permission(self.feature, 'View', client, request.user)
        elif request.method == 'GET':
            return False
        return True

"""
    permission for Project User
"""
class ProjectUserPermission(BaseClientPermission):
    feature = 'MANAGE_USERS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        project = request.GET.get('project')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and project:
            return True
        elif view.action == 'list':
            return False
        elif request.method == 'POST':
            project = request.data.get('project')
            client = Project.objects.get(id=project).client
            return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
        return True


"""
    permission for Delete Project User
"""
class DeleteProjectUserPermission(BaseClientPermission):
    feature = 'MANAGE_USERS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.method == 'DELETE':
            project = request.GET.get('project')
            client = Project.objects.get(id=project).client
            return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
        return False
