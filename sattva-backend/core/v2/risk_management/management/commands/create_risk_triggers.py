# coding=utf-8

from django.core.management.base import BaseCommand
import csv

from v2.risk_management.models import RiskCategory, Risk, RiskVariables, RiskTemplate


class Command(BaseCommand):
    help = 'Creating Risk Triggers & Templates'

    def handle(self, *args, **options):
        import os
        cwd = os.getcwd()
        with open(cwd+'/risk_triggers.csv', 'rt')as f:
            data = csv.reader(f)
            header = next(data)
            for row in data:
                if len(row[0]):
                    rc, _= RiskCategory.objects.get_or_create(name=row[0])
                    if len(row[2]):
                        risk,_ = Risk.objects.get_or_create(
                            name=row[2],
                            severity=int(row[3]) if len(row[3]) else 0,
                            category=rc,
                            description=row[1]+" - "+ row[5],
                            trigger_name=str((row[2].strip())[:220]).lower().replace(' ', '_')
                        )
                        if len(row[5]) and risk:
                            risk_template,_ = RiskTemplate.objects.get_or_create(
                                content= row[5].strip(),
                                risk = risk
                            )
                        if len(row[6]) and risk:
                            RiskVariables.objects.get_or_create(
                                name = str((row[6].strip())[:220]).lower().replace(' ', '_'),
                                display_name = row[6],
                                default_value = float(row[7]) if len(str(row[7])) else 0,
                                variable_type = str(row[8]).strip() if len(row[8]) else "",
                                risk = risk
                            )
