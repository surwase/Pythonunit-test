import csv
from datetime import datetime, timezone, timedelta
from dateutil import relativedelta
import math

import dateutil.parser

from v2.project.models import Disbursement, Project, Utilisation, \
    UtilisationExpense, UtilisationUpload, ProjectTargetSegment, UtilisationComment


class BulkUtilisationException(Exception):
    def __init__(self, errors):
        self.errors = errors


class BulkUtilisation:
    def __init__(self):
        self.errors = []
        self.month_frequency_map = {
            'MONTHLY': 1,
            'QUARTERLY': 3,
            'HALF-YEARLY': 6,
            'ANNUALLY': 12
        };

    def read_csv(self, csv_file):
        csv_rows = []

        # with open(csv_file) as csvfile:

        reader = csv.reader(
            csv_file,
            quotechar='"',
            delimiter=',',
            quoting=csv.QUOTE_ALL,
            skipinitialspace=True
        )

        titles = next(reader)
        data = []
        for row in reader:
            row_data = {}
            for i, value in enumerate(row):
                if i >= len(titles):
                    raise BulkUtilisationException(f"CSV Read Error: Index out of range: Titles: {titles}, Row: {row}")
                title = titles[i]
                row_data[title] = value
            data.append(row_data)
        return data

    def format_data(self, data):

        INTEGER_FIELDS = [
            "project_id",
            "planned_cost",
            "avg_unit",
            "avg_unit_cost",
            "estimate_cost",
            "number_of_units",
            "unit_cost",
            "actual_cost"
        ]
        TIMESTAMP_FIELDS = [
            "from_date",
            "to_date",
            "period_start_date",
            "period_end_date"
        ]
        for row_no, row in enumerate(data):
            for key, value in row.items():
                if key in INTEGER_FIELDS:
                    if not value:
                        row[key] = None
                    else:
                        try:
                            row[key] = float(value)
                        except ValueError:
                            self.errors.append(
                                f"Invalid value for row: {row_no + 1}, column: {key}, " \
                                    f"value: {value} - Expected Float"
                            )

                if key in TIMESTAMP_FIELDS and value:
                    try:
                        date = dateutil.parser.parse(value, dayfirst=True)
                        date = date.replace(tzinfo=timezone.utc).astimezone(tz=None)
                        date = date + timedelta(hours=11)
                        row[key] = date
                    except ValueError:
                        self.errors.append(
                            f"Invalid timestamp value for row: {row_no + 1}, column: {key}, value: {value}"
                        )
                if key == "split_frequency":
                    try:
                        frequency = ['MONTHLY', 'QUARTERLY', 'HALF-YEARLY', 'ANNUALLY']
                        value = value.strip().upper()
                        value_assigned = False
                        if value in frequency:
                            row[key] = value
                            value_assigned = True
                        else:
                            if value in ['YEARLY', 'ANNUAL'] or 'ANNUA' in value:
                                row[key] = 'ANNUALLY'
                                value_assigned = True
                            elif 'MONTH' in value:
                                row[key] = 'MONTHLY'
                                value_assigned = True
                            elif value in ["HALFYEARLY", 'HALFYEAR'] or 'HALF' in value or (
                                'YEAR' in value and value[0] is 'Y'):
                                row[key] = "HALF-YEARLY"
                                value_assigned = True
                            if value_assigned is False:
                                for freq in frequency:
                                    if freq.find(value) != -1:
                                        row[key] = freq
                                        value_assigned = True
                                        break
                        if value_assigned is False:
                            self.errors.append(
                                f"Invalid Split Frequency value for row: {row_no + 1}, column: {key}, value: {value}. \
                                                        \nPlease add a value from the following choices: Monthly, Quarterly, Half-Yearly, Annually."
                            )
                    except ValueError:
                        self.errors.append(
                            f"Invalid Split Frequency value for row: {row_no + 1}, column: {key}, value: {value}. \
                                                        \nPlease add a value from the following choices: Monthly, Quarterly, Half-Yearly, Annually."
                        )

                if key == "tags":
                    row[key] = value.split(";")

        return data

    def _get_utilisation_object(self, data_dict, project):
        if not project:
            project = Project.objects.filter(
                id=data_dict.get("project_id")
            ).first()

            # Raise exception if project is not found
            if not (project and data_dict.get("project_id")):
                raise BulkUtilisationException(f"Project with project_id: {data_dict.get('project_id')} not found")

        disbursement_obj = Disbursement.objects.filter(
            project=project,
            disbursement_name=data_dict.get("disbursement_name")
        ).first()

        # Mutliple utilisation_obj?
        utilisation_obj = Utilisation.objects.filter(
            project=project,
            particulars=data_dict.get("particulars"),
            disbursement=disbursement_obj
        ).first()

        if not utilisation_obj:
            utilisation_obj = Utilisation.objects.create(
                project=project,
                disbursement=disbursement_obj,
                particulars=data_dict.get("particulars"),
                expense_category=data_dict.get("expense_category"),
                planned_cost=data_dict.get("planned_cost"),
                avg_unit=data_dict.get("avg_unit"),
                avg_unit_cost=data_dict.get("avg_unit_cost"),
                start_date=data_dict.get("from_date"),
                end_date=data_dict.get("to_date"),
                description=data_dict.get("utilisation_description"),
                tags=data_dict.get("tags")
            )
        else:
            # Update Utilisation object

            utilisation_obj.expense_category = data_dict.get("expense_category")
            utilisation_obj.planned_cost = data_dict.get("planned_cost")
            utilisation_obj.avg_unit = data_dict.get("avg_unit")
            utilisation_obj.avg_unit_cost = data_dict.get("avg_unit_cost")
            utilisation_obj.start_date = data_dict.get("from_date")
            utilisation_obj.end_date = data_dict.get("to_date")
            utilisation_obj.description = data_dict.get("utilisation_description")
            utilisation_obj.save()
        return utilisation_obj

    def _create_or_update_utilisation_expense_object(self, data_dict,
                                                     utilisation_obj, utilisation_upload):

        utilisation_expense = UtilisationExpense.objects.filter(
            split_frequency=data_dict.get("split_frequency"),
            period_start_date=data_dict.get("period_start_date"),
            period_end_date=data_dict.get("period_end_date"),
            utilisation=utilisation_obj
        ).first()

        if not utilisation_expense:
            obj = UtilisationExpense.objects.create(
                # description=data_dict.get("utilisation_description"),
                split_frequency=data_dict.get("split_frequency"),
                period_start_date=data_dict.get("period_start_date"),
                period_end_date=data_dict.get("period_end_date"),
                remarks=data_dict.get("remarks"),
                # comments=data_dict.get("comments"),
                estimate_cost=data_dict.get("estimate_cost"),
                number_of_units=data_dict.get("number_of_units"),
                unit_cost=data_dict.get("unit_cost"),
                actual_cost=data_dict.get("actual_cost"),
                utilisation=utilisation_obj,
                utilisation_upload=utilisation_upload
            )
            for comment in data_dict.get("comments", '').split(';'):
                if comment:
                    UtilisationComment.objects.create(
                        comment=comment,
                        utilisation=utilisation_obj
                    )
        else:
            # utilisation_expense.description = data_dict.get("utilisation_description")
            utilisation_expense.remarks = data_dict.get("remarks")
            # utilisation_expense.comments = data_dict.get("comments")
            utilisation_expense.estimate_cost = data_dict.get("estimate_cost")
            utilisation_expense.number_of_units = data_dict.get("number_of_units")
            utilisation_expense.unit_cost = data_dict.get("unit_cost")
            utilisation_expense.actual_cost = data_dict.get("actual_cost")
            utilisation_expense.utilisation_upload = utilisation_upload
            utilisation_expense.save()
            for comment in data_dict.get("comments", '').split(';'):
                if comment:
                    UtilisationComment.objects.create(
                        comment=comment,
                        utilisation=utilisation_obj
                    )

    def generate_period(self, row, row_no):
        start_date = row['from_date']
        end_date = row['to_date']
        frequency = row['split_frequency']
        period_duration = self.get_month_diff(start_date, end_date)
        number_of_periods = period_duration / self.month_frequency_map[frequency]
        number_of_periods = math.ceil(number_of_periods);
        period = []
        if (period_duration <= self.month_frequency_map[frequency]):
            if row['period_start_date'] != start_date or row['period_end_date'] != end_date:
                period.append({
                    'period_start_date': start_date.strftime("%d/%m/%Y"),
                    'period_end_date': end_date.strftime("%d/%m/%Y")
                })
                self.errors.append({
                    'message': f"Invalid start date and end date for row {row_no+1}",
                    'possible_choices': period,
                    'received': {
                        'period_start_date': row['period_start_date'].strftime("%d/%m/%Y"),
                        'period_end_date': row['period_end_date'].strftime("%d/%m/%Y")
                    }
                })
                return False
            return True
        else:
            for i in range(1, number_of_periods+1):
                period_start_date = start_date + (i-1) * relativedelta.relativedelta(months=self.month_frequency_map[frequency])
                period_end_date = start_date + i * relativedelta.relativedelta(months=self.month_frequency_map[frequency]) - timedelta(days=1) \
                if i < number_of_periods else end_date
                period.append({
                    'period_start_date': period_start_date.strftime("%d/%m/%Y"),
                    'period_end_date': period_end_date.strftime("%d/%m/%Y")
                })
                if row['period_start_date'] == period_start_date and row['period_end_date'] == period_end_date:
                    return True
            self.errors.append({
                'message': f"Invalid start date and end date for row {row_no+1}",
                'possible_choices': period,
                'received': {
                    'period_start_date': row['period_start_date'].strftime("%d/%m/%Y"),
                    'period_end_date': row['period_end_date'].strftime("%d/%m/%Y")
                }
            })
        return False

    def get_month_diff(self, start_date, end_date):
        r = relativedelta.relativedelta(end_date, start_date)
        months_difference = (r.years * 12) + r.months
        if r.days > 0:
            months_difference += 1
        return months_difference


    def validate_periods(self, data):
        for row_no, row in enumerate(data):
            period = self.generate_period(row, row_no)
            
    def save_utilisation_expenses(self, data, upload_obj, project=None):
        utlization_expenses = []
        for key, item in data.items():
            if item and item.get("project_id") is not None:
                utilisation_obj = self._get_utilisation_object(item, project)
                for expense_data in item.get("expenses", []):
                    self._create_or_update_utilisation_expense_object(expense_data,
                                                                    utilisation_obj, upload_obj)

    def _check_if_utilisation_details_matches(self, existing, new, row_no):

        matching_keys = ["expense_category", "planned_cost", "avg_unit", "avg_unit_cost", "avg_unit_cost",
                         "from_date", "to_date", "tags"]

        for key, value in existing.items():
            if key in matching_keys:
                if value != new.get(key):
                    self.errors.append(
                        f"Utilisation values do not match for " \
                            f"row: {row_no}, column: {key}, " \
                            f"expected_value: {value}, value: {new.get(key)}"
                    )

    def group_data(self, data):
        grouped_data = {}
        for row_no, item in enumerate(data):
            key = f'{item.get("project_id")}|{item.get("disbursement_name")}|{item.get("particulars")}'
            expense = {
                "split_frequency": item.get("split_frequency"),
                "period_start_date": item.get("period_start_date"),
                "period_end_date": item.get("period_end_date"),
                "estimate_cost": item.get("estimate_cost"),
                "number_of_units": item.get("number_of_units"),
                "unit_cost": item.get("unit_cost"),
                "actual_cost": item.get("actual_cost"),
                "remarks": item.get("remarks"),
                "comments": item.get("comments"),
                # "utilisation_description": item.get("utilisation_description"),
            }
            if key not in grouped_data:
                utilisation_data = {
                    "project_id": item.get("project_id"),
                    "disbursement_name": item.get("disbursement_name"),
                    "particulars": item.get("particulars"),
                    "expense_category": item.get("expense_category"),
                    "planned_cost": item.get("planned_cost"),
                    "avg_unit": item.get("avg_unit"),
                    "avg_unit_cost": item.get("avg_unit_cost"),
                    "from_date": item.get("from_date"),
                    "to_date": item.get("to_date"),
                    "tags": item.get("tags"),
                    "utilisation_description": item.get("utilisation_description"),
                    "expenses": [expense]
                }
                grouped_data[key] = utilisation_data
            else:
                self._check_if_utilisation_details_matches(grouped_data[key], item, row_no + 1)

                grouped_data[key]["expenses"].append(expense)
        return grouped_data

    def process(self, file_data, utilisation_upload_obj, project):

        formatted_data = self.format_data(file_data)

        if self.errors:
            raise BulkUtilisationException(self.errors)

        self.validate_periods(formatted_data)

        if self.errors:
            raise BulkUtilisationException(self.errors)

        data = self.group_data(formatted_data)

        if self.errors:
            raise BulkUtilisationException(self.errors)

        self.save_utilisation_expenses(data, utilisation_upload_obj, project)
