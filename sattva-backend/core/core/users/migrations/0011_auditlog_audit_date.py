# Generated by Django 2.0.5 on 2020-07-03 05:22

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0010_auditlog'),
    ]

    operations = [
        migrations.AddField(
            model_name='auditlog',
            name='audit_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now, null=True),
        ),
    ]
