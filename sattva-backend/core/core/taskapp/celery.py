import datetime
import os

import pytz
from celery import Celery
from celery.schedules import crontab
from django.apps import apps, AppConfig
from django.conf import settings

if not settings.configured:
    # set the default Django settings module for the 'celery' program.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')  # pragma: no cover

app = Celery('core')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

# def nowfun():
#     return datetime.datetime.now(pytz.timezone('Asia/Kolkata'))


app.conf.beat_schedule = {
    'disbursement_notifications': {
        'task': 'v2.project.tasks.notification_for_disbursements',
        'schedule': crontab(minute=30, hour=3)
    },
    'utilisation_notifications': {
        'task': 'v2.project.tasks.notification_for_utilisation',
        'schedule': crontab(hour=3, minute=30)
    },
    'project_status_calculation': {
        'task' : 'v2.project.tasks.calculate_all_project_status',
        'schedule': crontab(hour=3, minute=30)
    },
    'total_and_planned_disbursement_amount_calculation': {
        'task' : 'v2.project.tasks.calculate_all_project_total_and_planned_disbursement_amount',
        'schedule': crontab(hour=3, minute=30)
    },
    'client_data_sync_up': {
        'task' : 'v2.client.tasks.sync_data_for_all_client_tables',
        'schedule': crontab(hour=3, minute=30)
    }
}


# class CeleryConfig(AppConfig):
#     name = 'core.taskapp'
#     verbose_name = 'Celery Config'
#
#     def ready(self):
#         # Using a string here means the worker will not have to
#         # pickle the object when using Windows.
#         app.config_from_object('django.conf:settings')
#         installed_apps = [app_config.name for app_config in apps.get_app_configs()]
#         app.autodiscover_tasks(lambda: installed_apps, force=True)
#
#         if hasattr(settings, 'RAVEN_CONFIG'):
#             # Celery signal registration
#             # Since raven is required in production only,
#             # imports might (most surely will) be wiped out
#             # during PyCharm code clean up started
#             # in other environments.
#             # @formatter:off
#             from raven import Client as RavenClient
#             from raven.contrib.celery import register_signal as raven_register_signal
#             from raven.contrib.celery import register_logger_signal as raven_register_logger_signal
#             # @formatter:on
#
#             raven_client = RavenClient(dsn=settings.RAVEN_CONFIG['dsn'])
#             raven_register_logger_signal(raven_client)
#             raven_register_signal(raven_client)


@app.task(bind=True)
def debug_task(self):
    print('Request: {self.request!r}')  # pragma: no cover
