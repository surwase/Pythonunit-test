from django.conf.urls import url

from .views import *

app_name = "client"

urlpatterns = [
    # url(r'^export/(?P<company_id>\d+)/$', CompanyExportAPIView.as_view({'get': 'list', 'post': 'create'})),
    url(r'^$', KoboAssetModelViewSet.as_view(
        {'get': 'list'}),
        name='kobo'),
    url(r'^sync_schema/$', KoboSyncDbAPIView.as_view(),
        name='kobo'),
]
