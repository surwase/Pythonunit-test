from celery import shared_task


@shared_task
def sync_data_for_client_tables(client_id):
    from v2.client.models import Client
    from v2.data_scripts.client_tables import ClientTableHelper

    client = Client.objects.get(id=client_id)
    cl_helper = ClientTableHelper(client)
    cl_helper.sync_data()


@shared_task
def sync_data_for_all_client_tables():
    from v2.client.models import Client

    clients = Client.objects.all()
    for client in clients:
        sync_data_for_client_tables.apply_async(kwargs={"client_id": client.id})
