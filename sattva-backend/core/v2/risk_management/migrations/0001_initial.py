# Generated by Django 2.0.5 on 2019-11-29 09:31

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('plant', '0010_auto_20191120_1210'),
        ('client', '0013_clientimage'),
        ('project', '0023_projectimage'),
    ]

    operations = [
        migrations.CreateModel(
            name='Risk',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('name', models.CharField(max_length=255)),
                ('trigger_name', models.CharField(max_length=255)),
                ('description', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RiskCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RiskEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('name', models.CharField(max_length=255)),
                ('code', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('status', models.SmallIntegerField(choices=[(0, 'OPEN'), (1, 'CLOSED')], default=0)),
                ('severity', models.SmallIntegerField(choices=[(1, 'HIGH'), (1, 'CLOSED'), (2, 'MEDIUM'), (3, 'CRITICAL')], default=0)),
                ('risk_type', models.SmallIntegerField(choices=[(0, 'OPEN'), (1, 'CLOSED')], default=0)),
                ('content', models.TextField()),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='client.Client')),
                ('plant', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='plant.Plant')),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='project.Project')),
                ('risk', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='risk_management.Risk')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RiskTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('content', models.TextField()),
                ('risk', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='risk_management.Risk')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RiskVariables',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('name', models.CharField(max_length=255)),
                ('content', models.TextField()),
                ('default_value', models.CharField(max_length=255)),
                ('risk', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='risk_management.Risk')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='RiskVariablesClientSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('new_value', models.CharField(max_length=255)),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='client.Client')),
                ('risk_variable', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='risk_management.RiskVariables')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='risk',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='risk_management.RiskCategory'),
        ),
    ]
