import datetime
import os

from celery import shared_task
import pytz
from celery import Celery

from v2.client.models import UserNotificationSettings
from v2.notification import settings_choices, notification_choices
from v2.notification.models import Notification
from v2.project.models import (
    Disbursement,
    DisbursementWorkflowRuleAssignee,
    DisbursementWorkflowRuleSet,
    DisbursementWorkflowRule,
    Utilisation,
    UtilisationWorkflowRuleAssignee,
    CSVUpload,
    ImpactWorkflowRule,
    ProjectOutput,
    ProjectOutputStatus,
    ProjectOutcome,
    ProjectOutcomeStatus,
    ImpactUploadStatus,
    Project,
    Template,
    CSVUpload,
    ProjectIndicator,
)


def get_user_notification_settings(user, settings_name):
    user_setting, created = UserNotificationSettings.objects.get_or_create(
        user=user,
        settings_name=settings_name,
        defaults={"is_enabled": True, "is_email_enabled": False, "days": 7},
    )
    return user_setting


def send_notification_for_due_date_reminder(disbursement, next_rule):
    rule_assignee_list = DisbursementWorkflowRuleAssignee.objects.filter(
        rule__id=next_rule.get("id"), project=disbursement.project
    )
    utc = pytz.UTC
    expected_date = disbursement.expected_date.replace(tzinfo=utc)
    current_time = datetime.datetime.now().replace(tzinfo=utc)
    app_url = os.environ.get("LOGIN_URL")
    app_url = app_url + "/client/{}/project/{}/financials".format(
        disbursement.project.client.id, disbursement.project.id
    )
    for assignee in rule_assignee_list:
        due_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.NOTIFICATION_BEFORE_DUE_DATE_REMINDER
        )
        overdue_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.ALERT_AFTER_OVERDUE_ALERT
        )
        if (
            due_date_reminder_setting.is_enabled
            or due_date_reminder_setting.is_email_enabled
        ) and (
            current_time
            < expected_date
            <= current_time + datetime.timedelta(days=due_date_reminder_setting.days)
        ):
            # SEND NOTIFICATION
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Due Date Reminder for Disbursement!"
            notification_text = 'Reminder: Please submit all the necessary documents for disbursement "{}" of the project "{}" and request for approval in next "{}" days.'.format(
                disbursement.disbursement_name,
                disbursement.project.get_name(),
                due_date_reminder_setting.days,
            )
            platform_users = (
                [assignee.user.user] if due_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user]
                if due_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=1,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
                app_url=app_url,
            )
        if (
            overdue_date_reminder_setting.is_enabled
            or overdue_date_reminder_setting.is_email_enabled
        ) and (
            expected_date + datetime.timedelta(days=overdue_date_reminder_setting.days)
            <= current_time
            and disbursement.expected_date < current_time
        ):
            # SEND ALERT
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Due Date Reminder for Disbursement!"
            notification_text = 'Delay Alert: Disbursement "{}" of the project "{}" has already passed its expected date and Approval Request hasn\'t been sent along with required documents'.format(
                disbursement.disbursement_name, disbursement.project.get_name()
            )
            platform_users = (
                [assignee.user.user] if overdue_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user.username]
                if overdue_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=0,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
                app_url=app_url,
            )


def send_notification_for_rejection_update(disbursement, next_rule):
    concerned_rules = DisbursementWorkflowRule.objects.filter(
        level__lt=DisbursementWorkflowRule.objects.get(id=next_rule.get("id")).level
    )
    rule_assignee_list = DisbursementWorkflowRuleAssignee.objects.filter(
        rule__in=concerned_rules, project=disbursement.project
    )
    utc = pytz.UTC
    expected_date = disbursement.invoice_request_date.replace(tzinfo=utc)
    current_time = datetime.datetime.now().replace(tzinfo=utc)
    app_url = os.environ.get("LOGIN_URL")
    app_url = app_url + "/client/{}/project/{}/financials".format(
        disbursement.project.client.id, disbursement.project.id
    )
    for assignee in rule_assignee_list:
        due_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.NOTIFICATION_BEFORE_DUE_DATE_REMINDER
        )
        overdue_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.ALERT_AFTER_OVERDUE_ALERT
        )
        if (
            due_date_reminder_setting.is_enabled
            or due_date_reminder_setting.is_email_enabled
        ) and (
            current_time
            < expected_date
            <= current_time + datetime.timedelta(days=due_date_reminder_setting.days)
        ):
            # SEND NOTIFICATION
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Due Date Reminder for Rejected Disbursement!"
            notification_text = 'Reminder: Please review and submit all the necessary documents for disbursement "{}" of the project "{}" and request for re-approval in next "{}" days.'.format(
                disbursement.disbursement_name,
                disbursement.project.get_name(),
                due_date_reminder_setting.days,
            )
            platform_users = (
                [assignee.user.user] if due_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user]
                if due_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=1,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
                app_url=app_url,
            )
        if (
            overdue_date_reminder_setting.is_enabled
            or overdue_date_reminder_setting.is_email_enabled
        ) and (
            expected_date + datetime.timedelta(days=overdue_date_reminder_setting.days)
            <= current_time
            and disbursement.expected_date < current_time
        ):
            # SEND ALERT
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Due Date Reminder for Rejected Disbursement!"
            notification_text = 'Delay Alert: Disbursement "{}" of the project "{}" has already passed its expected date and Re-Approval Request hasn\'t been sent along with required documents'.format(
                disbursement.disbursement_name, disbursement.project.get_name()
            )
            platform_users = (
                [assignee.user.user] if overdue_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user.username]
                if overdue_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=0,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
                app_url=app_url,
            )


def send_notification_for_review_pending(disbursement, current_status, next_rule):
    rule_assignee_list = DisbursementWorkflowRuleAssignee.objects.filter(
        rule__id=next_rule.get("id"), project=disbursement.project
    )
    utc = pytz.UTC
    invoice_request_date = disbursement.invoice_request_date.replace(tzinfo=utc)
    current_time = datetime.datetime.now().replace(tzinfo=utc)
    app_url = os.environ.get("LOGIN_URL")
    app_url = app_url + "/client/{}/project/{}/financials".format(
        disbursement.project.client.id, disbursement.project.id
    )
    for assignee in rule_assignee_list:
        due_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.NOTIFICATION_BEFORE_DUE_DATE_REMINDER
        )
        overdue_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.ALERT_AFTER_OVERDUE_ALERT
        )
        if (
            due_date_reminder_setting.is_enabled
            or due_date_reminder_setting.is_email_enabled
        ) and current_time < invoice_request_date <= current_time + datetime.timedelta(
            days=due_date_reminder_setting.days
        ):
            # SEND NOTIFICATION
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Review Pending Reminder for Disbursement!"
            notification_text = 'Reminder: Disbursement "{}" of the project "{}" is pending to be reviewed in the next "{}" days'.format(
                disbursement.disbursement_name,
                disbursement.project.get_name(),
                due_date_reminder_setting.days,
            )
            platform_users = (
                [assignee.user.user] if due_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user]
                if due_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=1,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
                app_url=app_url,
            )
        if (
            overdue_date_reminder_setting.is_enabled
            or overdue_date_reminder_setting.is_email_enabled
        ) and (
            current_time + datetime.timedelta(days=overdue_date_reminder_setting.days)
            > invoice_request_date
            < current_time
        ):
            # SEND ALERT
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Alert: Review Pending for Disbursement."
            notification_text = 'Delay Alert: Disbursement "{}" of the project "{}" is pending to be reviewed.'.format(
                disbursement.disbursement_name, disbursement.project.get_name()
            )
            platform_users = (
                [assignee.user.user] if overdue_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user.username]
                if overdue_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=0,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
                app_url=app_url,
            )


def send_notification_to_release_payment(disbursement, current_status, next_rule):
    final_approver_rule = disbursement.get_final_approver_rule()
    rule_assignee_list = DisbursementWorkflowRuleAssignee.objects.filter(
        rule=final_approver_rule, project=disbursement.project
    )
    utc = pytz.UTC
    final_approval_date = disbursement.get_final_approval_date().replace(tzinfo=utc)
    current_time = datetime.datetime.now().replace(tzinfo=utc)
    app_url = os.environ.get("LOGIN_URL")
    app_url = app_url + "/client/{}/project/{}/financials".format(
        disbursement.project.client.id, disbursement.project.id
    )
    for assignee in rule_assignee_list:
        due_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.NOTIFICATION_BEFORE_DUE_DATE_REMINDER
        )
        overdue_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.ALERT_AFTER_OVERDUE_ALERT
        )
        # if (due_date_reminder_setting.is_enabled or due_date_reminder_setting.is_email_enabled) and \
        #     (disbursement.get_final_approval_date() <= datetime.datetime.now() + datetime.timedelta(
        #     days=due_date_reminder_setting.days)):
        #     # SEND NOTIFICATION
        #     notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
        #     notification_title = "Review Pending Reminder for Disbursement!"
        #     notification_text = "Reminder: Disbursement \"{}\" of the project \"{}\" is pending to be released in the next \"{}\" days". \
        #         format(disbursement.disbursement_name, disbursement.project.get_name(), due_date_reminder_setting.days)
        #     platform_users = [assignee.user.user] if due_date_reminder_setting.is_enabled else []
        #     email_users = [assignee.user.user] if due_date_reminder_setting.is_email_enabled else []
        #     Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
        #                         action_params={"disbursement_id": disbursement.id},
        #                         users_to_send_to=platform_users, title=notification_title,
        #                         text=notification_text, notification_tag="Disbursement",
        #                         email_users=email_users)
        if (
            overdue_date_reminder_setting.is_enabled
            or overdue_date_reminder_setting.is_email_enabled
        ) and (
            current_time
            >= final_approval_date
            + datetime.timedelta(days=overdue_date_reminder_setting.days)
        ):
            # SEND ALERT
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Alert: Review Pending for Disbursement."
            notification_text = 'Delay Alert: Disbursement "{}" of the project "{}" is pending to be released.'.format(
                disbursement.disbursement_name, disbursement.project.get_name()
            )
            platform_users = (
                [assignee.user.user] if overdue_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user.username]
                if overdue_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=0,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
                app_url=app_url,
            )


def send_notification_for_payment_acknowledgement(
    disbursement, current_status, next_rule
):
    requestor_rule = disbursement.get_requestor_rule()
    rule_assignee_list = DisbursementWorkflowRuleAssignee.objects.filter(
        rule=requestor_rule, project=disbursement.project
    )
    utc = pytz.UTC
    actual_date = disbursement.actual_date.replace(tzinfo=utc)
    current_time = datetime.datetime.now().replace(tzinfo=utc)
    app_url = os.environ.get("LOGIN_URL")
    app_url = app_url + "/client/{}/project/{}/financials".format(
        disbursement.project.client.id, disbursement.project.id
    )
    for assignee in rule_assignee_list:
        due_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.NOTIFICATION_BEFORE_DUE_DATE_REMINDER
        )
        overdue_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.ALERT_AFTER_OVERDUE_ALERT
        )
        if (
            due_date_reminder_setting.is_enabled
            or due_date_reminder_setting.is_email_enabled
        ) and (
            actual_date
            <= current_time + datetime.timedelta(days=due_date_reminder_setting.days)
        ):
            # SEND NOTIFICATION
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Awaiting Payment Acknowledgement for Disbursement!"
            notification_text = 'Reminder: Disbursement "{}" of the project "{}" is pending acknowledgement of the receipt.'.format(
                disbursement.disbursement_name,
                disbursement.project.get_name(),
                due_date_reminder_setting.days,
            )
            platform_users = (
                [assignee.user.user] if due_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user]
                if due_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=1,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
            )
        if (
            overdue_date_reminder_setting.is_enabled
            or overdue_date_reminder_setting.is_email_enabled
        ) and (
            actual_date + datetime.timedelta(days=overdue_date_reminder_setting.days)
            >= current_time
        ):
            # SEND ALERT
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Alert: Review Pending for Disbursement."
            notification_text = 'Delay Alert: Disbursement "{}" of the project "{}" is pending to be released.'.format(
                disbursement.disbursement_name, disbursement.project.get_name()
            )
            platform_users = (
                [assignee.user.user] if overdue_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user.username]
                if overdue_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=0,
                action_user=None,
                action_performed_on_user=None,
                action_params={"disbursement_id": disbursement.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Disbursement",
                email_users=email_users,
                app_url=app_url,
            )


@shared_task()
def notification_for_disbursements():
    disbursements = Disbursement.objects.filter(is_custom_workflow=True)
    for disbursement in disbursements:
        current_status = disbursement.get_current_status()
        next_rule = disbursement.get_next_rule()
        if current_status.get("status") == "SCHEDULED":
            send_notification_for_due_date_reminder(disbursement, next_rule)
        elif current_status.get("status") == "REJECTED":
            # SEND MAILS TO ALL LOWER LEVELS
            send_notification_for_rejection_update(disbursement, next_rule)
        elif (
            next_rule
            and next_rule.get("rule_type") in ["CHECKER", "APPROVER"]
            and current_status.get("status") != "REJECTED"
        ):
            send_notification_for_review_pending(
                disbursement, current_status, next_rule
            )
        elif next_rule and next_rule.get("next_status") == "PAYMENT_RELEASED":
            send_notification_to_release_payment(
                disbursement, current_status, next_rule
            )
        elif next_rule and next_rule.get("next_status") == "PAYMENT_RECEIVED":
            send_notification_for_payment_acknowledgement(
                disbursement, current_status, next_rule
            )


def send_notification_for_utilisation_verificaiton(utilisation, next_rule):
    rule_assignee_list = UtilisationWorkflowRuleAssignee.objects.filter(
        rule__id=next_rule.get("id"), project=utilisation.project
    )
    app_url = os.environ.get("LOGIN_URL")
    app_url = app_url + "/client/{}/project/{}/financials".format(
        utilisation.project.client.id, utilisation.project.id
    )
    for assignee in rule_assignee_list:
        due_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.NOTIFICATION_BEFORE_DUE_DATE_REMINDER
        )
        overdue_date_reminder_setting = get_user_notification_settings(
            assignee.user.user, settings_choices.ALERT_AFTER_OVERDUE_ALERT
        )
        if (
            due_date_reminder_setting.is_enabled
            or due_date_reminder_setting.is_email_enabled
            and utilisation.disbursement.end_date
            < datetime.datetime.now()
            + datetime.timedelta(days=due_date_reminder_setting.days)
        ):
            # SEND NOTIFICATION
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Review Pending Reminder for Utilisation!"
            notification_text = 'Reminder: Utilization "{}" associated with disbursement "{}" of the project "{}" needs to be verified'.format(
                utilisation.particulars,
                utilisation.disbursement.disbursement_name,
                utilisation.disbursement.project.get_name(),
            )
            platform_users = (
                [assignee.user.user] if due_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user]
                if due_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=1,
                action_user=None,
                action_performed_on_user=None,
                action_params={"utilisation_id": utilisation.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Utilisation",
                email_users=email_users,
                app_url=app_url,
            )
        if (
            overdue_date_reminder_setting.is_enabled
            or overdue_date_reminder_setting.is_email_enabled
            and utilisation.disbursement.end_date
            + datetime.timedelta(days=overdue_date_reminder_setting.days)
            >= datetime.datetime.now()
        ):
            # SEND ALERT
            notification_type = notification_choices.TYPE_DISBURSEMENT_DUE_DATE_REMINDER
            notification_title = "Alert: Review Pending for Utilisation."
            notification_text = 'Delay Alert: Utilization "{}" associated with disbursement "{}" of the project "" is pending to be verified.'.format(
                utilisation.particulars,
                utilisation.disbursement.disbursement_name,
                utilisation.disbursement.project.get_name(),
            )
            platform_users = (
                [assignee.user.user] if overdue_date_reminder_setting.is_enabled else []
            )
            email_users = (
                [assignee.user.user.username]
                if overdue_date_reminder_setting.is_email_enabled
                else []
            )
            Notification.create(
                notification_type,
                category=0,
                action_user=None,
                action_performed_on_user=None,
                action_params={"utilisation_id": utilisation.id},
                users_to_send_to=platform_users,
                title=notification_title,
                text=notification_text,
                notification_tag="Utilisation",
                email_users=email_users,
                app_url=app_url,
            )


@shared_task()
def notification_for_utilisation():
    utilisations = Utilisation.objects.filter(
        is_custom_workflow=True, disbursement__isnull=False
    )
    for utilisation in utilisations:
        current_status = utilisation.get_current_status()
        next_rule = utilisation.get_next_rule()
        if current_status.get("status") == "NEW_REQUEST":
            send_notification_for_utilisation_verificaiton(utilisation, next_rule)
    disbursements = Disbursement.objects.all().values_list("id", flat=True)
    # for disbursement in disbursements:
    #     has_utilisation = Utilisation.objects.filter(disbursement_id=disbursement).exists()
    #     if not has_utilisation:
    #         send_utilisation_creation


@shared_task()
def indicator_update_on_workflow(project_id):
    project = Project.objects.get(id=project_id)
    outputs = ProjectOutput.objects.filter(project=project)
    for output in outputs:
        output_status_list = ProjectOutputStatus.objects.filter(projectOutput=output)
        if len(output_status_list) == 0:
            # If object is not present, create one
            ProjectOutputStatus.objects.create(
                projectOutput=output,
                action_by=None,
                status="UNVERIFIED",
                rule=ImpactWorkflowRule.objects.get(
                    rule_type="REQUESTER", ruleset__projects__in=[project]
                ),
            )
    outcomes = ProjectOutcome.objects.filter(project=project)
    for outcome in outcomes:
        # Switch on workflow
        outcome_status_list = ProjectOutcomeStatus.objects.filter(
            projectOutcome=outcome
        )
        if len(outcome_status_list) == 0:
            ProjectOutcomeStatus.objects.create(
                projectOutcome=outcome,
                action_by=None,
                status="UNVERIFIED",
                rule=ImpactWorkflowRule.objects.get(
                    rule_type="REQUESTER", ruleset__projects__in=[project]
                ),
            )
    uploads = CSVUpload.objects.filter(project=project)
    for upload in uploads:
        upload_status_list = ImpactUploadStatus.objects.filter(impact_upload=upload)
        if len(upload_status_list) == 0:
            ImpactUploadStatus.objects.create(
                impact_upload=upload,
                action_by=None,
                status="UNVERIFIED",
                rule=ImpactWorkflowRule.objects.get(
                    rule_type="REQUESTER", ruleset__projects__in=[project]
                ),
            )


@shared_task()
def calculate_all_project_status():
    from v2.project.models import Project

    projects = Project.objects.all().values_list("id", flat=True)
    for project in projects:
        calculate_project_status(project)


@shared_task()
def calculate_project_status(project_id):
    from v2.project.models import Project

    project = Project.objects.get(id=project_id)
    milestones = project.get_milestones()
    milestone_count = milestones.count()
    project_status = 0
    for milestone in milestones:
        tasks = project.get_milestone_tasks(milestone)
        milestone_status = 0
        task_count = tasks.count()
        if (
            milestone.status == "Completed"
            or milestone.status == "Completed with Delay"
        ):
            milestone_status = 1
        else:
            for task in tasks:
                milestone_status += (1 / task_count) * (
                    task.percentage_completed / 100
                    if task.percentage_completed
                    else 0 / 100
                )
        project_status += (1 / milestone_count) * milestone_status
    project.project_status = round(project_status * 100, 2)
    project.save()


@shared_task()
def calculate_impact_data_status(template_id):
    template = Template.objects.get(id=template_id)
    batches = {}
    csv_uploads = CSVUpload.objects.filter(
        template=template, is_deleted=False
    ).order_by("created_date")
    if csv_uploads.count() == 0:
        template.data_status = "Unverified"
    else:
        for csv_upload in csv_uploads:
            data_verified = True
            next_rule = csv_upload.get_impact_upload_next_rule()
            if (
                next_rule is not None
                and next_rule.__contains__("id")
                and next_rule["id"] is not None
            ):
                template.data_status = "Unverified"
                data_verified = False
                break
        if data_verified:
            template.data_status = "Verified"
    template.save(activity_log=True)


@shared_task()
def calculate_output_data_status(indicator_id):
    indicator = ProjectIndicator.objects.get(id=indicator_id)
    batches = {}
    outputs = ProjectOutput.objects.filter(project_indicator=indicator).order_by(
        "created_date"
    )
    if outputs.count() == 0:
        indicator.output_status = "Unverified"
    else:
        for output in outputs:
            data_verified = True
            next_rule = output.get_output_next_rule()
            if (
                next_rule is not None
                and next_rule.__contains__("id")
                and next_rule["id"] is not None
            ):
                indicator.output_status = "Unverified"
                data_verified = False
                break
        if data_verified:
            indicator.output_status = "Verified"
    indicator.save(activity_log=True)


@shared_task()
def calculate_outcome_data_status(indicator_id):
    indicator = ProjectIndicator.objects.get(id=indicator_id)
    batches = {}
    outcomes = ProjectOutcome.objects.filter(project_indicator=indicator).order_by(
        "created_date"
    )
    if outcomes.count() == 0:
        indicator.outcome_status = "Unverified"
    else:
        for outcome in outcomes:
            data_verified = True
            next_rule = outcome.get_outcome_next_rule()
            if (
                next_rule is not None
                and next_rule.__contains__("id")
                and next_rule["id"] is not None
            ):
                indicator.outcome_status = "Unverified"
                data_verified = False
                break
        if data_verified:
            indicator.outcome_status = "Verified"
    indicator.save(activity_log=True)


@shared_task()
def calculate_all_project_total_and_planned_disbursement_amount():
    from v2.project.models import Project

    projects = Project.objects.all().values_list("id", flat=True)
    for project in projects:
        calculate_total_and_planned_disbursement_amount(project)


@shared_task()
def calculate_total_and_planned_disbursement_amount(project_id):
    from v2.project.models import Project, Utilisation
    from django.db.models.aggregates import Sum

    project = Project.objects.get(id=project_id)
    standalone_utilization_amount = 0
    total_utilised_amount = 0
    disbursements = project.get_all_disbursements()
    project.total_disbursement_amount = (
        disbursements.aggregate(Sum("actual_amount")).get("actual_amount__sum", 0)
        if disbursements
        else 0
    ) or 0
    project.planned_disbursement_amount = (
        disbursements.aggregate(Sum("expected_amount")).get("expected_amount__sum", 0)
        if disbursements
        else 0
    ) or 0
    standalone_utilization_amount = (
        Utilisation.objects.filter(disbursement=None, project=project)
        .aggregate(Sum("actual_cost"))
        .get("actual_cost__sum", 0)
        or 0
    )
    for disbursement in disbursements:
        total_utilised_amount += disbursement.get_overall_utilistation_total()
    project.total_utilised_amount = (
        total_utilised_amount + standalone_utilization_amount
    )
    project.save()
