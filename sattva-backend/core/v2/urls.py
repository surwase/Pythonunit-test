from django.conf.urls import include, url

app_name = "client"

urlpatterns = [
    url(r'^client/', include("v2.client.urls", namespace="client")),
    url(r'^project/', include("v2.project.urls", namespace="projects")),
    url(r'^plant/', include("v2.plant.urls", namespace="plants")),
    url(r'^users/', include("v2.user_management.urls", namespace="users")),
    url(r'^risk-management/', include("v2.risk_management.urls", namespace="risk_management")),
    url(r'^notification/', include("v2.notification.urls", namespace="notification")),
    url(r'^governance/', include("v2.governance.urls", namespace="governance")),
    url(r'^ngo/', include("v2.ngo.urls", namespace="ngo")),
    url(r'^help/', include("v2.help.urls", namespace="help")),
    url(r'^kobo/', include("v2.kobo.urls", namespace="kobo")),
    url(r'^domain_libraries/', include("v2.domain_libraries.urls", namespace="domain_libraries")),
]
