
from django.db.models.query_utils import Q
import django_filters
from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.views import APIView
from v2.help.models import HelpSections, HelpSubSections, HelpArticles, VideoDetails
from v2.help.serializers import HelpSectionsSerializer, HelpSubSectionDetailSerializer, HelpArticleSerializer, \
    HelpArticleListSerializer, VideoDetailSerializer
from rest_framework import viewsets
from v2.authentication import KeycloakUserUpdateAuthentication


"""
   Viewset for help section allows read operation
"""
class HelpSectionModelViewSet(viewsets.ModelViewSet):
    serializer_class = HelpSectionsSerializer
    queryset = HelpSections.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    authentication_classes = [KeycloakUserUpdateAuthentication]


"""
   filter for HelpSubSections filters section by exact id and exact section__section_name
"""
class HelpSubSectionFilter(django_filters.FilterSet):
    class Meta:
        model = HelpSubSections
        fields = {
            'section': ['exact'],
            'section__section_name': ['exact'],
        }

"""
   Viewset for Help SubSection Detail allows read operation
"""
class HelpSubSectionModelViewSet(viewsets.ModelViewSet):
    serializer_class = HelpSubSectionDetailSerializer
    queryset = HelpSubSections.objects.all().order_by('level')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = HelpSubSectionFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]


"""
   Viewset for Help Article allows read operation
"""
class HelpArticleModelViewSet(viewsets.ModelViewSet):
    serializer_class = HelpArticleSerializer
    queryset = HelpArticles.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Api view for Search Help Article
"""
class SearchHelpArticleAPIView(APIView):
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request):
        """
        This function is used for making get request for search help articles.
        """
        user_role = request.user.application_role.role_name
        search_word = request.GET.get('search')
        help_articles = HelpArticles.objects.all()
        if user_role != 'sattva-admin':
            help_articles = help_articles.filter(sub_section__roles__icontains=user_role)
        if search_word is not None:
            help_articles = help_articles.filter(Q(content__icontains=search_word) | Q(title__icontains=search_word))
        serializer = HelpArticleListSerializer(help_articles, many=True)
        return Response(serializer.data)

"""
   Api view for Search Help Video
"""
class SearchHelpVideoAPIView(APIView):
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request):
        """
        This function is used for making get request for search help videos.
        """
        user_role = request.user.application_role.role_name
        search_word = request.GET.get('search')
        videos = VideoDetails.objects.all()
        if user_role != 'sattva-admin' :
            videos = videos.filter(sub_section__roles__icontains=user_role)
        if search_word is not None:
            videos = videos.filter(Q(description__icontains=search_word) | Q(title__icontains=search_word))
        serializer = VideoDetailSerializer(videos, many=True)
        return Response(serializer.data)
