from django.contrib.auth.models import User
from v2.ngo.models import NGOPartner
from v2.project.models import Project
from v2.ngo.models import NGOPartner


def add_ngos():
    """
    SCRIPT TO ADD NGOs
    """
    ngos = [
        "Sehgal Foundation",
        "Collective Good Foundation (CGF)",
        "Piramal Sarvajal",
        "Wockhardt Foundation",
        "Akshaya Patra Foundation",
        "Sir Syed Trust",
        "SRIJAN (Self Reliant Initiative through Joint Action)",
        "AFPRO (Action for Food Production)",
        "Youth Dreamers Foundation (YDF)",
        "IIM- Calcutta Innovation Park",
        "SAHE",
        "Smile Foundation",
        "Mary Kom Regional Boxing Foundation (MKRBF)",
        "Mushroom Development Foundation (MDF)",
        "Dentsu Inc",
        "Anthill Creations",
        "Kendriya Sainik Board",
        "Saahas",
        "Bala Vikasa",
        "Drishti Foundation",
        "Next Education Pvt. Ltd.",
        "IIIT- Hyderabad",
        "SAFE",
        "Genesis Foundation",
        "Rasta Peth Education Society",
        "NIIT Foundation",
        "Sarthak Educational Trust",
        "Yuvraj Singh Foundation",
        "CYSD",
        "Evolutionary",
        "Sahayam Charitable Trust",
        "Ambuja Cement Foundation",
        "SwitchOn Foundation",
        "Enable India",
        "CDF (Connecting Dreams Foundation)",
        "Connecting Dreams Foundation CDF)",
        "BuddyStudy",
        "Vitor",
        "Sukhibhava",
    ]

    for ngo in ngos:
        NGOPartner.objects.create(ngo_name=ngo)


def map_ngo_partner_to_hardcoded_value():
    projects = Project.objects.all()

    for project in projects:
        if project.partner and project.partner != 0:
            project.ngo_partner = NGOPartner.objects.get(id=project.partner)
            project.save()


def old_v2_project_elements_to_new_table():
    from v2.plant.models import (
        Plant,
        PlantFocusArea,
        PlantLocation,
        PlantSubFocusArea,
        PlantTargetSegment,
        PlantSubTargetSegment,
        PlantSDGGoals,
    )
    from v2.project.models import (
        Project,
        ProjectSDGGoals,
        ProjectSubTargetSegment,
        ProjectSubFocusArea,
        ProjectFocusArea,
        ProjectLocation,
        ProjectTargetSegment,
    )

    projects = Project.objects.all()
    for project in projects:
        focus_area = project.old_focus_area
        if focus_area:
            existing_objects = ProjectFocusArea.objects.filter(project=project)
            existing_objects.delete()
            ProjectFocusArea.objects.create(project=project, focus_area=focus_area)
        locations = project.old_location
        if locations:
            existing_objects = ProjectLocation.objects.filter(project=project)
            existing_objects.delete()
            for location in locations:
                ProjectLocation.objects.create(project=project, location=location)
        sub_focus_area = project.old_sub_focus_area
        if sub_focus_area:
            existing_objects = ProjectSubFocusArea.objects.filter(project=project)
            existing_objects.delete()
            for s_f_a in sub_focus_area:
                ProjectSubFocusArea.objects.create(
                    project=project, sub_focus_area=s_f_a
                )
        target_segment = project.old_target_segment
        if target_segment:
            existing_objects = ProjectTargetSegment.objects.filter(project=project)
            existing_objects.delete()
            ProjectTargetSegment.objects.create(
                project=project, target_segment=target_segment
            )
        sub_target_segment = project.old_sub_target_segment
        if sub_target_segment:
            existing_objects = ProjectSubTargetSegment.objects.filter(project=project)
            existing_objects.delete()
            ProjectSubTargetSegment.objects.create(
                project=project, sub_target_segment=sub_target_segment
            )
        sdg_goals = project.old_sdg_goals
        if sdg_goals:
            existing_objects = ProjectSDGGoals.objects.filter(project=project)
            existing_objects.delete()
            for sdg_goal in sdg_goals:
                ProjectSDGGoals.objects.create(project=project, sdg_goal=sdg_goal)

    plants = Plant.objects.all()
    for plant in plants:
        focus_area = plant.old_focus_area
        if focus_area:
            existing_objects = PlantFocusArea.objects.filter(plant=plant)
            existing_objects.delete()
            PlantFocusArea.objects.create(plant=plant, focus_area=focus_area)
        locations = plant.old_location
        if locations:
            existing_objects = PlantLocation.objects.filter(plant=plant)
            existing_objects.delete()
            for location in locations:
                PlantLocation.objects.create(plant=plant, location=location)
        sub_focus_area = plant.old_sub_focus_area
        if sub_focus_area:
            existing_objects = PlantSubFocusArea.objects.filter(plant=plant)
            existing_objects.delete()
            for s_f_a in sub_focus_area:
                PlantSubFocusArea.objects.create(plant=plant, sub_focus_area=s_f_a)
        target_segment = plant.old_target_segment
        if target_segment:
            existing_objects = PlantTargetSegment.objects.filter(plant=plant)
            existing_objects.delete()
            PlantTargetSegment.objects.create(
                plant=plant, target_segment=target_segment
            )
        sub_target_segment = plant.old_sub_target_segment
        if sub_target_segment:
            existing_objects = PlantSubTargetSegment.objects.filter(plant=plant)
            existing_objects.delete()
            PlantSubTargetSegment.objects.create(
                plant=plant, sub_target_segment=sub_target_segment
            )


def create_batch_id_for_old_evaluation():
    from v2.client.models import ClientComplianceEvaluation

    compliance_evaluation = ClientComplianceEvaluation.objects.all()
    for comp in compliance_evaluation:
        comp.batch_id = (
            "C_" + str(comp.client.id) + "B_" + comp.date_captured.strftime("%s")
        )
        comp.save()


def add_goal_name_in_project_sdg():
    from v2.project.models import ProjectSDGGoals

    goals = ProjectSDGGoals.objects.all()
    for goal in goals:
        if not goal.sdg_goal:
            continue
        goal.sdg_goal_name = goal.sdg_goal.get("name", "")
        goal.save()


def collect_location_for_project():
    from v2.project.models import ProjectLocation

    locations = ProjectLocation.objects.all()
    for location in locations:
        if location.location.get("details"):
            location_details = location.location["details"]
            location.latitude = location_details["geometry"]["location"]["lat"]
            location.longitude = location_details["geometry"]["location"]["lng"]
            for component in location_details["address_components"]:
                if (
                    "political" in component["types"]
                    and "locality" in component["types"]
                ):
                    location.area = component["long_name"]
                elif (
                    "political" in component["types"]
                    and "administrative_area_level_2" in component["types"]
                ):
                    location.city = component["long_name"]
                elif (
                    "political" in component["types"]
                    and "administrative_area_level_1" in component["types"]
                ):
                    location.state = component["long_name"]
                elif (
                    "political" in component["types"]
                    and "country" in component["types"]
                ):
                    location.country = component["long_name"]
            if not location.city:
                location.city = location.area
            if not location.area:
                location.area = location.city
            location.location["area"] = location.area
            location.location["city"] = location.city
            location.location["state"] = location.state
            location.location["country"] = location.country
            location.location["latitude"] = location.latitude
            location.location["longitude"] = location.longitude
            print("area  ", location.area)
            print("city  ", location.city)
            print("state  ", location.state)
            print("country  ", location.country)
            print("latitude  ", location.latitude)
            print("longitude  ", location.longitude)
            location.save()
        else:
            print(location.id)
            location.delete()


def collect_location_for_plant():
    from v2.plant.models import PlantLocation

    locations = PlantLocation.objects.all()
    for location in locations:
        print("\n" * 5)
        print(location.location)
        if location.location.get("details"):
            location_details = location.location["details"]
            location.latitude = location_details["geometry"]["location"]["lat"]
            location.longitude = location_details["geometry"]["location"]["lng"]
            for component in location_details["address_components"]:
                if (
                    "political" in component["types"]
                    and "locality" in component["types"]
                ):
                    location.area = component["long_name"]
                elif (
                    "political" in component["types"]
                    and "administrative_area_level_2" in component["types"]
                ):
                    location.city = component["long_name"]
                elif (
                    "political" in component["types"]
                    and "administrative_area_level_1" in component["types"]
                ):
                    location.state = component["long_name"]
                elif (
                    "political" in component["types"]
                    and "country" in component["types"]
                ):
                    location.country = component["long_name"]
            if not location.city:
                location.city = location.area
            if not location.area:
                location.area = location.city
            location.location["area"] = location.area
            location.location["city"] = location.city
            location.location["state"] = location.state
            location.location["country"] = location.country
            location.location["latitude"] = location.latitude
            location.location["longitude"] = location.longitude
            print("area  ", location.area)
            print("city  ", location.city)
            print("state  ", location.state)
            print("country  ", location.country)
            print("latitude  ", location.latitude)
            print("longitude  ", location.longitude)
            location.save()
        else:
            print(location.id)
            location.delete()


def populate_period_end_date():
    from v2.project.models import ProjectOutputOutcome

    indicators = ProjectOutputOutcome.objects.all()
    indicator_batches = indicators.values_list("indicator_batch", flat=True).distinct()
    for indicator_batch in indicator_batches:
        filtered_indicators = indicators.filter(
            indicator_batch=indicator_batch
        ).order_by("scheduled_date")
        for i in range(0, len(filtered_indicators)):
            if i < len(filtered_indicators) - 1:
                filtered_indicators[i].period_end_date = filtered_indicators[
                    i + 1
                ].scheduled_date
                filtered_indicators[i].save()
            else:
                if filtered_indicators[i].milestone:
                    filtered_indicators[i].period_end_date = filtered_indicators[
                        i
                    ].milestone.end_date
                else:
                    filtered_indicators[i].period_end_date = filtered_indicators[
                        i
                    ].project.end_date
                if (
                    filtered_indicators[i].period_end_date.date()
                    < filtered_indicators[i].scheduled_date
                ):
                    filtered_indicators[i].period_end_date = filtered_indicators[
                        i
                    ].scheduled_date
                filtered_indicators[i].save()
        print("\n")


def populate_period_end_date_1():
    from v2.project.models import ProjectOutputOutcome

    indicators = ProjectOutputOutcome.objects.all()
    indicator_batches = indicators.values_list("indicator_batch", flat=True).distinct()
    for indicator_batch in indicator_batches:
        filtered_indicators = indicators.filter(
            indicator_batch=indicator_batch
        ).order_by("scheduled_date")
        for i in range(0, len(filtered_indicators)):
            if (
                filtered_indicators[i].scheduled_date
                > filtered_indicators[i].period_end_date
            ):
                print(filtered_indicators[i].indicator_batch)
                print(filtered_indicators[i].scheduled_date)
                print(filtered_indicators[i].period_end_date)
                print(filtered_indicators[i].frequency)
                print("\n")
        print("\n")


def check_user_client_plants_projects():
    from core.users.models import User
    from v2.project.models import ProjectUser
    from v2.client.models import ClientUser
    from v2.plant.models import PlantUser

    users = User.objects.all()
    for user in users:
        projects = ProjectUser.objects.filter(user=user)
        for project in projects:
            if not ClientUser.objects.filter(
                client=project.project.client, user=user
            ).exists():
                print(user.username)
                print("project")
                print(project.project)
                print(project.project.client)
            if (
                project.project.plant
                and not PlantUser.objects.filter(
                    plant=project.project.plant, user=user
                ).exists()
            ):
                print(user.username)
                print("plant")
                print(project.project.plant)
                print(project.project.client)
                print(project.project)
        plants = PlantUser.objects.filter(user=user)
        for plant in plants:
            if not ClientUser.objects.filter(
                client=plant.plant.client, user=user
            ).exists():
                print(user.username)
                print("plantppppp")
                print(plant.plant)
                print(plant.plant.client)


def check_tasks_client_assignee():
    from v2.project.models import Task

    tasks = Task.objects.filter(project=None)
    print(tasks)
    for task in tasks:
        print(task.assignee)


def map_milestone_status():
    from v2.project.models import Milestone

    status_map = {
        "Open": "Pending",
        "Closed": "Completed",
        "Suspended": "Not Started",
        "Reopened": "Pending",
    }
    milestones = Milestone.objects.filter(
        status__in=["Open", "Closed", "Suspended", "Reopened"]
    )
    for milestone in milestones:
        print(milestone.status)
        print(status_map[milestone.status])
        milestone.status = status_map[milestone.status]
        print("*" * 100)
        milestone.save()


def map_tasks_to_client():
    from v2.project.models import Task

    tasks = Task.objects.filter(client=None)
    for task in tasks:
        if task.project:
            task.client = task.project.client
            task.save()
        elif task.milestone:
            task.client = task.milestone.project.client
            task.save()


def check_task_completion():
    from v2.project.models import Task

    tasks = Task.objects.all()
    for task in tasks:
        if task.status == "Completed" and task.percentage_completed != 100:
            task.percentage_completed = 100
            task.save()


def map_clients_to_ngo():
    from v2.ngo.models import NGOPartner
    from v2.project.models import Project

    ngo_list = NGOPartner.objects.all()
    for ngo in ngo_list:
        clients = Project.objects.filter(ngo_partner=ngo).values_list(
            "client", flat=True
        )
        clients = list(set(clients))
        ngo.clients.add(*clients)


def map_user_to_ngo():
    from core.users.models import User
    from v2.project.models import ProjectUser, Project

    partner_users = User.objects.filter(application_role__role_name="ngo-user")
    for user in partner_users:
        projects = ProjectUser.objects.filter(user=user).values_list(
            "project", flat=True
        )
        print(projects)
        if user.ngo:
            print(user.username)
            continue
        for project in projects:
            p = Project.objects.get(id=project)
            if p.ngo_partner:
                user.ngo = p.ngo_partner
                user.save()
                break


def map_risks_to_plant():
    from v2.risk_management.models import RiskEntry

    risks = RiskEntry.objects.all()
    for risk in risks:
        if risk.project and risk.project.plant:
            risk.plant = risk.project.plant
            risk.save()


def delete_old_remarks():
    from v2.project.models import ProjectRemark

    remarks = ProjectRemark.objects.filter(qualitative_outcome="")
    remarks.delete()


def update_batch_id_for_all_tables():
    from v2.project.models import Template
    from v2.project.views import (
        get_parent_table_name,
        check_if_table_exists,
        connection,
    )

    templates = Template.objects.all()
    for template in templates:
        if not template.project:
            print(table_name)
            print("no table")
            continue
        table_name = get_parent_table_name(
            template.project.client.id, template.project.id, template.id
        )
        if check_if_table_exists(table_name):
            sql = "update " + table_name + " SET batch_id=1"
            try:
                with connection.cursor() as cursor:
                    cursor.execute(sql)
                    print("executed")
                    print(table_name)
                    value = 0
                    for row in cursor.fetchall():
                        value = row[0]
                    if value == 1:
                        print(table_name)
                        print("done")
                    else:
                        print(table_name)
                        print("not done")
            except Exception as error:
                print(error)
                print(table_name)
                print("not done")


def change_disbursement_status_to_client_approved():
    from v2.project.models import Disbursement

    disb = Disbursement.objects.filter(status="APPROVED")
    disb.update(status="CLIENT_APPROVED")


def change_disbursement_status_to_scheduled():
    from v2.project.models import Disbursement

    disb = Disbursement.objects.filter(status="REQUEST_PENDING")
    disb.update(status="SCHEDULED")


def transfer_tags_from_task():
    from v2.project.models import Task

    tasks = Task.objects.all()
    for task in tasks:
        task.tags = [task.task_tag] if task.task_tag else []
        task.save()


def delete_blank_tags():
    from v2.project.models import ProjectTag

    tags = ProjectTag.objects.filter(tag="")
    for tag in tags:
        if not tag.tag or tag.tag == "" or tag.tag == " ":
            tag.delete()


def remove_blank_task_tags():
    from v2.project.models import Task
    task_with_blank_tags = Task.objects.filter(tags__contains=[""])
    task_with_blank_tags.update(tags=[])

    tasks = Task.objects.filter(client=None)
    for task in tasks:
        task.client = task.project.client
        task.save()

def create_default_client_settings():
    from v2.client.models import Client, ClientSettings

    clients = Client.objects.all()
    for client in clients:
        ClientSettings.objects.update_or_create(
            client=client,
            defaults={"disbursement_workflow": True, "utilisation_workflow": True},
        )


"""
UPDATE project_disbursement
SET status = 'SCHEDULED'
WHERE status='REQUEST_PENDING';
UPDATE project_disbursement
SET status = 'NEW_REQUEST'
WHERE status='REQUESTED';
UPDATE project_disbursement
SET status = 'APPROVED'
WHERE status='SATTVA_APPROVED';

UPDATE project_disbursement
SET status = 'APPROVED'
WHERE status='CLIENT_APPROVED';

UPDATE project_disbursement
SET status = 'NEW_REQUEST'
WHERE status='REQUESTED';

"""


def check_sub_target_segment_for_milestone():
    from v2.project.models import Milestone

    milestones = Milestone.objects.all()
    for milestone in milestones:
        if type(milestone.sub_target_segment) == list:
            milestone.sub_target_segment = (
                milestone.sub_target_segment[0]
                if len(milestone.sub_target_segment)
                else ""
            )
            milestone.save()


def disable_emails_for_all():
    from v2.client.models import UserNotificationSettings

    settings = UserNotificationSettings.objects.all()
    settings.update(is_email_enabled=False)


def old_indicator_elements_to_new_table():
    from v2.project.models import (
        ProjectOutputOutcome,
        ProjectIndicator,
        ProjectOutput,
        ProjectOutcome,
        ProjectOutputStatus,
        ProjectOutcomeStatus,
        IndicatorOutcome,
        ProjectIndicatorOutcome,
    )

    existing_objects = ProjectIndicator.objects.all()
    existing_objects.delete()
    existing_objects = ProjectOutput.objects.all()
    existing_objects.delete()
    existing_objects = ProjectOutcome.objects.all()
    existing_objects.delete()
    existing_objects = ProjectIndicatorOutcome.objects.all()
    existing_objects.delete()

    indicator_batches = ProjectOutputOutcome.objects.values_list(
        "indicator_batch", flat=True
    ).distinct()

    for indicator_batch in indicator_batches:
        projectOutputOutcomes = ProjectOutputOutcome.objects.filter(
            indicator_batch=indicator_batch
        )
        output_indicator = projectOutputOutcomes[0].output_indicator
        outcome_indicator = projectOutputOutcomes[0].outcome_indicator
        frequency = projectOutputOutcomes[0].frequency
        output_calculation = projectOutputOutcomes[0].output_calculation
        outcome_calculation = projectOutputOutcomes[0].outcome_calculation
        milestone = projectOutputOutcomes[0].milestone
        project = projectOutputOutcomes[0].project
        indicator_outcomes = []
        indicator_outcomes = IndicatorOutcome.objects.filter(
            indicator_batch=indicator_batch
        )
        for indicator_outcome in indicator_outcomes:
            outcome = indicator_outcome.outcome
            outcome_type = indicator_outcome.outcome_type
            ProjectIndicatorOutcome.objects.create(
                project_indicator=project_indicator,
                outcome=outcome,
                outcome_type=outcome_type,
            )
        if frequency and project:
            project_indicator = ProjectIndicator.objects.create(
                project=project,
                frequency=frequency,
                output_indicator=output_indicator,
                outcome_indicator=outcome_indicator,
                output_calculation=output_calculation,
                outcome_calculation=outcome_calculation,
                milestone=milestone,
            )
            for projectOutputOutcome in projectOutputOutcomes:
                planned_output = projectOutputOutcome.planned_output
                actual_output = projectOutputOutcome.actual_output
                planned_outcome = projectOutputOutcome.planned_outcome
                actual_outcome = projectOutputOutcome.actual_outcome
                scheduled_date = projectOutputOutcome.scheduled_date
                period_end_date = projectOutputOutcome.period_end_date
                is_included = projectOutputOutcome.is_included
                period_name = projectOutputOutcome.period_name
                ProjectOutput.objects.create(
                    project_indicator=project_indicator,
                    planned_output=planned_output,
                    actual_output=actual_output,
                    is_included=is_included,
                    project=project,
                    scheduled_date=scheduled_date,
                    period_end_date=period_end_date,
                    period_name=period_name,
                )
                ProjectOutcome.objects.create(
                    project_indicator=project_indicator,
                    planned_outcome=planned_outcome,
                    actual_outcome=actual_outcome,
                    is_included=is_included,
                    project=project,
                    scheduled_date=scheduled_date,
                    period_end_date=period_end_date,
                    period_name=period_name,
                )


def calculate_project_status():
    from v2.project.models import Project

    projects = Project.objects.all()
    for project in projects:
        milestones = project.get_milestones()
        milestone_count = milestones.count()
        project_status = 0
        for milestone in milestones:
            tasks = project.get_milestone_tasks(milestone)
            milestone_status = 0
            task_count = tasks.count()
            if milestone.status == "Completed":
                milestone_status = 1
            else:
                for task in tasks:
                    milestone_status += (1 / task_count) * (
                        task.percentage_completed / 100
                        if task.percentage_completed
                        else 0 / 100
                    )
            project_status += (1 / milestone_count) * milestone_status
        project.project_status = round(project_status * 100, 2)
        project.save()


def calculate_impact_data_status():
    from v2.project.models import Template, CSVUpload

    templates = Template.objects.all()
    for template in templates:
        csv_uploads = CSVUpload.objects.filter(
            template=template, is_deleted=False
        ).order_by("created_date")
        data_verified = True
        if csv_uploads.count() == 0:
            template.data_status = "Unverified"
        else:
            for csv_upload in csv_uploads:
                next_rule = csv_upload.get_impact_upload_next_rule()
                if (
                    next_rule is not None
                    and next_rule.__contains__("id")
                    and next_rule["id"] is not None
                ):
                    template.data_status = "Unverified"
                    data_verified = False
            if data_verified:
                template.data_status = "Verified"
        template.save(activity_log=True)


def calculate_output_data_status():
    from v2.project.models import ProjectIndicator, ProjectOutput, ProjectOutcome

    indicators = ProjectIndicator.objects.all()
    for indicator in indicators:
        outputs = ProjectOutput.objects.filter(project_indicator=indicator).order_by(
            "created_date"
        )
        data_verified = True
        if outputs.count() == 0:
            indicator.output_status = "Unverified"
        else:
            for output in outputs:
                next_rule = output.get_output_next_rule()
                if (
                    next_rule is not None
                    and next_rule.__contains__("id")
                    and next_rule["id"] is not None
                ):
                    indicator.output_status = "Unverified"
                    data_verified = False
                    break
            if data_verified:
                indicator.output_status = "Verified"

        outcomes = ProjectOutcome.objects.filter(project_indicator=indicator).order_by(
            "created_date"
        )
        data_verified = True
        if outcomes.count() == 0:
            indicator.outcome_status = "Unverified"
        else:
            for outcome in outcomes:
                next_rule = outcome.get_outcome_next_rule()
                if (
                    next_rule is not None
                    and next_rule.__contains__("id")
                    and next_rule["id"] is not None
                ):
                    indicator.outcome_status = "Unverified"
                    data_verified = False
                    break
            if data_verified:
                indicator.outcome_status = "Verified"
        indicator.save(activity_log=True)


def update_position_milestone():
    from v2.project.models import Milestone, Project

    projects = Project.objects.all()
    for project in projects:
        milestones = Milestone.objects.filter(project=project).order_by(
            "start_date", "milestone_name"
        )
        i = 1
        for milestone in milestones:
            milestone.position = i
            i = i + 1
            milestone.save()


def update_position_disbursement():
    from v2.project.models import Disbursement, Project

    projects = Project.objects.all()
    for project in projects:
        disbursements = Disbursement.objects.filter(project=project).order_by(
            "start_date", "disbursement_name"
        )
        i = 1
        for disbursement in disbursements:
            disbursement.position = i
            i = i + 1
            disbursement.save(activity_log=True)


def update_position_indicators():
    from v2.project.models import ProjectIndicator

    projects = Project.objects.all()
    for project in projects:
        indicators = ProjectIndicator.objects.filter(project=project).order_by(
            "milestone"
        )
        i = 1
        for indicator in indicators:
            indicator.position = i
            i = i + 1
            indicator.save()


def update_position_task():
    from v2.project.models import Task, Project, Milestone

    projects = Project.objects.all()
    for project in projects:
        milestones = Milestone.objects.filter(project=project)
        for milestone in milestones:
            i = 1
            tasks = Task.objects.filter(milestone=milestone).order_by(
                "start_date", "task_name"
            )
            for task in tasks:
                task.position = i
                i = i + 1
                task.save()
        standalone_tasks = Task.objects.filter(project=project, milestone__isnull=True)
        i = 1
        for task in standalone_tasks:
            task.position = i
            i = i + 1
            task.save()


def update_position_utilisation():
    from v2.project.models import Disbursement, Project, Utilisation

    projects = Project.objects.all()
    for project in projects:
        disbursements = Disbursement.objects.filter(project=project)
        for disbursement in disbursements:
            i = 1
            utilisations = Utilisation.objects.filter(
                disbursement=disbursement
            ).order_by("start_date", "particulars")
            for utilisation in utilisations:
                utilisation.position = i
                i = i + 1
                utilisation.save()
        standalone_utilisations = Utilisation.objects.filter(
            project=project, disbursement__isnull=True
        )
        i = 1
        for utilisation in standalone_utilisations:
            utilisation.position = i
            i = i + 1
            utilisation.save()


# Get checklist in csv file
def get_checklist():
    from domainlibraries.models import CSRComplianceCheckList
    import csv
    import json

    cols = [
        "area",
        "indicator",
        "document_required",
        "updated_by_username",
        "created_by",
        "created_by_username",
        "updated_date",
        "created_date",
        "updated_by",
    ]
    from django.http import JsonResponse
    from django.core import serializers

    csr_checklist = CSRComplianceCheckList.objects.all()
    json_list = []
    data = serializers.serialize("json", csr_checklist)
    json_data = json.loads(data)
    for row in json_data:
        for key in row:
            if key == "fields":
                json_list.append(row[key])
    print(json_list)
    path = "/home/diksha/Documents/checklist.csv"
    with open(path, "w") as f:
        wr = csv.DictWriter(f, fieldnames=cols)
        wr.writeheader()
        wr.writerows(json_list)


def update_project_permission():
    from v2.user_management.models import ProjectFeaturePermission, ProjectFeature
    from core.users.models import User
    from v2.project.models import ProjectUser, Project

    users = User.objects.all()

    ProjectFeature.objects.create(
        default_permissions={
            "csr-user": ["View", "Change", "Delete"],
            "ngo-user": ["View", "Change", "Delete"],
            "csr-admin": ["View", "Change", "Delete"],
            "sattva-user": ["View", "Change", "Delete"],
        },
        feature_code="PROJECT_INSIGHTS",
        feature_name="Project Insights",
        applicable_roles=["sattva-user", "csr-admin", "csr-user", "ngo-user"],
    )
    for user in users:
        projects = ProjectUser.objects.filter(user=user)
        projects = projects.values_list("project__id", flat=True)
        for project in projects:
            project_obj = Project.objects.get(id=project)
            features = ProjectFeaturePermission.objects.filter(
                project=project_obj, feature="PROJECT_INSIGHTS"
            )
            user.project_feature_permission.add(*list(features))
        user.save()


def update_client_permission():
    from v2.user_management.models import ClientFeaturePermission, ClientFeature
    from core.users.models import User
    from v2.client.models import ClientUser, Client

    users = User.objects.all()
    ClientFeature.objects.create(
        default_permissions={
            "csr-user": ["View", "Change", "Delete"],
            "ngo-user": [],
            "csr-admin": ["View", "Change", "Delete"],
            "sattva-user": ["View", "Change", "Delete"],
        },
        feature_code="PORTFOLIO_INSIGHTS",
        feature_name="Portfolio Insights",
        applicable_roles=["sattva-user", "csr-admin", "csr-user"],
    )

    ClientFeature.objects.create(
        default_permissions={
            "csr-user": ["View", "Change", "Delete"],
            "ngo-user": [],
            "csr-admin": ["View", "Change", "Delete"],
            "sattva-user": ["View", "Change", "Delete"],
        },
        feature_code="PROGRAM_INSIGHTS",
        feature_name="Program Insights",
        applicable_roles=["sattva-user", "csr-admin", "csr-user"],
    )

    for user in users:
        clients = Client.objects.filter(
            id__in=ClientUser.objects.filter(user=user).values_list("client", flat=True)
        ).values_list("id", flat=True)
        user_permissions = []
        for client in clients:
            client_obj = Client.objects.get(id=client)
            client_permissions = ClientFeaturePermission.objects.filter(
                client=client_obj, feature="PORTFOLIO_INSIGHTS"
            )
            for permission in client_permissions:
                allowed_permissions = permission.feature.default_permissions.get(
                    user.application_role.role_name, []
                )
                if permission.permission in allowed_permissions:
                    user_permissions.append(permission)
            client_permissions = ClientFeaturePermission.objects.filter(
                client=client_obj, feature="PROGRAM_INSIGHTS"
            )
            for permission in client_permissions:
                allowed_permissions = permission.feature.default_permissions.get(
                    user.application_role.role_name, []
                )
                if permission.permission in allowed_permissions:
                    user_permissions.append(permission)
        user.client_feature_permission.add(*list(user_permissions))
        user.save()


def create_help_sections_subsections():
    from v2.help.models import HelpSections, HelpSubSections

    sections = [
        {
            "heading": "Getting Started",
            "path": "getting-started",
            "subsections": [
                {
                    "title": "Shift Intro",
                    "code": "SHIFT_INTRO",
                    "path": "shift-intro",
                    "link": "#shift-intro",
                    "head": False,
                    "roles": ["csr-admin", "csr-user", "ngo-user", "sattva-user"],
                },
                {
                    "title": "Consultant Quick Start guide",
                    "code": "CONSULTANT_QUICK_START_GUIDE",
                    "path": "consultant-start-guide",
                    "link": "#consultant-start-guide",
                    "head": False,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "Partner Quick Start guide",
                    "code": "PARTNER_QUICK_START_GUIDE",
                    "path": "partner-start-guide",
                    "link": "#partner-start-guide",
                    "head": False,
                    "roles": ["ngo-user"],
                },
                {
                    "title": "Client Quick Start guide",
                    "code": "CLIENT_QUICK_START_GUIDE",
                    "path": "client-start-guide",
                    "link": "#client-start-guide",
                    "head": False,
                    "roles": ["csr-admin", "csr-user", "client-user"],
                },
            ],
        },
        {
            "heading": "Video Tutorial",
            "path": "video-tutorial",
            "subsections": [
                {
                    "title": "Home",
                    "code": "VIDEO_TUTORIAL_HOME",
                    "path": "home",
                    "link": "#home",
                    "head": True,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Portfolio Overview",
                    "code": "VIDEO_TUTORIAL_PORTFOLIO_OVERVIEW",
                    "path": "portfolio-overview",
                    "link": "#portfolio-overview",
                    "head": False,
                    "roles": ["csr-admin", "csr-user", "ngo-user", "sattva-user"],
                },
                {
                    "title": "Portfolio Insights",
                    "code": "VIDEO_TUTORIAL_PORTFOLIO_INSIGHTS",
                    "path": "portfolio-insights",
                    "link": "#portfolio-insights",
                    "head": False,
                    "roles": ["csr-admin", "csr-user", "ngo-user", "sattva-user"],
                },
                {
                    "title": "Programs",
                    "code": "VIDEO_TUTORIAL_PROGRAMS",
                    "path": "programs",
                    "link": "#programs",
                    "head": True,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Program Insights",
                    "code": "VIDEO_TUTORIAL_PROGRAM_INSIGHTS",
                    "path": "program-insights",
                    "link": "#program-insights",
                    "head": False,
                    "roles": ["csr-admin", "csr-user", "ngo-user", "sattva-user"],
                },
                {
                    "title": "Project Insights",
                    "code": "VIDEO_TUTORIAL_PROJECT_INSIGHTS",
                    "path": "project-insights",
                    "link": "#project-insights",
                    "head": False,
                    "roles": ["csr-admin", "csr-user", "ngo-user", "sattva-user"],
                },
                {
                    "title": "Project Plan",
                    "code": "VIDEO_TUTORIAL_PROJECT_PLAN",
                    "path": "project-plan",
                    "link": "#project-plan",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Financials",
                    "code": "VIDEO_TUTORIAL_FINANCIALS",
                    "path": "financials",
                    "link": "#financials",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Impact Data",
                    "code": "VIDEO_TUTORIAL_IMPACT_DATA",
                    "path": "impact-data",
                    "link": "#impact-data",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Compliance",
                    "code": "VIDEO_TUTORIAL_COMPLIANCE",
                    "path": "compliance",
                    "link": "#compliance",
                    "head": True,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "Milestones",
                    "code": "VIDEO_TUTORIAL_MILESTONES",
                    "path": "milestones",
                    "link": "#milestones",
                    "head": False,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "NGO Due Diligence",
                    "code": "VIDEO_TUTORIAL_NGO_DUE_DILIGENCE",
                    "path": "due-diligence",
                    "link": "#due-diligence",
                    "head": True,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "Governance",
                    "code": "VIDEO_TUTORIAL_GOVERNANCE",
                    "path": "governance",
                    "link": "#governance",
                    "head": True,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "All Tasks",
                    "code": "VIDEO_TUTORIAL_ALL_TASKS",
                    "path": "all-task",
                    "link": "#all-task",
                    "head": True,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "Documents",
                    "code": "VIDEO_TUTORIAL_DOCUMENTS",
                    "path": "documents",
                    "link": "#documents",
                    "head": True,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Users",
                    "code": "VIDEO_TUTORIAL_USERS",
                    "path": "users",
                    "link": "#users",
                    "head": True,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "More",
                    "code": "VIDEO_TUTORIAL_MORE",
                    "path": "more",
                    "link": "#more",
                    "head": True,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "Alerts & Notifications",
                    "code": "VIDEO_TUTORIAL_ALERTS_AND_NOTIFICATIONS",
                    "path": "alerts",
                    "link": "#alerts",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Calendar",
                    "code": "VIDEO_TUTORIAL_CALENDAR",
                    "path": "calendar",
                    "link": "#calendar",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
            ],
        },
        {"heading": "Tech Support", "path": "tech-support", "subsections": []},
        {
            "heading": "FAQs",
            "path": "faq",
            "subsections": [
                {
                    "title": "Home",
                    "code": "FAQ_HOME",
                    "path": "home-faq",
                    "link": "#home-faq",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Programs",
                    "code": "FAQ_PROGRAMS",
                    "path": "programs-faq",
                    "link": "#programs-faq",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Compliance",
                    "code": "FAQ_COMPLIANCE",
                    "path": "compliance-faq",
                    "link": "#compliance-faq",
                    "head": False,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "Governance",
                    "code": "FAQ_GOVERNANCE",
                    "path": "governance-faq",
                    "link": "#governance-faq",
                    "head": False,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "All Tasks",
                    "code": "FAQ_ALL_TASKS",
                    "path": "all-tasks-faq",
                    "link": "#all-task-faq",
                    "head": False,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "Documents",
                    "code": "FAQ_DOCUMENTS",
                    "path": "documents-faq",
                    "link": "#documents-faq",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
                {
                    "title": "Users",
                    "code": "FAQ_USERS",
                    "path": "users-faq",
                    "link": "#users",
                    "head": False,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "More",
                    "code": "FAQ_MORE",
                    "path": "more-faq",
                    "link": "#more-faq",
                    "head": False,
                    "roles": ["sattva-user"],
                },
                {
                    "title": "Alerts & Notifications",
                    "code": "FAQ_ALERTS_AND_NOTIFICATIONS",
                    "path": "alerts-faq",
                    "link": "#alerts",
                    "head": False,
                    "roles": ["ngo-user", "sattva-user"],
                },
            ],
        },
    ]
    for section in sections:
        section_obj = HelpSections.objects.create(
            section_name=section["heading"], path=section["path"]
        )
        level = 100
        for subsection in section["subsections"]:
            HelpSubSections.objects.create(
                section=section_obj,
                title=subsection["title"],
                level=level,
                path=subsection["path"],
                roles=subsection["roles"],
                is_head=subsection["head"],
                code=subsection["code"],
            )
            level += 100


def calculate_total_and_planned_disbursement_amount():
    from v2.project.models import Project, Utilisation
    from django.db.models.aggregates import Sum

    projects = Project.objects.all()
    for project in projects:
        standalone_utilization_amount = 0
        total_utilised_amount = 0
        disbursements = project.get_all_disbursements()
        project.total_disbursement_amount = (
            disbursements.aggregate(Sum("actual_amount")).get("actual_amount__sum", 0)
            if disbursements
            else 0
        ) or 0
        project.planned_disbursement_amount = (
            disbursements.aggregate(Sum("expected_amount")).get(
                "expected_amount__sum", 0
            )
            if disbursements
            else 0
        ) or 0
        standalone_utilization_amount = (
            Utilisation.objects.filter(disbursement=None, project=project)
            .aggregate(Sum("actual_cost"))
            .get("actual_cost__sum", 0)
            or 0
        )
        for disbursement in disbursements:
            total_utilised_amount += disbursement.get_overall_utilistation_total()
        project.total_utilised_amount = (
            total_utilised_amount + standalone_utilization_amount
        )
        project.save()


def change_permissions_for_watcher():
    from v2.user_management.models import ClientFeature, ProjectFeature

    default_client_features = [
        {
            "feature": "CLIENT_OVERVIEW",
            "applicable_roles": ["sattva-user", "csr-admin", "csr-user", "csr-watcher"],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": [],
                "csr-watcher": ["View"],
            },
        },
        {
            "feature": "PROGRAM_OVERVIEW",
            "applicable_roles": ["sattva-user", "csr-admin", "csr-user", "csr-watcher"],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": [],
                "csr-watcher": ["View"],
            },
        },
        {
            "feature": "PORTFOLIO_INSIGHTS",
            "applicable_roles": ["sattva-user", "csr-admin", "csr-user"],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": [],
                "csr-watcher": [],
            },
        },
        {
            "feature": "PROGRAM_INSIGHTS",
            "applicable_roles": ["sattva-user", "csr-admin", "csr-user"],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": [],
                "csr-watcher": [],
            },
        },
        {
            "feature": "ALL_TASKS",
            "applicable_roles": [
                "sattva-user",
                "csr-admin",
                "csr-user",
                "ngo-user",
                "csr-watcher",
            ],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": ["View", "Change", "Delete"],
                "csr-watcher": ["View"],
            },
        },
        {
            "feature": "GOVERNANCE",
            "applicable_roles": ["sattva-user", "csr-admin", "csr-user"],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": [],
                "csr-watcher": [],
            },
        },
        {
            "feature": "COMPLIANCE",
            "applicable_roles": ["sattva-user", "csr-admin", "csr-user", "csr-watcher"],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": [],
                "csr-watcher": ["View"],
            },
        },
        {
            "feature": "DOCUMENTS",
            "applicable_roles": [
                "sattva-user",
                "csr-admin",
                "csr-user",
                "ngo-user",
                "csr-watcher",
            ],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": ["View", "Change", "Delete"],
                "csr-watcher": ["View"],
            },
        },
        {
            "feature": "MANAGE_USERS",
            "applicable_roles": ["sattva-user", "csr-admin", "csr-user"],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": [],
            },
        },
    ]
    for client_feature in default_client_features:
        feature_obj = ClientFeature.objects.get(feature_code=client_feature["feature"])
        feature_obj.applicable_roles = client_feature["applicable_roles"]
        feature_obj.default_permissions = client_feature["default_permissions"]
        feature_obj.save()
    default_project_features = [
        {
            "feature": "PROJECT_OVERVIEW",
            "applicable_roles": [
                "sattva-user",
                "csr-admin",
                "csr-user",
                "ngo-user",
                "csr-watcher",
            ],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": ["View", "Change", "Delete"],
                "csr-watcher": ["View"],
            },
        },
        {
            "feature": "PROJECT_INSIGHTS",
            "applicable_roles": ["sattva-user", "csr-admin", "csr-user", "ngo-user"],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": ["View", "Change", "Delete"],
                "csr-watcher": [],
            },
        },
        {
            "feature": "PROJECT_PLAN",
            "applicable_roles": [
                "sattva-user",
                "csr-admin",
                "csr-user",
                "ngo-user",
                "csr-watcher",
            ],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": ["View", "Change", "Delete"],
                "csr-watcher": ["View"],
            },
        },
        {
            "feature": "FINANCIALS",
            "applicable_roles": [
                "sattva-user",
                "csr-admin",
                "csr-user",
                "ngo-user",
                "csr-watcher",
            ],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": ["View", "Change", "Delete"],
                "csr-watcher": ["View"],
            },
        },
        {
            "feature": "IMPACT",
            "applicable_roles": [
                "sattva-user",
                "csr-admin",
                "csr-user",
                "ngo-user",
                "csr-watcher",
            ],
            "default_permissions": {
                "sattva-user": ["View", "Change", "Delete"],
                "csr-admin": ["View", "Change", "Delete"],
                "csr-user": ["View", "Change", "Delete"],
                "ngo-user": ["View", "Change", "Delete"],
                "csr-watcher": ["View"],
            },
        },
    ]
    for project_feature in default_project_features:
        feature_obj = ProjectFeature.objects.get(
            feature_code=project_feature["feature"]
        )
        feature_obj.applicable_roles = project_feature["applicable_roles"]
        feature_obj.default_permissions = project_feature["default_permissions"]
        feature_obj.save()


"""
CREATE OR REPLACE VIEW project_projectoutputoutcome as select frequency, output_indicator, outcome_indicator, is_beneficiary,
                  project_projectoutput.scheduled_date, project_projectoutput.period_end_date, planned_output, actual_output, planned_outcome,
                  actual_outcome, milestone_id as milestone_id, project_projectindicator.project_id as project_id, project_projectindicator.created_date,
                  project_projectindicator.created_by, project_projectindicator.updated_date, project_projectindicator.updated_by, project_projectindicator.created_by as indicator_batch,
                  project_projectindicator.updated_by_username, project_projectindicator.created_by_username, project_projectoutcome.period_name,
                  project_projectoutput.is_included, output_calculation, outcome_calculation, project_projectindicator.id as id from project_projectindicator,
                  project_projectoutcome, project_projectoutput where project_projectindicator.id = project_projectoutcome.project_indicator_id and
                  project_projectindicator.id = project_projectoutput.project_indicator_id
                  and project_projectoutcome.period_name = project_projectoutput.period_name;"""


# Get help data from production
def get_prod_help_data():
    from v2.help.models import HelpArticles
    import csv
    import json

    cols = [
        "content",
        "title",
        "sub_section",
        "updated_by_username",
        "created_by",
        "created_by_username",
        "updated_date",
        "created_date",
        "updated_by",
    ]
    from django.core import serializers

    help_articles = HelpArticles.objects.all()
    json_list = []
    data = serializers.serialize("json", help_articles)
    json_data = json.loads(data)
    for row in json_data:
        for key in row:
            if key == "fields":
                json_list.append(row[key])
    print(json_list)
    path = "/app/v2/data_scripts/help_articles.csv"
    with open(path, "w") as f:
        wr = csv.DictWriter(f, fieldnames=cols)
        wr.writeheader()
        wr.writerows(json_list)


def save_prod_help_data_staging():
    from v2.help.models import HelpArticles, HelpSubSections
    import csv
    from django.core.files.storage import default_storage

    path = "/home/diksha/Documents/help_articles.csv"
    csv_file = open(path, encoding="utf-8-sig")
    reader = csv.reader(
        csv_file,
        quotechar='"',
        delimiter=",",
        quoting=csv.QUOTE_ALL,
        skipinitialspace=True,
    )
    titles = next(reader)
    data = []
    for row in reader:
        row_data = {}
        for i, value in enumerate(row):
            if i >= len(titles):
                return
            title = titles[i]
            row_data[title] = value
        data.append(row_data)
    for row_no, item in enumerate(data):
        HelpArticles.objects.create(
            content=item.get("content"),
            title=item.get("title"),
            sub_section=HelpSubSections.objects.get(id=item.get("sub_section")),
        )


def create_db_tables_for_all_clients():
    from v2.client.models import Client
    from v2.data_scripts.client_tables import ClientTableHelper

    clients = Client.objects.all()
    for client in clients:
        cl_helper = ClientTableHelper(client)
        cl_helper.create_tables()


def clear_all_client_tables():
    from v2.client.models import Client
    from v2.data_scripts.client_tables import ClientTableClearer

    clients = Client.objects.all()
    for client in clients:
        cl_helper = ClientTableClearer(client)
        cl_helper.clear()


def add_mfa_to_existing_user():
    from core.users.models import User
    from core.keycloak.helper import KeyclockHelper
    keycloak_helper = KeyclockHelper()
    for user in User.objects.all():
        try:
            keycloak_helper.add_mfa_to_user(user.email)
            print(user.email)
            print('done')
        except Exception as ex:
            print(user.email)
            print(ex)


def add_partner_table():
    from v2.client.models import Client
    from v2.data_scripts.client_tables import ClientTableHelper
    from django.db import connection
    clients = Client.objects.all()
    for client in clients:
        query = '''
        CREATE TABLE {1}.ngo_ngopartner AS
        SELECT * FROM ngo_ngopartner;
        '''
        cl_helper = ClientTableHelper(client)
        query = query.format(cl_helper.client.id, cl_helper.schema_name)
        cursor = connection.cursor()
        cursor.execute(query)
        cursor.close()
        # cl_helper.create_tables()


# def add_missing_key_constraint():
#     from v2.client.models import Client
#     from v2.data_scripts.client_tables import ClientTableHelper

#     clients = Client.objects.all()
#     for client in clients:
#         cl_helper = ClientTableHelper(client)
#         cl_helper.create_missing_key_constraint()
