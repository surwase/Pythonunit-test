from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from jose import jwt
from django.core.exceptions import PermissionDenied

from .serializers import *
from .models import *
import django_filters
from django_filters import rest_framework as filters
import logging
from django.core.mail import send_mail
from django.conf import settings
import os

# Get an instance of a logger
logger = logging.getLogger(__name__)

"""
    Filter for FocusArea where we filter FocusArea by exact focus_area_name.
"""
class FocusAreaFilter(django_filters.FilterSet):
    class Meta:
        model = FocusArea
        fields = {
            'focus_area_name': ['icontains'],
        }

"""
    Viewset for FocusArea allows CRUD operation
"""
class FocusAreaViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'focusarea:view',
        'POST': 'focusarea:add',
        'PUT': 'focusarea:change',
        'DELETE': 'focusarea:delete',
        'PATCH': 'focusarea:patch'
    }
    serializer_class = FocusAreaSerializer
    queryset = FocusArea.objects.all().order_by('focus_area_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = FocusAreaFilter

"""
    Filter for SubFocusArea where we filter SubFocusArea by exact focus_area_name or sub focus_area_name.
"""
class SubFocusAreaFilter(django_filters.FilterSet):
    class Meta:
        model = SubFocusArea
        fields = {
            'focus_area_name': ['exact'],
            'sub_focus_area_name': ['icontains'],
        }

"""
    Viewset for SubFocusArea allows CRUD operation
"""
class SubFocusAreaViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'subfocusarea:view',
        'POST': 'subfocusarea:add',
        'PUT': 'subfocusarea:change',
        'DELETE': 'subfocusarea:delete',
        'PATCH': 'subfocusarea:patch'
    }
    serializer_class = SubFocusAreaSerializer
    queryset = SubFocusArea.objects.all().order_by('sub_focus_area_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = SubFocusAreaFilter

"""
    Filter for Institution where we filter Institution by exact institution_name.
"""
class InstitutionFilter(django_filters.FilterSet):
    class Meta:
        model = Institution
        fields = {
            'institution_name': ['icontains'],
        }

"""
    Viewset for Institution allows CRUD operation
"""
class InstitutionViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'institution:view',
        'POST': 'institution:add',
        'PUT': 'institution:change',
        'DELETE': 'institution:delete',
        'PATCH': 'institution:patch'
    }
    serializer_class = InstitutionSerializer
    queryset = Institution.objects.all().order_by('institution_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = InstitutionFilter

"""
    Filter for SDGGoal where we filter SDGGoal by SDGGoal contained .
"""
class SDGGoalFilter(django_filters.FilterSet):
    class Meta:
        model = SDGGoal
        fields = {
            'sdg_goal': ['icontains'],
        }

"""
    Viewset for SDGGoal allows CRUD operation
"""
class SDGGoalViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'sdggoal:view',
        'POST': 'sdggoal:add',
        'PUT': 'sdggoal:change',
        'DELETE': 'sdggoal:delete',
        'PATCH': 'sdggoal:patch'
    }
    serializer_class = SDGGoalSerializer
    queryset = SDGGoal.objects.all().order_by('sdg_goal')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = SDGGoalFilter

"""
    Viewset for TargetSegment allows CRUD operation
"""
class TargetSegmentViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'targetsegment:view',
        'POST': 'targetsegment:add',
        'PUT': 'targetsegment:change',
        'DELETE': 'targetsegment:delete',
        'PATCH': 'targetsegment:patch'
    }
    serializer_class = TargetSegmentSerializer
    queryset = TargetSegment.objects.all().order_by('target_segment')

"""
    Filter for SubTargetSegment where we filter SubTargetSegment by exact target_segment or sub target_segment.
"""
class SubTargetSegmentFilter(django_filters.FilterSet):
    class Meta:
        model = SubTargetSegment
        fields = {
            'target_segment': ['exact'],
            'sub_target_segment': ['icontains']
        }

"""
    Viewset for sub TargetSegment allows CRUD operation
"""
class SubTargetSegmentViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'subtargetsegment:view',
        'POST': 'subtargetsegment:add',
        'PUT': 'subtargetsegment:change',
        'DELETE': 'subtargetsegment:delete',
        'PATCH': 'subtargetsegment:patch'
    }
    serializer_class = SubTargetSegmentSerializer
    queryset = SubTargetSegment.objects.all().order_by('sub_target_segment')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = SubTargetSegmentFilter


"""
    Viewset for ApplicationRole allows CRUD operation
"""
class ApplicationRoleViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'applicationrole:view',
        'POST': 'applicationrole:add',
        'PUT': 'applicationrole:change',
        'DELETE': 'applicationrole:delete',
        'PATCH': 'applicationrole:patch'
    }
    serializer_class = ApplicationRoleSerializer
    queryset = ApplicationRole.objects.all().order_by('role_name')

"""
    Viewset for DataType allows CRUD operation
"""
class DataTypeViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'datatype:view',
        'POST': 'datatype:add',
        'PUT': 'datatype:change',
        'DELETE': 'datatype:delete',
        'PATCH': 'datatype:patch'
    }
    serializer_class = DataTypeSerializer
    queryset = DataType.objects.all().order_by('data_type')

"""
    Viewset for Frequency allows CRUD operation
"""
class FrequencyViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'frequency:view',
        'POST': 'frequency:add',
        'PUT': 'frequency:change',
        'DELETE': 'frequency:delete',
        'PATCH': 'frequency:patch'
    }
    serializer_class = FrequencySerializer
    queryset = Frequency.objects.all().order_by('frequency')

"""
    Viewset for EntityType allows CRUD operation
"""
class EntityTypeViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'entitytype:view',
        'POST': 'entitytype:add',
        'PUT': 'entitytype:change',
        'DELETE': 'entitytype:delete',
        'PATCH': 'entitytype:patch'
    }
    serializer_class = EntityTypeSerializer
    queryset = EntityType.objects.all().order_by('entity_type')

"""
    Filter for Entity where we filter Entity by exact entity_name, entity_type ,entity_focus_area,entity_sub_focus_area,attribute_name
"""
class EntityFilter(django_filters.FilterSet):
    class Meta:
        model = Entity
        fields = {
            'entity_name': ['exact'],
            'entity_type': ['exact'],
            'entity_focus_area': ['exact'],
            'entity_sub_focus_area': ['exact'],
            'attribute_name': ['exact'],
            'attribute_type': ['exact'],
            'attribute_grouping': ['exact'],
            'attribute_lead_indicator_flag': ['exact'],
            'attribute_classification_indicator_flag': ['exact'],
        }

"""
    Viewset for Entity allows CRUD operation
"""
class EntityViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'entity:view',
        'POST': 'entity:add',
        'PUT': 'entity:change',
        'DELETE': 'entity:delete',
        'PATCH': 'entity:patch'
    }
    serializer_class = EntityCreateSerializer
    queryset = Entity.objects.all().order_by('entity_name', 'attribute_order', 'attribute_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = EntityFilter

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return EntityReadSerializer
        return EntityCreateSerializer


@api_view(['GET'])
# @permission_classes((IsAuthenticated,))
def get_entity_by_entity_type_focus_area_sub_focus_area(request, entity_type=None, focus_area_name=None,
                                                        sub_focus_area_name=None):
    token = request.META.get('HTTP_AUTHORIZATION')
    if not token:
        error_msg = 'TokenMissing'
        raise PermissionDenied
    if entity_type is not None:
        if focus_area_name is not None:
            if sub_focus_area_name is not None:
                entity = Entity.objects.filter(
                    entity_type=entity_type,
                    entity_focus_area=focus_area_name,
                    entity_sub_focus_area=sub_focus_area_name). \
                    values_list('entity_name').distinct().order_by('entity_name')
            else:
                entity = Entity.objects.filter(
                    entity_type=entity_type,
                    entity_focus_area=focus_area_name). \
                    values_list('entity_name').distinct().order_by('entity_name')
        else:
            if sub_focus_area_name is not None:
                entity = Entity.objects.filter(
                    entity_type=entity_type,
                    entity_sub_focus_area=sub_focus_area_name). \
                    values_list('entity_name').distinct().order_by('entity_name')
            else:
                entity = Entity.objects.filter(
                    entity_type=entity_type). \
                    values_list('entity_name').distinct().order_by('entity_name')
    else:
        if focus_area_name is not None:
            if sub_focus_area_name is not None:
                entity = Entity.objects.filter(
                    entity_focus_area=focus_area_name,
                    entity_sub_focus_area=sub_focus_area_name). \
                    values_list('entity_name').distinct().order_by('entity_name')
            else:
                entity = Entity.objects.filter(
                    entity_focus_area=focus_area_name). \
                    values_list('entity_name').distinct().order_by('entity_name')
        else:
            if sub_focus_area_name is not None:
                entity = Entity.objects.filter(
                    entity_sub_focus_area=sub_focus_area_name). \
                    values_list('entity_name').distinct().order_by('entity_name')
            else:
                entity = Entity.objects.values_list('entity_name').distinct().order_by('entity_name')

    return Response({'entity': list(entity)})

"""
    Viewset for Aggregate Function allows CRUD operation
"""
class AggregateFunctionViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'aggfunc:view',
        'POST': 'aggfunc:add',
        'PUT': 'aggfunc:change',
        'DELETE': 'aggfunc:delete',
        'PATCH': 'aggfunc:patch'
    }
    serializer_class = AggregateFunctionSerializer
    queryset = AggregateFunction.objects.all().order_by('agg_func')

"""
    Filter for DocumentType where we filter DocumentType by document_type contained
"""
class DocumentTypeFilter(django_filters.FilterSet):
    class Meta:
        model = DocumentType
        fields = {
            'document_type': ['icontains']
        }

"""
    Viewset for Document Type allows CRUD operation
"""
class DocumentTypeViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'doctype:view',
        'POST': 'doctype:add',
        'PUT': 'doctype:change',
        'DELETE': 'doctype:delete',
        'PATCH': 'doctype:patch'
    }
    serializer_class = DocumentTypeSerializer
    queryset = DocumentType.objects.all().order_by('document_type')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DocumentTypeFilter

"""
    Filter for DocumentClass where we filter DocumentClass by exact document_type document_source,document_status,document_requirement,document_class_name contained
"""
class DocumentClassFilter(django_filters.FilterSet):
    class Meta:
        model = DocumentClass
        fields = {
            'document_source': ['icontains'],
            'document_type': ['exact'],
            'document_status': ['icontains'],
            'document_requirement': ['icontains'],
            'document_class_name': ['icontains'],
        }

"""
    Viewset for DocumentClass allows CRUD operation
"""
class DocumentClassViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'docclass:view',
        'POST': 'docclass:add',
        'PUT': 'docclass:change',
        'DELETE': 'docclass:delete',
        'PATCH': 'docclass:patch'
    }
    serializer_class = DocumentClassSerializer
    queryset = DocumentClass.objects.filter(document_status='valid').order_by('document_source', 'document_type')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = DocumentClassFilter

"""
    Filter for CSRComplianceCheckList where we filter CheckList by area and indicator contained
"""
class CSRComplianceCheckListFilter(django_filters.FilterSet):
    class Meta:
        model = CSRComplianceCheckList
        fields = {
            'area': ['icontains'],
            'indicator': ['icontains'],
        }

"""
    Viewset for CSR Compliance CheckList allows CRUD operation
"""
class CSRComplianceCheckListViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'csrcompliancechklst:view',
        'POST': 'csrcompliancechklst:add',
        'PUT': 'csrcompliancechklst:change',
        'DELETE': 'csrcompliancechklst:delete',
        'PATCH': 'csrcompliancechklst:patch'
    }
    serializer_class = CSRComplianceCheckListSerializer
    queryset = CSRComplianceCheckList.objects.all().order_by('area', 'indicator')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CSRComplianceCheckListFilter


"""
    Filter for NGOSelectionCheckList where we filter CheckList by head and detail  contained
"""
class NGOSelectionCheckListFilter(django_filters.FilterSet):
    class Meta:
        model = NGOSelectionCheckList
        fields = {
            'head': ['icontains'],
            'detail': ['icontains'],
        }

"""
    Viewset for NGO Selection CheckList allows CRUD operation
"""
class NGOSelectionCheckListViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'ngoselectionchklst:view',
        'POST': 'ngoselectionchklst:add',
        'PUT': 'ngoselectionchklst:change',
        'DELETE': 'ngoselectionchklst:delete',
        'PATCH': 'ngoselectionchklst:patch'
    }
    serializer_class = NGOSelectionCheckListSerializer
    queryset = NGOSelectionCheckList.objects.all().order_by('head', 'detail')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = NGOSelectionCheckListFilter


"""
    Viewset for RiskImpact allows CRUD operation
"""
class RiskImpactViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'riskimpact:view',
        'POST': 'riskimpact:add',
        'PUT': 'riskimpact:change',
        'DELETE': 'riskimpact:delete',
        'PATCH': 'riskimpact:patch'
    }
    serializer_class = RiskImpactSerializer
    queryset = RiskImpact.objects.all().order_by('risk_impact')

"""
    Viewset for Risk Probability allows CRUD operation
"""
class RiskProbabilityViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'riskprobability:view',
        'POST': 'riskprobability:add',
        'PUT': 'riskprobability:change',
        'DELETE': 'riskprobability:delete',
        'PATCH': 'riskprobability:patch'
    }
    serializer_class = RiskProbabilitySerializer
    queryset = RiskProbability.objects.all().order_by('risk_probability')

"""
    Viewset for Risk status allows CRUD operation
"""
class RiskStatusViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'riskstatus:view',
        'POST': 'riskstatus:add',
        'PUT': 'riskstatus:change',
        'DELETE': 'riskstatus:delete',
        'PATCH': 'riskstatus:patch'
    }
    serializer_class = RiskStatusSerializer
    queryset = RiskStatus.objects.all().order_by('risk_status')

"""
    Viewset for Risk Type allows CRUD operation
"""
class RiskTypeViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'risktype:view',
        'POST': 'risktype:add',
        'PUT': 'risktype:change',
        'DELETE': 'risktype:delete',
        'PATCH': 'risktype:patch'
    }
    serializer_class = RiskTypeSerializer
    queryset = RiskType.objects.all().order_by('risk_type')


"""
    Viewset for Risk Owner allows CRUD operation
"""
class RiskOwnerViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'riskowner:view',
        'POST': 'riskowner:add',
        'PUT': 'riskowner:change',
        'DELETE': 'riskowner:delete',
        'PATCH': 'riskowner:patch'
    }
    serializer_class = RiskOwnerSerializer
    queryset = RiskOwner.objects.all().order_by('risk_owner')

"""
    Viewset for Mitigation Status allows CRUD operation
"""
class MitigationStatusViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'mitigationstatus:view',
        'POST': 'mitigationstatus:add',
        'PUT': 'mitigationstatus:change',
        'DELETE': 'mitigationstatus:delete',
        'PATCH': 'mitigationstatus:patch'
    }
    serializer_class = MitigationStatusSerializer
    queryset = MitigationStatus.objects.all().order_by('mitigation_status')

"""
    Viewset for Task Category allows CRUD operation
"""
class TaskCategoryViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'taskcategory:view',
        'POST': 'taskcategory:add',
        'PUT': 'taskcategory:change',
        'DELETE': 'taskcategory:delete',
        'PATCH': 'taskcategory:patch'
    }
    serializer_class = TaskCategorySerializer
    queryset = TaskCategory.objects.all().order_by('task_category')


# @api_view(['POST'])
def send_welcome_email_to_user(user):
    # return
    # user = request.data
    email = user['email']
    name = user['user_name']
    password = user['password']
    role = user['role_name']
    login_url = os.environ.get('LOGIN_URL')
    if role == 'ngo-user':
        html_message = open(str(settings.ROOT_DIR) + '/deployment/templates/NGO.html', 'r').read()
        html_message = html_message.replace('$name', name)
        html_message = html_message.replace('$password', password)
        html_message = html_message.replace('$login_url', login_url)
    else:
        html_message = open(str(settings.ROOT_DIR) + '/deployment/templates/test.html', 'r').read()
        html_message = html_message.replace('$name', name)
        html_message = html_message.replace('$password', password)
        html_message = html_message.replace('$login_url', login_url)
    try:
        send_mail(
            subject='Welcome ' + name,
            message='',
            html_message=html_message,
            from_email='products@sattva.co.in',
            recipient_list=list(email.split(',')),
            fail_silently=False
        )
    except Exception as inst:
        logger.error(str(inst))
        return
    return


def v2_send_welcome_email_to_user(email, password, name):
    login_url = os.environ.get('LOGIN_URL')
    html_message = open(str(settings.ROOT_DIR) + '/deployment/templates/new-welcome.html', 'r').read()
    html_message = html_message.replace('$name', name)
    html_message = html_message.replace('$email', email)
    html_message = html_message.replace('$password', password)
    html_message = html_message.replace('$login_url', login_url)
    try:
        send_mail(
            subject='Welcome ' + name,
            message='',
            html_message=html_message,
            from_email='shift@sattva.co.in',
            recipient_list=list(email.split(',')),
            fail_silently=False
        )
    except Exception as inst:
        logger.error(str(inst))
        return
    return
