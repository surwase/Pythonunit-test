import datetime

from django.db.models import Sum
from rest_framework import serializers
from rest_framework_bulk import BulkSerializerMixin, BulkListSerializer
from v2.project.models import ProjectTag
from core.authentication import CurrentUser
from core.users.models import User
from core.utils import check_null_and_replace_to_none
from v2.plant.models import Plant, PlantImage, PlantUser, PlantDocument, PlantDashboard, PlantScheduleVII, \
    PlantLocation, PlantFocusArea, PlantSubFocusArea, PlantTargetSegment, PlantSubTargetSegment, PlantSDGGoals,\
    PlantMCA
from v2.project.models import Project, ProjectIndicator, ProjectScheduleVII
from v2.project.models import ProjectUser
from v2.project.serializers import ProjectDetailMiniSerializer, ProjectIndicatorListSerializer, ProjectOptionSerializer, \
    ProjectSerializer, UserSerializerForProject, ProjectScheduleVIIAggregationSerializer, \
    MiniUserSerializerForProject, ListProjectSerializer, ProjectMicroSerializer, ProjectMiniMicroSerializer

# filter for project milestones
def apply_filters_for_project_milestones(self, obj):
    start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
    end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
    location = check_null_and_replace_to_none(self.context.get('request').query_params.get('location'))
    focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('focus_area'))
    sub_focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('sub_focus_area'))
    total_milestones = obj.get_milestones()
    if not ((start_date is None) | (end_date is None)):
        total_milestones = total_milestones.filter(start_date__lte=end_date, end_date__gte=start_date)
    if location is not None:
        total_milestones = total_milestones.filter(location__in=location)
    if sub_focus_area is not None:
        total_milestones = total_milestones.filter(sub_focus_area__in=[sub_focus_area])
    return total_milestones

# filter for project disbursement
def apply_filter_get_project_disbursement(self, obj):
    start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
    end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
    project_milestones = apply_filters_for_project_milestones(self, obj)
    project_disbursements = obj.get_all_disbursements(). \
        filter(milestone__id__in=project_milestones.values_list('id', flat=True))
    if not ((start_date is None) | (end_date is None)):
        project_disbursements = project_disbursements.filter(start_date__lte=end_date, end_date__gte=start_date)
    return project_disbursements


"""
    This class will serialize Plant model and return fields id, plant_name.
"""
class PlantMicroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plant
        fields = ['id', 'plant_name']


"""
    This class will serialize Plant model.
"""
class PlantSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField('get_projects')
    plant_users = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()
    focus_area = serializers.SerializerMethodField()
    sub_focus_area = serializers.SerializerMethodField()
    target_segment = serializers.SerializerMethodField()
    sub_target_segment = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    total_project_budget = serializers.SerializerMethodField()
    mca_sector = serializers.SerializerMethodField()

    @staticmethod
    # fetch total_project_budget
    def get_total_project_budget(obj):
        projects = obj.get_projects()
        budget = projects.aggregate(Sum('budget')).get('budget__sum', 0)
        return budget

    @staticmethod
    # fetch location
    def get_location(obj):
        locations = PlantLocation.objects.filter(plant=obj).values_list('location', flat=True)
        return locations

    @staticmethod
    # fetch focus area
    def get_focus_area(obj):
        focus_area = PlantFocusArea.objects.filter(plant=obj).values_list('focus_area', flat=True)
        return focus_area

    @staticmethod
    # fetch sub focus area
    def get_sub_focus_area(obj):
        sub_focus_area = PlantSubFocusArea.objects.filter(plant=obj).values_list('sub_focus_area', flat=True)
        return sub_focus_area

    @staticmethod
    # fetch target segment
    def get_target_segment(obj):
        target_segment = PlantTargetSegment.objects.filter(plant=obj).values_list('target_segment', flat=True)
        return target_segment

    @staticmethod
    # fetch sub target segment
    def get_sub_target_segment(obj):
        sub_target_segment = PlantSubTargetSegment.objects.filter(plant=obj).values_list('sub_target_segment',
                                                                                         flat=True)
        return sub_target_segment

    @staticmethod
    # fetch sdg goals
    def get_sdg_goals(obj):
        sdg_goals = PlantSDGGoals.objects.filter(plant=obj).values_list('sdg_goal', flat=True)
        return sdg_goals

    @staticmethod
    # fetch mca_sector
    def get_mca_sector(obj):
        mca_sector = PlantMCA.objects.filter(plant=obj).values_list('mca', flat=True)
        return mca_sector

    @staticmethod
    # fetch plant users
    def get_plant_users(plant):
        project_users = PlantUser.objects.filter(plant=plant).values_list('user', flat=True)
        return UserSerializerForProject(User.objects.filter(id__in=project_users), many=True).data

    # fetch projects
    def get_projects(self, plant):
        projects = Project.objects.filter(plant=plant)
        user = self.context.get('user', self.context.get('request').user) or CurrentUser.get_current_user()
        if user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=user).values_list('project__id', flat=True)
            projects = projects.filter(id__in=user_projects)
        serializer = ProjectSerializer(projects, many=True)
        return serializer.data

    class Meta:
        model = Plant
        fields = '__all__'


"""
    This class will serialize Plant model and return fields budget, created_date, end_date, focus_area, id, plant_description, plant_name, plant_users,
    plant_users_count, project, sdg_goals, start_date.
"""
class ListPlantsSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField('get_projects')
    plant_users = serializers.SerializerMethodField()
    focus_area = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    plant_users_count = serializers.SerializerMethodField()

    @staticmethod
    # fetch plant users
    def get_plant_users(plant):
        project_users = PlantUser.objects.filter(plant=plant)[:2].values_list('user', flat=True)
        return MiniUserSerializerForProject(User.objects.filter(id__in=project_users), many=True).data

    @staticmethod
    # fetch plant users count
    def get_plant_users_count(plant):
        project_users = PlantUser.objects.filter(plant=plant).values_list('user', flat=True).count()
        return project_users

    # fetch projects
    def get_projects(self, plant):
        projects = Project.objects.filter(plant=plant)
        user = self.context.get('user', self.context.get('request').user) or CurrentUser.get_current_user()
        if user.application_role.role_name != 'sattva-admin':
            user_projects = ProjectUser.objects.filter(user=user).values_list('project__id', flat=True)
            projects = projects.filter(id__in=user_projects)
        serializer = ListProjectSerializer(projects, many=True)
        return serializer.data

    @staticmethod
    # fetch focus area
    def get_focus_area(obj):
        return obj.get_project_focus_areas_list()


    @staticmethod
    # fetch sdg goals
    def get_sdg_goals(obj):
        return obj.get_projects_sdg_goals()

    class Meta:
        model = Plant
        fields = ['budget', 'created_date', 'end_date', 'focus_area', 'id', 'plant_description', 'plant_name', 'plant_users',
        'plant_users_count', 'project', 'sdg_goals', 'start_date']
        # fields = ['id', 'plant_name', 'plant_users', 'plant_users_count', 'project', 'start_date', 'end_date']

"""
   This class will serialize Plant model and return field plant_name.
"""
class PlantListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plant
        fields = ('plant_name',)


def apply_plant_details_api_filter(self, obj):
    projects = obj.get_projects()
    start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
    end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
    location = check_null_and_replace_to_none(self.context.get('request').query_params.get('location'))
    focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('focus_area'))
    sub_focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('sub_focus_area'))
    tags = check_null_and_replace_to_none(self.context.get('request').query_params.get('project_tag'))
    if not ((start_date is None) | (end_date is None)):
        projects = obj.get_all_projects_list_by_date_range(start_date=start_date, end_date=end_date)
    if not location is None:
        projects = projects.filter(location == location)
    if not focus_area is None:
        projects = projects.filter(focus_area == focus_area)
    if not sub_focus_area is None:
        projects = projects.filter(sub_focus_area__in=[sub_focus_area])
    if not tags is None:
        tag_name = ProjectTag.objects.get(id=tags)
        projects = projects.filter(projecttag__tag=tag_name.tag)
    return projects


"""
   This class will serialize plant details in Plant model.
"""
class PlantDetailSerializer(serializers.ModelSerializer):
    # images = serializers.SerializerMethodField()
    project_locations = serializers.SerializerMethodField()
    focus_area = serializers.SerializerMethodField()
    sub_focus_area = serializers.SerializerMethodField()
    target_segment = serializers.SerializerMethodField()
    sub_target_segment = serializers.SerializerMethodField()
    budget = serializers.SerializerMethodField()
    disbursement_budget = serializers.SerializerMethodField()
    over_utilization = serializers.SerializerMethodField()
    total_projects = serializers.SerializerMethodField()
    live_projects = serializers.SerializerMethodField()
    project_status = serializers.SerializerMethodField()
    project_status_chart = serializers.SerializerMethodField()
    financial_utilization = serializers.SerializerMethodField()
    risk_status = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    mca_sector = serializers.SerializerMethodField()
    total_disbursement = serializers.SerializerMethodField()
    planned_disbursement = serializers.SerializerMethodField()
    dashboard = serializers.SerializerMethodField()
    projects = serializers.SerializerMethodField()

    # fetch images
    def get_images(self, obj):
        return PlantImagesSerializer(PlantImage.objects.filter(plant=obj), many=True).data

    # fetch project locations
    def get_project_locations(self, obj):
        return obj.get_all_project_locations()

    # fetch focus area
    def get_focus_area(self, obj):
        return obj.get_project_focus_areas_list()

    # fetch mca sector
    def get_mca_sector(self, obj):
        return obj.get_project_mca_sector_list()

    # fetch sub focus area
    def get_sub_focus_area(self, obj):
        return obj.get_project_sub_focus_areas_list()

    # fetch target segment
    def get_target_segment(self, obj):
        return obj.get_project_target_segment_list()

    # fetch sub target segment
    def get_sub_target_segment(self, obj):
        return obj.get_project_sub_target_segment_list()

    # fetch the budget
    def get_budget(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_total_budget(projects=projects)

    # fetch disbursement budget
    def get_disbursement_budget(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_total_disbursement_amount(projects=projects)

    # fetch over utilization
    def get_over_utilization(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_project_over_utilization(projects=projects)

    # fetch project status
    def get_project_status(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return str(round(obj.get_all_projects_average_status(projects=projects), 2)) + "%"

    # fetch total projects
    def get_total_projects(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return projects.count()

    # fetch live projects
    def get_live_projects(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_all_live_projects().filter(id__in=projects.values_list('id', flat=True)).count()

    # fetch project status chart
    def get_project_status_chart(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        total_projects = projects.count()
        projects_list = projects.values_list('id', flat=True)
        if total_projects > 0:
            overall_status = []
            completed_projects = len([project for project in projects if project.project_status >= 100])
            on_going_projects = len([project for project in projects if project.project_status > 0 and project.project_status < 100])
            open_projects = len([project for project in projects if project.project_status <= 0])
            if completed_projects > 0:
                overall_status.append({"status": "Completed", "count": completed_projects,
                                       "percentage": round((completed_projects / total_projects) * 100, 2)})
            if open_projects > 0:
                overall_status.append({"status": "Open", "count": open_projects,
                                       "percentage": round((open_projects / total_projects) * 100, 2)})
            if on_going_projects > 0:
                overall_status.append({"status": "On Going", "count": on_going_projects,
                                       "percentage": round((on_going_projects / total_projects) * 100, 2)})
            return overall_status
        else:
            return []

    # fetch financial utilization
    def get_financial_utilization(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        unutilized_amount = obj.get_total_disbursement_amount(projects=projects) - obj.get_total_utilized_amount(
            projects=projects)
        return [{"Utilised": obj.get_total_utilized_amount(projects=projects),
                 "Not Utilised": 0 if unutilized_amount < 0 else unutilized_amount}]

    # fetch total disbursement
    def get_total_disbursement(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_total_disbursement_amount(projects=projects)

    # fetch planned disbursement
    def get_planned_disbursement(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_planned_disbursement_amount(projects=projects)

    # fetch risk status
    def get_risk_status(self, obj):
        start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
        end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
        projects = apply_plant_details_api_filter(self, obj)
        location = check_null_and_replace_to_none(self.context.get('request').query_params.get('location'))
        focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('focus_area'))
        sub_focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('sub_focus_area'))
        if not ((start_date is None) | (end_date is None)):
            return obj.get_count_of_risk_status(start_date=start_date, end_date=end_date, projects=projects)
        elif (location is not None) | (focus_area is not None) | (sub_focus_area is not None):
            return obj.get_count_of_risk_status(projects=projects)
        else:
            return obj.get_count_of_risk_status()

    # fetch sdg goals
    def get_sdg_goals(self, obj):
        start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
        end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
        return obj.get_projects_sdg_goals()

    # fetch dashboard
    def get_dashboard(self, obj):
        return PlantDashboardMiniSerializer(obj.get_dashboard(), many=True).data

    # fetch projects
    def get_projects(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return ProjectDetailMiniSerializer(projects, many=True).data

    class Meta:
        model = Plant
        fields = ['id', 'plant_name', 'project_locations', "budget", "disbursement_budget", "focus_area",
                  "sub_focus_area", "target_segment", "sub_target_segment", "over_utilization",
                  "project_status", "total_projects", "live_projects", "project_status_chart", "financial_utilization",
                  "risk_status", "sdg_goals", "dashboard", "total_disbursement", "projects", "planned_disbursement",
                  "mca_sector"]


"""
   This class will serialize plant model and return fields id, plant_name, project, plant_users, locations.
"""
class PlantProjectStatusSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField('get_projects')
    plant_users = serializers.SerializerMethodField()
    locations = serializers.SerializerMethodField()

    @staticmethod
    # fetch plant users
    def get_plant_users(plant):
        project_users = PlantUser.objects.filter(plant=plant).values_list('user', flat=True)
        return UserSerializerForProject(User.objects.filter(id__in=project_users), many=True).data

    @staticmethod
    # fetch projects
    def get_projects(plant):
        projects = Project.objects.filter(plant=plant)
        serializer = ProjectSerializer(projects, many=True)
        return serializer.data

    # fetch locations
    def get_locations(self, obj):
        return obj.get_all_project_locations()

    class Meta:
        model = Plant
        fields = ['id', 'plant_name', 'project', 'plant_users', 'locations']


"""
   This class will serialize Plant Project Aggregation.
"""
class PlantProjectAggregationSerializer(serializers.ModelSerializer):
    project_status = serializers.SerializerMethodField()
    project_summary = serializers.SerializerMethodField()
    financial_utilization = serializers.SerializerMethodField()
    total_disbursement = serializers.SerializerMethodField()
    planned_disbursement = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    projects = serializers.SerializerMethodField()
    schedule_vii_activities = serializers.SerializerMethodField()

    # fetch sdg goals
    def get_sdg_goals(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_projects_sdg_goals(projects)

    # fetch project status
    def get_project_status(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return str(obj.get_all_project_status(projects)) + "%"

    # fetch project summary
    def get_project_summary(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        total_projects = projects.count()
        live_projects = projects.filter(start_date__lte=datetime.datetime.now(),
                                        end_date__gte=datetime.datetime.now()).count()
        return {'total_projects': total_projects, 'live_projects': live_projects}

    # fetch financial utilization
    def get_financial_utilization(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        total_disbursement = obj.get_all_project_disbursement_total_amount(projects)
        total_utilisation = obj.get_all_project_total_utilised_amount(projects)
        if total_disbursement > 0:
            unutilized_amount = total_disbursement - total_utilisation
            return [{"Utilised": total_utilisation,
                     "Not Utilised": 0 if unutilized_amount < 0 else unutilized_amount}]
        else:
            return []

    # fetch total disbursement
    def get_total_disbursement(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_all_project_disbursement_total_amount(projects)

    # fetch planned disbursement
    def get_planned_disbursement(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return obj.get_all_project_disbursement_planned_amount(projects)

    # fetch schedule_vii_activities
    def get_schedule_vii_activities(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        schedule_vii_activities = ProjectScheduleVII.objects.filter(
            project__in=[pr.id for pr in projects]).values_list('schedule_vii_activity__activity_description',
                                                                flat=True)
        schedule_vii_activities = set(list(schedule_vii_activities))
        return schedule_vii_activities

    # fetch projects
    def get_projects(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        return ProjectMicroSerializer(projects, many=True).data

    class Meta:
        model = Plant
        fields = ['id', 'plant_name', 'project_status', 'financial_utilization',
                  'project_summary', 'total_disbursement', 'planned_disbursement', 'sdg_goals',
                  'schedule_vii_activities', 'projects']

"""
   This class will serialize Plant Indicator.
"""
class PlantIndicatorSerializer(serializers.ModelSerializer):
    indicator = serializers.SerializerMethodField()

    def get_indicator(self, obj):
        projects = apply_plant_details_api_filter(self, obj)
        project_indicator = ProjectIndicator.objects.filter(project__in=[pr.id for pr in projects] )
        return ProjectIndicatorListSerializer(project_indicator, many=True).data

    class Meta:
        model = Plant
        fields = ['id', 'plant_name', 'indicator']

"""
   This class will serialize Plant Images.
"""
class PlantImagesSerializer(serializers.ModelSerializer):
    file_name = serializers.SerializerMethodField()
    file_path = serializers.SerializerMethodField()
    file_type = serializers.SerializerMethodField()

    def get_file_name(self, obj):
        return obj.image_file.url.split("/")[-1] if obj.image_file else ''

    def get_file_path(self, obj):
        return obj.image_file.url if obj.image_file else ''

    def get_file_type(self, obj):
        file_parts = obj.image_file.name.split(".") if obj.image_file else []
        if len(file_parts) > 1:
            return file_parts[1].lower()
        return ""

    class Meta:
        model = PlantImage
        fields = ['id', 'file_name', 'file_path', 'file_type', 'created_date']

"""
   This class will serialize Plant projects.
"""
class PlantProjectListSerializer(serializers.ModelSerializer):
    projects = serializers.SerializerMethodField()

    def get_projects(self, obj):
        return ProjectDetailMiniSerializer(obj.get_projects(), many=True).data

    class Meta:
        model = Plant
        fields = ['id', 'plant_name', 'projects']


"""
   This class will serialize Plant user model.
"""
class PlantUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlantUser
        fields = '__all__'


"""
   This class will serialize Plant model and return field id, plant_name, project, client.
"""
class PlantOptionSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField('get_projects')

    @staticmethod
    def get_projects(plant):
        projects = Project.objects.filter(plant=plant)
        serializer = ProjectMiniMicroSerializer(projects, many=True)
        return serializer.data

    class Meta:
        model = Plant
        fields = ('id', 'plant_name', 'project', 'client')


"""
   This class will serialize Plant model for write operation.
"""
class PlantWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plant
        fields = '__all__'


"""
   This class will serialize PlantDocument model.
"""
class PlantDocumentSerializer(serializers.ModelSerializer):
    doc_name = serializers.SerializerMethodField()

    def get_doc_name(self, obj):
        return str(obj.document_file) if obj.document_file else ''

    class Meta:
        model = PlantDocument
        fields = '__all__'


"""
   This class will serialize PlantImage model.
"""
class PlantImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlantImage
        fields = '__all__'

"""
   This class will serialize PlantDashboard model.
"""
class PlantDashboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlantDashboard
        fields = '__all__'


"""
   This class will serialize PlantDashboard model.
"""
class PlantDashboardMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlantDashboard
        fields = ['url', 'embedded_content', 'password', 'dashboard_name', 'id']


"""
   This class will serialize PlantScheduleVII model.
"""
class PlantScheduleVIIActivitySerializer(BulkSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = PlantScheduleVII
        fields = '__all__'
