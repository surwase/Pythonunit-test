from django.urls import reverse
from .test_setup import TestSetUpwatcher
from rest_framework import status
from v2.project.models import Project, TaskComment, Milestone, Task, Disbursement, ProjectUser, ProjectLocation, ProjectFocusArea, ProjectSubFocusArea, ProjectTargetSegment, ProjectSubTargetSegment, ProjectSDGGoals, ProjectTag, ProjectUser, ProjectImage, ProjectDocument, MilestoneNote, MilestoneDocument, Task, SubTask, TaskDocument, DisbursementComment, DisbursementDocument, ProjectIndicator, ProjectOutput, ProjectOutcome
from v2.project.views import *
from unittest.mock import patch
from domainlibraries.models import ApplicationRole
from django.core.files.uploadedfile import SimpleUploadedFile

class TestProject(TestSetUpwatcher):
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):

        super().setUp()

        self.ngo_obj = NGOPartner.objects.create(
            ngo_name="ngo1",
            company_type="type1",
            website_url="www.ngo1.com",
            about="hi i am ngo test",
            address="indira nagar",
            employee_count=100,
            annual_budget=1000,
            experience=5,
            awards="abc award",
            state="up"
        )
        self.plant_obj = Plant.objects.create(
            plant_name="plant1",
            plant_description="plantdec",
            client=self.client_obj
        )
        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            plant=self.plant_obj,
            budget=1000,
            ngo_partner=self.ngo_obj,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.projdata={
            "project_name":"abcd",
            "project_description":"hi i am tested",
            "client":self.client_obj.id,
            "plant":self.plant_obj.id,
            "budget":1000,
            "ngo_partner":self.ngo_obj.id,
            "project_status":72.22,
            "total_disbursement_amount":8804465,
            "planned_disbursement_amount":8804465,
            "total_utilised_amount":7000000
        }
        self.user2 = User.objects.create_user(
            username='sonika',
            password='password',
            first_name='sonika',
            last_name='sharan',
            name='sonika sharan',
            email='sonika@test.com'
        )
        self.user2.application_role, created = ApplicationRole.objects.get_or_create(role_name='sattva-admin')

        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user=self.user)

        self.projuserdata={
            "project":self.proj.id,
            "user":self.user2.id
        }
        self.projdoc= ProjectDocument.objects.create(
            project=self.proj,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size=10,
            document_tag=['tag1'],
            path="image")

        self.projdocdata= {
            "project":self.proj.id,
            # "document_file": SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size":20,
            "document_tag":['tag2'],
            "path":"image2"}


        #urls
        self.project_url= reverse('v2:project:project')
        self.projectmicrodetails_url= reverse('v2:project:project_details')
        self.unaddedproject_url= reverse('v2:project:unadded_projects')
        self.unaddedplant_url= reverse('v2:project:unadded_plants')
        self.projectuser_url= reverse('v2:project:project_user')
        self.deleteprojectuser_url= reverse('v2:project:delete_project_user')
        self.projectdoc_url= reverse('v2:project:project_doc')


    def test_project_list(self):
        request = self.factory.get(self.project_url)
        request.user = self.user
        view = ProjectModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_projectmicrodetails_list(self):
        request = self.factory.get(self.projectmicrodetails_url)
        request.user = self.user
        view = ProjectMicroModelViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_unadded_projects_url(self):
        request = self.factory.get(self.projectmicrodetails_url)
        request.user = self.user
        view = UserUnAddedProjectsAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_unadded_plants_url(self):
        request = self.factory.get(self.unaddedplant_url)
        request.user = self.user
        view = UserUnAddedPlantAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_projectuser_list(self):
        request = self.factory.get(self.projectuser_url)
        request.user = self.user
        view = UserProjectViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


    def test_projectdoc_list(self):
        request = self.factory.get(self.projectdoc_url)
        request.user = self.user
        view = ProjectDocumentModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_projectdoc_create(self):
    #     request = self.factory.post(self.projectdoc_url, self.projdocdata, format='json')
    #     request.user = self.user
    #     view = ProjectDocumentModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['path'],self.projdoc['path'])

    def test_projectdoc_get(self):
        request = self.factory.get(self.projectdoc_url,self.projdocdata, format='json')
        request.user = self.user
        view = ProjectDocumentModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projdoc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'],'image')

    def test_projectdoc_patch(self):
        request = self.factory.patch(self.projectdoc_url,self.projdocdata, format='json')
        request.user = self.user
        view = ProjectDocumentModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projdoc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'],'image2')

    def test_projectdoc_delete(self):
        request = self.factory.delete(self.projectdoc_url,self.projdocdata, format='json')
        request.user = self.user
        view = ProjectDocumentModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projdoc.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class TestProjectTagModelViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()
        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.projdata={
            "project_name":"abcd",
            "project_description":"hi i am tested",
            "client":self.client_obj.id,
            "budget":1000,
            "project_status":72.22,
            "total_disbursement_amount":8804465,
            "planned_disbursement_amount":8804465,
            "total_utilised_amount":7000000
        }

        self.projtag=ProjectTag.objects.create(
            project= self.proj,
            tag=['tag12'])

        #urls
        self.projecttag_url= reverse('v2:project:project_tags')

    def test_projecttag_list(self):
        request = self.factory.get(self.projecttag_url)
        request.user = self.user
        view =  ProjectTagModelViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class TestProjectImageModelViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):

        super().setUp()
        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.proj2=Project.objects.create(
            project_name="ppp",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        self.projimg=ProjectImage.objects.create(
            project=self.proj,
            image_file = SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            image_size=10,
            document_tag=['tag1'],
            path="image")

        self.projimgdata={
            "project":self.proj.id,
            # image_file = SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "image_size":20,
            "document_tag":['tag1'],
            "path":"image2"}

        self.projimgdata2={
            "project":self.proj2.id,
            # image_file = SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "image_size":20,
            "document_tag":['tag1'],
            "path":"image2"}

        #urls
        self.projectimage_url= reverse('v2:project:project_images')

    def test_projectimage_list(self):
        request = self.factory.get(self.projectimage_url)
        request.user = self.user
        view = ProjectImageModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_projectimage_create(self):
    #     request = self.factory.post(self.projectimage_url,self.projimgdata2, format='json')
    #     request.user = self.user
    #     view = ProjectImageModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['path'], "image2")

    def test_projectimage_get(self):
        request = self.factory.get(self.projectimage_url,self.projimgdata, format='json')
        request.user = self.user
        view = ProjectImageModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projimg.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # def test_projectimage_put(self):
    #     request = self.factory.put(self.projectimage_url,self.projimgdata, format='json')
    #     request.user = self.user
    #     view = ProjectImageModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.projimg.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['path'],'image2')

    def test_projectimage_patch(self):
        request = self.factory.patch(self.projectimage_url,self.projimgdata, format='json')
        request.user = self.user
        view =  ProjectImageModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projimg.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_projectimage_delete(self):
        request = self.factory.delete(self.projectimage_url,self.projimgdata, format='json')
        request.user = self.user
        view =  ProjectImageModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projimg.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class TestProjectSettingModelViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):

        super().setUp()
        self.ngo_obj = NGOPartner.objects.create(
            ngo_name="ngo1",
            company_type="type1",
            website_url="www.ngo1.com",
            about="hi i am ngo test",
            address="indira nagar",
            employee_count=100,
            annual_budget=1000,
            experience=5,
            awards="abc award",
            state="up"
        )
        self.plant_obj = Plant.objects.create(
            plant_name="plant1",
            plant_description="plantdec",
            client=self.client_obj
        )
        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            plant=self.plant_obj,
            budget=1000,
            ngo_partner=self.ngo_obj,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.proj2=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            plant=self.plant_obj,
            budget=1000,
            ngo_partner=self.ngo_obj,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.projectsetting=ProjectSetting.objects.create(
            project = self.proj,
            disbursement_visible_fields = ['visible'],
            utilisation_visible_fields = ['visible'],
            expense_category = ['xyz'],
            task_visible_fields = ['visible'],
            milestone_visible_fields = ['visible']
        )
        self.projectsettingdata={
            "project" : self.proj.id,
            "disbursement_visible_fields" : ['notvisible'],
            "utilisation_visible_fields":['notvisible'],
            "expense_category" :['ppp'],
            "task_visible_fields" : ['visible'],
            "milestone_visible_fields" : ['visible']
        }
        self.projectsettingdata2={
            "project" : self.proj2.id,
            "disbursement_visible_fields" : ['notvisible'],
            "utilisation_visible_fields":['notvisible'],
            "expense_category" :['ppp'],
            "task_visible_fields" : ['visible'],
            "milestone_visible_fields" : ['visible']
        }


        #urls
        self.projectsetting_url= reverse('v2:project:project_settings')

    def test_projectsetting_list(self):
        request = self.factory.get(self.projectsetting_url)
        request.user = self.user
        view = ProjectSettingModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_projectsetting_get(self):
        request = self.factory.get(self.projectsetting_url,self.projectsettingdata, format='json')
        request.user = self.user
        view =  ProjectSettingModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectsetting.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['expense_category'], ['xyz'])

    def test_projectsetting_put(self):
        request = self.factory.put(self.projectsetting_url,self.projectsettingdata, format='json')
        request.user = self.user
        view =  ProjectSettingModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectsetting.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['expense_category'], ['ppp'])

    def test_projectsetting_patch(self):
        request = self.factory.patch(self.projectsetting_url,self.projectsettingdata, format='json')
        request.user = self.user
        view =   ProjectSettingModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectsetting.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['expense_category'], ['ppp'])

    def test_projectsetting_delete(self):
        request = self.factory.delete(self.projectsetting_url,self.projectsettingdata, format='json')
        request.user = self.user
        view =   ProjectSettingModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectsetting.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class TestMilestoneModelViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        self.project=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.milestone=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.project,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        self.milestonedata={
            "milestone_name":"milestone2",
            "milestone_description":"hi i am milestone",
            "status":"complete",
            "project":self.project.id,
            "position":1,
            "sub_focus_area":['malnutrition'],
            "sub_target_segment":['children'],
            "location":['lucknow'],
            "tags":['tag1', 'tag2'],
        }
        self.milestonedata2={
            "milestone_name":"milestone2000",
            "milestone_description":"hi i am milestone",
            "status":"complete",
            "project":self.project.id,
            "position":1,
            "sub_focus_area":['malnutrition'],
            "sub_target_segment":['children'],
            "location":['lucknow'],
            "tags":['tag1', 'tag2'],
        }

        #urls
        self.milestonelist_url= reverse('v2:project:milestone-list')
        self.minimilestone_url= reverse('v2:project:mini_milestone')
        self.milestone_url= reverse('v2:project:milestone')

    def test_milestone_list(self):
        request = self.factory.get(self.milestonelist_url)
        request.user = self.user
        view =  MilestoneListModelViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_minimilestone_list(self):
        request = self.factory.get(self.minimilestone_url)
        request.user = self.user
        view = MiniMilestoneModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # @patch('v2.project.tasks.calculate_project_status.apply_async')
    # def test_minimilestone_create(self,mock_send):
    #     request = self.factory.post(self.minimilestone_url,self.milestonedata2,format='json')
    #     request.user = self.user
    #     view = MiniMilestoneModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['milestone_name'], "milestone2000")

    def test_milestone_list(self):
        request = self.factory.get(self.milestone_url)
        request.user = self.user
        view = MilestoneModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_milestone_create(self,mock_send):
        request = self.factory.post(self.milestone_url,self.milestonedata,format='json')
        request.user = self.user
        view = MilestoneModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['milestone_name'], "milestone2")

    def test_milestone_get(self):
        request = self.factory.get(self.milestone_url,self.milestonedata,format='json')
        request.user = self.user
        view = MilestoneModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.milestone.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_milestone_put(self, mock_send):
        request = self.factory.put(self.milestone_url,self.milestonedata,format='json')
        request.user = self.user
        view = MilestoneModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.milestone.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_milestone_patch(self, mock_send):
        request = self.factory.patch(self.milestone_url,self.milestonedata,format='json')
        request.user = self.user
        view = MilestoneModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.milestone.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_milestone_delete(self, mock_send):
        request = self.factory.delete(self.milestone_url,self.milestonedata,format='json')
        request.user = self.user
        view = MilestoneModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.milestone.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class TestMilestoneNoteModelViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        self.milestone=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.proj,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        self.notes=MilestoneNote.objects.create(
            milestone=self.milestone,
            note="hi i am note",
            is_read=True
        )
        self.data={
            "milestone":self.milestone.id,
            "note":"hi i am note3",
            "is_read":True
        }

        #url
        self.milestonenote_url= reverse('v2:project:project_doc')

    def test_milestonenotes_list(self):
        request = self.factory.get(self.milestonenote_url)
        request.user = self.user
        view = MilestoneNoteModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_milestonenotes_create(self):
        request = self.factory.post(self.milestonenote_url,self.data,format='json')
        request.user = self.user
        view = MilestoneNoteModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['note'], "hi i am note3")


class TestMilestoneDocumentModelViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.milestone=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.proj,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )
        self.doc=MilestoneDocument.objects.create(
            milestone=self.milestone,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size=10,
            document_tag=['tag1'],
            path="image"
        )
        self.data={
            "milestone":self.milestone.id,
            #  document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size":10,
            "document_tag":['tag1'],
            "path":"image2"
        }
        self.data2={
            "milestone":self.milestone.id,
            #  document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size":20,
            "document_tag":['tag1'],
            "path":"image2"
        }
        #url
        self.milestonenote_url= reverse('v2:project:project_doc')

    def test_milestonedoc_list(self):
        request = self.factory.get(self.milestonenote_url)
        request.user = self.user
        view = MilestoneDocumentModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TestTaskModelViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        self.mile=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.proj,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        self.task=Task.objects.create(
            task_name="task1",
            task_description="i am task",
            priority="high",
            status="pending",
            task_tag="tag1",
            tags=['tag1'],
            milestone=self.mile,
            client=self.client_obj,
            project=self.proj,
            percentage_completed=79.6,
            position=100
        )
        self.data={
            "task_name":"task2",
            "task_description":"i am task",
            "priority":"high",
            "status":"pending",
            "task_tag":"tag1",
            "tags":['tag1'],
            "milestone":self.mile.id,
            "client":self.client_obj.id,
            "project":self.proj.id,
            "percentage_completed":79.6,
            "position":100
        }
        self.comment=TaskComment.objects.create(
            task=self.task,
            comment="task1",
            is_read=True
        )
        self.cmntdata={
            "task":self.task.id,
            "comment":"task12",
            "is_read":True
        }
        self.doc=TaskDocument.objects.create(
            task=self.task,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size=10,
            document_tag=['tag1'],
            path="image"
        )
        self.docdata={
            "task":self.task.id,
           # "document_file": SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size":20,
            "document_tag":['tag2'],
            "path":"image2"
        }
        self.activitylog=TaskActivityLog.objects.create(
            task = self.task,
            old_value = ['old'],
            new_value = ['new'],
            action = "action",
            update_text = "text"
        )

        #url
        self.task_url= reverse('v2:project:task')
        self.taskcomment_url= reverse('v2:project:task_comment')
        self.taskdoc_url=reverse('v2:project:task_document')
        self.task_activity_log_url=reverse('v2:project:task_activity_log')

    def test_task_list(self):
        request = self.factory.get(self.task_url)
        request.user = self.user
        view = TaskModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_task_create(self, mock_send):
        request = self.factory.post(self.task_url,self.data,format='json')
        request.user = self.user
        view = TaskModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['task_name'], "task2")

    def test_task_get(self):
        request = self.factory.get(self.task_url,self.data,format='json')
        request.user = self.user
        view = TaskModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.task.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_task_put(self,mock_send):
        request = self.factory.put(self.task_url,self.data,format='json')
        request.user = self.user
        view = TaskModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.task.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_task_patch(self, mock_send):
        request = self.factory.patch(self.task_url,self.data,format='json')
        request.user = self.user
        view =TaskModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.task.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_task_delete(self, mock_send):
        request = self.factory.delete(self.task_url,self.data,format='json')
        request.user = self.user
        view = TaskModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.task.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_taskactivitylog_list(self):
        request = self.factory.get(self.task_activity_log_url)
        request.user = self.user
        view = TaskActivityLogViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    #task comments
    def test_taskcmnt_list(self):
        request = self.factory.get(self.taskcomment_url)
        request.user = self.user
        view = TaskCommentModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_taskcmnt_create(self):
        request = self.factory.post(self.taskcomment_url,self.cmntdata,format='json')
        request.user = self.user
        view = TaskCommentModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comment'], "task12")

    #task document
    def test_taskdoc_list(self):
        request = self.factory.get(self.taskdoc_url)
        request.user = self.user
        view = TaskDocumentModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TestSubTaskModelViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()
        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user=self.user)

        self.mile=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.proj,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        self.task=Task.objects.create(
            task_name="task1",
            task_description="i am task",
            priority="high",
            status="pending",
            task_tag="tag1",
            tags=['tag1'],
            milestone=self.mile,
            client=self.client_obj,
            project=self.proj,
            percentage_completed=79.6,
            position=100
        )
        self.subtask=SubTask.objects.create(
            task = self.task,
            sub_task_description = "hi i am sub task",

        )
        self.subtask.participants.add(self.projuser)
        self.data={
            "task":self.task.id,
            "sub_task_description":"hi i am sub task2",
        }

        #url
        self.subtask_url= reverse('v2:project:sub_task')
        self.milestone_activity_log_url=reverse('v2:project:milestone-activity_log')

    def test_subtask_list(self):
        request = self.factory.get(self.subtask_url)
        request.user = self.user
        view = SubTaskModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_subtask_create(self):
    #     request = self.factory.post(self.subtask_url,self.data,format='json')
    #     request.user = self.user
    #     view = SubTaskModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['sub_task_description'], "hi i am sub task2")

    def test_milestoneactivitylog_list(self):
        request = self.factory.get(self.milestone_activity_log_url)
        request.user = self.user
        view = MilestoneActivityLogViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class TestProjectIndicatorViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        self.mile=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.proj,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        self.indicator=ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = self.mile,
            period_name = "period1",
            project = self.proj,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )

        self.data={
            "frequency" : "MONTHLY",
            "milestone" : self.mile.id,
            "period_name" : "period1",
            "project" : self.proj.id,
            "output_indicator" : "indicatoroutput",
            "output_calculation" : "SUM",
            "outcome_indicator" :"indicatoroutcome",
            "outcome_calculation" : "AVERAGE",
            "is_custom_workflow" : True,
            "is_beneficiary" : False,
            "output_status" : "Verified",
            "outcome_status" : "Verified",
            "position" : 1,
        }
        self.doc=IndicatorDocument.objects.create(
            indicator = self.indicator,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size = 10,
            document_tag = ['tag1'],
            path = "xyz"
        )
        self.inddata={
            "indicator ": self.indicator.id,
            #"document_file": SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size" : 20,
            "document_tag" : ['tag2'],
            "path" : "abc"
        }
        self.comment= IndicatorComment.objects.create(
            indicator = self.indicator,
            comment = "comment1"
        )
        self.commentdata={
            "indicator": self.indicator.id,
            "comment": "comment2"
        }
        self.indicatoractvitylog=IndicatorActivityLog.objects.create(
            indicator = self.indicator,
            old_value = ['old'],
            new_value = ['new'],
            action = "action",
            update_text = "text"
        )
        self.indicatoractvitylogdata={
            "indicator" : self.indicator.id,
            "old_value" : ['old'],
            "new_value" : ['new'],
            "action" : "action",
            "update_text" : "text"
        }

        #url
        self.delete_project_indicator_url= reverse('v2:project:delete_project_indicator',kwargs={'pk':self.indicator.id})
        self.indicator_document_url=reverse('v2:project:indicator_document')
        self.indicator_comment_url=reverse('v2:project:indicator_comment')
        self.indicator_activity_log_url=reverse('v2:project:indicator_activity_log')

    def test_indicator_document_list(self):
        request = self.factory.get(self.indicator_document_url)
        request.user = self.user
        view = IndicatorDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_indicator_document_create(self):
    #     request = self.factory.post(self.indicator_document_url,self.inddata,format='json')
    #     request.user = self.user
    #     view = IndicatorDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['path'], "abc")

    def test_indicator_document_get(self):
        request = self.factory.get(self.indicator_document_url,self.inddata,format='json')
        request.user = self.user
        view = IndicatorDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.doc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "xyz")

 #comment

    def test_indicator_comment_list(self):
        request = self.factory.get(self.indicator_comment_url)
        request.user = self.user
        view = IndicatorCommentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_indicator_comment_create(self):
        request = self.factory.post(self.indicator_comment_url,self.commentdata,format='json')
        request.user = self.user
        view = IndicatorCommentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comment'], "comment2")

    def test_indicator_comment_get(self):
        request = self.factory.get(self.indicator_comment_url,self.commentdata,format='json')
        request.user = self.user
        view = IndicatorCommentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.comment.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], "comment1")

    def test_indicator_activity_log_url_list(self):
        request = self.factory.get(self.indicator_activity_log_url)
        request.user = self.user
        view = IndicatorActivityLogViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class TestProjectOutputMiniViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.user2 = User.objects.create_user(
            username='sonika',
            password='password',
            first_name='sonika',
            last_name='sharan',
            name='sonika sharan',
            email='sonika@test.com'
        )
        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user= self.user)

        self.mile=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.proj,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        self.indicator=ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = self.mile,
            period_name = "period1",
            project = self.proj,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )

        self.projectoutput=ProjectOutput.objects.create(
            project_indicator = self.indicator,
            # scheduled_date = "2020-06-22",
            # period_end_date ="2021-03-22",
            planned_output = 12.5,
            actual_output = 15.5,
            period_name = "project1",
            project = self.proj,
            is_included = True,
            comments = "comment1"
        )
        self.data={
            "project_indicator": self.indicator.id,
            "scheduled_date": "2020-06-22",
            "period_end_date":"2021-03-22",
            # "planned_output": 12.5,
            # "actual_output": 15.5,
            "period_name": "project1",
            "project": self.proj.id,
            "is_included": True,
            "comments": "comment2"
        }
        self.projectoutcome=ProjectOutcome.objects.create(
            project_indicator = self.indicator,
            scheduled_date = "2020-06-22",
            period_end_date ="2021-03-22",
            # planned_output = 12.5,
            # actual_output = 15.5,
            period_name = "project1",
            project = self.proj,
            is_included = True,
            comments = "comment1"
        )
        self.outcomedata={
            "project_indicator": self.indicator.id,
            "scheduled_date": "2020-06-22",
            "period_end_date":"2021-03-22",
            # "planned_output": 12.5,
            # "actual_output": 15.5,
            "period_name": "project1",
            "project": self.proj.id,
            "is_included": True,
            "comments": "comment1"
        }
        self.projectoutputdoc=ProjectOutputDocument.objects.create(
            project_output = self.projectoutput,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size = 10,
            document_tag = ['tag1'],
            path = "xyz",
            parent = "parent"
                )
        self.podata={
            "project_output" : self.projectoutput.id,
            "doc_size" : 20,
            "document_tag" : ['tag2'],
            "path" : "abc",
            "parent" :"parent"

        }
        self.projectoutcomedoc=ProjectOutcomeDocument.objects.create(
            project_outcome = self.projectoutcome,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size = 10,
            document_tag = ['tag1'],
            path = "xyz",
            parent = "parent"
                )
        self.pooutcomedocdata={
            "project_outcome" : self.projectoutcome.id,
            "doc_size" : 20,
            "document_tag" : ['tag2'],
            "path" : "abc",
            "parent" :"parent"

        }

       #urls
        self.projectoutput_url=reverse('v2:project:project_indicator_output')
        self.projectoutcome_url=reverse('v2:project:project_indicator_outcome')
        self.projectoutput_document_url=reverse('v2:project:project_output_document')
        self.projectoutcome_document_url=reverse('v2:project:project_outcome_document')
        self.projectoutputt_url=reverse('v2:project:project_output')
        self.projectoutcomee_url=reverse('v2:project:project_outcome')


    def test_projectoutput_url_list(self):
        request = self.factory.get(self.projectoutput_url)
        request.user = self.user
        view = ProjectOutputMiniViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_projectoutcome_url_list(self):
        request = self.factory.get(self.projectoutcome_url)
        request.user = self.user
        view = ProjectOutcomeMiniViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

#document

    def test_projectoutput_document_list(self):
        request = self.factory.get(self.projectoutput_document_url)
        request.user = self.user
        view = ProjectOutputDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    # def test_projectoutput_document_create(self):
    #     request = self.factory.post(self.projectoutput_document_url,self.podata,format='json')
    #     request.user = self.user
    #     view = ProjectOutputDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['path'], "abc")

    def test_projectoutput_document_get(self):
        request = self.factory.get(self.projectoutput_document_url,self.podata,format='json')
        request.user = self.user
        view = ProjectOutputDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectoutputdoc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "xyz")

    # def test_projectoutput_document_put(self):
    #     request = self.factory.put(self.projectoutput_document_url,self.podata,format='json')
    #     request.user = self.user
    #     view = ProjectOutputDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.projectoutputdoc.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['path'], "xyz")

    def test_projectoutput_document_patch(self):
        request = self.factory.patch(self.projectoutput_document_url,self.podata,format='json')
        request.user = self.user
        view =ProjectOutputDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectoutputdoc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "abc")

    def test_projectoutput_document_delete(self):
        request = self.factory.delete(self.projectoutput_document_url,self.podata,format='json')
        request.user = self.user
        view = ProjectOutputDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectoutputdoc.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#document outcome

    def test_projectoutcome_document_list(self):
        request = self.factory.get(self.projectoutcome_document_url)
        request.user = self.user
        view = ProjectOutcomeDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    # def test_projectoutcome_document_create(self):
    #     request = self.factory.post(self.projectoutcome_document_url,self.podata,format='json')
    #     request.user = self.user
    #     view = ProjectOutcomeDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['path'], "abc")

    def test_projectoutcome_document_get(self):
        request = self.factory.get(self.projectoutcome_document_url,self.podata,format='json')
        request.user = self.user
        view = ProjectOutcomeDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectoutcomedoc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "xyz")

    # def test_projectoutcome_document_put(self):
    #     request = self.factory.put(self.projectoutcome_document_url,self.podata,format='json')
    #     request.user = self.user
    #     view = ProjectOutcomeDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.projectoutcomedoc.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['path'], "xyz")

    def test_projectoutcome_document_patch(self):
        request = self.factory.patch(self.projectoutcome_document_url,self.podata,format='json')
        request.user = self.user
        view =ProjectOutcomeDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectoutcomedoc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "abc")

    def test_projectoutcome_document_delete(self):
        request = self.factory.delete(self.projectoutcome_document_url,self.podata,format='json')
        request.user = self.user
        view = ProjectOutcomeDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectoutcomedoc.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#output

    def test_projectoutputt__list(self):
        request = self.factory.get(self.projectoutputt_url)
        request.user = self.user
        view = ProjectOutputViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_projectoutputt_create(self):
        request = self.factory.post(self.projectoutputt_url, self.data, format='json')
        request.user = self.user
        view = ProjectOutputViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comments'], self.data['comments'])

    def test_projectoutputt_get(self):
        request = self.factory.get(self.projectoutputt_url, self.data, format='json')
        request.user = self.user
        view = ProjectOutputViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectoutput.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comments'],'comment1')

#outcome

    def test_projectoutcomee__list(self):
        request = self.factory.get(self.projectoutcomee_url)
        request.user = self.user
        view = ProjectOutcomeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_projectoutcomee_create(self):
        request = self.factory.post(self.projectoutcomee_url, self.outcomedata, format='json')
        request.user = self.user
        view = ProjectOutcomeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comments'], self.outcomedata['comments'])

    def test_projectoutcomee_get(self):
        request = self.factory.get(self.projectoutcomee_url, self.outcomedata, format='json')
        request.user = self.user
        view = ProjectOutcomeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.projectoutcome.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comments'],'comment1')


class TestProjectOutcomeStatusViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_outcome_data_status.apply_async')
    @patch('v2.project.tasks.calculate_output_data_status.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2,mock_send3):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.user2 = User.objects.create_user(
            username='sonika',
            password='password',
            first_name='sonika',
            last_name='sharan',
            name='sonika sharan',
            email='sonika@test.com'
        )
        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user= self.user)

        self.mile=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.proj,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        self.indicator=ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = self.mile,
            period_name = "period1",
            project = self.proj,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )

        self.projectoutput=ProjectOutput.objects.create(
            project_indicator = self.indicator,
            # scheduled_date = "2020-06-22",
            # period_end_date ="2021-03-22",
            planned_output = 12.5,
            actual_output = 15.5,
            period_name = "project1",
            project = self.proj,
            is_included = True,
            comments = "comment1"
        )
        self.projectoutcome=ProjectOutcome.objects.create(
            project_indicator = self.indicator,
            scheduled_date = "2020-06-22",
            period_end_date ="2021-03-22",
            # planned_output = 12.5,
            # actual_output = 15.5,
            period_name = "project1",
            project = self.proj,
            is_included = True,
            comments = "comment1"
        )
        self.outputstatus=ProjectOutputStatus.objects.create(
            projectOutput = self.projectoutput,
            status = "APPROVED",
            comment = "comment1"
        )
        self.outputstatusdata={
            "projectOutput" : self.projectoutput.id,
            "action_by" : self.projuser.id,
            "status" : "APPROVED",
            "comment" : "comment2"
        }
        self.outcomestatus=ProjectOutcomeStatus.objects.create(
            projectOutcome = self.projectoutcome,
            status = "APPROVED",
            comment = "comment1"
        )
        self.outcomestatusdata={
            "projectOutcome" : self.projectoutcome.id,
            "status" : "APPROVED",
            "comment" : "comment2"
        }

        #url
        self.projectoutputstatus_url=reverse('v2:project:project_output_status')
        self.projectoutcomestatus_url=reverse('v2:project:project_outcome_status')

    #status output

    @patch('v2.project.tasks.calculate_output_data_status.apply_async')
    def test_projectoutputstatus__list(self,mock_send):
        request = self.factory.get(self.projectoutputstatus_url)
        request.user = self.user
        view = ProjectOutputStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    @patch('v2.project.tasks.calculate_output_data_status.apply_async')
    def test_projectoutputstatus_create(self, mock_send):
        request = self.factory.post(self.projectoutputstatus_url, self.outputstatusdata, format='json')
        request.user = self.user
        view = ProjectOutputStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comment'], self.outputstatusdata['comment'])

    @patch('v2.project.tasks.calculate_output_data_status.apply_async')
    def test_projectoutputstatus_get(self, mock_send):
        request = self.factory.get(self.projectoutputstatus_url, self.outputstatusdata, format='json')
        request.user = self.user
        view = ProjectOutputStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.outputstatus.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'],'comment1')

    @patch('v2.project.tasks.calculate_output_data_status.apply_async')
    def test_projectoutputstatus_put(self,mock_send):
        request = self.factory.put(self.projectoutputstatus_url, self.outputstatusdata, format='json')
        request.user = self.user
        view = ProjectOutputStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.outputstatus.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], self.outputstatusdata['comment'])

    @patch('v2.project.tasks.calculate_output_data_status.apply_async')
    def test_projectoutputstatus_patch(self, mock_send):
        request = self.factory.patch(self.projectoutputstatus_url, self.outputstatusdata, format='json')
        request.user = self.user
        view = ProjectOutputStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.outputstatus.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], self.outputstatusdata['comment'])

    @patch('v2.project.tasks.calculate_output_data_status.apply_async')
    def test_projectoutputstatus_delete(self, mock_send):
        request = self.factory.delete(self.projectoutputstatus_url, self.outputstatusdata, format='json')
        request.user = self.user
        view = ProjectOutputStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.outputstatus.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

# #outcome status

    @patch('v2.project.tasks.calculate_outcome_data_status.apply_async')
    def test_projectoutcomestatus__list(self, mock_send):
        request = self.factory.get(self.projectoutcomestatus_url)
        request.user = self.user
        view = ProjectOutcomeStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    @patch('v2.project.tasks.calculate_outcome_data_status.apply_async')
    def test_projectoutcomestatus_create(self, mock_send):
        request = self.factory.post(self.projectoutcomestatus_url, self.outcomestatusdata, format='json')
        request.user = self.user
        view = ProjectOutcomeStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comment'], self.outcomestatusdata['comment'])

    @patch('v2.project.tasks.calculate_outcome_data_status.apply_async')
    def test_projectoutcomestatus_get(self, mock_send):
        request = self.factory.get(self.projectoutcomestatus_url, self.outcomestatusdata, format='json')
        request.user = self.user
        view = ProjectOutcomeStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.outcomestatus.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'],'comment1')

    @patch('v2.project.tasks.calculate_outcome_data_status.apply_async')
    def test_projectoutcomestatus_put(self,mock_send):
        request = self.factory.put(self.projectoutcomestatus_url, self.outcomestatusdata, format='json')
        request.user = self.user
        view = ProjectOutcomeStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.outcomestatus.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], self.outcomestatusdata['comment'])

    @patch('v2.project.tasks.calculate_outcome_data_status.apply_async')
    def test_projectoutcomestatus_patch(self, mock_send):
        request = self.factory.patch(self.projectoutcomestatus_url, self.outcomestatusdata, format='json')
        request.user = self.user
        view = ProjectOutcomeStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.outcomestatus.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], self.outcomestatusdata['comment'])

    @patch('v2.project.tasks.calculate_outcome_data_status.apply_async')
    def test_projectoutcomestatus_delete(self, mock_send):
        request = self.factory.delete(self.projectoutcomestatus_url, self.outcomestatusdata, format='json')
        request.user = self.user
        view = ProjectOutcomeStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.outcomestatus.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class TestImpactCaseStudyViewSetViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.impactcasestudy=ImpactCaseStudy.objects.create(
            project = self.proj,
            title = "title",
            description = "hi i am impact",
            focus_area = ['focus'],
            sub_focus_area = ['subfocus'],
            target_segment = ['target'],
            sub_target_segment = ['subtarget'],
            location = ['lucknow'],
            tags = ['tag1'],
            is_key_case_study = True
        )
        self.data={
            "project" : self.proj.id,
            "title" : "titleeee",
            "description" : "hi i am impact",
            "focus_area" : ['focus'],
            "sub_focus_area" : ['subfocus'],
            "target_segment" : ['target'],
            "sub_target_segment" : ['subtarget'],
            "location" : ['lucknow'],
            "tags" : ['tag1'],
            "is_key_case_study" : True
        }

        self.documents = CaseStudyDocuments.objects.create(
            case_study = self.impactcasestudy,
            #  document_file: FileField(file_name='angular.png', content=open('D:\\angular.png', 'rb').read(), content_type='image/png'),
            doc_size = 10,
            document_tag = ['tag1'],
            path = "xyz"
        )
        self.documentsdoc = {
            "case_study" : self.impactcasestudy.id,
            #  document_file: FileField(file_name='angular.png', content=open('D:\\angular.png', 'rb').read(), content_type='image/png'),
            "doc_size" :10,
            "document_tag" : ['tag1'],
            "path" : "xyz"
        }
        self.casestudyactivitylog=CaseStudyActivityLog.objects.create(
            case_study = self.impactcasestudy,
            old_value = ['old'],
            new_value = ['newvalue'],
            action = "action",
            update_text = "updated text"
        )
        self.activitydata={
            "case_study" : self.impactcasestudy.id,
            "old_value" : ['old2'],
            "new_value" : ['newvalue2'],
            "action" : "action2",
            "update_text" : "updated text"
        }

        #urls
        self.impactcasestudy_url=reverse('v2:project:case_study')
        self.impactcasestudydoc_url=reverse('v2:project:case_study_documents')
        self.activitylog_url=reverse('v2:project:case_study_activity_log')

    def test_impactcasestudy__list(self):
        request = self.factory.get(self.impactcasestudy_url)
        request.user = self.user
        view = ImpactCaseStudyViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_impactcasestudy_create(self):
        request = self.factory.post(self.impactcasestudy_url, self.data, format='json')
        request.user = self.user
        view = ImpactCaseStudyViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['title'], self.data['title'])

    def test_impactcasestudy_get(self):
        request = self.factory.get(self.impactcasestudy_url, self.data, format='json')
        request.user = self.user
        view = ImpactCaseStudyViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.impactcasestudy.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'],'title')

#document

    def test_impactcasestudydoc__list(self):
        request = self.factory.get(self.impactcasestudydoc_url)
        request.user = self.user
        view =  CaseStudyDocumentsViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_impactcasestudydoc_create(self):
    #     request = self.factory.post(self.impactcasestudydoc_url, self.documentsdoc, format='json')
    #     request.user = self.user
    #     view =  CaseStudyDocumentsViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['path'], self.documentsdoc['path'])

    def test_impactcasestudydoc_get(self):
        request = self.factory.get(self.impactcasestudydoc_url, self.documentsdoc, format='json')
        request.user = self.user
        view = CaseStudyDocumentsViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.documents.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'],'xyz')

    # def test_impactcasestudydoc_put(self):
    #     request = self.factory.put(self.impactcasestudydoc_url, self.documentsdoc, format='json')
    #     request.user = self.user
    #     view =  CaseStudyDocumentsViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.documents.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['path'], self.documentsdoc['path'])

    def test_impactcasestudyctivitylog__list(self):
        request = self.factory.get(self.activitylog_url)
        request.user = self.user
        view = CaseStudyActivityLogViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class TestProjectIndicatorOutcomeViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user= self.user)

        self.mile=Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=self.proj,
            position=1,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        self.indicator=ProjectIndicator.objects.create(
            frequency = "MONTHLY",
            milestone = self.mile,
            period_name = "period1",
            project = self.proj,
            output_indicator = "indicatoroutput",
            output_calculation = "SUM",
            outcome_indicator ="indicatoroutcome",
            outcome_calculation = "AVERAGE",
            is_custom_workflow = True,
            is_beneficiary = False,
            output_status = "Verified",
            outcome_status = "Verified",
            position = 1,
        )
        self.projindicatoroutcome=ProjectIndicatorOutcome.objects.create(
            project_indicator = self.indicator,
            outcome = "outcome2",
            outcome_type = "SHORT"
        )

        self.data={
            "project_indicator" : self.indicator.id,
            "outcome" : "outcome21",
            "outcome_type" : "SHORT"
        }

        #url
        self.indicator_outcome_url=reverse('v2:project:indicator_outcome')

    def test_indicator_outcome__list(self):
        request = self.factory.get(self.indicator_outcome_url)
        request.user = self.user
        view = ProjectIndicatorOutcomeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_indicator_outcome_create(self):
        request = self.factory.post(self.indicator_outcome_url, self.data, format='json')
        request.user = self.user
        view = ProjectIndicatorOutcomeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['outcome'], self.data['outcome'])

    def test_indicator_outcome_get(self):
        request = self.factory.get(self.indicator_outcome_url, self.data, format='json')
        request.user = self.user
        view = ProjectIndicatorOutcomeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.projindicatoroutcome.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['outcome'],'outcome2')


class TestProjectRemarkViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )

        self.projectremark=ProjectRemark.objects.create(
            project = self.proj,
            qualitative_outcome = "text1",
            long_term_remark = "text2",
            short_term_remark = "text3"
        )
        self.data={
            "project" : self.proj.id,
            "qualitative_outcome" : "text1",
            "long_term_remark" : "text2",
            "short_term_remark" : "text3"
        }

        #url
        self.project_remark_url=reverse('v2:project:project_remark')

    def test_project_remark__list(self):
        request = self.factory.get(self.project_remark_url)
        request.user = self.user
        view = ProjectRemarkViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_project_remark_create(self):
        request = self.factory.post(self.project_remark_url, self.data, format='json')
        request.user = self.user
        view = ProjectRemarkViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['short_term_remark'], self.data['short_term_remark'])

    def test_project_remark_get(self):
        request = self.factory.get(self.project_remark_url, self.data, format='json')
        request.user = self.user
        view = ProjectRemarkViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.projectremark.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['short_term_remark'],'text3')

class TestTemplateTypeViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.templatetype = TemplateType.objects.create(
            template_type_id = 1,
            template_type ="hi"
        )

        self.temp=Template.objects.create(
            template_name = "temp1",
            template_type = self.templatetype,
            project = self.proj,
            client = self.client_obj,
            is_added = True,
            is_standard = True,
            standardization_requested = True,
            table_created = False,
            data_status = "Verified"
        )
        self.tempdata={
            "template_name" : "temp2",
            "template_type" : self.templatetype.template_type_id,
            "project" : self.proj.id,
            "client" : self.client_obj.id,
            "is_added" : True,
            "is_standard" : True,
            "standardization_requested" : True,
            "table_created" : False,
            "data_status" : "Verified"
        }
        self.csvupload=CSVUpload.objects.create(
            # csv_file = FileField(file_name='angular.png', content=open('D:\\angular.png', 'rb').read(), content_type='image/png'),
            client = self.client_obj,
            project = self.proj,
            template = self.temp,
            status = "Deleted",
            table_name = "tablename",
            batch_id = 2,
            upload_successful = True,
            is_deleted = True,
            is_custom_workflow = False
        )
        self.csvdata={
             # csv_file = FileField(file_name='angular.png', content=open('D:\\angular.png', 'rb').read(), content_type='image/png'),
            "client" :self.client_obj.id,
            "project":self.proj.id,
            "template" :self.temp.id,
            "status" :"Deleted",
            "table_name" :"tablename",
            "batch_id":1,
            "upload_successful" :True,
            "is_deleted" :True,
            "is_custom_workflow" :False
        }

        #url
        self.template_type_url=reverse('v2:project:template_type')
        self.template_url=reverse('v2:project:template')
        self.csvupload_list_url=reverse('v2:project:csvupload-list')
        self.csvupload_detail_url=reverse('v2:project:csvupload-detail',kwargs={'pk': self.csvupload.id})

    def test_template_type__list(self):
        request = self.factory.get(self.template_type_url)
        request.user = self.user
        view = TemplateTypeViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_template__list(self):
        request = self.factory.get(self.template_url)
        request.user = self.user
        view = TemplateViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_template_create(self):
        request = self.factory.post(self.template_url, self.tempdata, format='json')
        request.user = self.user
        view = TemplateViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['template_name'], self.tempdata['template_name'])

    def test_template_get(self):
        request = self.factory.get(self.template_url, self.tempdata, format='json')
        request.user = self.user
        view = TemplateViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.temp.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['template_name'],'temp1')

    def test_template_put(self):
        request = self.factory.put(self.template_url, self.tempdata, format='json')
        request.user = self.user
        view =  TemplateViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.temp.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['template_name'], self.tempdata['template_name'])

    def test_template_patch(self):
        request = self.factory.patch(self.template_url, self.tempdata, format='json')
        request.user = self.user
        view =  TemplateViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.temp.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['template_name'], self.tempdata['template_name'])

    def test_template_delete(self):
        request = self.factory.delete(self.template_url, self.tempdata, format='json')
        request.user = self.user
        view =  TemplateViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.temp.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#csv

    def test_csvupload_list(self):
        request = self.factory.get(self.csvupload_list_url)
        request.user = self.user
        view = TemplateViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    # def test_csvupload_create(self):
    #     request = self.factory.post(self.csvupload_list_url, self.csvdata, format='json')
    #     request.user = self.user
    #     view = TemplateViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['batch_id'], self.csvdata['batch_id'])

    def test_csvupload_detail_get(self):
        request = self.factory.get(self.csvupload_detail_url, self.csvdata, format='json')
        request.user = self.user
        view = CSVUploadViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.csvupload.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['batch_id'], 2)

    # def test_csvupload_detail_put(self):
    #     request = self.factory.put(self.csvupload_detail_url, self.csvdata, format='json')
    #     request.user = self.user
    #     view =  CSVUploadViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk= self.csvupload.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['batch_id'], self.csvdata['batch_id'])


class TestImpactDataAndTemplateActivityLog(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.templatetype = TemplateType.objects.create(
            template_type_id = 1,
            template_type ="hi"
        )

        self.temp=Template.objects.create(
            template_name = "temp1",
            template_type = self.templatetype,
            project = self.proj,
            client = self.client_obj,
            is_added = True,
            is_standard = True,
            standardization_requested = True,
            table_created = False,
            data_status = "Verified"
        )

        self.activitylog=ImpactDataAndTemplateActivityLog.objects.create(
            template = self.temp,
            old_value = ['oldvalue'],
            new_value = ['newvalue'],
            action = "action",
            update_text = "updatetext"
        )

        self.activitydata={
            "template" : self.temp.id,
            "old_value" : ['oldvalue'],
            "new_value" : ['newvalue'],
            "action" : "action",
            "update_text" : "updatetext"
        }

        #url
        self.template_activity_log_url=reverse('v2:project:template_activity_log')

    # def test_template_activity_log_list(self):
    #     request = self.factory.get(self.template_activity_log_url)
    #     request.user = self.user
    #     view = TemplateActivityLogViewSet.as_view({'get': 'list'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), 2)


class TestImpactUploadStatusViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_impact_data_status.apply_async')
    def setUp(self, mock_send):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user=self.user)

        self.templatetype = TemplateType.objects.create(
            template_type_id = 1,
            template_type ="hi"
        )

        self.temp=Template.objects.create(
            template_name = "temp1",
            template_type = self.templatetype,
            project = self.proj,
            client = self.client_obj,
            is_added = True,
            is_standard = True,
            standardization_requested = True,
            table_created = False,
            data_status = "Verified"
        )
        self.csvupload=CSVUpload.objects.create(
            # csv_file = FileField(file_name='angular.png', content=open('D:\\angular.png', 'rb').read(), content_type='image/png'),
            client = self.client_obj,
            project = self.proj,
            template = self.temp,
            status = "Deleted",
            table_name = "tablename",
            batch_id = 2,
            upload_successful = True,
            is_deleted = True,
            is_custom_workflow = False
        )
        self.impactupload=ImpactUploadStatus.objects.create(
            impact_upload =  self.csvupload,
            status ="APPROVED",
            comment = "comment",
        )
        self.data={
            "impact_upload": self.csvupload.id,
            "status":"APPROVED",
            "comment": "comment2",
        }

        #urls
        self.impact_upload_status_url=reverse('v2:project:impact_upload_status')

    def test_impact_upload_status__list(self):
        request = self.factory.get(self.impact_upload_status_url)
        request.user = self.user
        view = ImpactUploadStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    @patch('v2.project.tasks.calculate_impact_data_status.apply_async')
    def test_impact_upload_status_create(self, mock_send):
        request = self.factory.post(self.impact_upload_status_url, self.data, format='json')
        request.user = self.user
        view = ImpactUploadStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comment'], self.data['comment'])

    def test_impact_upload_status_get(self):
        request = self.factory.get(self.impact_upload_status_url, self.data, format='json')
        request.user = self.user
        view = ImpactUploadStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactupload.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'],'comment')

    @patch('v2.project.tasks.calculate_impact_data_status.apply_async')
    def test_impact_upload_status_put(self, mock_send):
        request = self.factory.put(self.impact_upload_status_url, self.data, format='json')
        request.user = self.user
        view =  ImpactUploadStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactupload.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], self.data['comment'])

    @patch('v2.project.tasks.calculate_impact_data_status.apply_async')
    def test_impact_upload_status_patch(self, mock_send):
        request = self.factory.patch(self.impact_upload_status_url, self.data, format='json')
        request.user = self.user
        view =  ImpactUploadStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactupload.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], self.data['comment'])

    def test_impact_upload_status_delete(self):
        request = self.factory.delete(self.impact_upload_status_url, self.data, format='json')
        request.user = self.user
        view =  ImpactUploadStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactupload.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class TestProjectDashboardModelViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.dashboard=ProjectDashboard.objects.create(
            project = self.proj,
            provider = 0,
            dashboard_name = "dashboardname",
            url = "www.url.com",
            embedded_content = "content",
            password = "password"
        )
        self.data={
            "project" : self.proj.id,
            "provider" : 1,
            "dashboard_name" : "dashboard",
            "url" : "www.url.com",
            "embedded_content" : "content",
            "password" : "passwordd"
        }

        #url
        self.dashboard_url=reverse('v2:project:dashboard')

    def test_dashboard__list(self):
        request = self.factory.get(self.dashboard_url)
        request.user = self.user
        view = ProjectDashboardModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_dashboard_create(self):
        request = self.factory.post(self.dashboard_url, self.data, format='json')
        request.user = self.user
        view = ProjectDashboardModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['dashboard_name'], self.data['dashboard_name'])

    def test_dashboard_get(self):
        request = self.factory.get(self.dashboard_url, self.data, format='json')
        request.user = self.user
        view = ProjectDashboardModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.dashboard.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['dashboard_name'],'dashboardname')

    def test_dashboard_put(self):
        request = self.factory.put(self.dashboard_url, self.data, format='json')
        request.user = self.user
        view =  ProjectDashboardModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.dashboard.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['dashboard_name'], self.data['dashboard_name'])

    def test_dashboard_patch(self):
        request = self.factory.patch(self.dashboard_url, self.data, format='json')
        request.user = self.user
        view =  ProjectDashboardModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.dashboard.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['dashboard_name'], self.data['dashboard_name'])

    def test_dashboard_delete(self):
        request = self.factory.delete(self.dashboard_url, self.data, format='json')
        request.user = self.user
        view =  ProjectDashboardModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.dashboard.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class TestImpactWorkflowRuleSetViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.proj2=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user=self.user)

        self.impactworkflow=ImpactWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )

        self.data={
            "client" : self.client_obj.id,
            "rule_name" : "rule2",
            "tags" : ['tag2'],
            "enabled" : True
        }

        self.impactworkflowrule=ImpactWorkflowRule.objects.create(
            ruleset = self.impactworkflow,
            rule_type = "REQUESTER",
            level = 1,
            level_name = "level1"
        )
        self.data2={
            "ruleset" : self.impactworkflow.id,
            "rule_type" : "REQUESTER",
            "level" : 2,
            "level_name" : "level2"
        }

        self.assignee=ImpactWorkflowRuleAssignee.objects.create(
            project = self.proj,
            user = self.projuser,
            rule = self.impactworkflowrule
        )
        self.assigneedata={
            "project" : self.proj2.id,
            "user" : self.projuser.id,
            "rule" : self.impactworkflowrule.id
        }

        #url
        self.impact_workflow_ruleset_url=reverse('v2:project:impact_workflow_ruleset')
        self.impact_workflow_rule_url=reverse('v2:project:impact_workflow_rule')
        self.impact_workflow_rule_assignee_url=reverse('v2:project:impact_workflow_rule_assignee')

    def test_impact_workflow_ruleset__list(self):
        request = self.factory.get(self.impact_workflow_ruleset_url)
        request.user = self.user
        view = ImpactWorkflowRuleSetViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_impact_workflow_ruleset_create(self):
        request = self.factory.post(self.impact_workflow_ruleset_url, self.data, format='json')
        request.user = self.user
        view = ImpactWorkflowRuleSetViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['rule_name'], self.data['rule_name'])

    def test_impact_workflow_ruleset_get(self):
        request = self.factory.get(self.impact_workflow_ruleset_url, self.data, format='json')
        request.user = self.user
        view = ImpactWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactworkflow.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule_name'],'rule1')

    def test_impact_workflow_ruleset_put(self):
        request = self.factory.put(self.impact_workflow_ruleset_url, self.data, format='json')
        request.user = self.user
        view =  ImpactWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactworkflow.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule_name'], self.data['rule_name'])

    def test_impact_workflow_ruleset_patch(self):
        request = self.factory.patch(self.impact_workflow_ruleset_url, self.data, format='json')
        request.user = self.user
        view =  ImpactWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactworkflow.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule_name'], self.data['rule_name'])

    def test_impact_workflow_ruleset_delete(self):
        request = self.factory.delete(self.impact_workflow_ruleset_url, self.data, format='json')
        request.user = self.user
        view =  ImpactWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactworkflow.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#rule

    def test_impact_workflow_rule__list(self):
        request = self.factory.get(self.impact_workflow_rule_url)
        request.user = self.user
        view = ImpactWorkflowRuleViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_impact_workflow_rule_create(self):
        request = self.factory.post(self.impact_workflow_rule_url, self.data2, format='json')
        request.user = self.user
        view = ImpactWorkflowRuleViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['level_name'], self.data2['level_name'])

    def test_impact_workflow_rule_get(self):
        request = self.factory.get(self.impact_workflow_rule_url, self.data2, format='json')
        request.user = self.user
        view = ImpactWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactworkflowrule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'],'level1')

    def test_impact_workflow_rule_put(self):
        request = self.factory.put(self.impact_workflow_rule_url, self.data2, format='json')
        request.user = self.user
        view =  ImpactWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactworkflowrule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'], self.data2['level_name'])

    def test_impact_workflow_rule_patch(self):
        request = self.factory.patch(self.impact_workflow_rule_url, self.data2, format='json')
        request.user = self.user
        view =  ImpactWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactworkflowrule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'], self.data2['level_name'])

    def test_impact_workflow_rule_delete(self):
        request = self.factory.delete(self.impact_workflow_rule_url, self.data2, format='json')
        request.user = self.user
        view =  ImpactWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.impactworkflowrule.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#assignee

    def test_impact_workflow_rule_assignee__list(self):
        request = self.factory.get(self.impact_workflow_rule_assignee_url)
        request.user = self.user
        view = ImpactWorkflowAssigneeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_impact_workflow_rule_assignee_create(self):
        request = self.factory.post(self.impact_workflow_rule_assignee_url, self.assigneedata, format='json')
        request.user = self.user
        view = ImpactWorkflowAssigneeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['rule'], self.assigneedata['rule'])

    def test_impact_workflow_rule_assignee_get(self):
        request = self.factory.get(self.impact_workflow_rule_assignee_url, self.assigneedata, format='json')
        request.user = self.user
        view = ImpactWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=  self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_impact_workflow_rule_assignee_put(self):
        request = self.factory.put(self.impact_workflow_rule_assignee_url, self.assigneedata, format='json')
        request.user = self.user
        view =  ImpactWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=  self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule'], self.assigneedata['rule'])

    def test_impact_workflow_rule_assignee_patch(self):
        request = self.factory.patch(self.impact_workflow_rule_assignee_url, self.assigneedata, format='json')
        request.user = self.user
        view =  ImpactWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=  self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule'], self.assigneedata['rule'])

    def test_impact_workflow_rule_assignee_delete(self):
        request = self.factory.delete(self.impact_workflow_rule_assignee_url, self.assigneedata, format='json')
        request.user = self.user
        view =  ImpactWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=  self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class TestDisbursementModelViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send,mock_send2):

        super().setUp()
        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )

        self.mile=Milestone.objects.create(
            milestone_name = "milestone1",
            milestone_description = "hi i am milestone",
            status = "complete",
            project = self.proj,
            position = 10,
            sub_focus_area = ['malnutrition'],
            sub_target_segment = ['children'],
            location =['lucknow'],
            tags = ['tag1','tag2'],
        )

        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )

        self.dis=Disbursement.objects.create(
            disbursement_name = "db1",
            disbursement_description = "i am db",
            project =self.proj,
            milestone =self.mile,
            start_date = "2020-06-22T18:30:00Z",
            end_date ="2021-03-22T18:30:00Z",
            invoice_request_date = "2020-06-23T18:30:00Z",
            expected_date = "2020-06-24T18:30:00Z",
            actual_date = "2020-06-24T18:30:00Z",
            approval_date ="2020-06-26T18:30:00Z",
            status = "APPROVED",
            expected_amount = 100000,
            actual_amount = 10000,
            expense_category = "xyz",
            sattva_approver = self.projuser,
            client_approver = self.projuser,
            due_diligence = True,
            sattva_approval_required = True,
            sattva_approval_status =True,
            tags = ['tag1'],
            client_approval_status = True,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            position =1
        )
        self.data={
            "disbursement_name" : "db2",
            "disbursement_description" : "i am db2",
            "project" :self.proj.id,
            "milestone" :self.mile.id,
            "start_date" : "2020-06-22T18:30:00Z",
            "end_date" :"2021-03-22T18:30:00Z",
            "invoice_request_date" : "2020-06-23T18:30:00Z",
            "expected_date" : "2020-06-24T18:30:00Z",
            "actual_date" : "2020-06-24T18:30:00Z",
            "approval_date" :"2020-06-26T18:30:00Z",
            "status" : "APPROVED",
            "expected_amount" : 100000,
            "actual_amount" : 10000,
            "expense_category" : "xyz",
            "sattva_approver" : self.projuser.id,
            "client_approver" : self.projuser.id,
            "due_diligence" : True,
            "sattva_approval_required" : True,
            "sattva_approval_status" :True,
            "tags" : ['tag1'],
            "client_approval_status" : True,
            "is_custom_workflow" :True,
            "previous_utilisation_check" : True,
            "position" :1
        }
        self.minidata={
            "disbursement_name" : "db2",
            "start_date" : "2020-06-22T18:30:00Z",
            "end_date" :"2021-03-22T18:30:00Z",
            "expected_amount" : 100000,
            "actual_amount" : 10000,

        }
        self.discomment=DisbursementComment.objects.create(
            disbursement = self.dis,
            comment = "comment1",
            is_read = True
        )
        self.comment={
            "disbursement" : self.dis.id,
            "comment" : "comment2",
            "is_read" : True
        }

        #url
        self.disbursement_url=reverse('v2:project:disbursement')
        self.disbursementcomment_url=reverse('v2:project:disbursement_comment')
        self.disbursementmini_url=reverse('v2:project:mini_disbursement')

    def test_disbursement__list(self):
        request = self.factory.get(self.disbursement_url)
        request.user = self.user
        view = DisbursementModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_disbursement_create(self, mock_send,mock_send2):
        request = self.factory.post(self.disbursement_url, self.data, format='json')
        request.user = self.user
        view = DisbursementModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['disbursement_name'], self.data['disbursement_name'])


# minidisbursement
    def test_disbursementmini__list(self):
        request = self.factory.get(self.disbursementmini_url)
        request.user = self.user
        view = MiniDisbursementModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    # @patch('v2.project.tasks.calculate_project_status.apply_async')
    # def test_disbursementmini_create(self, mock_send,mock_send2):
    #     request = self.factory.post(self.disbursementmini_url, self.data, format='json')
    #     request.user = self.user
    #     view = MiniDisbursementModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['disbursement_name'], self.data['disbursement_name'])

#comments

    def test_disbursementcomment__list(self):
        request = self.factory.get(self.disbursementcomment_url)
        request.user = self.user
        view = DisbursementCommentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_disbursementcomment_create(self):
        request = self.factory.post(self.disbursementcomment_url,  self.comment, format='json')
        request.user = self.user
        view = DisbursementCommentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comment'],  self.comment['comment'])

    def est_disbursementcomment__get(self):
        request = self.factory.get(self.disbursementcomment_url,  self.comment, format='json')
        request.user = self.user
        view = DisbursementCommentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.discomment.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], "comment1")


class TestDisbursementStatusViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send,mock_send2):

        super().setUp()
        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )

        self.mile=Milestone.objects.create(
            milestone_name = "milestone1",
            milestone_description = "hi i am milestone",
            status = "complete",
            project = self.proj,
            position = 10,
            sub_focus_area = ['malnutrition'],
            sub_target_segment = ['children'],
            location =['lucknow'],
            tags = ['tag1','tag2'],
        )

        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )

        self.dis=Disbursement.objects.create(
            disbursement_name = "db1",
            disbursement_description = "i am db",
            project =self.proj,
            milestone =self.mile,
            start_date = "2020-06-22T18:30:00Z",
            end_date ="2021-03-22T18:30:00Z",
            invoice_request_date = "2020-06-23T18:30:00Z",
            expected_date = "2020-06-24T18:30:00Z",
            actual_date = "2020-06-24T18:30:00Z",
            approval_date ="2020-06-26T18:30:00Z",
            status = "APPROVED",
            expected_amount = 100000,
            actual_amount = 10000,
            expense_category = "xyz",
            sattva_approver = self.projuser,
            client_approver = self.projuser,
            due_diligence = True,
            sattva_approval_required = True,
            sattva_approval_status =True,
            tags = ['tag1'],
            client_approval_status = True,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            position =1
        )
        self.status=DisbursementStatus.objects.create(
            disbursement = self.dis,
            status = "SCHEDULED",
            comment = "comment1"
        )
        self.data={
            "disbursement" : self.dis.id,
            "status" : "SCHEDULED",
            "comment" : "comment2"
        }
        self.doc=DisbursementDocument.objects.create(
            disbursement = self.dis,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size = 20,
            document_tag = ['tag1'],
            path = "path1"
        )
        self.docdata={
            "disbursement" : self.dis.id,
           #  document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size": 20,
            "document_tag": ['tag1'],
            "path": "path"
        }
        self.activitylog=DisbursementActivityLog.objects.create(
            disbursement = self.dis,
            old_value = ['old'],
            new_value = ['new'],
            action = "action",
            update_text = "text"
        )
        self.actdata={
            "disbursement" : self.dis.id,
            "old_value" : ['old'],
            "new_value" : ['new'],
            "action" : "action",
            "update_text" : "text"
        }

        #url
        self.disbursement_status_url=reverse('v2:project:disbursement_status')
        self.disbursement_document_url=reverse('v2:project:disbursement_document')
        self.disbursement_activity_log_url=reverse('v2:project:disbursement_activity_log')

    def test_disbursement_status__list(self):
        request = self.factory.get(self.disbursement_status_url)
        request.user = self.user
        view = DisbursementStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_disbursement_status_create(self):
        request = self.factory.post(self.disbursement_status_url,  self.data, format='json')
        request.user = self.user
        view = DisbursementStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comment'],  self.data['comment'])

    def est_disbursement_status__get(self):
        request = self.factory.get(self.disbursement_status_url,  self.data, format='json')
        request.user = self.user
        view = DisbursementStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.status.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], "comment1")

#documents

    def test_disbursement_document__list(self):
        request = self.factory.get(self.disbursement_document_url)
        request.user = self.user
        view = DisbursementDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_disbursement_document_create(self):
    #     request = self.factory.post(self.disbursement_document_url,  self.docdata, format='json')
    #     request.user = self.user
    #     view = DisbursementDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['path'],  self.docdata['path'])

    def est_disbursement_document__get(self):
        request = self.factory.get(self.disbursement_document_url,  self.docdata, format='json')
        request.user = self.user
        view = DisbursementDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.doc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "path1")

#activitylog

def test_disbursement_activity_log__list(self):
        request = self.factory.get(self.disbursement_activity_log_url)
        request.user = self.user
        view = DisbursementActivityLogViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

class TestDisbursementWorkflowRuleSetViewSet(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.proj2=Project.objects.create(
            project_name="xyzzzzz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user=self.user)
        self.ruleset=DisbursementWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )
        self.data={
            "client": self.client_obj.id,
            "rule_name": "rule1",
            "tags": ['tag2'],
            "enabled": True
        }
        self.rule=DisbursementWorkflowRule.objects.create(
            ruleset = self.ruleset,
            rule_type ="REQUESTER",
            level = 2,
            level_name = "level1"
        )
        self.ruledata={
            "ruleset" : self.ruleset.id,
            "rule_type" :"REQUESTER",
            "level" : 2,
            "level_name" : "level2"
        }
        self.assignee=DisbursementWorkflowRuleAssignee.objects.create(
            project = self.proj,
            user = self.projuser,
            rule = self.rule
        )
        self.assigneedata={
            "project" : self.proj2.id,
            "user" : self.projuser.id,
            "rule" : self.rule.id
        }

        #url
        self.disbursement_workflow_ruleset_url=reverse('v2:project:disbursement_workflow_ruleset')
        self.disbursement_workflow_rule_url=reverse('v2:project:disbursement_workflow_rules')
        self.disbursement_workflow_rule_assignee_url=reverse('v2:project:disbursement_workflow_rule_assignee')

    #ruleset

    def test_disbursement_workflow_ruleset__list(self):
        request = self.factory.get(self.disbursement_workflow_ruleset_url)
        request.user = self.user
        view = DisbursementWorkflowRuleSetViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_disbursement_workflow_ruleset_create(self):
        request = self.factory.post(self.disbursement_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view =DisbursementWorkflowRuleSetViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['tags'],  self.data['tags'])

    def est_disbursement_workflow_ruleset__get(self):
        request = self.factory.get(self.disbursement_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view = DisbursementWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.ruleset.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['tags'], "tags1")


    def test_disbursement_workflow_ruleset_put(self):
        request = self.factory.put(self.disbursement_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view =  DisbursementWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.ruleset.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['tags'],  self.data['tags'])

    def test_disbursement_workflow_ruleset_patch(self):
        request = self.factory.patch(self.disbursement_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view =  DisbursementWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.ruleset.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['tags'],  self.data['tags'])

    def test_disbursement_workflow_ruleset_delete(self):
        request = self.factory.delete(self.disbursement_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view =  DisbursementWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.ruleset.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#rule

    def test_disbursement_workflow_rule__list(self):
        request = self.factory.get(self.disbursement_workflow_rule_url)
        request.user = self.user
        view = DisbursementWorkflowRuleViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_disbursement_workflow_rule_create(self):
        request = self.factory.post(self.disbursement_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view = DisbursementWorkflowRuleViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['level_name'],  self.ruledata['level_name'])

    def est_disbursement_workflow_rule__get(self):
        request = self.factory.get(self.disbursement_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view = DisbursementWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.rule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'], "level1")


    def test_disbursement_workflow_rule_put(self):
        request = self.factory.put(self.disbursement_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view =  DisbursementWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.rule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'],  self.ruledata['level_name'])

    def test_disbursement_workflow_rule_patch(self):
        request = self.factory.patch(self.disbursement_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view =  DisbursementWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.rule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'],  self.ruledata['level_name'])

    def test_disbursement_workflow_rule_delete(self):
        request = self.factory.delete(self.disbursement_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view =  DisbursementWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.rule.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#assignee

    def test_disbursement_workflow_rule_assignee__list(self):
        request = self.factory.get(self.disbursement_workflow_rule_assignee_url)
        request.user = self.user
        view = DisbursementWorkflowAssigneeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_disbursement_workflow_rule_assignee_create(self):
        request = self.factory.post(self.disbursement_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view =DisbursementWorkflowAssigneeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['rule'],  self.assigneedata['rule'])

    def est_disbursement_workflow_rule_assignee__get(self):
        request = self.factory.get(self.disbursement_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view = DisbursementWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_disbursement_workflow_rule_assignee_put(self):
        request = self.factory.put(self.disbursement_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view =  DisbursementWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule'],  self.assigneedata['rule'])

    def test_disbursement_workflow_rule_assignee_patch(self):
        request = self.factory.patch(self.disbursement_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view =  DisbursementWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule'],  self.assigneedata['rule'])

    def test_disbursement_workflow_rule_assignee_delete(self):
        request = self.factory.delete(self.disbursement_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view =  DisbursementWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class TestUtilisationViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send,mock_send2):

        super().setUp()
        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )

        self.mile=Milestone.objects.create(
            milestone_name = "milestone1",
            milestone_description = "hi i am milestone",
            status = "complete",
            project = self.proj,
            position = 10,
            sub_focus_area = ['malnutrition'],
            sub_target_segment = ['children'],
            location =['lucknow'],
            tags = ['tag1','tag2'],
        )

        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )

        self.dis=Disbursement.objects.create(
            disbursement_name = "db1",
            disbursement_description = "i am db",
            project =self.proj,
            milestone =self.mile,
            start_date = "2020-06-22T18:30:00Z",
            end_date ="2021-03-22T18:30:00Z",
            invoice_request_date = "2020-06-23T18:30:00Z",
            expected_date = "2020-06-24T18:30:00Z",
            actual_date = "2020-06-24T18:30:00Z",
            approval_date ="2020-06-26T18:30:00Z",
            status = "APPROVED",
            expected_amount = 100000,
            actual_amount = 10000,
            expense_category = "xyz",
            sattva_approver = self.projuser,
            client_approver = self.projuser,
            due_diligence = True,
            sattva_approval_required = True,
            sattva_approval_status =True,
            tags = ['tag1'],
            client_approval_status = True,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            position =1
        )
        self.utl=Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =self.dis,
            project = self.proj,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )

        self.data={
            "unit_cost": 11.2,
            "number_of_units" :13.2,
            "planned_cost" :57.2,
            "avg_unit" :77.2,
            "avg_unit_cost" :97.2,
            "total_estimate_cost" :57.2,
            "expense_category" :"category",
            "description" :"text",
            "particulars" :"particular",
            "actual_cost" :100000.3,
            "disbursement" :self.dis.id,
            "project" :self.proj.id,
            "is_custom_workflow" :True,
            "previous_utilisation_check" : True,
            "tags" : ['tag1'],
            "position" : 0
        }

        self.ruleset=UtilisationWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )
        self.rule=UtilisationWorkflowRule.objects.create(
            ruleset = self.ruleset,
            rule_type ="REQUESTER",
            level = 2,
            level_name = "level1"
        )

        self.utlcomment=UtilisationComment.objects.create(
            utilisation = self.utl,
            comment = "comment1",
            is_read = True
        )
        self.comment={
            "utilisation" : self.utl.id,
            "comment" : "comment2",
            "is_read" : True
        }
        self.doc=UtilisationDocument.objects.create(
            utilisation = self.utl,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size = 20,
            document_tag = ['tag1'],
            path = "path1"
        )
        self.docdata={
            "utilisation" : self.utl.id,
           #  document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size": 20,
            "document_tag": ['tag1'],
            "path": "path"
        }
        self.status=UtilisationStatus.objects.create(
            utilisation = self.utl,
            status = "SCHEDULED",
            action_by = self.projuser,
            comment = "comment1"
        )
        self.data2={
            "utilisation" : self.utl.id,
            "status" : "SCHEDULED",
            "action_by" : "self.projuser",
            "comment" : "comment2"
        }


        #url
        self.utilisation_url=reverse('v2:project:utilisation')
        self.utilisation_comment_url=reverse('v2:project:utilisation_comment')
        self.utilisation_document_url=reverse('v2:project:utilisation_document')

    def test_utilisation__list(self):
        request = self.factory.get(self.utilisation_url)
        request.user = self.user
        view = UtilisationViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    # @patch('v2.project.tasks.calculate_project_status.apply_async')
    # def test_utilisation_create(self, mock_send,mock_send2):
    #     request = self.factory.post(self.utilisation_url, self.data, format='json')
    #     request.user = self.user
    #     view = UtilisationViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['unit_cost'], self.data['unit_cost'])

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def est_utilisation__get(self, mock_send,mock_send2):
        request = self.factory.get(self.utilisation_url, self.data, format='json')
        request.user = self.user
        view = UtilisationViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.utl.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['unit_cost'], self.utl['unit_cost'])


#comments

    def test_utilisation_comment__list(self):
        request = self.factory.get(self.utilisation_comment_url)
        request.user = self.user
        view = UtilisationCommentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_utilisation_comment_create(self):
        request = self.factory.post(self.utilisation_comment_url,  self.comment, format='json')
        request.user = self.user
        view = UtilisationCommentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comment'],  self.comment['comment'])

    def est_utilisation_comment__get(self):
        request = self.factory.get(self.utilisation_comment_url,  self.comment, format='json')
        request.user = self.user
        view = UtilisationCommentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.utlcomment.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], "comment1")

#documents

    def test_utilisation_document__list(self):
        request = self.factory.get(self.utilisation_document_url)
        request.user = self.user
        view = UtilisationDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def est_utilisation_document__get(self):
        request = self.factory.get(self.utilisation_document_url,  self.docdata, format='json')
        request.user = self.user
        view = UtilisationDocumentViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.doc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "path1")


class TestUtilisationStatusViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send,mock_send2):

        super().setUp()
        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )

        self.mile=Milestone.objects.create(
            milestone_name = "milestone1",
            milestone_description = "hi i am milestone",
            status = "complete",
            project = self.proj,
            position = 10,
            sub_focus_area = ['malnutrition'],
            sub_target_segment = ['children'],
            location =['lucknow'],
            tags = ['tag1','tag2'],
        )

        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )

        self.dis=Disbursement.objects.create(
            disbursement_name = "db1",
            disbursement_description = "i am db",
            project =self.proj,
            milestone =self.mile,
            start_date = "2020-06-22T18:30:00Z",
            end_date ="2021-03-22T18:30:00Z",
            invoice_request_date = "2020-06-23T18:30:00Z",
            expected_date = "2020-06-24T18:30:00Z",
            actual_date = "2020-06-24T18:30:00Z",
            approval_date ="2020-06-26T18:30:00Z",
            status = "APPROVED",
            expected_amount = 100000,
            actual_amount = 10000,
            expense_category = "xyz",
            sattva_approver = self.projuser,
            client_approver = self.projuser,
            due_diligence = True,
            sattva_approval_required = True,
            sattva_approval_status =True,
            tags = ['tag1'],
            client_approval_status = True,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            position =1
        )
        self.utl=Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =self.dis,
            project = self.proj,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )
        self.activitylog=UtilisationActivityLog.objects.create(
            utilisation = self.utl,
            old_value = ['old'],
            new_value = ['new'],
            action = "action",
            update_text = "text"
        )
        self.actdata={
            "utilisation" : self.utl.id,
            "old_value" : ['old'],
            "new_value" : ['new'],
            "action" : "action",
            "update_text" : "text"
        }

        self.status=UtilisationStatus.objects.create(
            utilisation = self.utl,
            status = "SCHEDULED",
            comment = "comment1"
        )
        self.data2={
            "utilisation" : self.utl.id,
            "status" : "SCHEDULED",
            "comment" : "comment2"
        }

        #url
        self.utilisation_status_url=reverse('v2:project:utilisation_status')
        self.utilisation_activity_log_url=reverse('v2:project:utilisation_activity_log')

#status

    def test_utilisation_status__list(self):
        request = self.factory.get(self.utilisation_status_url)
        request.user = self.user
        view = UtilisationStatusViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_utilisation_status_create(self):
    #     request = self.factory.post(self.utilisation_status_url,self.data2, format='json')
    #     request.user = self.user
    #     view = UtilisationStatusViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['comment'],self.data2['comment'])

    def est_utilisation_status__get(self):
        request = self.factory.get(self.utilisation_status_url,self.data2, format='json')
        request.user = self.user
        view = UtilisationStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.status.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comment'], "comment1")


    # def test_utilisation_status_put(self):
    #     request = self.factory.put(self.utilisation_status_url,self.data2, format='json')
    #     request.user = self.user
    #     view =  UtilisationStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.status.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['comment'],self.data2['comment'])

    # def test_utilisation_status_patch(self):
    #     request = self.factory.patch(self.utilisation_status_url,self.data2, format='json')
    #     request.user = self.user
    #     view =  UtilisationStatusViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.status.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['comment'],self.data2['comment'])


#activitylog

def test_utilisation_activity_log__list(self):
        request = self.factory.get(self.utilisation_activity_log_url)
        request.user = self.user
        view = UtilisationActivityLogViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

class TestUtilisationWorkflowRuleSetViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send,mock_send2):

        super().setUp()
        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )
        self.proj2=Project.objects.create(
            project_name = "pppppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )

        self.mile=Milestone.objects.create(
            milestone_name = "milestone1",
            milestone_description = "hi i am milestone",
            status = "complete",
            project = self.proj,
            position = 10,
            sub_focus_area = ['malnutrition'],
            sub_target_segment = ['children'],
            location =['lucknow'],
            tags = ['tag1','tag2'],
        )

        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )

        self.dis=Disbursement.objects.create(
            disbursement_name = "db1",
            disbursement_description = "i am db",
            project =self.proj,
            milestone =self.mile,
            start_date = "2020-06-22T18:30:00Z",
            end_date ="2021-03-22T18:30:00Z",
            invoice_request_date = "2020-06-23T18:30:00Z",
            expected_date = "2020-06-24T18:30:00Z",
            actual_date = "2020-06-24T18:30:00Z",
            approval_date ="2020-06-26T18:30:00Z",
            status = "APPROVED",
            expected_amount = 100000,
            actual_amount = 10000,
            expense_category = "xyz",
            sattva_approver = self.projuser,
            client_approver = self.projuser,
            due_diligence = True,
            sattva_approval_required = True,
            sattva_approval_status =True,
            tags = ['tag1'],
            client_approval_status = True,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            position =1
        )
        self.utl=Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =self.dis,
            project = self.proj,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )

        self.ruleset=UtilisationWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )
        self.data={
            "client": self.client_obj.id,
            "rule_name": "rule1",
            "tags": ['tag2'],
            "enabled": True
        }
        self.rule=UtilisationWorkflowRule.objects.create(
            ruleset = self.ruleset,
            rule_type ="REQUESTER",
            level = 2,
            level_name = "level1"
        )
        self.ruledata={
            "ruleset" : self.ruleset.id,
            "rule_type" :"REQUESTER",
            "level" : 2,
            "level_name" : "level2"
        }
        self.assignee=UtilisationWorkflowRuleAssignee.objects.create(
            project = self.proj,
            user = self.projuser,
            rule = self.rule
        )
        self.assigneedata={
            "project" : self.proj2.id,
            "user" : self.projuser.id,
            "rule" : self.rule.id
        }

        #url
        self.utilisation_workflow_ruleset_url=reverse('v2:project:utilisation_workflow_ruleset')
        self.utilisation_workflow_rule_url=reverse('v2:project:utilisation_workflow_rule')
        self.utilisation_workflow_rule_assignee_url=reverse('v2:project:utilisation_workflow_rule_assignee')

#ruleset

    def test_utilisation_workflow_ruleset__list(self):
        request = self.factory.get(self.utilisation_workflow_ruleset_url)
        request.user = self.user
        view = UtilisationWorkflowRuleSetViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_utilisation_workflow_ruleset_create(self):
        request = self.factory.post(self.utilisation_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view =UtilisationWorkflowRuleSetViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['tags'],  self.data['tags'])

    def est_utilisation_workflow_ruleset__get(self):
        request = self.factory.get(self.utilisation_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view = UtilisationWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.ruleset.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['tags'], "tags1")


    def test_utilisation_workflow_ruleset_put(self):
        request = self.factory.put(self.utilisation_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view =  UtilisationWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.ruleset.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['tags'],  self.data['tags'])

    def test_utilisation_workflow_ruleset_patch(self):
        request = self.factory.patch(self.utilisation_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view =  UtilisationWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.ruleset.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['tags'],  self.data['tags'])

    def test_utilisation_workflow_ruleset_delete(self):
        request = self.factory.delete(self.utilisation_workflow_ruleset_url,  self.data, format='json')
        request.user = self.user
        view =  UtilisationWorkflowRuleSetViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.ruleset.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#rule

    def test_utilisation_workflow_rule__list(self):
        request = self.factory.get(self.utilisation_workflow_rule_url)
        request.user = self.user
        view = UtilisationWorkflowRuleViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_utilisation_workflow_rule_create(self):
        request = self.factory.post(self.utilisation_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view = UtilisationWorkflowRuleViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['level_name'],  self.ruledata['level_name'])

    def est_utilisation_workflow_rule__get(self):
        request = self.factory.get(self.utilisation_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view = UtilisationWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.rule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'], "level1")


    def test_utilisation_workflow_rule_put(self):
        request = self.factory.put(self.utilisation_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view =  UtilisationWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.rule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'],  self.ruledata['level_name'])

    def test_utilisation_workflow_rule_patch(self):
        request = self.factory.patch(self.utilisation_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view =  UtilisationWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.rule.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['level_name'],  self.ruledata['level_name'])

    def test_utilisation_workflow_rule_delete(self):
        request = self.factory.delete(self.utilisation_workflow_rule_url,  self.ruledata, format='json')
        request.user = self.user
        view =  UtilisationWorkflowRuleViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.rule.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

#assignee

    def test_utilisation_workflow_rule_assignee__list(self):
        request = self.factory.get(self.utilisation_workflow_rule_assignee_url)
        request.user = self.user
        view = UtilisationWorkflowAssigneeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_utilisation_workflow_rule_assignee_create(self):
        request = self.factory.post(self.utilisation_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view =UtilisationWorkflowAssigneeViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['rule'],  self.assigneedata['rule'])

    def est_utilisation_workflow_rule_assignee__get(self):
        request = self.factory.get(self.utilisation_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view = UtilisationWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_utilisation_workflow_rule_assignee_put(self):
        request = self.factory.put(self.utilisation_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view =  UtilisationWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule'],  self.assigneedata['rule'])

    def test_utilisation_workflow_rule_assignee_patch(self):
        request = self.factory.patch(self.utilisation_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view =  UtilisationWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['rule'],  self.assigneedata['rule'])

    def test_utilisation_workflow_rule_assignee_delete(self):
        request = self.factory.delete(self.utilisation_workflow_rule_assignee_url,  self.assigneedata, format='json')
        request.user = self.user
        view =  UtilisationWorkflowAssigneeViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk= self.assignee.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class TestUtilisationUploadViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send,mock_send2):

        super().setUp()
        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )
        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )
        self.upload=UtilisationUpload.objects.create(
            upload_name = "upload1",
            batch_id = "batch2",
            file_name ="file1",
            upload_type = "jpg",
            project = self.proj,
            tags = ['tag1'],
            is_verified = True
        )
        self.data={
            "upload_name" : "upload2",
            "batch_id" : "batch2",
            "file_name" :"file1",
            "upload_type" : "jpg",
            "project" : self.proj.id,
            "tags" : ['tag1'],
            "is_verified" : True
        }

        #url
        self.utilisation_upload_url=reverse('v2:project:utilisation_upload')

    def test_utilisation_upload__list(self):
        request = self.factory.get(self.utilisation_upload_url)
        request.user = self.user
        view = UtilisationUploadViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_utilisation_upload_create(self):
    #     request = self.factory.post(self.utilisation_upload_url, self.data, format='json')
    #     request.user = self.user
    #     view = UtilisationUploadViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['upload_name'], self.data['upload_name'])


class TestUtilisationExpenseViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):

        super().setUp()
        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )
        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )
        self.mile=Milestone.objects.create(
            milestone_name = "milestone1",
            milestone_description = "hi i am milestone",
            status = "complete",
            project = self.proj,
            position = 10,
            sub_focus_area = ['malnutrition'],
            sub_target_segment = ['children'],
            location =['lucknow'],
            tags = ['tag1','tag2'],
        )

        self.dis=Disbursement.objects.create(
            disbursement_name = "db1",
            disbursement_description = "i am db",
            project =self.proj,
            milestone =self.mile,
            start_date = "2020-06-22T18:30:00Z",
            end_date ="2021-03-22T18:30:00Z",
            invoice_request_date = "2020-06-23T18:30:00Z",
            expected_date = "2020-06-24T18:30:00Z",
            actual_date = "2020-06-24T18:30:00Z",
            approval_date ="2020-06-26T18:30:00Z",
            status = "APPROVED",
            expected_amount = 100000,
            actual_amount = 10000,
            expense_category = "xyz",
            sattva_approver = self.projuser,
            client_approver = self.projuser,
            due_diligence = True,
            sattva_approval_required = True,
            sattva_approval_status =True,
            tags = ['tag1'],
            client_approval_status = True,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            position =1
        )
        self.utl=Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =self.dis,
            project = self.proj,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )
        self.upload=UtilisationUpload.objects.create(
            upload_name = "upload1",
            batch_id = "batch2",
            file_name ="file1",
            upload_type = "jpg",
            project = self.proj,
            tags = ['tag1'],
            is_verified = True
        )
        self.expense =UtilisationExpense.objects.create(
            description ="hi i am expense",
            split_frequency = "f1",
            estimate_cost = 100000.0,
            number_of_units = 2.5,
            unit_cost = 10000.7,
            actual_cost = 19000999,
            planned_cost = 19000999,
            remarks ="i am remark",
            comments ="comment1",
            utilisation = self.utl,
            utilisation_upload = self.upload
        )
        self.data={
            "description" :"hi i am expense",
            "split_frequency" : "f1",
            "estimate_cost" : 100000.0,
            "number_of_units" : 2.5,
            "unit_cost" : 10000.7,
            "actual_cost" : 19000999,
            "planned_cost" : 19000999,
            "remarks" :"i am remark",
            "comments" :"comment",
            "utilisation" : self.utl.id,
            "utilisation_upload" : self.upload.id
        }

        #url
        self.utilisation_expense_url=reverse('v2:project:utilisation_expense')

    def test_utilisation_expense__list(self):
        request = self.factory.get(self.utilisation_expense_url)
        request.user = self.user
        view = UtilisationExpenseViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_utilisation_expense_create(self, mock_send,mock_send2):
        request = self.factory.post(self.utilisation_expense_url,self.data, format='json')
        request.user = self.user
        view = UtilisationExpenseViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['comments'],self.data['comments'])

    def est_utilisation_expense__get(self):
        request = self.factory.get(self.utilisation_expense_url,self.data, format='json')
        request.user = self.user
        view = UtilisationExpenseViewSet.as_view({'get': 'retrieve', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.expense.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['comments'], "comment1")

# class CreateProjectTemplateAPIViewTestCase(TestSetUpwatcher):

#     def setUp(self):
#         super().setUp()

#         self.proj=Project.objects.create(
#             project_name="xyz",
#             project_description="hi i am tested",
#             client=self.client_obj,
#             budget=1000,
#             project_status=72.22,
#             total_disbursement_amount=8804465,
#             planned_disbursement_amount=8804465,
#             total_utilised_amount=7000000
#         )
#         self.templatetype = TemplateType.objects.create(
#             template_type_id = 1,
#             template_type ="hi"
#         )

#         self.temp=Template.objects.create(
#             template_name = "temp1",
#             template_type = self.templatetype,
#             project = self.proj,
#             client = self.client_obj,
#             is_added = True,
#             is_standard = True,
#             standardization_requested = True,
#             table_created = False,
#             data_status = "Verified"
#         )
#         self.data={
#             "template_name" : "temp2",
#             "template_type" : self.templatetype.template_type_id,
#             "project" : self.proj.id,
#             "client" : self.client_obj.id,
#             "is_added" : True,
#             "is_standard" : True,
#             "standardization_requested" : True,
#             "table_created" : False,
#             "data_status" : "Verified"
#         }

#         self.create_project_template = reverse('v2:project:create_project_template', kwargs={'project_id': self.proj.id,'template_id':self.temp.id})

#     def test_create_project_template_api_view(self):
#         request = self.factory.post(self.create_project_template,self.data, format='json')
#         request.user = self.user
#         view = CreateProjectTemplateAPIView.as_view()
#         response = view(request,project_id= self.proj.id,template_id=self.temp.id)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

class TestUtilisationExpenseDocumentViewSet(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):

        super().setUp()
        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )
        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )
        self.mile=Milestone.objects.create(
            milestone_name = "milestone1",
            milestone_description = "hi i am milestone",
            status = "complete",
            project = self.proj,
            position = 10,
            sub_focus_area = ['malnutrition'],
            sub_target_segment = ['children'],
            location =['lucknow'],
            tags = ['tag1','tag2'],
        )

        self.dis=Disbursement.objects.create(
            disbursement_name = "db1",
            disbursement_description = "i am db",
            project =self.proj,
            milestone =self.mile,
            start_date = "2020-06-22T18:30:00Z",
            end_date ="2021-03-22T18:30:00Z",
            invoice_request_date = "2020-06-23T18:30:00Z",
            expected_date = "2020-06-24T18:30:00Z",
            actual_date = "2020-06-24T18:30:00Z",
            approval_date ="2020-06-26T18:30:00Z",
            status = "APPROVED",
            expected_amount = 100000,
            actual_amount = 10000,
            expense_category = "xyz",
            sattva_approver = self.projuser,
            client_approver = self.projuser,
            due_diligence = True,
            sattva_approval_required = True,
            sattva_approval_status =True,
            tags = ['tag1'],
            client_approval_status = True,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            position =1
        )
        self.utl=Utilisation.objects.create(
            unit_cost = 10.2,
            number_of_units = 11.2,
            planned_cost = 57.2,
            avg_unit = 57.2,
            avg_unit_cost = 57.2,
            total_estimate_cost = 57.2,
            expense_category = "category",
            description = "text",
            particulars = "particular",
            actual_cost = 100000.3,
            disbursement =self.dis,
            project = self.proj,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            tags = ['tag1'],
            position = 0
                )
        self.upload=UtilisationUpload.objects.create(
            upload_name = "upload1",
            batch_id = "batch2",
            file_name ="file1",
            upload_type = "jpg",
            project = self.proj,
            tags = ['tag1'],
            is_verified = True
        )
        self.expense =UtilisationExpense.objects.create(
            description ="hi i am expense",
            split_frequency = "f1",
            estimate_cost = 100000.0,
            number_of_units = 2.5,
            unit_cost = 10000.7,
            actual_cost = 19000999,
            planned_cost = 19000999,
            remarks ="i am remark",
            comments ="comment1",
            utilisation = self.utl,
            utilisation_upload = self.upload
        )
        self.doc=UtilisationExpenseDocument.objects.create(
            utilisation_expense = self.expense ,
            document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size = 20,
            document_tag = ['tag1'],
            path = "path1"
        )
        self.docdata={
            "utilisation_expense" : self.expense .id,
           #  document_file= SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size": 10,
            "document_tag": ['tag1'],
            "path": "path"
        }

        #url
        self.utilisation_expense_document_url=reverse('v2:project:utilisation_expense_document')

    def test_utilisation_expense_document__list(self):
        request = self.factory.get(self.utilisation_expense_document_url)
        request.user = self.user
        view = UtilisationExpenseDocumentViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class DeleteProjectTemplateAPIViewTestCase(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.templatetype = TemplateType.objects.create(
            template_type_id = 1,
            template_type ="hi"
        )

        self.temp=Template.objects.create(
            template_name = "temp1",
            template_type = self.templatetype,
            project = self.proj,
            client = self.client_obj,
            is_added = True,
            is_standard = True,
            standardization_requested = True,
            table_created = False,
            data_status = "Verified"
        )
        self.data={
            "template_name" : "temp1",
            "template_type" : self.templatetype.template_type_id,
            "project" : self.proj.id,
            "client" : self.client_obj.id,
            "is_added" : True,
            "is_standard" : True,
            "standardization_requested" : True,
            "table_created" : False,
            "data_status" : "Verified"
        }

        self.delete_project_template = reverse('v2:project:delete_project_template', kwargs={'project_id': self.proj.id,'template_id':self.temp.id})

    def test_delete_project_template_api_view(self):
        request = self.factory.delete(self.delete_project_template,self.data, format='json')
        request.user = self.user
        view = DeleteProjectTemplateAPIView.as_view()
        response = view(request,project_id= self.proj.id,template_id=self.temp.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class DownloadCSVDataAPIViewTestCase(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.templatetype = TemplateType.objects.create(
            template_type_id = 1,
            template_type ="hi"
        )

        self.temp=Template.objects.create(
            template_name = "temp1",
            template_type = self.templatetype,
            project = self.proj,
            client = self.client_obj,
            is_added = True,
            is_standard = True,
            standardization_requested = True,
            table_created = False,
            data_status = "Verified"
        )
        self.data={
            "template_name" : "temp2",
            "template_type" : self.templatetype.template_type_id,
            "project" : self.proj.id,
            "client" : self.client_obj.id,
            "is_added" : True,
            "is_standard" : True,
            "standardization_requested" : True,
            "table_created" : False,
            "data_status" : "Verified"
        }
        self.csvupload=CSVUpload.objects.create(
            csv_file = SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            client = self.client_obj,
            project = self.proj,
            template = self.temp,
            status = "Deleted",
            table_name = "tablename",
            batch_id = 2,
            upload_successful = True,
            is_deleted = True,
            is_custom_workflow = False
        )
        self.csvdata={
             # csv_file = SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "client" :self.client_obj.id,
            "project":self.proj.id,
            "template" :self.temp.id,
            "status" :"Deleted",
            "table_name" :"tablename",
            "batch_id":1,
            "upload_successful" :True,
            "is_deleted" :True,
            "is_custom_workflow" :False
        }

        self.download_csv_data = reverse('v2:project:download_csv_data', kwargs={'project_id': self.proj.id,'template_id':self.temp.id})

    # def test_download_csv_data_api_view(self):
    #     request = self.factory.get(self.download_csv_data,self.csvdata, format='json')
    #     request.user = self.user
    #     view =DownloadCSVDataAPIView.as_view()
    #     response = view(request,project_id= self.proj.id,template_id=self.temp.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)


class DisbursementWorkflowProjectAPIViewTestCase(TestSetUpwatcher):

    def setUp(self):
        super().setUp()

        self.disbursement_workflow_project = reverse('v2:project:disbursement_workflow_project', kwargs={'client': self.client_obj.id})

    def test_disbursement_workflow_project_api_view(self):
        request = self.factory.get(self.disbursement_workflow_project)
        request.user = self.user
        view = DisbursementWorkflowProjectAPIView.as_view()
        response = view(request,client=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UtilisationWorkflowProjectAPIViewTestCase(TestSetUpwatcher):
    def setUp(self):
        super().setUp()

        self.utilisation_workflow_project = reverse('v2:project:utilisation_workflow_project', kwargs={'client': self.client_obj.id})

    def test_utilisation_workflow_project_api_view(self):
        request = self.factory.get(self.utilisation_workflow_project)
        request.user = self.user
        view = UtilisationWorkflowProjectAPIView.as_view()
        response = view(request,client=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ImpactWorkflowProjectAPIViewTestCase(TestSetUpwatcher):
    def setUp(self):
        super().setUp()
        self.impact_workflow_project = reverse('v2:project:impact_workflow_project', kwargs={'client': self.client_obj.id})

    def test_impact_workflow_project_api_view(self):
        request = self.factory.get(self.impact_workflow_project)
        request.user = self.user
        view = ImpactWorkflowProjectAPIView.as_view()
        response = view(request,client=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class RemoveDisbursementWorkflowAssigneeAPIViewTestCase(TestSetUpwatcher):
    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )
        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )
        self.ruleset=DisbursementWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )

        self.rule=DisbursementWorkflowRule.objects.create(
            ruleset = self.ruleset,
            rule_type ="REQUESTER",
            level = 2,
            level_name = "level1"
        )

        self.assignee=DisbursementWorkflowRuleAssignee.objects.create(
            project = self.proj,
            user = self.projuser,
            rule = self.rule
        )
        self.data={
            "project" : self.proj.id,
            "user" : self.projuser.id,
            "rule" : self.rule.id
        }

        self.remove_disbursement_workflow_assignee = reverse('v2:project:remove_disbursement_workflow_assignee')

    def test_remove_disbursement_workflow_assignee_api_view(self):
        request = self.factory.post(self.remove_disbursement_workflow_assignee,self.data, format='json')
        request.user = self.user
        view = RemoveDisbursementWorkflowAssigneeAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class RemoveUtilisationWorkflowAssigneeAPIViewTestCase(TestSetUpwatcher):
    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )
        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )
        self.ruleset=UtilisationWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )

        self.rule=UtilisationWorkflowRule.objects.create(
            ruleset = self.ruleset,
            rule_type ="REQUESTER",
            level = 2,
            level_name = "level1"
        )

        self.assignee=UtilisationWorkflowRuleAssignee.objects.create(
            project = self.proj,
            user = self.projuser,
            rule = self.rule
        )
        self.data={
            "project" : self.proj.id,
            "user" : self.projuser.id,
            "rule" : self.rule.id
        }

        self.remove_disbursement_workflow_assignee = reverse('v2:project:remove_disbursement_workflow_assignee')

    def test_remove_disbursement_workflow_assignee_api_view(self):
        request = self.factory.post(self.remove_disbursement_workflow_assignee,self.data, format='json')
        request.user = self.user
        view = RemoveUtilisationWorkflowAssigneeAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class RemoveImpactWorkflowAssigneeAPIViewTestCase(TestSetUpwatcher):
    def setUp(self):
        super().setUp()

        self.proj=Project.objects.create(
            project_name = "ppp",
            project_description = "hi i am tested",
            client = self.client_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
       )
        self.projuser=ProjectUser.objects.create(
            project = self.proj,
            user = self.user
        )
        self.ruleset=ImpactWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )

        self.rule=ImpactWorkflowRule.objects.create(
            ruleset = self.ruleset,
            rule_type ="REQUESTER",
            level = 2,
            level_name = "level1"
        )

        self.assignee=ImpactWorkflowRuleAssignee.objects.create(
            project = self.proj,
            user = self.projuser,
            rule = self.rule
        )
        self.data={
            "project" : self.proj.id,
            "user" : self.projuser.id,
            "rule" : self.rule.id
        }

        self.remove_impact_workflow_assignee = reverse('v2:project:remove_impact_workflow_assignee')

    def test_remove_impact_workflow_assignee_api_view(self):
        request = self.factory.post(self.remove_impact_workflow_assignee,self.data, format='json')
        request.user = self.user
        view = RemoveImpactWorkflowAssigneeAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

class CheckDisbursementUnassignedRuleAPIViewTestCase(TestSetUpwatcher):

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()

        self.proj=Project.objects.create(
            project_name="xyz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.proj2=Project.objects.create(
            project_name="xyzzzzz",
            project_description="hi i am tested",
            client=self.client_obj,
            budget=1000,
            project_status=72.22,
            total_disbursement_amount=8804465,
            planned_disbursement_amount=8804465,
            total_utilised_amount=7000000
        )
        self.projuser= ProjectUser.objects.create(
            project=self.proj,
            user=self.user)

        self.mile=Milestone.objects.create(
            milestone_name = "milestone1",
            milestone_description = "hi i am milestone",
            status = "complete",
            project = self.proj,
            position = 10,
            sub_focus_area = ['malnutrition'],
            sub_target_segment = ['children'],
            location =['lucknow'],
            tags = ['tag1','tag2'],
        )

        self.dis=Disbursement.objects.create(
            disbursement_name = "db1",
            disbursement_description = "i am db",
            project =self.proj,
            milestone =self.mile,
            start_date = "2020-06-22T18:30:00Z",
            end_date ="2021-03-22T18:30:00Z",
            invoice_request_date = "2020-06-23T18:30:00Z",
            expected_date = "2020-06-24T18:30:00Z",
            actual_date = "2020-06-24T18:30:00Z",
            approval_date ="2020-06-26T18:30:00Z",
            status = "APPROVED",
            expected_amount = 100000,
            actual_amount = 10000,
            expense_category = "xyz",
            sattva_approver = self.projuser,
            client_approver = self.projuser,
            due_diligence = True,
            sattva_approval_required = True,
            sattva_approval_status =True,
            tags = ['tag1'],
            client_approval_status = True,
            is_custom_workflow =True,
            previous_utilisation_check = True,
            position =1
        )

        self.ruleset=DisbursementWorkflowRuleSet.objects.create(
            client = self.client_obj,
            rule_name = "rule1",
            tags = ['tag1'],
            enabled = True
        )

        self.rule=DisbursementWorkflowRule.objects.create(
            ruleset = self.ruleset,
            rule_type ="REQUESTER",
            level = 2,
            level_name = "level1"
        )

        self.assignee=DisbursementWorkflowRuleAssignee.objects.create(
            project = self.proj,
            user = self.projuser,
            rule = self.rule
        )

        #url
        self.check_disbursement_unassigned_rule = reverse('v2:project:check_disbursement_unassigned_rule')

    def test_check_disbursement_unassigned_rule_api_view(self):
        request = self.factory.get(self.check_disbursement_unassigned_rule)
        request.user = self.user
        view = CheckDisbursementUnassignedRuleAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # def test_check_disbursement_unassigned_rulee_api_view(self):
    #     request = self.factory.get(self.check_disbursement_unassigned_rule)
    #     request.user = self.user
    #     view = CheckPreviousUtilisationForDisbursementAPIView.as_view()
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)



