from core.users.models import User
from rest_framework import serializers

from core.utils import get_value_or_default
from v2.notification.models import NotificationSentLog
from v2.user_management.serializers import UserMicroSerializer
from datetime import datetime, timedelta


"""
    This class will serialize NotificationSentLog model.
"""
class NotificationSentLogSerializer(serializers.ModelSerializer):
    notification_text = serializers.SerializerMethodField()
    notification_title = serializers.SerializerMethodField()
    notification_type = serializers.SerializerMethodField()
    notification_type_text = serializers.SerializerMethodField()
    notification_category = serializers.SerializerMethodField()
    notification_app_url = serializers.SerializerMethodField()
    notification_category_text = serializers.SerializerMethodField()
    action_user = serializers.SerializerMethodField()
    action_performed_on_user = serializers.SerializerMethodField()
    action_params = serializers.SerializerMethodField()
    notification_tag = serializers.SerializerMethodField()

    def get_notification_text(self, obj):
        return obj.notification.text

    def get_notification_title(self, obj):
        return obj.notification.title

    def get_notification_type(self, obj):
        return obj.notification.notification_type

    def get_notification_type_text(self, obj):
        return obj.notification.get_notification_type_display()

    def get_notification_category(self, obj):
        return obj.notification.category

    def get_notification_app_url(self, obj):
        return obj.notification.app_url

    def get_notification_category_text(self, obj):
        return obj.notification.get_category_display()

    def get_action_user(self, obj):
        return UserMicroSerializer(obj.notification.action_user).data if obj.notification.action_user else []

    def get_action_performed_on_user(self, obj):
        return UserMicroSerializer(
            obj.notification.action_performed_on_user).data if obj.notification.action_performed_on_user else []

    def get_action_params(self, obj):
        return obj.notification.action_params

    def get_notification_tag(self, obj):
        return obj.notification.notification_tag.capitalize() if obj.notification.notification_tag else ""

    class Meta:
        model = NotificationSentLog
        fields = ['id', 'read', 'created_date', 'notification_text', 'notification_title', 'notification_type',
                  "notification_type_text",
                  'notification_category', "notification_category_text", "action_user", "action_performed_on_user",
                  "action_params", "notification_tag", 'notification_app_url']

"""
    This class will serialize User model.
"""
class NotificationCountSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    total_notification_count = serializers.SerializerMethodField()
    total_unread_notification_count = serializers.SerializerMethodField()

    def get_user(self, obj):
        return UserMicroSerializer(obj).data

    def get_total_notification_count(self, obj):
        last_month = datetime.today() - timedelta(days=31)
        return NotificationSentLog.objects.filter(user=obj, created_date__gte=last_month).count()

    def get_total_unread_notification_count(self, obj):
        last_month = datetime.today() - timedelta(days=31)
        return NotificationSentLog.objects.filter(user=obj, read=False, created_date__gte=last_month).count()

    class Meta:
        model = User
        fields = ['user', 'total_notification_count', 'total_unread_notification_count']
