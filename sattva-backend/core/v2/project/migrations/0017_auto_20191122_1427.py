# Generated by Django 2.0.5 on 2019-11-22 08:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0016_auto_20191122_1419'),
    ]

    operations = [
        migrations.AlterField(
            model_name='disbursementdocument',
            name='document_tag',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
