from rest_framework.permissions import BasePermission

from v2.user_management.models import ClientFeaturePermission


def check_if_user_has_client_feature_permission(feature, permission, client, user):
    client_feature_permission = ClientFeaturePermission.objects.get(feature__feature_code=feature,
                                                                    permission=permission,
                                                                    client=client)
    if user.client_feature_permission.filter(id=client_feature_permission.id).exists():
        return True
    return False


def check_if_user_has_permission_for_client_project_or_plant(client, user):
    from v2.project.models import ProjectUser
    from v2.plant.models import PlantUser
    project_user = ProjectUser.objects.filter(user=user, project__client=client).exists()
    plant_user = PlantUser.objects.filter(user=user, plant__client=client).exists()
    return project_user or plant_user


"""
    permission for Super Admin
"""
class SuperAdminPermission(BasePermission):
    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        else:
            return False

"""
    permission for Consultant
"""
class ConsultantPermission(BasePermission):
    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.application_role.role_name in ('sattva-admin', 'sattva-user'):
            return True
        else:
            return False

"""
    permission for Client Create
"""
class ClientCreatePermission(BasePermission):
    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.method == 'POST' and request.user.application_role.role_name in ('sattva-admin', 'sattva-user'):
            return True
        elif request.method == 'POST':
            return False
        else:
            return True


class BaseClientPermission(BasePermission):
    """
    A base class from which all permission classes should inherit.
    """
    feature = ''

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT', 'POST'):
            return check_if_user_has_client_feature_permission(self.feature, 'Change', obj, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', obj, request.user)
        if request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', obj, request.user)
        return False

"""
    permission for Client
"""
class ClientPermission(BaseClientPermission):
    feature = 'CLIENT_OVERVIEW'

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT', 'POST'):
            return check_if_user_has_client_feature_permission(self.feature, 'Change', obj, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', obj, request.user)
        if request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', obj, request.user) or \
                   check_if_user_has_permission_for_client_project_or_plant(obj, request.user)
        return False

"""
    permission for clientOverview
"""
class ClientOverviewPermission(BaseClientPermission):
    feature = 'CLIENT_OVERVIEW'

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT', 'POST'):
            return check_if_user_has_client_feature_permission(self.feature, 'Change', obj.client, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', obj.client, request.user)
        if request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', obj.client, request.user) or \
                   check_if_user_has_permission_for_client_project_or_plant(obj.client, request.user)
        return False

"""
    permission for ClientEntity
"""
class ClientEntityPermission(ClientPermission):
    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT', 'POST'):
            return check_if_user_has_client_feature_permission(self.feature, 'Change', obj.client, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', obj.client, request.user)
        if request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', obj.client, request.user)
        return False


class CompliancePermission(BaseClientPermission):
    feature = 'COMPLIANCE'


"""
    permission for Governance
"""
class GovernancePermission(BaseClientPermission):
    feature = 'GOVERNANCE'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        client = request.GET.get('client')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and client:
            return check_if_user_has_client_feature_permission(self.feature, 'View', client, request.user)
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_client_feature_permission(self.feature, 'Change', obj.client, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', obj.client, request.user)
        if request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', obj.client, request.user)
        return False

"""
    permission for ClientFiles
"""
class ClientFilesPermission(BaseClientPermission):
    feature = 'DOCUMENTS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', view.kwargs.get('pk'),
                                                               request.user)
        elif request.method in ['POST', 'PATCH']:
            return check_if_user_has_client_feature_permission(self.feature, 'Change', view.kwargs.get('pk'),
                                                               request.user)
        elif request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', view.kwargs.get('pk'),
                                                               request.user)
        return True

"""
    permission for ClientFolder
"""
class ClientFolderPermission(BaseClientPermission):
    feature = 'DOCUMENTS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', view.kwargs.get('pk'),
                                                               request.user)
        elif request.method in ['POST', 'PATCH']:
            pk = view.kwargs.get('pk')
            if pk is None:
                pk = request.data.get('client')
            return check_if_user_has_client_feature_permission(self.feature, 'Change', pk,
                                                               request.user)
        elif request.method == 'DELETE':
            client = view.kwargs['client']
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', client,
                                                               request.user)
        return True

"""
    permission for Compliance Checklist
"""
class ComplianceChecklistPermission(CompliancePermission):

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        client = request.GET.get('client')
        if not client:
            return False
        else:
            if request.user.is_anonymous:
                return False
            if request.user.application_role.role_name == 'sattva-admin':
                return True
            elif request.method in ('PUT', 'POST'):
                return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
            elif request.method == 'GET':
                return check_if_user_has_client_feature_permission(self.feature, 'View', client, request.user)
        return False

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT'):
            return check_if_user_has_client_feature_permission(self.feature, 'Change', obj.client, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', obj.client, request.user)
        if request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', obj.client, request.user)
        return False



"""
    permission for Client Insights
"""
class ClientInsightsPermission(ClientPermission):
    feature = 'PORTFOLIO_INSIGHTS'

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        print("client")
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT', 'POST'):
            return check_if_user_has_client_feature_permission(self.feature, 'Change', obj.client, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', obj.client, request.user)
        if request.method == 'GET':
            return check_if_user_has_client_feature_permission(self.feature, 'View', obj.client, request.user)
        return False
