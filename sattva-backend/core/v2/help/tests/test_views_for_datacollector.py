from django.urls import reverse
from .test_setup import TestSetUpdatacollector
from rest_framework import status
from v2.help.models import HelpSections,HelpSubSections,HelpArticles,VideoDetails
from v2.help.views import HelpSectionModelViewSet,HelpSubSectionModelViewSet,HelpArticleModelViewSet,SearchHelpArticleAPIView,SearchHelpVideoAPIView
from ckeditor_uploader.fields import RichTextUploadingField

class TestHelpSectionModelViewSet(TestSetUpdatacollector):

    def setUp(self):
        super().setUp()
        self.help=HelpSections.objects.create(
                section_name = "section",
                path = "xyz"
        )
        self.helpsubsection=HelpSubSections.objects.create(
                section = self.help,
                title = "help",
                path = "path",
                code = "c1",
                roles = ['admin'],
                level = 10.5,
                is_head = True
        )
        self.article=HelpArticles.objects.create(
                content = RichTextUploadingField("hi"),
                title = "article",
                sub_section =self.helpsubsection,)

        self.video=VideoDetails.objects.create(
                link = "link1",
                title = "video",
                description = "ddd",
                sub_section =self.helpsubsection
        )

        #urls
        self.sections_url= reverse('v2:help:sections')
        self.sub_sections_url= reverse('v2:help:sub-sections')
        self.help_article_url=reverse('v2:help:help-article',kwargs={'pk':self.article.id})
        self.search_help_url=reverse('v2:help:search-help')

    def test_sections_list(self):
        request = self.factory.get(self.sections_url)
        request.user = self.user
        view = HelpSectionModelViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_subsection_list(self):
        request = self.factory.get( self.sub_sections_url)
        request.user = self.user
        view = HelpSubSectionModelViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_helparticle(self):
        request = self.factory.get(self.help_article_url)
        request.user = self.user
        view = HelpArticleModelViewSet.as_view({'get': 'retrieve'})
        response = view(request,pk=self.article.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'],self.article.title)

    def test_searchhelparticle(self):
        request = self.factory.get(self.search_help_url)
        request.user = self.user
        view =  SearchHelpArticleAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
