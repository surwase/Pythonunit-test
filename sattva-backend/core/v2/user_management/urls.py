from django.conf.urls import url

from v2.user_management.views import ClientFeaturesViewSet, ProjectFeaturesViewSet, CurrentUserAPIView, \
    ClientFeaturePermissionViewSet, ProjectFeaturePermissionViewSet, SaveClientFeaturePermissionsForUserAPIView, \
    SaveProjectFeaturePermissionsForUserAPIView, UserProjectFeatureAPIView, UserClientFeatureAPIView, \
    CurrentUserARIDView
from .views import UserDetailModelViewSet, UserModelViewSet, UserActivityLogViewSet, UserMiniDetailModelViewSet

app_name = "user_management"

urlpatterns = [
    url(r'^$', UserModelViewSet.as_view(
        {'get': 'list'}),
        name='user'),
    url(r'^(?P<pk>\d+)/$', UserMiniDetailModelViewSet.as_view(
        {'get': 'retrieve'}),
        name='user'),
    url(r'^user_detail/(?P<pk>\d+)/$', UserDetailModelViewSet.as_view(
        {'get': 'retrieve'}),
        name='user'),
    url(r'^project_feature_permission/(?P<user_id>\d+)/(?P<project_id>\d+)/$', UserProjectFeatureAPIView.as_view(),
        name='user'),
    url(r'^client_feature_permission/(?P<user_id>\d+)/(?P<client_id>\d+)/$', UserClientFeatureAPIView.as_view(),
        name='user'),
    url(r'^user_activity_log/$', UserActivityLogViewSet.as_view(
        {'get': 'list'}),
        name='user'),
    url(r'^client_feature/$', ClientFeaturesViewSet.as_view(
        {'get': 'list'}),
        name='client_feature'),
    url(r'^save_client_feature/$', SaveClientFeaturePermissionsForUserAPIView.as_view(),
        name='save_client_feature'),
    url(r'^save_project_feature/$', SaveProjectFeaturePermissionsForUserAPIView.as_view(),
        name='save_project_feature'),
    url(r'^project_feature/$', ProjectFeaturesViewSet.as_view(
        {'get': 'list'}),
        name='project_feature'),
    url(r'^client_feature_permission/$', ClientFeaturePermissionViewSet.as_view(
        {'get': 'list'}),
        name='client_feature_permission'),
    url(r'^project_feature_permission/$', ProjectFeaturePermissionViewSet.as_view(
        {'get': 'list'}),
        name='project_feature_permission'),
    url(r'^current_user/$', CurrentUserAPIView.as_view(),
        name='current user'),
    url(r'^current_user/application_role/$', CurrentUserARIDView.as_view(),
        name='current user'),
]
