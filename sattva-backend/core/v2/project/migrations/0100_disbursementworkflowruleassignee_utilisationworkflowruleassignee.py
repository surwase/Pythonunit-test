# Generated by Django 2.0.5 on 2020-07-14 17:25

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0099_auto_20200708_0536'),
    ]

    operations = [
        migrations.CreateModel(
            name='DisbursementWorkflowRuleAssignee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('created_by_username', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('updated_by_username', models.CharField(blank=True, max_length=50, null=True)),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.ProjectTag')),
                ('rule', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.DisbursementWorkflowRule')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.ProjectUser')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='UtilisationWorkflowRuleAssignee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('created_by_username', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('updated_by_username', models.CharField(blank=True, max_length=50, null=True)),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.ProjectTag')),
                ('rule', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.UtilisationWorkflowRule')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.ProjectUser')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
