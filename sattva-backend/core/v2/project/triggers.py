import datetime
import os

from v2.notification import notification_choices
from v2.notification.models import Notification
from v2.notification.settings_choices import ALERT_TASK_OVER_DUE_BY_PORTAL
from v2.notification import settings_choices
from v2.project.models import Task, DisbursementWorkflowRuleAssignee, DisbursementWorkflowRule, \
    UtilisationWorkflowRuleAssignee, UtilisationWorkflowRule
from v2.client.models import UserNotificationSettings


def create_project_task_alerts_on_due_date():
    all_task = Task.objects.all()
    current_time = datetime.datetime.now(datetime.timezone.utc).replace(hour=0, minute=0, second=0, microsecond=0)
    for task in all_task:
        if task.end_date <= current_time:
            notification_type = notification_choices.TYPE_TASK_OVERDUE
            notification_title = "Task due date crossed"
            notification_text = "A Task: {} has been updated in the project {}". \
                format(task.task_name, task.project.get_name() if task.project else "")
            project_users_list = []
            project_users_email_list = []
            if task.project:
                for user in task.project.get_all_project_users():
                    # Checking Notification settings for each user
                    if UserNotificationSettings.user_settings_check(user=user.user,
                                                                    settings_name=ALERT_TASK_OVER_DUE_BY_PORTAL):
                        project_users_list.append(user.user)
                    if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                                          settings_name=settings_choices.ALERT_TASK_OVER_DUE_BY_PORTAL):
                        project_users_email_list.append(user.user.username)
            Notification.create(notification_type, category=0, action_user=None, action_performed_on_user=None,
                                action_params={"task_id": task.id},
                                users_to_send_to=project_users_list, title=notification_title,
                                text=notification_text, notification_tag="Milestone",
                                email_users=project_users_email_list)


def create_partner_action_alert_on_milestone_creation(milestone, user):
    notification_type = notification_choices.TYPE_MILESTONE_CREATED
    notification_title = "New Milestone Created"
    notification_text = "A new milestone: {} has been created in the project {}". \
        format(milestone.milestone_name, milestone.project.project_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/project-plan'.format(milestone.project.client.id,
                                                                    milestone.project.id)
    for user in milestone.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_MILESTONE_CREATION):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_MILESTONE_CREATION):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"milestone_id": milestone.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Milestone", email_users=project_users_email_list,
                        app_url=app_url)


def create_partner_action_alert_on_milestone_update(milestone, user):
    notification_type = notification_choices.TYPE_MILESTONE_UPDATED
    notification_title = "New Milestone Updated"
    notification_text = "A new milestone: {} has been updated in the project {}". \
        format(milestone.milestone_name, milestone.project.project_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/project-plan'.format(milestone.project.client.id, milestone.project.id)
    for user in milestone.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_MILESTONE_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_MILESTONE_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"milestone_id": milestone.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Milestone", email_users=project_users_email_list,
                        app_url=app_url)


def create_partner_action_alert_on_task_creation(task, user):
    if not task.project:
        return
    notification_type = notification_choices.TYPE_TASK_CREATED
    notification_title = "Task has been created!"
    notification_text = "\"{}\" has been created in the project \"{}\" by \"{}\"". \
        format(task.task_name, task.project.get_name() if task.project else "", user.first_name + ' ' + user.last_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/project-plan'.format(task.client.id, task.project.id)
    if task.project:
        for user in task.project.get_all_project_users():
            # Checking Notification settings for each user
            if UserNotificationSettings.user_settings_check(user=user.user,
                                                            settings_name=settings_choices.NOTIFICATION_ON_TASK_CREATION):
                project_users_list.append(user.user)
            if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                                  settings_name=settings_choices.NOTIFICATION_ON_TASK_CREATION):
                project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"task_id": task.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Task", email_users=project_users_email_list,
                        app_url=app_url)


def create_partner_action_alert_on_task_update(task, user):
    if not task.project:
        return
    notification_type = notification_choices.TYPE_TASK_UPDATED
    notification_title = "Task has been updated!"
    notification_text = "\"{}\" has been updated in the project \"{}\" by \"{}\"". \
        format(task.task_name, task.project.get_name() if task.project else "", user.first_name + ' ' + user.last_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/project-plan'.format(task.client.id, task.project.id)
    for user in task.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_TASK_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_TASK_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"task_id": task.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Task", email_users=project_users_email_list,
                        app_url=app_url)


def create_partner_action_alert_on_indicator_creation(indicator, user):
    notification_type = notification_choices.TYPE_IMPACT_INDICATOR_CREATED
    notification_title = "Impact Indicator has been created!"
    notification_text = "\"{}\" has been created in the project \"{}\" by \"{}\"". \
        format(indicator.output_indicator, indicator.project.get_name(), user.first_name + ' ' + user.last_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/impact'.format(indicator.project.client.id, indicator.project.id)
    for user in indicator.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_IMPACT_INDICATOR_CREATION):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_IMPACT_INDICATOR_CREATION):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"indicator_id": indicator.indicator_batch},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Indicator", email_users=project_users_email_list,
                        app_url=app_url)


def create_partner_action_alert_on_indicator_update(indicator, user):
    notification_type = notification_choices.TYPE_IMPACT_INDICATOR_UPDATED
    notification_title = "Impact Indicator has been updated!"
    notification_text = "\"{}\" has been updated in the project \"{}\" by \"{}\"". \
        format(indicator.output_indicator, indicator.project.get_name(), user.first_name + ' ' + user.last_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/impact'.format(indicator.project.client.id, indicator.project.id)
    for user in indicator.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_IMPACT_INDICATOR_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_IMPACT_INDICATOR_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"indicator_id": indicator.indicator_batch},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Indicator", email_users=project_users_email_list,
                        app_url=app_url)


def create_partner_action_alert_on_template_creation(template, user):
    notification_type = notification_choices.TYPE_IMPACT_TEMPLATE_CREATED
    notification_title = "Impact Template has been created!"
    notification_text = "\"{}\" has been created in the project \"{}\" by \"{}\"". \
        format(template.template_name, template.project.get_name(), user.first_name + ' ' + user.last_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/impact'.format(template.project.client.id, template.project.id)
    for user in template.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_TEMPLATE_CREATION):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_TEMPLATE_CREATION):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"template_id": template.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Template", email_users=project_users_email_list,
                        app_url=app_url)


def create_partner_action_alert_on_template_update(template, user):
    notification_type = notification_choices.TYPE_IMPACT_TEMPLATE_UPDATED
    notification_title = "Impact Template has been updated!"
    notification_text = "\"{}\" has been updated in the project \"{}\" by \"{}\"". \
        format(template.template_name, template.project.get_name(), user.first_name + ' ' + user.last_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/impact'.format(template.project.client.id, template.project.id)
    for user in template.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_TEMPLATE_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_TEMPLATE_UPDATE):
            project_users_email_list.append(user.user.username)
    # project_users_list = template.project.get_all_project_users()
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"template_id": template.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Template", email_users=project_users_email_list,
                        app_url=app_url)


def create_partner_action_alert_on_impact_data_upload(template, user):
    notification_type = notification_choices.TYPE_IMPACT_DATA_UPLOADED
    notification_title = "Impact Data has been uploaded!"
    notification_text = "Data for template \"{}\" has been uploaded in the project \"{}\" by \"{}\"". \
        format(template.template_name, template.project.get_name(), user.first_name + ' ' + user.last_name)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/impact'.format(template.project.client.id, template.project.id)
    for user in template.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_IMPACT_DATA_UPLOAD):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_IMPACT_DATA_UPLOAD):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"template_id": template.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Impact Data", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_disbursement_creation(disbursement, user):
    notification_type = notification_choices.TYPE_DISBURSEMENT_CREATED
    notification_title = "Disbursement has been created!"
    notification_text = "A new financial disbursement \"{}\" of the project \"{}\" has been created by \"{}\"". \
        format(disbursement.disbursement_name, disbursement.project.get_name(), user)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(disbursement.project.client.id,
                                                                  disbursement.project.id)
    for user in disbursement.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"disbursement_id": disbursement.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Disbursement", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_disbursement_update(disbursement, user):
    notification_type = notification_choices.TYPE_DISBURSEMENT_UPDATED
    notification_title = "Disbursement has been updated!"
    notification_text = "Disbursement \"{}\" of the project \"{}\" has been updated by \"{}\"". \
        format(disbursement.disbursement_name, disbursement.project.get_name(), user)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(disbursement.project.client.id,
                                                                  disbursement.project.id)
    for user in disbursement.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"disbursement_id": disbursement.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Disbursement", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_disbursement_status_change(disbursement, user, old_status, new_status, status_dict):
    notification_type = notification_choices.TYPE_DISBURSEMENT_STATUS_CHANGE
    notification_title = "Status of Disbursement Changed"
    notification_text = "Status Change of disbursement \"{}\" from the project \"{}\": \"{}\" to \"{}\" by \"{}\"". \
        format(disbursement.disbursement_name, disbursement.project.get_name(), old_status, new_status, user)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(disbursement.project.client.id,
                                                                  disbursement.project.id)
    assignees = DisbursementWorkflowRuleAssignee.objects.filter(project=disbursement.project).distinct('user')
    if new_status == 'REJECTED':
        rule = status_dict.get('rule')
        concerned_rules = DisbursementWorkflowRule.objects.filter(
            level__lt=DisbursementWorkflowRule.objects.get(id=rule).level)
        assignees = assignees.filter(rule__in=concerned_rules)
    for assignee in assignees:
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=assignee.user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_STATUS_CHANGE):
            project_users_list.append(assignee.user.user)
        if UserNotificationSettings.user_email_settings_check(user=assignee.user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_STATUS_CHANGE):
            project_users_email_list.append(assignee.user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"disbursement_id": disbursement.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Disbursement", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_disbursement_delete(disbursement, user):
    notification_type = notification_choices.TYPE_DISBURSEMENT_DELETED
    notification_title = "Disbursement has been deleted!"
    notification_text = "Disbursement \"{}\" of the project \"{}\" has been deleted by \"{}\"". \
        format(disbursement.disbursement_name, disbursement.project.get_name(), user)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(disbursement.project.client.id,
                                                                  disbursement.project.id)
    for user in disbursement.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"disbursement_id": disbursement.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Disbursement", email_users=project_users_email_list,
                        app_url=app_url)


def create_action_alert_on_disbursement_action_required(disbursement, assignees):
    notification_type = notification_choices.TYPE_DISBURSEMENT_UPDATED
    notification_title = "Action required on Disbursement"
    notification_text = "\"{}\" of the project \"{}\" is awaiting your action.". \
        format(disbursement.disbursement_name, disbursement.project.get_name())
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(disbursement.project.client.id,
                                                                  disbursement.project.id)
    for assignee in assignees:
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=assignee.user.user,
                                                        settings_name=settings_choices.ALERT_ON_DISBURSEMENT_ACTION_REQUIRED):
            project_users_list.append(assignee.user.user)
        if UserNotificationSettings.user_email_settings_check(user=assignee.user.user,
                                                              settings_name=settings_choices.ALERT_ON_DISBURSEMENT_ACTION_REQUIRED):
            project_users_email_list.append(assignee.user.user.username)
    Notification.create(notification_type, category=0, action_user=None, action_performed_on_user=None,
                        action_params={"disbursement_id": disbursement.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Disbursement", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_utilisation_creation(utilisation, user):
    notification_type = notification_choices.TYPE_UTILISATION_CREATED
    notification_title = "Utilisation has been created!"
    if utilisation.disbursement:
        notification_text = "Utilization \"{}\" associated with disbursement \"{}\" has been added in {} by \"{}\"". \
            format(utilisation.particulars, utilisation.disbursement.disbursement_name, utilisation.project.get_name(),
                   user)
    else:
        notification_text = "Standalone Utilisation \"{}\" has been created in the project \"{}\" by \"{}\"". \
            format(utilisation.particulars, utilisation.project.get_name(), user)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(utilisation.project.client.id,
                                                                  utilisation.project.id)
    for user in utilisation.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"utilisation": utilisation.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Utilisation", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_utilisation_update(utilisation, user):
    notification_type = notification_choices.TYPE_UTILISATION_UPDATED
    notification_title = "Utilisation has been updated!"
    if utilisation.disbursement:
        notification_text = "Utilization \"{}\" associated with disbursement \"{}\" has been edited in {} by \"{}\"". \
            format(utilisation.particulars, utilisation.disbursement.disbursement_name, utilisation.project.get_name(),
                   user)
    else:
        notification_text = "Standalone Utilisation \"{}\" has been edited in the project \"{}\"". \
            format(utilisation.particulars, utilisation.project.get_name())
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(utilisation.project.client.id,
                                                                  utilisation.project.id)
    for user in utilisation.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"utilisation": utilisation.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Utilisation", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_utilisation_status_change(utilisation, user, old_status, new_status, status_dict):
    notification_type = notification_choices.TYPE_UTILISATION_STATUS_CHANGE
    notification_title = "Status of Utilisation Changed"
    project_users_list = []
    project_users_email_list = []
    assignees = UtilisationWorkflowRuleAssignee.objects.filter(project=utilisation.project).distinct('user')
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(utilisation.project.client.id,
                                                                  utilisation.project.id)
    if new_status == 'REJECTED':
        if utilisation.disbursement:
            notification_text = "Utilization \"{}\" associated with disbursement \"{}\" has been rejected in \"{}\" by \"{}\".". \
                format(utilisation.particulars, utilisation.disbursement.disbursement_name,
                       utilisation.project.get_name(), old_status, new_status, user)
        else:
            notification_text = "Standalone Utilization \"{}\" has been rejected in \"{}\".". \
                format(utilisation.particulars, utilisation.project.get_name(), old_status,
                       new_status)
        rule = status_dict.get('rule')
        concerned_rules = UtilisationWorkflowRule.objects.filter(
            level__lt=UtilisationWorkflowRule.objects.get(id=rule).level)
        assignees = assignees.filter(rule__in=concerned_rules)
    else:
        if utilisation.disbursement:
            notification_text = "Utilization \"{}\" associated with disbursement \"{}\" has been verified in \"{}\" by \"{}\".". \
                format(utilisation.particulars, utilisation.disbursement.disbursement_name,
                       utilisation.project.get_name(), old_status, new_status, user)
        else:
            notification_text = "Standalone Utilization \"{}\" has been verified in \"{}\".". \
                format(utilisation.particulars, utilisation.project.get_name(), old_status,
                       new_status)
    for assignee in assignees:
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=assignee.user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_STATUS_CHANGE):
            project_users_list.append(assignee.user.user)
        if UserNotificationSettings.user_email_settings_check(user=assignee.user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_STATUS_CHANGE):
            project_users_email_list.append(assignee.user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"utilisation_id": utilisation.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Utilisation", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_utilisation_delete(utilisation, user):
    notification_type = notification_choices.TYPE_UTILISATION_DELETED
    notification_title = "Utilisation has been deleted!"
    if utilisation.disbursement:
        notification_text = "Utilization \"{}\" associated with disbursement \"{}\" has been deleted in {} by \"{}\"". \
            format(utilisation.particulars, utilisation.disbursement.disbursement_name, utilisation.project.get_name(),
                   user)
    else:
        notification_text = "Standalone Utilisation \"{}\" has been deleted in the project \"{}\" by \"{}\"". \
            format(utilisation.particulars, utilisation.project.get_name(), user)
    project_users_list = []
    project_users_email_list = []
    app_url = os.environ.get('LOGIN_URL')
    app_url = app_url + '/client/{}/project/{}/financials'.format(utilisation.project.client.id,
                                                                  utilisation.project.id)

    for user in utilisation.project.get_all_project_users():
        # Checking Notification settings for each user
        if UserNotificationSettings.user_settings_check(user=user.user,
                                                        settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_list.append(user.user)
        if UserNotificationSettings.user_email_settings_check(user=user.user,
                                                              settings_name=settings_choices.NOTIFICATION_ON_FINANCIAL_ADD_UPDATE):
            project_users_email_list.append(user.user.username)
    Notification.create(notification_type, category=1, action_user=None, action_performed_on_user=None,
                        action_params={"utilisation_id": utilisation.id},
                        users_to_send_to=project_users_list, title=notification_title,
                        text=notification_text, notification_tag="Utilisation", email_users=project_users_email_list,
                        app_url=app_url)


def create_notification_on_project_creation(project, user):
    notification_type = notification_choices.TYPE_PROJECT_CREATED
    notification_title = "Project Created!"
    notification_text = "Project \"{}\" has been created by user \"{}\".". \
        format(project.project_name, user.username)
    project_users_list = []

    # for user in project.participants.all():
    #     # Checking Notification settings for each user
    #     if UserNotificationSettings.user_settings_check(user=user.user,
    #                                                     settings_name=settings_choices.ALERT_ON_DISBURSEMENT_ACTION_REQUIRED):
    #         project_users_list.append(user.user)
    # Notification.create(notification_type, category=0, action_user=None, action_performed_on_user=None,
    #                     action_params={"project_id": project.id},
    #                     users_to_send_to=project_users_list, title=notification_title,
    #                     text=notification_text, notification_tag="Disbursement")


def create_alert_on_project_deletion(project):
    pass


def create_alert_on_milestone_deletion(milestone):
    pass


def create_alert_on_task_deletion(task):
    pass


def create_alert_on_disbursement_deletion(disbursement):
    pass


def create_alert_on_utilisation_deletion(utilisation):
    pass


def create_alert_on_user_removal(user):
    pass


def create_notification_on_user_addition(user):
    pass

#
# def
