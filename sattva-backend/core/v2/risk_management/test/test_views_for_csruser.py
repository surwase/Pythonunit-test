from django.urls import reverse
from .test_setup import TestSetUpCsrUser
from rest_framework import status
from core.users.models import User
from v2.risk_management.models import RiskCategory,Risk,RiskEntry,RiskTemplate,RiskVariables,RiskVariablesClientSettings
from v2.risk_management.views import RiskViewSet,RiskCategoryViewSet
from v2.project.models import Project,Plant,Milestone
from unittest.mock import patch

class TestRiskViewSet(TestSetUpCsrUser):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send):
        super().setUp()
        self.riskcategory=RiskCategory.objects.create(
        name = "risk1"
        )
        self.risk=Risk.objects.create(
            severity =  0,
            name = "risk1",
            trigger_name = "r1",
            category = self.riskcategory,
            description = "hi i am risk"
        )
        self.plant=Plant.objects.create(
                plant_name = "plant1",
                plant_description = "plantdec",
                client = self.client_obj
            )

        self.project=Project.objects.create(
            project_name = "xyz",
            project_description = "hi i am tested",
            client = self.client_obj,
            plant = self.plant,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
            )

        self.milestone=Milestone.objects.create(
                milestone_name = "milestone1",
                milestone_description = "hi i am milestone",
                status = "complete",
                project = self.project,
                position = 10,
                sub_focus_area = ['malnutrition'],
                sub_target_segment = ['children'],
                location =['lucknow'],
                tags = ['tag1','tag2'],
                )

        RiskCategory.objects.create(
        name = "riskcat1"
        )

        rc=RiskCategory.objects.get(name = "riskcat1" )
        Risk.objects.create(
            severity = 0,
            name = "risk1",
            trigger_name = "r1",
            category = rc,
            description = "hi i am risk"
        )
        self.riskentry=RiskEntry.objects.create(
            name = "riskent1",
            project = self.project,
            client = self.client_obj,
            plant = self.plant,
            milestone = self.milestone,
            risk = self.risk,
            status = 0,
            severity = 1,
            risk_type = 0,
            category = rc,
            content = "hhhhh",
            comments = "jjjj"
        )
        self.data={
            "name" :"riskent2",
            "project" :self.project.id,
            "client" :self.client_obj.id,
            "plant" :self.plant.id,
            "milestone" :self.milestone.id,
            "risk" :self.risk.id,
            "status" :0,
            "severity" :1,
            "risk_type" :0,
            "category" :self.riskcategory.id,
            "content" :"hhhhh",
            "comments" :"jjjj"

        }
        #urls
        self.project_risk_management_url= reverse('v2:risk_management:project_risk_management')
        self.risk_management_category_url= reverse('v2:risk_management:risk_management_category')

    def test_project_risk_management_list(self):
        request = self.factory.get(self.project_risk_management_url)
        request.user = self.user
        view = RiskViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_project_risk_management_create(self):
        request = self.factory.post(self.project_risk_management_url, self.data, format='json')
        request.user = self.user
        view = RiskViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code,status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'], self.data['name'])

    def test_project_risk_management(self):
        request = self.factory.get(self.project_risk_management_url)
        request.user = self.user
        view = RiskViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request,pk=self.riskentry.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),1)

    def test_project_risk_category(self):
        request = self.factory.get(self.risk_management_category_url)
        request.user = self.user
        view = RiskCategoryViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data),2)
