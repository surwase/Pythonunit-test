from django.test import TestCase
from .test_setup import TestSetUp
from rest_framework.fields import FileField
from v2.user_management.models import ClientFeature,ProjectFeature

class TestClientFeature(TestSetUp):

    def setUp(self):
        super().setUp()
        ClientFeature.objects.create(
        feature_name = "f1",
        feature_code = "c2",
        default_permissions = ['view'],
        applicable_roles = ['admin']
        )

    def test_values(self):
        cf=ClientFeature.objects.get(feature_name ="f1")
        self.assertEqual(cf.feature_name , "f1")
        self.assertEqual(cf.feature_code , "c2")
        self.assertEqual(cf.default_permissions , ['view'])
        self.assertEqual(cf.applicable_roles , ['admin'])

    def test__str__(self):
        cf=ClientFeature.objects.get(feature_name ="f1")
        self.assertEqual(cf.__str__(), "c2")


class TestProjectFeature(TestSetUp):

    def setUp(self):
        super().setUp()
        ProjectFeature.objects.create(
        feature_name = "f1",
        feature_code = "c2",
        default_permissions = ['view'],
        applicable_roles = ['admin']
        )

    def test_values(self):
        projectfeature1=ProjectFeature.objects.get(feature_name ="f1")
        self.assertEqual(projectfeature1.feature_name , "f1")
        self.assertEqual(projectfeature1.feature_code , "c2")
        self.assertEqual(projectfeature1.default_permissions , ['view'])
        self.assertEqual(projectfeature1.applicable_roles , ['admin'])

    def test__str__(self):
        projectfeature1=ProjectFeature.objects.get(feature_name ="f1")
        self.assertEqual(projectfeature1.__str__(), "c2")
