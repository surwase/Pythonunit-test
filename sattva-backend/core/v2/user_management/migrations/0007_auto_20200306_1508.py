# Generated by Django 2.0.5 on 2020-03-06 09:38

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_management', '0006_auto_20191227_1556'),
    ]

    operations = [
        migrations.AddField(
            model_name='clientfeature',
            name='applicable_roles',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=[]),
        ),
        migrations.AddField(
            model_name='projectfeature',
            name='applicable_roles',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=[]),
        ),
    ]
