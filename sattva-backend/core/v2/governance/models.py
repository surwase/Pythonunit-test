import datetime

from django.contrib.postgres.fields import JSONField
from django.db import models

# Create your models here.
from geolibraries.models import AuditFields
from v2.client.models import Client, ClientUser
from v2.project.models import Project, Task
from v2.risk_management.models import Risk


"""
    This Model is used to store the Governance.
"""
class Governance(AuditFields):
    TYPE_CLIENT_CALL = 0
    TYPE_CLIENT_VISIT = 1
    TYPE_PARTNER_CALL = 2
    TYPE_PARTNER_VISIT = 3
    TYPE_CHOICES = ((TYPE_CLIENT_CALL, 'Client Call'), (TYPE_CLIENT_VISIT, 'Client Visit'),
                    (TYPE_PARTNER_CALL, 'Partner Call'), (TYPE_PARTNER_VISIT, 'Partner Visit'))
    STATUS_OPEN = 0
    STATUS_PENDING = 1
    STATUS_CLOSED = 2
    STATUS_CANCELLED = 3
    STATUS_CHOICES = ((STATUS_OPEN, 'Open'), (STATUS_PENDING, 'Pending'),
                      (STATUS_CLOSED, 'Closed'), (STATUS_CANCELLED, 'Cancelled'))
    name = models.CharField(max_length=511)
    location = JSONField(default=[])
    description = models.TextField(null=True, blank=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    projects = models.ManyToManyField(Project, blank=True, null=True)
    governance_type = models.SmallIntegerField(choices=TYPE_CHOICES)
    status = models.SmallIntegerField(choices=STATUS_CHOICES)
    governance_date = models.DateTimeField(default=datetime.datetime.now)
    start_date = models.DateTimeField(default=datetime.datetime.now)
    end_date = models.DateTimeField(default=datetime.datetime.now)
    comments = models.TextField(null=True, blank=True)
    assignee = models.ManyToManyField(ClientUser, null=True, blank=True)
    is_milestone_selected = models.BooleanField(default=False)
    is_financial_selected = models.BooleanField(default=False)
    is_impact_selected = models.BooleanField(default=False)
    is_risk_selected = models.BooleanField(default=False)

    class Meta:
        db_table = 'client_governance'
        unique_together = ('name', 'client')
