from django.conf.urls import url
from django.urls import include

from .views import *

app_name = "help"


urlpatterns = [
    url(r'^sections/$', HelpSectionModelViewSet.as_view(
        {'get': 'list'}),
        name='sections'),
    url(r'^sub-sections/$', HelpSubSectionModelViewSet.as_view(
        {'get': 'list'}),
        name='sub-sections'),
    url(r'^sub-sections/(?P<pk>\d+)/$', HelpSubSectionModelViewSet.as_view(
        {'get': 'retrieve'}),
        name='sub-sections'),
    url(r'^help-article/(?P<pk>\d+)/$', HelpArticleModelViewSet.as_view(
        {'get': 'retrieve'}),
        name='help-article'),
    url(r'^search-help/$', SearchHelpArticleAPIView.as_view(),
        name='search-help'),
    url(r'^search-videos/$', SearchHelpVideoAPIView.as_view(),
        name='search-help')
        ]