from rest_framework import serializers

from v2.kobo.models import KoboAssets


class KoboAssetSerializer(serializers.ModelSerializer):
    owner = serializers.SerializerMethodField()
    is_active = serializers.SerializerMethodField()

    @staticmethod
    def get_owner(obj):
        return obj.owner.username

    @staticmethod
    def get_is_active(obj):
        return obj.get_is_active()

    class Meta:
        model = KoboAssets
        fields = ['id', 'uid', 'date_created', 'date_modified', 'settings', 'is_active', 'owner', 'shift_project',
                  'name']
