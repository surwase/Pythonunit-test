'''
    The notification type to be divided into categories.
    So the old ones are single digit. But they will have to be changed
    once we have the clarity on notification and alert.
    Each category to be given a range of 100 numbers.
    For example: Project category starts from 100 and will continue till 199
'''

TYPE_PROJECT_CREATED = 0  # After Creating Project
TYPE_PROJECT_UPDATED = 1  # After Updating Project
TYPE_PROJECT_DELETED = 102  # After deleting project

TYPE_MILESTONE_CREATED = 2  # After Creating Milestone
TYPE_MILESTONE_UPDATED = 3  # After Updating Milestone
TYPE_MILESTONE_DELETED = 203  # After Deleting Milestone

TYPE_TASK_CREATED = 4  # After Creating Project Task
TYPE_TASK_UPDATED = 5  # After Updating Project Task
TYPE_TASK_OVERDUE = 6  # After Project Task Due date Crossed
TYPE_TASK_DELETED = 307  # After Task is deleted

TYPE_IMPACT_INDICATOR_CREATED = 7
TYPE_IMPACT_INDICATOR_UPDATED = 8
TYPE_IMPACT_INDICATOR_DELETED = 409

TYPE_IMPACT_TEMPLATE_CREATED = 9
TYPE_IMPACT_TEMPLATE_UPDATED = 10
TYPE_IMPACT_TEMPLATE_DELETED = 510

TYPE_IMPACT_DATA_UPLOADED = 11

TYPE_DISBURSEMENT_CREATED = 12
TYPE_DISBURSEMENT_UPDATED = 13
TYPE_DISBURSEMENT_DELETED = 713
TYPE_DISBURSEMENT_STATUS_CHANGE = 714
TYPE_DISBURSEMENT_DUE_DATE_REMINDER = 715

TYPE_UTILISATION_CREATED = 14
TYPE_UTILISATION_UPDATED = 15
TYPE_UTILISATION_DELETED = 815
TYPE_UTILISATION_STATUS_CHANGE = 816

TYPE_CHOICES = (
    (TYPE_PROJECT_CREATED, 'Project Created'),
    (TYPE_PROJECT_UPDATED, 'Project Updated'),
    (TYPE_MILESTONE_CREATED, 'Milestone Created'),
    (TYPE_MILESTONE_UPDATED, 'Milestone Updated'),
    (TYPE_TASK_CREATED, 'Task Created'),
    (TYPE_TASK_UPDATED, 'Task Updated'),
    (TYPE_TASK_OVERDUE, 'Task Overdue'),
    (TYPE_IMPACT_INDICATOR_CREATED, 'Impact Indicator Created'),
    (TYPE_IMPACT_INDICATOR_UPDATED, 'Impact Indicator Updated'),
    (TYPE_IMPACT_TEMPLATE_CREATED, 'Impact Template Created'),
    (TYPE_IMPACT_TEMPLATE_UPDATED, 'Impact Template Updated'),
    (TYPE_IMPACT_DATA_UPLOADED, 'Impact Data Uploaded'),
    (TYPE_DISBURSEMENT_CREATED, 'Disbursement Created'),
    (TYPE_DISBURSEMENT_UPDATED, 'Disbursement Updated'),
    (TYPE_DISBURSEMENT_STATUS_CHANGE, 'Disbursement Status Change'),
    (TYPE_UTILISATION_CREATED, 'Utilisation Created'),
    (TYPE_UTILISATION_UPDATED, 'Utilisation Updated'),
)
