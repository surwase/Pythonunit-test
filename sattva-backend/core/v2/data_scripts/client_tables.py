import random

from django.db import connection

import v2.data_scripts.constants as constants
from v2.client.models import ClientDatabaseDetail
from v2.project.models import Template, Project


class ClientTableHelper(object):
    username = ""
    schema_name = ""
    password = ""
    db_details = None
    client_initialized = False

    def __init__(self, client):
        self.client = client
        self.initialize_client()
        if not self.client_initialized:
            self.get_username()
            self.get_schema_name()
            self.generate_password()
            self.save_client_db_details()
            self.create_role_for_base_client_user()
            self.create_schema_for_client()
            self.grant_schema_to_role()
            self.grant_schema_tables_to_role()
            self.grant_future_schema_tables_to_role()

    def initialize_client(self):
        """
        This is a function to initialize the client.
        It fetches the db details of a client and checks if it si already initialized.
        If not, it initializes the client with db details.
        """
        self.db_details = ClientDatabaseDetail.objects.filter(
            client=self.client
        ).first()
        self.client_initialized = True if self.db_details else False
        if self.client_initialized:
            self.username = self.db_details.username
            self.password = self.db_details.password
            self.schema_name = self.db_details.schema_name

    def save_client_db_details(self):
        ClientDatabaseDetail.objects.create(
            username=self.username,
            password=self.password,
            schema_name=self.schema_name,
            client=self.client,
        )

    @staticmethod
    def run_query(query):
        cursor = connection.cursor()
        cursor.execute(query)
        cursor.close()

    def get_username(self):
        self.username = (
            "cl" + str(self.client.id) + constants.DB_BASE_USER_POSTFIX
        ).lower()

    def get_schema_name(self):
        self.schema_name = (constants.DB_NAME_PREFIX + str(self.client.id)).lower()

    def generate_password(self):
        p = "".join(random.sample(constants.SMALL_CASE, constants.SMALL_CASE_LENGTH))
        p = p + "".join(
            random.sample(constants.CAPITAL_CASE, constants.CAPITAL_CASE_LENGTH)
        )
        p = p + "".join(random.sample(constants.DIGITS, constants.DIGIT_LENGTH))
        self.password = p

    def create_role_for_base_client_user(self):
        query = constants.CREATE_USER_QUERY.format(self.username, self.password)
        self.run_query(query)

    def create_schema_for_client(self):
        query = constants.CREATE_SCHEMA_QUERY.format(self.schema_name)
        self.run_query(query)

    def grant_schema_to_role(self):
        query = constants.GRANT_SCHEMA_PERMISSION.format(
            self.schema_name, self.username
        )
        self.run_query(query)

    def grant_schema_tables_to_role(self):
        query = constants.GRANT_EXISTING_TABLES_TO_ROLE.format(
            self.schema_name, self.username
        )
        self.run_query(query)

    def grant_future_schema_tables_to_role(self):
        query = constants.GRANT_FUTURE_TABLES_TO_ROLE.format(
            self.schema_name, self.username
        )
        self.run_query(query)

    def create_client_based_view(self, query):
        query = query.format(self.client.id, self.schema_name)
        self.run_query(query)

    def create_tables(self):
        for view in constants.CLIENT_TABLES:
            self.create_client_based_view(view[1])
        self.create_impact_data_tables()
        self.create_primary_keys()
        self.create_foreign_key_constraints()

    def create_impact_data_tables(self):
        templates = Template.objects.filter(
            project__in=Project.objects.filter(client=self.client)
        )
        for template in templates:
            table_name = template.get_table_name()
            if self.check_if_table_exists(table_name):
                query = constants.IMPACT_DATA_TABLE.format(
                    table_name, table_name, self.schema_name
                )
                self.run_query(query)

    def check_if_table_exists(self, table_name):
        query = constants.TABLE_EXISTS_QUERY.format(table_name.lower())
        cursor = connection.cursor()
        cursor.execute(query)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        cursor.close()
        if value == 1:
            return True
        else:
            return False

    def create_primary_keys(self):
        query = constants.CREATE_PRIMARY_KEY.format(self.schema_name)
        self.run_query(query)

    def create_foreign_key_constraints(self):
        query = constants.CREATE_FOREGIN_KEY_CONSTRAINTS.format(self.schema_name)
        self.run_query(query)

    def sync_data(self):
        # delete schema
        client_clearer = ClientTableClearer(self.client)
        client_clearer.drop_schema_for_client()
        # create schema and tables with latest data
        self.create_schema_for_client()
        self.grant_schema_to_role()
        self.grant_schema_tables_to_role()
        self.grant_future_schema_tables_to_role()
        self.create_tables()


class ClientTableClearer(object):
    username = ""
    schema_name = ""
    password = ""
    db_details = None
    client_initialized = False

    def __init__(self, client):
        self.client = client
        self.initialize_client()

    def clear(self):
        if self.client_initialized:
            self.delete_client_db_details()
            self.drop_schema_for_client()
            self.drop_role_for_base_client_user()

    def initialize_client(self):
        """
        This is a function to initialize the client.
        It fetches the db details of a client and checks if it si already initialized.
        If not, it initializes the client with db details.
        """
        self.db_details = ClientDatabaseDetail.objects.filter(
            client=self.client
        ).first()
        self.client_initialized = True if self.db_details else False
        if self.client_initialized:
            self.username = self.db_details.username
            self.password = self.db_details.password
            self.schema_name = self.db_details.schema_name

    def delete_client_db_details(self):
        ClientDatabaseDetail.objects.filter(client=self.client).delete()

    @staticmethod
    def run_query(query):
        cursor = connection.cursor()
        cursor.execute(query)
        cursor.close()

    def drop_role_for_base_client_user(self):
        query = constants.DROP_USER_QUERY.format(self.username, self.password)
        self.run_query(query)

    def drop_schema_for_client(self):
        query = constants.DROP_SCHEMA_QUERY.format(self.schema_name)
        self.run_query(query)
