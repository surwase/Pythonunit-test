# Generated by Django 2.0.5 on 2019-12-07 07:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('client', '0014_clientdocument'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserNotificationSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('settings_name', models.SmallIntegerField(choices=[(0, 'Alert to over due of any milestone'), (1, 'Alert to over due of any milestone'), (2, 'Alert to over due of any task'), (3, 'Alert to over due of any task')])),
                ('is_enabled', models.BooleanField(default=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='usernotificationsettings',
            unique_together={('user', 'settings_name')},
        ),
    ]
