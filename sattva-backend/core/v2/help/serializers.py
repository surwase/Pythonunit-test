

from rest_framework import serializers
from django.utils.html import strip_tags
from v2.help.models import HelpSections, HelpSubSections, HelpArticles, VideoDetails



"""
    This class will serialize HelpSections model and return fields section_name, path, sub_sections.
"""
class HelpSectionsSerializer(serializers.ModelSerializer):
    sub_sections = serializers.SerializerMethodField()

    def get_sub_sections(self, obj):
        sub_sections = HelpSubSections.objects.filter(section=obj).order_by('level')
        return HelpSubSectionSerializer(sub_sections, many=True).data

    class Meta:
        model = HelpSections
        fields = ['section_name', 'path', 'sub_sections']

"""
    This class will serialize HelpSections model and return fields section, title, path, roles, is_head, sub_section_link, articles_length, videos_length.
"""
class HelpSubSectionSerializer(serializers.ModelSerializer):
    sub_section_link = serializers.SerializerMethodField()
    articles_length = serializers.SerializerMethodField()
    videos_length = serializers.SerializerMethodField()

    def get_sub_section_link(self, obj):
        return '/help/' + obj.section.path + '#' + obj.path

    def get_articles_length(self, obj):
        return HelpArticles.objects.filter(sub_section=obj).count()

    def get_videos_length(self, obj):
        return VideoDetails.objects.filter(sub_section=obj).count()

    class Meta:
        model = HelpSubSections
        fields = ['section', 'title', 'path', 'roles', 'is_head', 'sub_section_link', 'articles_length', 'videos_length']


"""
    This class will serialize HelpSubSections model and return fields section, title, path, roles, is_head, pages, videos.
"""
class HelpSubSectionDetailSerializer(serializers.ModelSerializer):
    pages = serializers.SerializerMethodField()
    videos = serializers.SerializerMethodField()

    def get_pages(self, obj):
        return HelpArticleListMiniSerializer(HelpArticles.objects.filter(sub_section=obj), many=True).data

    def get_videos(self, obj):
        return VideoDetailSerializer(VideoDetails.objects.filter(sub_section=obj), many=True).data

    class Meta:
        model = HelpSubSections
        fields = ['section', 'title', 'path', 'roles', 'is_head', 'pages', 'videos']


"""
    This class will serialize Help Articles model return fields like title and id.
"""
class HelpArticleListMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = HelpArticles
        fields = ['title', 'id']

"""
    This class will serialize Help Articles model and return fields title, id, section_name, sub_section_name, content.
"""
class HelpArticleListSerializer(serializers.ModelSerializer):
    section_name = serializers.SerializerMethodField()
    sub_section_name = serializers.SerializerMethodField()
    content = serializers.SerializerMethodField()

    def get_section_name(self, obj):
        return obj.sub_section.section.section_name

    def get_sub_section_name(self, obj):
        return obj.sub_section.title

    def get_content(self, obj):
        content = strip_tags(obj.content)
        content = content.replace("&nbsp;", "")
        content = content.replace("&asp;", "")
        content = content.replace("&ldquo;", "")
        content = content.replace("&rdquo;", "")
        content = content.replace("\n", "")
        content = content.replace("\r", "")
        content = content[:150]
        return content

    class Meta:
        model = HelpArticles
        fields = ['title', 'id', 'section_name', 'sub_section_name', 'content']


"""
    This class will serialize Help Articles model.
"""
class HelpArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = HelpArticles
        fields = '__all__'


"""
    This class will serialize VideoDetails model.
"""
class VideoDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoDetails
        fields = '__all__'
