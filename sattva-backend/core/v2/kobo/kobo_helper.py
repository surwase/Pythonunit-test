import requests
from django.conf import settings


def create_user_in_kobo(username, password):
    if settings.KOBO_KPI_URL:
        try:
            user_dict = {
                "username": username,
                "email": username,
                "password": password
            }
            resp = requests.post(settings.KOBO_KPI_URL + 'remote/user/register/', json=user_dict)
            print(resp.content)
        except Exception as ex:
            # TODO: Add logging
            print(ex)
