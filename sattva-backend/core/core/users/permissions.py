from rest_framework.permissions import BasePermission


class SuperAdminPermission(BasePermission):
    """
    A base class from which all permission classes should inherit.
    """
    ROLE_CODE = 'SUPER_ADMIN'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.role.code == self.ROLE_CODE:
            return True
        return False


class SattvaCSRAdminPermission(SuperAdminPermission):
    """
    A base class from which all permission classes should inherit.
    """
    ROLE_CODE = 'SATTVA_CSR_ADMIN'


class CSRAdminPermission(SuperAdminPermission):
    """
    A base class from which all permission classes should inherit.
    """
    ROLE_CODE = 'CSR_ADMIN'


class CSRUserPermission(SuperAdminPermission):
    """
    A base class from which all permission classes should inherit.
    """
    ROLE_CODE = 'CSR_USER'


class NGOUserPermission(SuperAdminPermission):
    """
    A base class from which all permission classes should inherit.
    """
    ROLE_CODE = 'NGO_USER'
