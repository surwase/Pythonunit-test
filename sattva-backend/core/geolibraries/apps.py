from django.apps import AppConfig


class GeolibrariesConfig(AppConfig):
    name = 'geolibraries'
