ALERT_MILESTONE_OVER_DUE_BY_PORTAL = 0
ALERT_TASK_OVER_DUE_BY_PORTAL = 2

NOTIFICATION_ON_PROJECT_CREATION = 101
ALERT_ON_PROJECT_DELETION = 102

NOTIFICATION_ON_MILESTONE_CREATION = 201
NOTIFICATION_ON_MILESTONE_UPDATE = 202
ALERT_ON_MILESTONE_DELETION = 203

NOTIFICATION_ON_TASK_CREATION = 301
NOTIFICATION_ON_TASK_UPDATE = 302
ALERT_ON_TASK_DELETION = 303

NOTIFICATION_ON_IMPACT_INDICATOR_CREATION = 401
NOTIFICATION_ON_IMPACT_INDICATOR_UPDATE = 402
ALERT_ON_IMPACT_INDICATOR_DELETION = 403
NOTIFICATION_ON_TEMPLATE_CREATION = 404
NOTIFICATION_ON_TEMPLATE_UPDATE = 405
ALERT_ON_TEMPLATE_DELETION = 406
NOTIFICATION_ON_IMPACT_DATA_UPLOAD = 407

NOTIFICATION_ON_DISBURSEMENT_CREATION = 501
NOTIFICATION_ON_DISBURSEMENT_UPDATE = 502
ALERT_ON_DISBURSEMENT_DELETION = 503
ALERT_ON_DISBURSEMENT_ACTION_REQUIRED = 504
ALERT_ON_DISBURSEMENT_APPROVAL = 505
ALERT_ON_DISBURSEMENT_REJECTION = 506
NOTIFICATION_BEFORE_DUE_DATE_REMINDER = 508
ALERT_AFTER_OVERDUE_ALERT = 509
NOTIFICATION_ON_FINANCIAL_ADD_UPDATE = 510
NOTIFICATION_ON_FINANCIAL_STATUS_CHANGE = 511

NOTIFICATION_ON_UTILISATION_CREATION = 601
NOTIFICATION_ON_UTILISATION_UPDATE = 602
ALERT_ON_UTILISATION_DELETION = 603

ALERT_ON_USER_ADDITION = 701
ALERT_ON_USER_DELETION = 702

TYPE_CHOICES = (
    (ALERT_MILESTONE_OVER_DUE_BY_PORTAL, 'Alert to over due of any milestone'),
    (ALERT_TASK_OVER_DUE_BY_PORTAL, 'Alert to over due of any task'),

    # (ALERT_ON_PROJECT_DELETION, 'Alert when project is deleted'),

    (NOTIFICATION_ON_MILESTONE_CREATION, 'Notification when milestone is created'),
    (NOTIFICATION_ON_MILESTONE_UPDATE, 'Notification when milestone is updated'),
    # (ALERT_ON_MILESTONE_DELETION, 'Alert when milestone is deleted'),

    (NOTIFICATION_ON_TASK_CREATION, 'Notification when task is created'),
    (NOTIFICATION_ON_TASK_UPDATE, 'Notification when task is updated'),
    # (ALERT_ON_TASK_DELETION, 'Alert when task is deleted'),

    (NOTIFICATION_ON_IMPACT_INDICATOR_CREATION, 'Notification when impact indicator is created'),
    (NOTIFICATION_ON_IMPACT_INDICATOR_UPDATE, 'Notification when impact indicator is updated'),
    # (ALERT_ON_IMPACT_INDICATOR_DELETION, 'Alert when impact indicator is deleted'),
    (NOTIFICATION_ON_TEMPLATE_CREATION, 'Notification when impact template is created'),
    (NOTIFICATION_ON_TEMPLATE_UPDATE, 'Notification when impact template is created'),
    # (ALERT_ON_TEMPLATE_DELETION, 'Alert when impact template is deleted'),
    (NOTIFICATION_ON_IMPACT_DATA_UPLOAD, 'Notification when impact data is uploaded'),

    # (NOTIFICATION_ON_DISBURSEMENT_CREATION, 'Notification when disbursement is created'),
    # (NOTIFICATION_ON_DISBURSEMENT_UPDATE, 'Notification when disbursement is updated'),
    # (ALERT_ON_DISBURSEMENT_DELETION, 'Alert when disbursement is deleted'),
    (ALERT_ON_DISBURSEMENT_ACTION_REQUIRED, 'Alert when action is required on disbursement'),
    # (ALERT_ON_DISBURSEMENT_APPROVAL, 'Alert when disbursement is approved'),
    # (ALERT_ON_DISBURSEMENT_REJECTION, 'Alert when disbursement is rejected'),
    (NOTIFICATION_BEFORE_DUE_DATE_REMINDER, 'Notification before Due Date Reminder'),
    (ALERT_AFTER_OVERDUE_ALERT, 'Alert after Overdue Alert'),
    (NOTIFICATION_ON_FINANCIAL_ADD_UPDATE, 'Notification on Additions or Updates'),
    (NOTIFICATION_ON_FINANCIAL_STATUS_CHANGE, 'Notification on Status Change'),

    # (NOTIFICATION_ON_UTILISATION_CREATION, 'Notification when utilisation is created'),
    # (NOTIFICATION_ON_UTILISATION_UPDATE, 'Notification when utilisation is updated'),
    # (ALERT_ON_UTILISATION_DELETION, 'Alert when utilisation is deleted'),

    # (ALERT_ON_USER_ADDITION, 'Alert when a user is added'),
    # (ALERT_ON_USER_DELETION, 'Alert when a user is deleted'),

)

SETTINGS_CATEGORY = {
    ALERT_MILESTONE_OVER_DUE_BY_PORTAL: {
        'category': 'Milestone',
        'type': 'ALERT'
    },
    ALERT_TASK_OVER_DUE_BY_PORTAL: {
        'category': 'Task',
        'type': 'ALERT'
    },

    ALERT_ON_PROJECT_DELETION: {
        'category': 'PROJECT',
        'type': 'ALERT'
    },

    NOTIFICATION_ON_MILESTONE_CREATION: {
        'category': 'Milestone',
        'type': 'NOTIFICATION'
    },
    NOTIFICATION_ON_MILESTONE_UPDATE: {
        'category': 'Milestone',
        'type': 'NOTIFICATION'
    },
    ALERT_ON_MILESTONE_DELETION: {
        'category': 'Milestone',
        'type': 'ALERT'
    },

    NOTIFICATION_ON_TASK_CREATION: {
        'category': 'Task',
        'type': 'NOTIFICATION'
    },
    NOTIFICATION_ON_TASK_UPDATE: {
        'category': 'Task',
        'type': 'NOTIFICATION'
    },
    ALERT_ON_TASK_DELETION: {
        'category': 'Task',
        'type': 'ALERT'
    },

    NOTIFICATION_ON_IMPACT_INDICATOR_CREATION: {
        'category': 'Impact',
        'type': 'NOTIFICATION'
    },
    NOTIFICATION_ON_IMPACT_INDICATOR_UPDATE: {
        'category': 'Impact',
        'type': 'NOTIFICATION'
    },
    ALERT_ON_IMPACT_INDICATOR_DELETION: {
        'category': 'Impact',
        'type': 'ALERT'
    },
    NOTIFICATION_ON_TEMPLATE_CREATION: {
        'category': 'Impact',
        'type': 'NOTIFICATION'
    },
    NOTIFICATION_ON_TEMPLATE_UPDATE: {
        'category': 'Impact',
        'type': 'NOTIFICATION'
    },
    ALERT_ON_TEMPLATE_DELETION: {
        'category': 'Impact',
        'type': 'ALERT'
    },
    NOTIFICATION_ON_IMPACT_DATA_UPLOAD: {
        'category': 'Impact',
        'type': 'NOTIFICATION'
    },

    NOTIFICATION_ON_DISBURSEMENT_CREATION: {
        'category': 'Financials',
        'type': 'NOTIFICATION'
    },
    NOTIFICATION_ON_DISBURSEMENT_UPDATE: {
        'category': 'Financials',
        'type': 'NOTIFICATION'
    },
    ALERT_ON_DISBURSEMENT_DELETION: {
        'category': 'Financials',
        'type': 'ALERT'
    },
    ALERT_ON_DISBURSEMENT_ACTION_REQUIRED: {
        'category': 'Financials',
        'type': 'ALERT'
    },
    ALERT_ON_DISBURSEMENT_APPROVAL: {
        'category': 'Financials',
        'type': 'ALERT'
    },
    ALERT_ON_DISBURSEMENT_REJECTION: {
        'category': 'Financials',
        'type': 'ALERT'
    },
    NOTIFICATION_BEFORE_DUE_DATE_REMINDER: {
        'category': 'Financials',
        'type': 'NOTIFICATION',
        'is_periodic': True,
        'is_after': False
    },
    ALERT_AFTER_OVERDUE_ALERT: {
        'category': 'Financials',
        'type': 'ALERT',
        'is_periodic': True,
        'is_after': True
    },
    NOTIFICATION_ON_FINANCIAL_ADD_UPDATE: {
        'category': 'Financials',
        'type': 'NOTIFICATION'
    },

    NOTIFICATION_ON_FINANCIAL_STATUS_CHANGE: {
        'category': 'Financials',
        'type': 'NOTIFICATION'
    },

    NOTIFICATION_ON_UTILISATION_CREATION: {
        'category': 'Financials',
        'type': 'NOTIFICATION'
    },
    NOTIFICATION_ON_UTILISATION_UPDATE: {
        'category': 'Financials',
        'type': 'NOTIFICATION'
    },
    ALERT_ON_UTILISATION_DELETION: {
        'category': 'Financials',
        'type': 'ALERT'
    },

    ALERT_ON_USER_ADDITION: {
        'category': 'User',
        'type': 'ALERT'
    },
    ALERT_ON_USER_DELETION: {
        'category': 'User',
        'type': 'ALERT'
    },
}
