# Generated by Django 2.0.5 on 2019-10-15 07:04

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plant', '0002_auto_20191014_0933'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plant',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 10, 15, 12, 34, 50, 531520)),
        ),
        migrations.AlterField(
            model_name='plant',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 10, 15, 12, 34, 50, 531490)),
        ),
    ]
