from django.test import TestCase
from .test_setup import TestSetUp
from v2.governance.models import Governance

class TestGovernance(TestSetUp):

    def setUp(self):
        super().setUp()
        Governance.objects.create(
            name = "governance1",
            location = ['lucknow'],
            description = "hi i am a governance",
            client = self.client_obj,
            governance_type =0,
            status = 0,
            comments = "comment1",
            is_milestone_selected = True,
            is_financial_selected = True,
            is_impact_selected = False,
            is_risk_selected = False,

            )

    def test_validation(self):
        with self.assertRaises(ValueError):
            Governance.objects.create(
            name = 123,
            location = ['lucknow'],
            description = "hi i am a governance",
            client = 123,
            governance_type =0,
            status = 0,
            comments = "comment1",
            is_milestone_selected = True,
            is_financial_selected = True,
            is_impact_selected = False,
            is_risk_selected = False,

            )

    def test_values(self):
        gov1=Governance.objects.get(name = "governance1")
        self.assertEqual(gov1.name , "governance1")
        self.assertEqual(gov1.location , ['lucknow'])
        self.assertEqual(gov1.description , "hi i am a governance")
        self.assertEqual(gov1.client.client_name, "client Test10")
        self.assertEqual(gov1.governance_type , 0)
        self.assertEqual(gov1.status , 0)
        self.assertEqual(gov1.comments , "comment1")
        self.assertEqual(gov1.is_milestone_selected , True)
        self.assertEqual(gov1.is_financial_selected , True)
        self.assertEqual(gov1.is_impact_selected ,False)
        self.assertEqual(gov1.is_risk_selected ,False)
