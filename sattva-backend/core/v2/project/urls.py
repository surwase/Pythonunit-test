from django.conf.urls import url
from django.urls import include
from rest_framework_bulk.routes import BulkRouter

from .views import *

app_name = "project"

bulk_router = BulkRouter()
bulk_router.register(r'^template_attribute', TemplateAttributeViewSet, 'template_attributes')
bulk_router.register(r'^project_indicator', ProjectIndicatorViewSet, 'project_indicator')
bulk_router.register(r'^project_schedule_vii_activities', ProjectScheduleVIIActivityViewSet,
                     'project_schedule_vii_activities')

urlpatterns = [
    url(r'^$', ProjectModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project'),
    url(r'^(?P<pk>\d+)/$', ProjectModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project'),
    url(r'^micro/$', ProjectMicroModelViewSet.as_view(
        {'get': 'list'}),
        name='project_details'),
    url(r'^micro/(?P<pk>\d+)/$', ProjectMicroModelViewSet.as_view(
        {'get': 'retrieve'}),
        name='project_details'),
    url(r'^detail/(?P<pk>\d+)/$', ProjectModelDetailViewSet.as_view(
        {'get': 'retrieve'}),
        name='project_details'),
    url(r'^unadded_projects/$', UserUnAddedProjectsAPIView.as_view(),
        name='unadded_projects'),
    url(r'^unadded_plants/$', UserUnAddedPlantAPIView.as_view(),
        name='unadded_plants'),
    url(r'^project_user/$', UserProjectViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_user'),
    url(r'^project_user/(?P<pk>\d+)/$', UserProjectViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_user'),
    url(r'^delete_project_user/$', DeleteUserProjectAPIView.as_view(),
        name='delete_project_user'),
    url(r'^document/$', ProjectDocumentModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_doc'),
    url(r'^document/(?P<pk>\d+)/$', ProjectDocumentModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_doc'),
    url(r'^project_tags/$', ProjectTagModelViewSet.as_view(
        {'get': 'list'}),
        name='project_tags'),
    url(r'^images/$', ProjectImageModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_images'),
    url(r'^images/(?P<pk>\d+)/$', ProjectImageModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_images'),
    url(r'^settings/$', ProjectSettingModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_settings'),
    url(r'^settings/(?P<pk>\d+)/$', ProjectSettingModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_settings'),
    url(r'^milestone/$', MilestoneModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='milestone'),
    url(r'^milestone-list/$', MilestoneListModelViewSet.as_view(
        {'get': 'list'}),
        name='milestone-list'),
    url(r'^mini_milestone/$', MiniMilestoneModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='mini_milestone'),
    url(r'^milestone/(?P<pk>\d+)/$', MilestoneModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='milestone'),
    url(r'^milestone_note/$', MilestoneNoteModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_doc'),
    url(r'^milestone_note/(?P<pk>\d+)/$', MilestoneNoteModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_doc'),
    url(r'^milestone_document/$', MilestoneDocumentModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_doc'),
    url(r'^milestone_document/(?P<pk>\d+)/$', MilestoneDocumentModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_doc'),
    # url(r'^files/(?P<pk>\d+)/$', FilesModelViewset.as_view(),
    #     name='project_files'),
    # url(r'^files/$', FilesModelViewset.as_view(),
    #     name='project_files_upload'),
    url(r'^task/$', TaskModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='task'),
    url(r'^task/(?P<pk>\d+)/$', TaskModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='task'),
    url(r'^task_comment/$', TaskCommentModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='task_comment'),
    url(r'^task_comment/(?P<pk>\d+)/$', TaskCommentModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='task_comment'),
    url(r'^task_document/$', TaskDocumentModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='task_document'),
    url(r'^task_document/(?P<pk>\d+)/$', TaskDocumentModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='task_document'),
    url(r'^sub_task/$', SubTaskModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='sub_task'),
    url(r'^sub_task/(?P<pk>\d+)/$', SubTaskModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='sub_task'),
    url(r'^task_activity_log/$', TaskActivityLogViewSet.as_view(
        {'get': 'list'}),
        name='task_activity_log'),
    url(r'^milestone_activity_log/$', MilestoneActivityLogViewSet.as_view(
        {'get': 'list'}),
        name='milestone-activity_log'),
    url(r'^disbursement/$', DisbursementModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='disbursement'),
    url(r'^mini_disbursement/$', MiniDisbursementModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='mini_disbursement'),
    url(r'^disbursement/(?P<pk>\d+)/$', DisbursementModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='disbursement'),
    url(r'^disbursement_comment/$', DisbursementCommentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='disbursement_comment'),
    url(r'^disbursement_comment/(?P<pk>\d+)/$', DisbursementCommentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='disbursement_comment'),
    url(r'^disbursement_status/$', DisbursementStatusViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='disbursement_status'),
    url(r'^disbursement_status/(?P<pk>\d+)/$', DisbursementStatusViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='disbursement_status'),
    url(r'^disbursement_document/$', DisbursementDocumentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='disbursement_document'),
    url(r'^disbursement_document/(?P<pk>\d+)/$', DisbursementDocumentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='disbursement_document'),
    url(r'^disbursement_activity_log/$', DisbursementActivityLogViewSet.as_view(
        {'get': 'list'}),
        name='disbursement_activity_log'),
    url(r'^utilisation/$', UtilisationViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation'),
    url(r'^utilisation/(?P<pk>\d+)/$', UtilisationViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation'),
    url(r'^utilisation_comment/$', UtilisationCommentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_comment'),
    url(r'^utilisation_comment/(?P<pk>\d+)/$', UtilisationCommentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_comment'),
    url(r'^utilisation_document/$', UtilisationDocumentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_document'),
    url(r'^utilisation_document/(?P<pk>\d+)/$', UtilisationDocumentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_document'),
    url(r'^utilisation_expense_document/$', UtilisationExpenseDocumentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_expense_document'),
    url(r'^utilisation_expense_document/(?P<pk>\d+)/$', UtilisationExpenseDocumentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_expense_document'),
    url(r'^delete_project_indicator/(?P<pk>\d+)/$', ProjectIndicatorViewSet.as_view(
        {'delete': 'destroy'}),
        name='delete_project_indicator'),
    url(r'^indicator_document/$', IndicatorDocumentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='indicator_document'),
    url(r'^indicator_document/(?P<pk>\d+)/$', IndicatorDocumentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='indicator_document'),
    url(r'^indicator_comment/$', IndicatorCommentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='indicator_comment'),
    url(r'^indicator_comment/(?P<pk>\d+)/$', IndicatorCommentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='indicator_comment'),
    url(r'^indicator_activity_log/$', IndicatorActivityLogViewSet.as_view(
        {'get': 'list'}),
        name='indicator_activity_log'),
    url(r'^project_indicator_output/$', ProjectOutputMiniViewSet.as_view(
        {'get': 'list'}),
        name='project_indicator_output'),
    url(r'^project_indicator_outcome/$', ProjectOutcomeMiniViewSet.as_view(
        {'get': 'list'}),
        name='project_indicator_outcome'),
    url(r'^project_output_document/$', ProjectOutputDocumentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_output_document'),
    url(r'^project_output_document/(?P<pk>\d+)/$', ProjectOutputDocumentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_output_document'),
    url(r'^project_outcome_document/$', ProjectOutcomeDocumentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_outcome_document'),
    url(r'^project_outcome_document/(?P<pk>\d+)/$', ProjectOutcomeDocumentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_outcome_document'),
    url(r'^project_output/$', ProjectOutputViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_output'),
    url(r'^project_output/(?P<pk>\d+)/$', ProjectOutputViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_output'),
    url(r'^project_outcome/$', ProjectOutcomeViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_outcome'),
    url(r'^project_outcome/(?P<pk>\d+)/$', ProjectOutcomeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_outcome'),
    url(r'^utilisation_upload_document/$', UtilisationUploadDocumentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_upload_document'),
    url(r'^utilisation_upload_document/(?P<pk>\d+)/$', UtilisationUploadDocumentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_upload_document'),
    url(r'^utilisation_status/$', UtilisationStatusViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_status'),
    url(r'^utilisation_status/(?P<pk>\d+)/$', UtilisationStatusViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_status'),
    url(r'^project_output_status/$', ProjectOutputStatusViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_output_status'),
    url(r'^project_output_status/(?P<pk>\d+)/$', ProjectOutputStatusViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_output_status'),
    url(r'^project_outcome_status/$', ProjectOutcomeStatusViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_outcome_status'),
    url(r'^project_outcome_status/(?P<pk>\d+)/$', ProjectOutcomeStatusViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_outcome_status'),
    url(r'^case_study/$', ImpactCaseStudyViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='case_study'),
    url(r'^case_study/(?P<pk>\d+)/$', ImpactCaseStudyViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='case_study'),
    url(r'^case_study_documents/$', CaseStudyDocumentsViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='case_study_documents'),
    url(r'^case_study_documents/(?P<pk>\d+)/$', CaseStudyDocumentsViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='case_study_documents'),
    url(r'^case_study_activity_log/$', CaseStudyActivityLogViewSet.as_view(
        {'get': 'list'}),
        name='case_study_activity_log'),
    url(r'^utilisation_activity_log/$', UtilisationActivityLogViewSet.as_view(
        {'get': 'list'}),
        name='utilisation_activity_log'),
    url(r'^utilisation_upload/$', UtilisationUploadViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_upload'),
    url(r'^utilisation_upload/(?P<pk>\d+)/$', UtilisationUploadViewSet.as_view(
        {'get': 'retrieve', 'patch': 'partial_update'}),
        name='utilisation_upload'),
    url(r'^utilisation_upload_template/$', BulkUtilisationTemplateAPIView.as_view(),
        name='utilisation_upload_template'),
    url(r'^utilisation_expense/$', UtilisationExpenseViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_expense'),
    url(r'^utilisation_expense/(?P<pk>\d+)/$', UtilisationExpenseViewSet.as_view(
        {'get': 'retrieve', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_expense'),
    url(r'^indicator_outcome/$', ProjectIndicatorOutcomeViewSet.as_view(
        {'get': 'list', 'post': 'create'}), name='indicator_outcome'),
    url(r'^indicator_outcome/(?P<pk>\d+)/$', ProjectIndicatorOutcomeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='indicator_outcome'),
    url(r'^project_remark/$', ProjectRemarkViewSet.as_view(
        {'get': 'list', 'post': 'create'}), name='project_remark'),
    url(r'^project_remark/(?P<pk>\d+)/$', ProjectRemarkViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_remark'),
    url(r'^template_type/$', TemplateTypeViewSet.as_view(
        {'get': 'list'}),
        name='template_type'),
    url(r'^template/$', TemplateViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='template'),
    url(r'^template/(?P<pk>\d+)/$', TemplateViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='template'),
    url(r'^download_template/(?P<project_id>\d+)/(?P<template_id>\d+)/$', DownloadTemplateAPIView.as_view(),
        name='download_template'),
    url(r'^create_project_template/(?P<project_id>\d+)/(?P<template_id>\d+)/$', CreateProjectTemplateAPIView.as_view(),
        name='create_project_template'),
    url(r'^delete_project_template/(?P<project_id>\d+)/(?P<template_id>\d+)/$', DeleteProjectTemplateAPIView.as_view(),
        name='delete_project_template'),
    url(r'^delete_project_template_data/(?P<project_id>\d+)/(?P<template_id>\d+)/$',
        DeleteProjectTemplateDataAPIView.as_view(),
        name='delete_project_template'),
    url(r'^download_csv_data/(?P<project_id>\d+)/(?P<template_id>\d+)/$', DownloadCSVDataAPIView.as_view(),
        name='download_csv_data'),
    url(r'^impact_data_batches/(?P<project_id>\d+)/(?P<template_id>\d+)/$', ImpactDataBatchesAPIView.as_view(),
        name='impact_data_batches'),
    url(r'^csvupload/$', CSVUploadViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='csvupload-list'),
    url(r'^csvupload/(?P<pk>\d+)/$', CSVUploadViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='csvupload-detail'),
    url(r'^template_activity_log/$', TemplateActivityLogViewSet.as_view(
        {'get': 'list'}),
        name='template_activity_log'),
    url(r'^impact_upload_status/$', ImpactUploadStatusViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='impact_upload_status'),
    url(r'^impact_upload_status/(?P<pk>\d+)/$', ImpactUploadStatusViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='impact_upload_status'),
    # url(r'^csvdata/$', CSVDataViewSet.as_view(),
    #     name='csv_data'),
    url(r'^dashboard/$', ProjectDashboardModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='dashboard'),
    url(r'^dashboard/(?P<pk>\d+)/$', ProjectDashboardModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='dashboard'),
    url(r'^location-extract/(?P<pk>\d+)/$', ProjectLocationExtract.as_view(),
        name='project_location_extract'),
    url(r'^disbursement_reminder/(?P<disbursement_id>\d+)/$', RemindUsersForActionOnDisbursement.as_view(),
        name='disbursement_reminder'),
    url(r'^unadded_project_template/(?P<project_id>\d+)/$', UnaddedProjectTemplateAPIView.as_view(),
        name='unadded_project_template'),
    url(r'^', include(bulk_router.urls)),
    url(r'^disbursement_workflow_ruleset/$', DisbursementWorkflowRuleSetViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='disbursement_workflow_ruleset'),
    url(r'^disbursement_workflow_ruleset/(?P<pk>\d+)/$', DisbursementWorkflowRuleSetViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='disbursement_workflow_ruleset'),
    url(r'^disbursement_workflow_rule/$', DisbursementWorkflowRuleViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='disbursement_workflow_rules'),
    url(r'^disbursement_workflow_rule/(?P<pk>\d+)/$', DisbursementWorkflowRuleViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='disbursement_workflow_rules'),
    url(r'^disbursement_workflow_assignee/$', DisbursementWorkflowAssigneeViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='disbursement_workflow_rule_assignee'),
    url(r'^disbursement_workflow_assignee/(?P<pk>\d+)/$', DisbursementWorkflowAssigneeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='disbursement_workflow_assignee'),
    url(r'^utilisation_workflow_ruleset/$', UtilisationWorkflowRuleSetViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_workflow_ruleset'),
    url(r'^utilisation_workflow_ruleset/(?P<pk>\d+)/$', UtilisationWorkflowRuleSetViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_workflow_ruleset'),
    url(r'^impact_workflow_ruleset/$', ImpactWorkflowRuleSetViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='impact_workflow_ruleset'),
    url(r'^impact_workflow_ruleset/(?P<pk>\d+)/$', ImpactWorkflowRuleSetViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='impact_workflow_ruleset'),
    url(r'^utilisation_workflow_rule/$', UtilisationWorkflowRuleViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_workflow_rule'),
    url(r'^utilisation_workflow_rule/(?P<pk>\d+)/$', UtilisationWorkflowRuleViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_workflow_rule'),
    url(r'^utilisation_workflow_assignee/$', UtilisationWorkflowAssigneeViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='utilisation_workflow_rule_assignee'),
    url(r'^utilisation_workflow_assignee/(?P<pk>\d+)/$', UtilisationWorkflowAssigneeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='utilisation_workflow_assignee'),
    url(r'^impact_workflow_rule/$', ImpactWorkflowRuleViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='impact_workflow_rule'),
    url(r'^impact_workflow_rule/(?P<pk>\d+)/$', ImpactWorkflowRuleViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='impact_workflow_rule'),
    url(r'^impact_workflow_assignee/$', ImpactWorkflowAssigneeViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='impact_workflow_rule_assignee'),
    url(r'^impact_workflow_assignee/(?P<pk>\d+)/$', ImpactWorkflowAssigneeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='impact_workflow_assignee'),
    url(r'^disbursement_workflow_project/(?P<client>\d+)/$', DisbursementWorkflowProjectAPIView.as_view(),
        name='disbursement_workflow_project'),
    url(r'^utilisation_workflow_project/(?P<client>\d+)/$', UtilisationWorkflowProjectAPIView.as_view(),
        name='utilisation_workflow_project'),
    url(r'^impact_workflow_project/(?P<client>\d+)/$', ImpactWorkflowProjectAPIView.as_view(),
        name='impact_workflow_project'),
    url(r'^remove_disbursement_workflow_assignee/$', RemoveDisbursementWorkflowAssigneeAPIView.as_view(),
        name='remove_disbursement_workflow_assignee'),
    url(r'^remove_utilisation_workflow_assignee/$', RemoveUtilisationWorkflowAssigneeAPIView.as_view(),
        name='remove_disbursement_workflow_assignee'),
    url(r'^remove_impact_workflow_assignee/$', RemoveImpactWorkflowAssigneeAPIView.as_view(),
        name='remove_impact_workflow_assignee'),
    url(r'^check_disbursement_unassigned_rule/$', CheckDisbursementUnassignedRuleAPIView.as_view(),
        name='check_disbursement_unassigned_rule'),
    url(r'^check_utilisation_for_disbursement/$', CheckPreviousUtilisationForDisbursementAPIView.as_view(),
        name='check_disbursement_unassigned_rule'),
    url(r'^bulkupload/$', BulkUploadModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='bulkupload-list'),
    url(r'^bulkupload/(?P<pk>\d+)/$', BulkUploadModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='bulkupload-detail'),
    url(r'^milestone_task/download/$', ProjectPlanExcelView.as_view(),
        name='milestone_task'),
    url(r'^disbursement_utilisation/download/$', FinancialExcelView.as_view(),
        name='disbursement_utilisation'),
    url(r'^all_task/download/$', AllTaskExcelView.as_view(),
        name='all_task'),
    url(r'^all_programs/download/$', AllProgramsExcelView.as_view(),
        name='all_programs'),
    url(r'^indicator/download/$', IndicatorExcelView.as_view(),
        name='project_indicator'),
    url(r'^project_location/download/$', LocationPortfolioExcelView.as_view(),
        name='project_location'),
    url(r'^location/$', LocationPortfolioViewSet.as_view(),
        name='project_location'),
    url(r'^bulkfile/$', BulkfileModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='bulkfile-list'),
    url(r'^bulkfile/(?P<pk>\d+)/$', BulkfileModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='bulkfile-detail'),
    url(r'^images/download/$', ProjectImageDownload.as_view(),
        name='project_files'),
    url(r'^case_study/pdf/download/$', GeneratePDF.as_view(),
            name='case_study_pdf'),
    url(r'^database/query$', DatabaseQuery.as_view(),
            name='query'),
    url(r'^database/query2$', DatabaseQuery2.as_view(),
            name='query2'),
    ]
