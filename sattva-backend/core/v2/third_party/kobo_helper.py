import requests
from django.conf import settings


def create_user_in_kobo(username, password):
    try:
        user_dict = {
            "username": username,
            "email": username,
            "password": password
        }
        resp = requests.post(settings.KOBO_KPI_URL + '/users/register/', json=user_dict)
    except Exception as ex:
        # TODO: Add logging
        print(ex)
