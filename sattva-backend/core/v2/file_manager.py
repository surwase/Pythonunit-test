import boto3


class FileManager(object):

    def __init__(self):
        self.s3 = boto3.resource('s3')
        pass

    def upload_file(self, filename, path):
        pass

    def delete_file(self, filename, path):
        pass

    def create_folder(self, folder_name, path):
        pass

    def delete_folder(self, folder_name, path):
        pass

    def rename_file(self, file_name, path):
        pass

    def rename_folder(self, folder_name, path):
        pass


def create_current_folders():
    import boto3
    from django.conf import settings
    s3 = boto3.client('s3')
    from v2.client.models import Client
    clients = Client.objects.all()
    for client in clients:
        s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key="media/file_system/{0}/".format(client.client_name))
        s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                      Key="media/file_system/{0}/{1}/".format(client.client_name, 'images'))
        s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                      Key="media/file_system/{0}/tasks/".format(client.client_name))
        plants = client.get_all_plants()
        for plant in plants:
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                          Key="media/file_system/{0}/{1}/".format(client.client_name, plant.plant_name))
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                          Key="media/file_system/{0}/{1}/images/".format(client.client_name, plant.plant_name))
            projects = plant.get_projects()
            for project in projects:
                s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                              Key="media/file_system/{0}/{1}/{2}/".format(client.client_name, plant.plant_name,
                                                                          project.project_name))
                s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                              Key="media/file_system/{0}/{1}/{2}/images/".format(client.client_name, plant.plant_name,
                                                                                 project.project_name))
                s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                              Key="media/file_system/{0}/{1}/{2}/tasks/".format(client.client_name, plant.plant_name,
                                                                                project.project_name))
                s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                              Key="media/file_system/{0}/{1}/{2}/milestones/".format(client.client_name,
                                                                                     plant.plant_name,
                                                                                     project.project_name))
                s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                              Key="media/file_system/{0}/{1}/{2}/disbursements/".format(client.client_name,
                                                                                        plant.plant_name,
                                                                                        project.project_name))
                s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                              Key="media/file_system/{0}/{1}/{2}/utilisations/".format(client.client_name,
                                                                                       plant.plant_name,
                                                                                       project.project_name))
        projects = client.get_all_standalone_projects()
        for project in projects:
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                          Key="media/file_system/{0}/{1}/".format(client.client_name,
                                                                  project.project_name))
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                          Key="media/file_system/{0}/{1}/images/".format(client.client_name,
                                                                         project.project_name))
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                          Key="media/file_system/{0}/{1}/tasks/".format(client.client_name,
                                                                        project.project_name))
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                          Key="media/file_system/{0}/{1}/milestones/".format(client.client_name,
                                                                             project.project_name))
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                          Key="media/file_system/{0}/{1}/disbursements/".format(client.client_name,
                                                                                project.project_name))
            s3.put_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                          Key="media/file_system/{0}/{1}/utilisations/".format(client.client_name,
                                                                               project.project_name))


def copy_all_folders():
    import boto3
    from v2.client.models import ClientDocument, ClientImage
    from v2.plant.models import PlantDocument, PlantImage
    from v2.project.models import ProjectDocument, ProjectImage, TaskDocument, UtilisationDocument, \
        DisbursementDocument, MilestoneDocument
    from v2.client.models import Client
    clients = Client.objects.all()
    for client in clients:
        # Saving client documents
        client_documents = ClientDocument.objects.filter(client=client)
        for document in client_documents:
            try:
                file = document.document_file.file
                document.document_file.save(
                    'file_system/{0}/{1}'.format(client.client_name, document.document_file.name),
                    content=file)
            except OSError:
                document.delete()
                continue
            document.save()
        # Saving client images
        client_images = ClientImage.objects.filter(client=client)
        for image in client_images:
            try:
                file = image.image_file.file
                image.image_file.save('file_system/{0}/images/{1}'.format(client.client_name, image.image_file.name),
                                      content=file)
                image.save()
            except OSError:
                image.delete()
                continue
            image.save()
        task_documents = TaskDocument.objects.filter(task__client=client, task__project=None)
        for document in task_documents:
            try:
                file = document.document_file.file
                document.document_file.save(
                    'file_system/{0}/tasks/{1}'.format(client.client_name, document.document_file.name),
                    content=file)
                document.save()
            except OSError:
                document.delete()
                continue
            document.save()
        plants = client.get_all_plants()
        for plant in plants:
            # Saving plant documents
            plant_documents = PlantDocument.objects.filter(plant=plant)
            for document in plant_documents:
                try:
                    file = document.document_file.file
                    document.document_file.save(
                        'file_system/{0}/{1}/{2}'.format(client.client_name, plant.plant_name,
                                                         document.document_file.name),
                        content=file)
                except OSError:
                    document.delete()
                    continue
                document.save()
            # Saving plant images
            plant_images = PlantImage.objects.filter(plant=plant)
            for image in plant_images:
                try:
                    file = image.image_file.file
                    image.image_file.save('file_system/{0}/{1}/images/{2}'.format(client.client_name, plant.plant_name,
                                                                                  image.image_file.name), content=file)
                    image.save()
                except OSError:
                    image.delete()
                    continue
                image.save()
            projects = plant.get_projects()
            for project in projects:
                project_documents = ProjectDocument.objects.filter(project=project)
                for document in project_documents:
                    try:
                        file = document.document_file.file
                        document.document_file.save(
                            'file_system/{0}/{1}/{2}/{3}'.format(client.client_name, plant.plant_name,
                                                                 project.project_name,
                                                                 document.document_file.name),
                            content=file)
                    except OSError:
                        document.delete()
                        continue
                    document.save()
                # Saving plant images
                project_images = ProjectImage.objects.filter(project=project)
                for image in project_images:
                    try:
                        file = image.image_file.file
                        image.image_file.save(
                            'file_system/{0}/{1}/{2}/images/{3}'.format(client.client_name, plant.plant_name,
                                                                        project.project_name, image.image_file.name),
                            content=file)
                    except OSError:
                        image.delete()
                        continue
                    image.save()
                project_tasks = TaskDocument.objects.filter(task__project=project)
                for document in project_tasks:
                    try:
                        file = document.document_file.file
                        document.document_file.save(
                            'file_system/{0}/{1}/{2}/tasks/{3}'.format(client.client_name, plant.plant_name,
                                                                       project.project_name,
                                                                       document.document_file.name),
                            content=file)
                    except OSError:
                        document.delete()
                        continue
                    document.save()
                milestone_documents = MilestoneDocument.objects.filter(milestone__project=project)
                for document in milestone_documents:
                    try:
                        file = document.document_file.file
                        document.document_file.save(
                            'file_system/{0}/{1}/{2}/milestones/{3}'.format(client.client_name, plant.plant_name,
                                                                            project.project_name,
                                                                            document.document_file.name),
                            content=file)
                    except OSError:
                        document.delete()
                        continue
                    document.save()
                disbursement_documents = DisbursementDocument.objects.filter(disbursement__project=project)
                for document in disbursement_documents:
                    try:
                        file = document.document_file.file
                        document.document_file.save(
                            'file_system/{0}/{1}/{2}/disbursements/{3}'.format(client.client_name, plant.plant_name,
                                                                               project.project_name,
                                                                               document.document_file.name),
                            content=file)
                    except OSError:
                        document.delete()
                        continue
                    document.save()
                utilisation_documents = UtilisationDocument.objects.filter(utilisation__project=project)
                for document in utilisation_documents:
                    try:
                        file = document.document_file.file
                        document.document_file.save(
                            'file_system/{0}/{1}/{2}/utilisations/{3}'.format(client.client_name, plant.plant_name,
                                                                              project.project_name,
                                                                              document.document_file.name),
                            content=file)
                    except OSError:
                        document.delete()
                        continue
                    document.save()
        projects = client.get_all_standalone_projects()
        for project in projects:
            project_documents = ProjectDocument.objects.filter(project=project)
            for document in project_documents:
                try:
                    file = document.document_file.file
                    document.document_file.save(
                        'file_system/{0}/{1}/{2}'.format(client.client_name, project.project_name,
                                                         document.document_file.name),
                        content=file)
                except OSError:
                    document.delete()
                    continue
                document.save()
            # Saving plant images
            project_images = ProjectImage.objects.filter(project=project)
            for image in project_images:
                try:
                    file = image.image_file.file
                    image.image_file.save(
                        'file_system/{0}/{1}/images/{2}'.format(client.client_name,
                                                                project.project_name, image.image_file.name),
                        content=file)
                except OSError:
                    image.delete()
                    continue
                image.save()
            project_tasks = TaskDocument.objects.filter(task__project=project)
            for document in project_tasks:
                try:
                    file = document.document_file.file
                    document.document_file.save(
                        'file_system/{0}/{1}/tasks/{2}'.format(client.client_name,
                                                               project.project_name, document.document_file.name),
                        content=file)
                except OSError:
                    document.delete()
                    continue
                document.save()
            milestone_documents = MilestoneDocument.objects.filter(milestone__project=project)
            for document in milestone_documents:
                try:
                    file = document.document_file.file
                    document.document_file.save(
                        'file_system/{0}/{1}/milestones/{2}'.format(client.client_name,
                                                                    project.project_name,
                                                                    document.document_file.name),
                        content=file)
                except OSError:
                    document.delete()
                    continue
                document.save()
            disbursement_documents = DisbursementDocument.objects.filter(disbursement__project=project)
            for document in disbursement_documents:
                try:
                    file = document.document_file.file
                    document.document_file.save(
                        'file_system/{0}/{1}/disbursements/{2}'.format(client.client_name,
                                                                       project.project_name,
                                                                       document.document_file.name),
                        content=file)
                except OSError:
                    document.delete()
                    continue
                document.save()
            utilisation_documents = UtilisationDocument.objects.filter(utilisation__project=project)
            for document in utilisation_documents:
                try:
                    file = document.document_file.file
                    document.document_file.save(
                        'file_system/{0}/{1}/utilisations/{2}'.format(client.client_name,
                                                                      project.project_name,
                                                                      document.document_file.name),
                        content=file)
                except OSError:
                    document.delete()
                    continue
                document.save()


def copy_client_doc_tags():
    from v2.client.models import ClientDocument
    docs = ClientDocument.objects.all()
    for doc in docs:
        doc.document_tag = []
        doc.save()


def save_blank_tags():
    from v2.client.models import ClientDocument
    docs = ClientDocument.objects.all()
    for doc in docs:
        if doc.document_tag == '' or not doc.document_tag:
            doc.document_tag = []
            doc.save()


def save_blank_tags_image():
    from v2.client.models import ClientImage
    from v2.plant.models import PlantImage
    from v2.project.models import ProjectImage
    docs = ClientImage.objects.all()
    for doc in docs:
        if doc.document_tag == '' or not doc.document_tag:
            doc.document_tag = []
            doc.save()
    docs = PlantImage.objects.all()
    for doc in docs:
        if doc.document_tag == '' or not doc.document_tag:
            doc.document_tag = []
            doc.save()
    docs = ProjectImage.objects.all()
    for doc in docs:
        if doc.document_tag == '' or not doc.document_tag:
            doc.document_tag = []
            doc.save()


def save_as_json():
    from v2.plant.models import PlantDocument
    from v2.project.models import ProjectDocument, DisbursementDocument, UtilisationDocument, TaskDocument, \
        MilestoneDocument
    docs = PlantDocument.objects.all()
    for doc in docs:
        doc.document_tag = []
        doc.save()
    docs = ProjectDocument.objects.all()
    for doc in docs:
        doc.document_tag = []
        doc.save()
    docs = DisbursementDocument.objects.all()
    for doc in docs:
        doc.document_tag = []
        doc.save()
    docs = UtilisationDocument.objects.all()
    for doc in docs:
        doc.document_tag = []
        doc.save()
    docs = TaskDocument.objects.all()
    for doc in docs:
        doc.document_tag = []
        doc.save()
    docs = MilestoneDocument.objects.all()
    for doc in docs:
        doc.document_tag = []
        doc.save()


def fix_project_images():
    import boto3
    from v2.project.models import ProjectImage
    from v2.client.models import Client
    clients = Client.objects.all()
    for client in clients:
        plants = client.get_all_plants()
        for plant in plants:
            projects = plant.get_projects()
            for project in projects:
                # Saving plant images
                project_images = ProjectImage.objects.filter(project=project)
                for image in project_images:
                    try:
                        # file = image.image_file.file
                        file_name = image.image_file.name.split('/')[-1]
                        s3 = boto3.resource('s3')
                        obj = s3.Object('sattva-staging', 'media/' + file_name)
                        body = obj.get()['Body'].read()
                        image.image_file.save(
                            'file_system/{0}/{1}/{2}/images/{3}'.format(client.client_name, plant.plant_name,
                                                                        project.project_name,
                                                                        file_name),
                            content=body)
                    except OSError:
                        image.delete()
                        continue
                    except Exception as ex:
                        print(ex)
                    image.save()
        projects = client.get_all_standalone_projects()
        for project in projects:
            project_images = ProjectImage.objects.filter(project=project)
            for image in project_images:
                try:
                    file = image.image_file.file
                    file_name = image.image_file.name.split('/')[-1]
                    s3 = boto3.resource('s3')
                    obj = s3.Object('sattva-staging', 'media/' + file_name)
                    body = obj.get()['Body'].read()
                    image.image_file.save(
                        'file_system/{0}/{1}/{2}/images/{3}'.format(client.client_name, plant.plant_name,
                                                                    project.project_name,
                                                                    file_name),
                        content=body)
                except OSError:
                    image.delete()
                    continue
                except Exception as ex:
                    print(ex)
                image.save()
