from rest_framework.authentication import BaseAuthentication


class KeycloakUserUpdateAuthentication(BaseAuthentication):
    """
    All authentication classes should extend BaseAuthentication.
    """

    def authenticate(self, request):
        """
        Authenticate the request and return a two-tuple of (user, token).
        """
        request.user = request._request.user
        return request.user, None
