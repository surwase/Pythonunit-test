from django.conf.urls import url
from django.urls import include
from rest_framework_bulk.routes import BulkRouter

from .views import *

app_name = "plant"

bulk_router = BulkRouter()
bulk_router.register(r'^plant_schedule_vii_activities', PlantScheduleVIIActivityViewSet,
                     'plant_schedule_vii_activities')

urlpatterns = [
    # url(r'^export/(?P<company_id>\d+)/$', CompanyExportAPIView.as_view({'get': 'list', 'post': 'create'})),
    url(r'^$', PlantModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='plant'),
    url(r'^project_indicator/(?P<pk>\d+)/$', PlantIndicatorViewSet.as_view(
        {'get': 'retrieve'}),
        name='plant_indicator'),
    url(r'^detail/(?P<pk>\d+)/$', PlantDetailViewSet.as_view(
        {'get': 'retrieve'}),
        name='plant_details'),
    url(r'^(?P<pk>\d+)/$', PlantModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='plant'),
    url(r'^projects/(?P<pk>\d+)/$', PlantModelProjectViewSet.as_view(
        {'get': 'retrieve'}),
        name='client_project_list'),
    url(r'^images/$', PlantImageModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='plant_doc'),
    url(r'images/^(?P<pk>\d+)/$', PlantImageModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='plant_doc'),
    url(r'^document/$', PlantDocumentModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='plant_doc'),
    url(r'^document/(?P<pk>\d+)/$', PlantDocumentModelViewSet.as_view(
            {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
            name='project'),
    url(r'^plant_user/$', PlantUserModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='plant_user'),
    url(r'plant_user/^(?P<pk>\d+)/$', PlantUserModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='plant_user'),
    url(r'^delete_plant_user/$', DeleteUserPlantAPIView.as_view(),
        name='delete_plant_user'),
    url(r'^dashboard/$', PlantDashboardModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='plant_dashboard'),
    url(r'^dashboard/(?P<pk>\d+)/$', PlantDashboardModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='plant_dashboard'),
    url(r'^', include(bulk_router.urls)),
    url(r'^images/download/$', PlantImageDownload.as_view(),
            name='plant_files'),
]
