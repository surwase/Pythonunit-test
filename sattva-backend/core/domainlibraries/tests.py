from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from core.users.models import User
from django.contrib.auth.models import Group, Permission


class TestFocusArea(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='john', password='johnpassword', email='miketyson@gmail.com',
                                             first_name='Mike', last_name='Tyson', name='Mike Tyson')
        self.group = Group.objects.create(name='superadmin')
        self.user.save()
        self.group.save()
        self.group.user_set.add(self.user)
        self.permission = 'view_focusarea'
        self.perm = Permission.objects.get(codename=self.permission)
        self.group.permissions.add(self.perm)
        self.permission = 'add_focusarea'
        self.perm = Permission.objects.get(codename=self.permission)
        self.group.permissions.add(self.perm)
        self.permission = 'change_focusarea'
        self.perm = Permission.objects.get(codename=self.permission)
        self.group.permissions.add(self.perm)
        self.permission = 'delete_focusarea'
        self.perm = Permission.objects.get(codename=self.permission)
        self.group.permissions.add(self.perm)
        self.client.login(username='john', password='johnpassword')
        self.create_data = {'focus_area_name': 'TestFocusAreaName', 'created_date': '2018-05-31T14:21:05',
                            'created_by': 'saikatdey'}
        self.update_data = {'focus_area_name': 'TestFocusAreaName', 'created_date': '2018-05-31T14:21:05',
                            'created_by': 'deysaikat'}

    # Order of tests important for PK values = 1, 2, 3, 4

    def test_can_create_focus_area(self):
        post_url = reverse('domainlibraries:focusarea-list')
        response = self.client.post(post_url, self.create_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['focus_area_name'], 'TestFocusAreaName')

    def test_can_delete_focus_area(self):
        post_url = reverse('domainlibraries:focusarea-list')
        self.client.post(post_url, self.create_data, format='json')
        delete_url = reverse('domainlibraries:focusarea-detail', kwargs={'pk': 2})
        response = self.client.delete(delete_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_can_read_focus_area(self):
        post_url = reverse('domainlibraries:focusarea-list')
        get_url = post_url
        self.client.post(post_url, self.create_data, format='json')
        response = self.client.get(get_url)
        self.assertEqual(response.data[0]['focus_area_name'], 'TestFocusAreaName')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_update_focus_area(self):
        post_url = reverse('domainlibraries:focusarea-list')
        self.client.post(post_url, self.create_data, format='json')
        update_url = reverse('domainlibraries:focusarea-detail', kwargs={'pk': 4})
        response = self.client.put(update_url, self.update_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        pass
