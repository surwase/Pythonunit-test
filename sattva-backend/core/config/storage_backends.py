from storages.backends.s3boto3 import S3Boto3Storage
from django.conf import settings


class MediaStorage(S3Boto3Storage):
    location = 'media'
    default_acl = 'private'
    custom_domain = False
    file_overwrite = False
    querystring_auth = True
    region_name = settings.AWS_S3_REGION


class StaticStorage(S3Boto3Storage):
    location = 'media'
    default_acl = 'public-read'
    custom_domain = False
    file_overwrite = False
    querystring_auth = False
    region_name = settings.AWS_S3_REGION
