from django.conf.urls import url

from .views import *

app_name = "governance"

urlpatterns = [
    url(r'^$', GovernanceModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='governance'),
    url(r'^(?P<pk>\d+)/$', GovernanceModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='governance'),
    url(r'^download/$', GovernanceExcelView.as_view(),
                    name='governance'),
]
