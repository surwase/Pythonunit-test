# Generated by Django 2.0.5 on 2020-06-21 16:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0026_auto_20200621_1419'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clientimage',
            name='image_file',
            field=models.FileField(max_length=2047, upload_to=''),
        ),
    ]
