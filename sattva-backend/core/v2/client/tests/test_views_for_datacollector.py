import json
from django.http import request, response
from django.urls import reverse
from .test_setup import TestSetUpdatacollector
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test.client import RequestFactory
from rest_framework import test
from rest_framework import status
from core.users.models import User
from v2.client.models import Client, ClientDashboard, ClientDetail, ClientImage, ClientOverview, ClientSettings, ClientUser
from v2.client.views import CheckClientNameAPIView, CheckComplianceListAPIView, ClientDashboardModelViewSet, ClientDetailModelViewSet, ClientImageModelViewSet, ClientMiniModelViewSet, ClientModelViewSet, ClientMicroModelViewSet, ClientModelRiskStatusViewSet, ClientModelProjectStatusViewSet, \
    ClientModelProjectViewSet, ClientModelPlantViewSet, ClientModelPlantAndProjectViewSet, ClientModelDetailViewSet, ClientOverviewModelViewSet, ClientSettingsModelViewSet, ClientUserModelViewSet, ClientComplianceScheduleAPIView, UserClientsAPIView, UserUnAddedClientsAPIView, DeleteUserAPIView


class ClientViewSetTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()
        # Create objects
        self.client_obj = Client.objects.create(
            client_name='client test123',
            display_name='client test123',
            website_url='www.company1.com',
            client_cin='FJOF033HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency = 'Annually',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )

        self.data = {
            "client_name": "Test Client",
            "display_name": "Test Client",
            "website_url": 'www.company1.com',
            "client_cin": 'FJOF033HFJEF',
            "plant_field_name": 'Test_name',
            "license_type": 'Standard',
            "engagement_model": 'Standalone',
            "is_kobo_enabled": True,
            "compliance_frequency": 'Annually',
            "csr_user": 5,
            "csr_admin": 5,
            "ngo_user": 5,
            "data_collector": 5,
            "csr_watcher": 5
        }

        # urls
        self.client_url = reverse('v2:client:client')
        self.mini_list_url = reverse('v2:client:client_mini')
        self.detail_url = reverse('v2:client:client', kwargs={'pk': self.client_obj.id})
        self.client_detail_url = reverse('v2:client:client_micro_detail', kwargs={'pk': self.client_obj.id})
        self.client_risk_status_url = reverse('v2:client:client_risk_status', kwargs={'pk': self.client_obj.id})
        self.client_project_status_url = reverse('v2:client:client_project_status', kwargs={'pk': self.client_obj.id})
        self.client_project_url = reverse('v2:client:client_project_list', kwargs={'pk': self.client_obj.id})
        self.client_plant_url = reverse('v2:client:client_plant_list', kwargs={'pk': self.client_obj.id})
        self.client_plant_project_url = reverse('v2:client:client_plant_project_list', kwargs={'pk': self.client_obj.id})

    def test_client_list(self):
        request = self.factory.get(self.client_url)
        request.user = self.user
        view = ClientModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_client_mini_list(self):
        request = self.factory.get(self.mini_list_url)
        request.user = self.user
        view = ClientMiniModelViewSet.as_view({'get': 'list'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_client_detail(self):
        request = self.factory.get(self.detail_url)
        request.user = self.user
        view = ClientModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request, pk=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_client_get(self):
        request = self.factory.get(self.client_url, self.data, format='json')
        request.user = self.user
        view = ClientModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request, pk=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_client_put(self):
        request = self.factory.put(self.client_url, self.data, format='json')
        request.user = self.user
        view = ClientModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request, pk=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_client_patch(self):
        request = self.factory.patch(self.detail_url, self.data, format='json')
        request.user = self.user
        view = ClientModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request, pk=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_client_delete(self):
        request = self.factory.delete(self.detail_url, self.data, format='json')
        request.user = self.user
        view = ClientModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request, pk=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class ClientOverviewViewSetTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()
        self.overview = ClientOverview.objects.create(client=self.client_obj, vision='vision', mission='mission')
        self.data = {
            'client': self.client_obj.id,
            'vision': 'client vision',
            'mission': 'client mission'
        }
        self.overview_url = reverse('v2:client:client_overview')
        self.overview_detail_url = reverse('v2:client:client_overview', kwargs={'client': self.overview.id})

    def test_overview_url(self):
        request = self.factory.get(self.overview_url)
        request.user = self.user
        view = ClientOverviewModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_overview_create(self):
        request = self.factory.post(self.overview_url, self.data, format='json')
        request.user = self.user
        view = ClientOverviewModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request, client=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ClientDetailViewSetTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()

        self.detail = ClientDetail.objects.create(
            client=self.client_obj,
            contact_name="Person name",
            contact_email="person_email@example.com",
            primary_phone='57236403',
            alternative_number='1263871263',
            address='32, street 1',
            pin_code='283203',
            state='Uttar Pradesh',
            city='City'
        )

        self.data = {
            'client': self.client_obj.id,
            'contact_name': "Person name",
            'contact_email': "person_email@example.com",
            'primary_phone': '57236403',
            'alternative_number': '1263871263',
            'address': '32, street 1',
            'pin_code': '283203',
            'state': 'Uttar Pradesh',
            'city': 'City'
        }

        self.detail_url = reverse('v2:client:client_detail')
        self.client_detail_url = reverse('v2:client:client_detail', kwargs={'client': self.detail.id})

    def test_detail_url(self):
        request = self.factory.get(self.detail_url)
        request.user = self.user
        view = ClientDetailModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_detail_create(self):
        request = self.factory.post(self.detail_url, self.data, format='json')
        request.user = self.user
        view = ClientDetailModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ClientDashboardViewSetTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()

        self.dashboard = ClientDashboard.objects.create(
            client=Client.objects.get(display_name='client Test10'),
            provider=0,
            url='http://www.qa.shift.sattva.co.in/',
            dashboard_name='Dashboard 1',
            embedded_content='content',
            password='Password123'
        )

        self.data = {
            'client': self.client_obj.id,
            'provider': 0,
            'url': 'http://www.qa.shift.sattva.co.in/',
            'dashboard_name': 'Dashboard 2',
            'embedded_content': 'content121',
            'password': 'Password12323'
        }

        self.dashboard_url = reverse('v2:client:dashboard')
        self.dashboard_detail_url = reverse('v2:client:dashboard', kwargs={'pk': self.dashboard.id})

    def test_dashboard_url(self):
        request = self.factory.get(self.dashboard_url)
        request.user = self.user
        view = ClientDashboardModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_dashboard_create(self):
        request = self.factory.post(self.dashboard_url, self.data, format='json')
        request.user = self.user
        view = ClientDashboardModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['dashboard_name'], self.data['dashboard_name'])
        self.assertEqual(response.data['url'], self.data['url'])


class ClientSettingsViewSetTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()

        self.setting = ClientSettings.objects.update_or_create(
            client=Client.objects.get(display_name='client Test10'),
            defaults={
                "disbursement_workflow":True,
                "utilisation_workflow":False,
                "impact_workflow":True
            }
        )

        self.settings_url = reverse('v2:client:client_settings', kwargs={'client': self.client_obj.id})


class ClientUserViewSetTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()

        self.client_user = ClientUser.objects.create(client=self.client_obj, user=User.objects.get(email='diksha@test.com'))
        self.client_user_url = reverse('v2:client:client_user')
        self.mini_client_user_url = reverse('v2:client:mini_client_user')
        self.client_user_detail_url = reverse('v2:client:client_user', kwargs={'pk': self.user.id})

class DeleteUserAPIViewTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()
        self.deletUserApiUrl = reverse('v2:client:delete_user', kwargs={'user_id': self.user.id})

    # def test_delete_user_api_view(self):
    #     request = self.factory.post(self.deletUserApiUrl)
    #     request.user = self.user
    #     view = DeleteUserAPIView.as_view()
    #     response = view(request, user_id=self.user.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserUnAddedClientsAPIViewTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()
        self.userUnAddedClientsApiUrl = reverse('v2:client:user_un_add_client_api_view')

    def test_user_un_add_client_api_view(self):
        request = self.factory.get(self.userUnAddedClientsApiUrl)
        request.user = self.user
        view = UserUnAddedClientsAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class UserClientsAPIViewTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()
        self.userClientApiUrl = reverse('v2:client:user_client_api_view')

    # def test_user_client_api_view(self):
    #     request = self.factory.get(self.userClientApiUrl)
    #     request.user = self.user
    #     view = UserClientsAPIView.as_view()
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)

class ClientComplianceScheduleAPIViewTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()
        self.clientComplianceScheduleUrl = reverse('v2:client:check_compliance_schedule', kwargs={'client_id': self.client_obj.id})

    def test_check_compliance_schedule(self):
        request = self.factory.get(self.clientComplianceScheduleUrl)
        request.user = self.user
        view = ClientComplianceScheduleAPIView.as_view()
        response = view(request, client_id=self.client_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class CheckClientNameAPIViewTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()
        self.check_client_name = reverse('v2:client:check_client_name')

    def test_check_client_name_url(self):
        request = self.factory.get(self.check_client_name)
        request.user = self.user
        view = CheckClientNameAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CheckComplianceListAPIViewTestCase(TestSetUpdatacollector):
    def setUp(self):
        super().setUp()
        self.check_compliance_list = reverse('v2:client:check_compliance_list')

    def test_check_compliance_list_url(self):
        request = self.factory.get(self.check_compliance_list)
        request.user = self.user
        view = CheckComplianceListAPIView.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ClientImageModelViewSetTestCase(TestSetUpdatacollector):

    def setUp(self):
        super().setUp()
        self.image = ClientImage.objects.create(
            client=self.client_obj,
            image_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            image_size = 10,
            path ="xyz",
            document_tag = ['tag', 'tag2']
                    )

        self.data={
            "client":self.client_obj.id,
            "image_file ": SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "image_size" : 20,
            "path" :"xyz",
            "document_tag" : ['tag', 'tag2']
        }

        #url
        self.image_url = reverse('v2:client:client_images')

    def test_image_url(self):
        request = self.factory.get(self.image_url)
        request.user = self.user
        view = ClientImageModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    # def test_image_create(self):
    #     request = self.factory.post(self.image_url, self.data, format='json')
    #     request.user = self.sattvauser
    #     view = ClientImageModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['image_size'], self.data['image_size'])
    #     self.assertEqual(response.data['path'], self.data['path'])
