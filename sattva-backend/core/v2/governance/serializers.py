import json

from rest_framework import serializers
from rest_framework_bulk import BulkSerializerMixin, BulkListSerializer

from v2.client.serializers import ClientUserMiniSerializer
from v2.governance.models import Governance
from v2.project.serializers import ProjectMicroSerializer, ProjectMiniSerializer
from v2.user_management.serializers import UserMicroSerializer


"""
    This class will serialize Governance model.
"""
class GovernanceSerializer(serializers.ModelSerializer):
    governance_type_name = serializers.SerializerMethodField()
    status_name = serializers.SerializerMethodField()
    projects = serializers.SerializerMethodField()
    assignee = serializers.SerializerMethodField()

    def get_governance_type_name(self, obj):
        return obj.get_governance_type_display()

    def get_status_name(self, obj):
        return obj.get_status_display()

    def get_projects(self, obj):
        return ProjectMiniSerializer(obj.projects.all(), many=True).data if obj.projects.all() else []

    def get_assignee(self, obj):
        return ClientUserMiniSerializer(obj.assignee.all(), many=True).data if obj.assignee.all() else []

    class Meta:
        model = Governance
        fields = ['id', 'name', 'projects', 'description', 'governance_type', 'governance_type_name', 'status',
                  'status_name',
                  'governance_date', 'start_date', 'end_date', 'comments', 'location', 'is_milestone_selected',
                  'is_financial_selected', 'is_impact_selected', 'is_risk_selected', 'assignee']


"""
    This class will serialize Governance model for write operations.
"""
class GovernanceWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Governance
        fields = '__all__'
