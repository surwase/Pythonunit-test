# Generated by Django 2.2.16 on 2021-02-17 06:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0142_auto_20210113_1100'),
        ('risk_management', '0008_auto_20200113_1331'),
    ]

    operations = [
        migrations.AddField(
            model_name='riskentry',
            name='milestone',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='project.Milestone'),
        ),
    ]
