import os

from django.conf import settings
from django.core.mail import send_mail
from django.db import models
from django.contrib.postgres.fields import JSONField

# Create your models here.
from django.template import loader
from django.template.loader import get_template

from geolibraries.models import AuditFields
from v2.notification import notification_choices


"""
    This Model is used to store the notification.
"""
class Notification(AuditFields):
    CATEGORY_ALERT = 0
    CATEGORY_NOTIFICATION = 1
    CATEGORY_CHOICES = ((CATEGORY_ALERT, 'Alert'), (CATEGORY_NOTIFICATION, 'Notification'))
    CATEGORY_ALERT = 0
    text = models.TextField()
    title = models.TextField()
    notification_type = models.SmallIntegerField(choices=notification_choices.TYPE_CHOICES)
    notification_tag = models.CharField(max_length=255, null=True, blank=True)
    category = models.SmallIntegerField(choices=CATEGORY_CHOICES)
    action_user = models.ForeignKey(settings.AUTH_USER_MODEL, help_text="The user who performed the action",
                                    related_name='notifications_caused_by_user', null=True, blank=True,
                                    on_delete=models.SET_NULL)
    action_performed_on_user = models.ForeignKey(settings.AUTH_USER_MODEL,
                                                 help_text="The user on whom performed the action",
                                                 related_name='notifications_for_actions_on_user', null=True,
                                                 blank=True, on_delete=models.SET_NULL)
    action_params = JSONField(null=True)
    app_url = models.URLField(max_length=2047, null=True, default='')

    class Meta:
        db_table = "notification"

    @staticmethod
    def create(notification_type, category, action_user, action_performed_on_user, action_params, users_to_send_to,
               title, text, notification_tag="", email_users=[], app_url=''):
        """

        :param title: The title of the notification
        :type title: str
        :param text: The text of the notification
        :type text: str
        :param notification_type: See notification_choices.TYPE_CHOICES
        :type notification_type: int
        :param source: see Notification.CATEGORY_CHOICES
        :type source: int
        :param action_user: The user who performed the action
        :type action_user: django.contrib.auth.models.User
        :param action_performed_on_user: The user on whom the action was performed on
        :type action_performed_on_user: django.contrib.auth.models.User
        :param action_params: The action params, different for different types of notifications
        :type action_params: dict
        :type users_to_send_to: list[django.contrib.auth.models.User]
        :type users_to_send_to: list[django.contrib.auth.models.User]
        :rtype: Notification
        """
        row = Notification.objects.create(notification_type=notification_type, category=category,
                                          action_user=action_user,
                                          action_performed_on_user=action_performed_on_user,
                                          action_params=action_params, title=title, text=text,
                                          notification_tag=notification_tag, app_url=app_url)
        for user in users_to_send_to:
            NotificationSentLog.create(row, user)
        if len(email_users):
            try:
                login_url = os.environ.get('LOGIN_URL')
                mail_body_template = get_template('notification_mail.html')
                html_message = mail_body_template.render({
                    'notification_text': text,
                    'app_link': app_url
                })
                send_mail(
                    subject=title,
                    message=text,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    html_message=html_message,
                    recipient_list=email_users,
                    fail_silently=False
                )
            except Exception as ex:
                print(ex)
        return row

    @staticmethod
    def mark_notifications_as_read(notifications, for_users=None):
        if for_users:
            NotificationSentLog.objects.filter(pk__in=notifications, user__in=for_users).update(read=True)
        else:
            NotificationSentLog.objects.filter(pk__in=notifications).update(read=True)

    def has_notification_been_read_by_user(self, user):
        return NotificationSentLog.objects.filter(notification=self, user=user, read=True).exists()


"""
    This Model is used to store the notification sent logs.
"""
class NotificationSentLog(AuditFields):
    notification = models.ForeignKey(Notification, related_name='sent_logs', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    read = models.BooleanField(default=False)

    class Meta:
        db_table = "notification_sent_log"

    @staticmethod
    def create(notification, user):
        """
        :type notification: Notification
        :type user: django.contrib.auth.models.User
        :rtype: NotificationSentLog
        """
        return NotificationSentLog.objects.create(notification=notification, user=user, read=False)
