from itertools import repeat

program = {
        'Plant Name': 'Name of the program as created on Shift (A program is a grouping of projects having a common theme)',
        'Project Name': 'Name of the project created on Shift',
        'Start Date': 'Date when the Project began',
        'End Date': 'Date of Project completion',
        'Partner': '[OPTIONAL] Name of the NGO partner, which is implementing the project',
        'Tags': '[OPTIONAL] Label added by any user (to provide a further descriptor)',
        'Location': "The list of locations where the project is implemented (The minimum granularity should be city/district level, & separate multiple locations with a ';')",
        'Project Objective': 'Brief details of what the project aims to achieve',
}

milestone = {
        'Project Name': 'Name of the project created on Shift',
        'Milestone Name': "Name of the Milestone (a grouping of verious tasks that contribute towards a project's progress)",
        'Milestone Description': 'Milestone related details including steps to be taken, outcome expectations, etc.',
        'Planned Start Date': "Planned Date of beginning the milestone's tasks (to be added before start)",
        'Planned End Date': "Planned Date of completing the milestone's tasks (to be added before start)",
        'Actual Start Date': "[OPTIONAL] Actual Date of beginning the milestone's tasks (to be added after starting them)",
        'Actual End Date': "[OPTIONAL] Actual Date of completing the milestone's tasks (to be added after starting them)",
        'Tags': '[OPTIONAL] Label added by any user (to provide a further descriptor)',
        'Status': "Present status of the particular Milestone. Helps identify project status",
        'Location': '[OPTIONAL] Name of the location specific to this Milestone (must match exactly with any of the locations entered about the project)',
        'Sub-Focus Area': "[OPTIONAL] Name of the area specific to this Milestone (must match exactly with any of the 'sub-focus areas' linked with the project)",
        'Sub-Target Segment': "[OPTIONAL] Name of the target segment specific to this Milestone (must match exactly with any of the 'sub-target segments' linked with the project)",

}

task = {
        'Project Name': 'Name of the project created on Shift',
        'Milestone Name': "[OPTIONAL] Name of the Milestone (a grouping of verious tasks that contribute towards a project's progress)",
        'Task Name': 'Name of the Task (subset of a Milestone)',
        'Task Description': 'Task related details including steps to be taken, outcome expectations, etc.',
        'Planned Start Date': "Planned Date of beginning the milestone's tasks (to be added before start)",
        'Planned End Date': "Planned Date of completing the milestone's tasks (to be added before start)",
        'Priority': "Relative urgency / importance for the task's completion",
        'Status': "Present status of the particular Task (Helps identify project status)",
        'Actual Start Date': '[OPTIONAL] Actual Date of beginning the tasks (to be added after starting them)',
        'Actual End Date': '[OPTIONAL] Actual Date of completing the tasks (to be added after starting them)',
        'Tags': '[OPTIONAL] Label added by any user (to provide a further descriptor)',
        'Task Completion Status': "[OPTIONAL]  A percentage indicator of a particular Task's status",
        'Assignee User': '[OPTIONAL]  A User to whom this task is specifically assigned (the user must be granted access to the project beforehand)',
}


disbursement = {
        'Project Name': 'Name of the project created on Shift',
        'Disbursement Name': 'Name of the disbursement (usually repesented as Instalment  no. 1, 2 or 3 and so on)',
        'Milestone Name': "[OPTIONAL] Name of the milestone, created in the project plan section, that is linked to a disbursement (may not be applicable)",
        'Disbursement Description': 'Details of how the funds will be utilised',
        'Expected Date': 'Planned Date of receiving the payment (as per the MoU)  (yyyy-mm-dd)',
        'Request Date': 'Actual Date of requesting the payment from the donor (yyyy-mm-dd)',
        'Planned Amount': 'The amount of funds that were supposed to be disbursed by the donor (as per the MoU)',
        'Expense Category': 'The category of how the funds will be utilised. The categories standardised on Shift are - "Capex", "Opex", "Travel", "HR", "Misc".',
        'Start Date': 'Start date of the utilisation period (yyyy-mm-dd)',
        'End Date': 'End date of the utilisation period (yyyy-mm-dd)',
        'Fund Request Date': "[OPTIONAL] Date when the partner sent the 'Fund Request' to the Donor (yyyy-mm-dd)",
        'Approved Amount': 'The amount of funds actually approved by the donor (to be shared by the donor)',
        'Approval Date': 'Date of approval of the payment (to be shared by the donor) (yyyy-mm-dd)',
        'Disbursement Date': '[OPTIONAL] Date of release of the payment (to be shared by the donor) (yyyy-mm-dd)',
        'Tags': '[OPTIONAL] Label added by any user (to provide a further descriptor)',
        'Status': 'Present status of the particular disbursement. It is primilarly needed to track if funds have been released or not'

}

utilisation = {
    'Project Name': 'Name of the project as created on Shift',
    'Disbursement Name': 'Name of the disbursement (usually repesented as Instalment  no. 1, 2 or 3 and so on)',
    'Particulars': 'Name of the utilization (Unique cost head associated with the program)',
    'Description': 'Details of how the funds have been utilised ',
    'Total Planned Cost': '[OPTIONAL] Total cost of the "particular" planned for the utilization project',
    'Unit Cost': '[OPTIONAL] Break-up of expenses - Cost of each unit',
    'Number Of Units': '[OPTIONAL] Break-up of expenses - Total no. of units',
    'Start Date': 'Start date of the utilisation period (yyyy-mm-dd)',
    'End Date': 'End date of the utilisation period (yyyy-mm-dd)',
    'Total Estimate Cost': 'Total expenses estimated within the "period of utilisation"',
    'Total Actual Cost': 'Total expenses incurred within the "period of utilisation"',
    'Expense Category': '[OPTIONAL] The category of how the funds will be utilised. The categories standardised on Shift are - "Capex", "Opex", "Travel", "HR", "Misc".',
    'Tags': '[OPTIONAL] Label added by any user (to provide a further descriptor)'
}

project_risk = {
        'Category': 'Type of Risk anticipated in a project. If the project name is blank, then the risk is applicable at a portfolio level',
        'Severity': 'A relative indicator of the criticality of the risk (to be added based on understanding of the stakeholders) ',
        'Particular': 'Details of the Risk',
        'Mitigation Comment': 'The steps or action items decided by the stakeholders to address the risk',
        'Milestone Name': 'Name of the milestone, created in the project, that may get affected because of this risk'

}


program_risk = {
        'Project Name': 'Name of the project created on Shift',
        'Start Date': 'Start date of the project period (yyyy-mm-dd)',
        'End Date': 'End date of the project period (yyyy-mm-dd)',
        'Category': 'Type of Risk anticipated in a project. If the project name is blank, then the risk is applicable at a portfolio level',
        'Severity': 'A relative indicator of the criticality of the risk (to be added based on understanding of the stakeholders) ',
        'Particular': 'Details of the Risk',
        'Mitigation Comment': 'The steps or action items decided by the stakeholders to address the risk'

}

client_risk = {
        'Program': 'Name of the program as created on Shift',
        'Project Name': 'Name of the project created on Shift',
        'Start Date': 'Start date of the project period (yyyy-mm-dd)',
        'End Date': 'End date of the project period (yyyy-mm-dd)',
        'Category': 'Type of Risk anticipated in a project. If the project name is blank, then the risk is applicable at a portfolio level',
        'Severity': 'A relative indicator of the criticality of the risk (to be added based on understanding of the stakeholders) ',
        'Particular': 'Details of the Risk',
        'Mitigation Comment': 'The steps or action items decided by the stakeholders to address the risk'

}


all_task = {
        'Project Name': 'Name of the project created on Shift',
        'Milestone Name': "[OPTIONAL] Name of the Milestone (a grouping of verious tasks that contribute towards a project's progress)",
        'Task Name': "Name of the Task (subset of a Milestone)",
        'Task Description': 'Task related details including steps to be taken, outcome expectations, etc.',
        'Planned Start Date': "Planned Date of beginning the milestone's tasks (to be added before start) (yyyy-mm-dd)",
        'Planned End Date': "Planned Date of completing the milestone's tasks (to be added before start) (yyyy-mm-dd)",
        'Actual Start Date': "[OPTIONAL] Actual Date of beginning the milestone's tasks (to be added after starting them) (yyyy-mm-dd)",
        'Actual End Date': "[OPTIONAL] Actual Date of completing the milestone's tasks (to be added after starting them) (yyyy-mm-dd)",
        'Tags': '[OPTIONAL] Label added by any user (to provide a further descriptor)',
        'Priority': "Relative urgency / importance for the task's completion",
        'status': "Present status of the particular Task. Helps identify project status",
        'Task Completion Status': "[OPTIONAL]  A percentage indicator of a particular Task's status"
}


governance = {
        'Identifier': 'Name of the Governance item',
        'Description': 'Governance related details including steps to be taken, expectations, etc.',
        'Governance Type': "The category of how the governance will be conducted ('Client Call', 'Client Visit', 'Partner Call', 'Partner Visit')",
        'Governance Date': 'Date of conducting the governance (yyyy-mm-dd)',
        'Project': 'Name of the project created on Shift',
        'Start Date': 'Date of beginning the governance period (yyyy-mm-dd)',
        'End Date': 'Date of completing the governance period (yyyy-mm-dd)',
        'Location': '[OPTIONAL] Name of the location specific to this governance (must match exactly with any other locations listed in the project)',
        'Status': "Present status of the particular governance item ('Open', 'Pending', 'Closed', ''Cancelled)",
        'Comments': '[OPTIONAL] Comments added about the governance, along with the username and date'

}

location = {
    'Program Name': 'Name of the program as created on Shift (A program is a grouping of projects having a common theme)',
    'Project Name': 'Name of the project created on Shift',
    'Country': 'Location of the programs (country-level granularity)',
    'State': 'Location of the programs (state-level granularity)',
    'District': 'Location of the programs (district-level granularity)',
    'City': 'Location of the programs (city-level granularity)'
}

indicator = {
    'Indicator': 'Name of the Output measured',
    'Milestone Name': '[OPTIONAL] Name of the milestone, created in the project plan section, that could be linked to this Impact Indicator',
    'Beneficiary': 'A toggle to highlight whether the indicator is a measure of the number of positively impacted beneficiaries',
    'Output Indicator': 'Name of the Output being measured',
    'Planned Output': 'Quantity of output planned to achieve',
    'Actual Output': 'Quantity of output actually achieved',
    'Outcome Indicator': 'Name of the Outcome being measured',
    'Planned outcome': 'Quantity of outcome planned to achieve',
    'Actual Outcome': 'Quantity of outcome actually achieved',
    'Short Term Outcome': '[OPTIONAL] Description of a qualitative outcome in the short term',
    'Long Term Outcome': '[OPTIONAL] Description of a qualitative outcome in the long term'

}



def get_label_to_excel(objects, member, comment, title):
    # field_description = {}
    lables_list = []
    comment_lable = f'[OPTIONAL] Comments added about the {title}, along with the username and date'
    member_lable = f'[OPTIONAL] Name of the users who must be assigned access to the {title}'
    if objects:
        if title == 'Program':
            for i in objects:
                lables_list.append(program.get(i))
        elif title == 'Milestone':
            for i in objects:
                lables_list.append(milestone.get(i))
        elif title == 'Task':
            for i in objects:
                lables_list.append(task.get(i))
        elif title == 'Disbursement':
            for i in objects:
                lables_list.append(disbursement.get(i))
        elif title == 'Utilisation':
            for i in objects:
                lables_list.append(utilisation.get(i))
        elif title == 'All Task':
            for i in objects:
                lables_list.append(all_task.get(i))
        elif title == 'Project Risk':
            for i in objects:
                lables_list.append(project_risk.get(i))
        elif title == 'Program Risk':
            for i in objects:
                lables_list.append(program_risk.get(i))
        elif title == 'Client Risk':
            for i in objects:
                lables_list.append(client_risk.get(i))
        elif title == 'Location':
            for i in objects:
                lables_list.append(location.get(i))
        elif title == 'Governance':
            for i in objects:
                lables_list.append(governance.get(i))
        elif title == 'Indicator':
            for i in objects:
                lables_list.append(indicator.get(i))
        # else:
        #     for i in objects:
        #         lables_list.append(field_description.get(i))
    if len(member)>1:
        lables_list.extend(repeat(member_lable, len(max(member.values(), key=len))))
    if comment:
        lables_list.extend(repeat(comment_lable, 10))
    return lables_list
