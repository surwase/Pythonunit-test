# Generated by Django 2.0.5 on 2019-12-02 02:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0029_auto_20191201_1425'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='template',
            unique_together={('project', 'template_name')},
        ),
    ]
