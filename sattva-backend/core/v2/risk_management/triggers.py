from v2.project.models import Project
from v2.risk_management.models import Risk, RiskEntry, RiskTemplate
from django.template import Template, Context

def project_utilization_crossed_planned_amount():
    """
    [Utilization Identifier] actual total amount > Planned amount
    """
    trigger = 'project_utilization_amount_cross_the_planned_amount'
    print(trigger)
    risk = Risk.get_risk_from_trigger(trigger)
    if risk:
        project_list = Project.objects.all()
        for project in project_list:
            risk_entries = RiskEntry.get_opened_risk_entry_for_project(trigger=trigger, project=project)
            risk_template_content = RiskTemplate.get_template_from_risk(risk)
            if project.get_all_disbursement_total_amount() > project.get_total_utilised_amount():
                if not risk_entries:
                    if risk_template_content:
                        custom_context = {"project_name": project.project_name}
                        custom_context.update({"utillzation_name":"Utiliazation" })
                        content = Template(risk_template_content.content).render(Context(custom_context))
                        risk_entry, _ = RiskEntry.create(name=risk.name, risk=risk, status=0, severity=1,
                                                         risk_type=0, project=project, content=content, category=risk.category)
            else:
                if risk_entries:
                    risk_entries.set_closed()


def project_total_distribution_utilization_crossed_total_distribution():
    """
    Total Utilization Amount of a disbursement > associated disbursement amount
    """
    trigger = 'total_utilization_amount_of_a_disbursements_crosses_associated_disbursement_amount'
    print(trigger)
    risk = Risk.get_risk_from_trigger(trigger)
    if risk:
        project_list = Project.objects.all()
        for project in project_list:
            risk_entries = RiskEntry.get_opened_risk_entry_for_project(trigger=trigger, project=project)
            risk_template_content = RiskTemplate.get_template_from_risk(risk)
            if project.get_total_utilised_amount() < project.get_all_disbursement_total_amount():
                if not risk_entries:
                    if risk_template_content:
                        custom_context = {"project_name": project.project_name}
                        custom_context.update({"disbursement_name":"Utiliazation" })
                        content = Template(risk_template_content.content).render(Context(custom_context))
                        risk_entry, _ = RiskEntry.create(name=risk.name, risk=risk, status=0, severity=1,
                                                         risk_type=0, project=project, content=content, category=risk.category)
            else:
                if risk_entries:
                    risk_entries.set_closed()





