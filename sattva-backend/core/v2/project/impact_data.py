import csv
import datetime
import itertools
from collections import defaultdict
from io import StringIO
from operator import itemgetter
import dateutil
from django.core.files.storage import default_storage
from django.db import connection
from v2.project.models import TemplateAttribute, Template


def get_parent_table_name(client_id, project_id, template_id):
    table_name = 'C' + str(client_id) + '_' + 'P' + str(project_id) + '_' \
                 + 'T' + str(template_id)
    return table_name


def alter_column_in_table(table_name, old_name, new_name):
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            cursor.execute('ALTER TABLE ' + table_name + ' RENAME COLUMN "' + old_name + '" TO "' + new_name + '";')
            altered = True
        else:
            altered = False
    return altered


def check_if_table_exists(table_name):
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            return True
        else:
            return False
    return False


def create_column_in_table(table_name, column_name, data_type):
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            if data_type == 'varchar':
                sql = 'ALTER TABLE ' + table_name + ' ADD COLUMN "' + column_name + '" ' + data_type + '(100);'
            else:
                sql = 'ALTER TABLE ' + table_name + ' ADD COLUMN "' + column_name + '" ' + data_type + ';'
            response = cursor.execute(sql)
            created = True
        else:
            created = False
    return create


def delete_column_in_table(table_name, column_name):
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" + str(
            table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            print(table_name)
            print(column_name)
            print('ALTER TABLE ' + table_name + ' DROP COLUMN "' + column_name + '";')
            response = cursor.execute('ALTER TABLE ' + table_name + ' DROP COLUMN "' + column_name + '";')
            dropped = True
        else:
            dropped = False
    return dropped


def create_parent_table_definition(client_id, project_id, template_id):
    attribute_list = TemplateAttribute.objects.filter(template__id=template_id).order_by('attribute_order', 'id')
    parent_table_name = get_parent_table_name(client_id, project_id, template_id)
    table_definition = 'CREATE TABLE IF NOT EXISTS ' + parent_table_name + ' ('
    table_definition += 'id serial, ' \
                        'batch_id bigint, ' \
                        'created_date timestamp default current_timestamp, ' \
                        'deleted boolean default false, '
    column_list = []
    for attribute in attribute_list:
        col_name = attribute.attribute_name.strip()
        data_type = attribute.attribute_type.data_type
        data_length = 100
        if data_type == 'varchar':
            table_definition += '"' + col_name + '" ' + data_type + '(' + str(data_length) + '),'
            column_list.append(col_name)
        else:
            table_definition += '"' + col_name + '" ' + data_type + ','
            column_list.append(col_name)
    table_definition = table_definition[:-1] + ')'
    return table_definition, column_list


def create_parent_table(client_id, project_id, template_id):
    parent_table_name = get_parent_table_name(client_id, project_id, template_id)
    with connection.cursor() as cursor:
        table_exists_sql = "SELECT 1 from information_schema.tables WHERE table_name = '" \
                           + str(parent_table_name).lower() + "'"
        cursor.execute(table_exists_sql)
        value = 0
        for row in cursor.fetchall():
            value = row[0]
        if value == 1:
            sql, column_list = create_parent_table_definition(client_id, project_id, template_id)
        else:
            sql, column_list = create_parent_table_definition(client_id, project_id, template_id)
            cursor.execute(sql)
    return column_list


def read_csv_header_column_list(file):
    with default_storage.open(file) as csvfile:
        fl = csvfile.read().decode("utf-8")
        f = StringIO(fl)
        reader = csv.reader(f, delimiter=',')
        header = next(reader)
    return header


def confirm_csv_header_vs_model_correctness(column_list, header_list):
    diff_in_cols = []
    matched = True
    for i, (col_name, csv_name) in enumerate(zip(column_list, header_list)):
        if col_name.strip().lower() == csv_name.strip().lower():
            diff_in_cols.append(col_name.strip() + ',' + csv_name.strip() + ',Y')
        else:
            matched = False
            diff_in_cols.append(col_name.strip() + ',' + csv_name.strip() + ',N')
    if len(column_list) == len(header_list):
        diff_in_cols.append('DBColCount:' + str(len(column_list)) + ',CSVColCount:' + str(len(header_list)) + ',Y')
    else:
        diff_in_cols.append('DBColCount:' + str(len(column_list)) + ',CSVColCount:' + str(len(header_list)) + ',N')
        matched = False
    return diff_in_cols, matched


def boolean_validation(value_list, column):
    results = []
    for index, value in enumerate(value_list):
        if value.lower() not in ('true', 'false'):
            dictionary = {'index': index + 1,
                          'value': value,
                          'column': column,
                          'error': 'Value should be either TRUE or FALSE; Change - ' + value}
            results.append(dictionary)
    return results


def timestamp_validation(value_list, column):
    results = []
    for index, value in enumerate(value_list):
        try:
            datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
        except ValueError:
            try:
                datetime.datetime.strptime(value, '%Y-%m-%d')
            except ValueError:
                dictionary = {'index': index + 1,
                              'value': value,
                              'column': column,
                              'error': 'Incorrect Date format, should be YYYY-MM-DD or YYYY-MM-DD HH24:MI:SS; Change - ' + value}
                results.append(dictionary)
    return results


def varchar_validation(value_list, length, column):
    results = []
    for index, value in enumerate(value_list):
        if len(value) > length:
            dictionary = {'index': index + 1,
                          'value': value,
                          'column': column,
                          'error': 'Value should be less than ' + str(length) + ' characters; Change - ' + value}
            results.append(dictionary)
    return results


def numeric_validation(value_list, column):
    results = []
    for index, value in enumerate(value_list):
        try:
            if value is None:
                pass
            elif value == '':
                pass
            else:
                float(value)
        except ValueError:
            dictionary = {'index': index + 1,
                          'value': value,
                          'column': column,
                          'error': 'Value is not a number; Change - ' + value}
            results.append(dictionary)
    return results


def date_validation(value_list, column):
    results = []
    for index, value in enumerate(value_list):
        try:
            if value is None:
                pass
            elif value == '':
                pass
            else:
                dateutil.parser.parse(value, dayfirst=True)
        except ValueError:
            dictionary = {'index': index + 1,
                          'value': value,
                          'column': column,
                          'error': 'Incorrect Date format, should be YYYY-MM-DD; Change - ' + value}
            results.append(dictionary)
    return results


def db_column_details(table_name):
    col_details = []
    sql = """
    select column_name,
        case when data_type = 'character varying' then 'varchar'
            when data_type = 'integer' then 'numeric'
            when data_type = 'date' then 'date'
            when data_type = 'timestamp without time zone' then 'timestamp'
            when data_type = 'timestamp without time zone' then 'timestamp'
            when data_type = 'boolean' then 'boolean'
            when data_type = 'text' then 'varchar'
            else data_type end as data_type,
        case when data_type = 'character varying' then character_maximum_length
            when data_type = 'text' then '1000'
            when data_type = 'varchar' then '1000' end as length
    from information_schema.columns
    where table_name = '""" + table_name + """'
    and column_name not in ('id', 'batch_id', 'created_date', 'deleted')
    and lower(column_name) not like lower('OC % Date Captured')
    and lower(column_name) not like lower('OP % Date Captured')
    order by ordinal_position"""
    with connection.cursor() as cursor:
        cursor.execute(sql)
        results = cursor.fetchall()
    for row in results:
        dictionary = {'col_name': row[0],
                      'data_type': row[1],
                      'length': row[2]}
        col_details.append(dictionary)
    return col_details


def validate_data(table, file):
    column_count = 0
    columns = defaultdict(list)
    db_columns = db_column_details(table.lower())
    with default_storage.open(file) as f:
        fl = f.read().decode("utf-8")
        csvfile = StringIO(fl)
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            for (i, v) in enumerate(row):
                columns[i].append(v)
                column_count = i + 1
    column_headers = []
    for i in range(0, column_count):
        column_header = columns[i].pop(0)
        column_headers.append(column_header)
    temp_errors = []
    # error_results_all = defaultdict(dict)
    for (i, v) in enumerate(column_headers):
        for dictionary in db_columns:
            # print('Inside - ' + dictionary['col_name'])
            if v == dictionary['col_name']:
                error_results = []
                if dictionary['data_type'] == 'varchar':
                    length = dictionary['length']
                    error_results = varchar_validation(columns[i], length, v)
                    # print(columns[i])
                    # print('varchar_validation(columns[i], ' + str(length) + ', ' + v + ')')
                elif dictionary['data_type'] == 'numeric':
                    error_results = numeric_validation(columns[i], v)
                    # print(columns[i])
                    # print('numeric_validation(columns[i], ' + v + ')')
                elif dictionary['data_type'] == 'date':
                    error_results = date_validation(columns[i], v)
                elif dictionary['data_type'] == 'timestamp':
                    error_results = timestamp_validation(columns[i], v)
                elif dictionary['data_type'] == 'boolean':
                    error_results = boolean_validation(columns[i], v)
            else:
                error_results = []
            if error_results:
                temp_errors.append(error_results)
    dict_list = []
    for x in temp_errors:
        for v in x:
            dict_list.append(v)
    dict_list = sorted(dict_list, key=itemgetter('index'))
    outer_dictionary = {}
    dictionary_list = []
    for key, value in itertools.groupby(dict_list, key=itemgetter('index')):
        outer_dictionary.update({'Record': str(key)})
        inner_dictionary_list = []
        for i in value:
            inner_dictionary = {}
            inner_dictionary.update({i.get('column'): i.get('error')})
            inner_dictionary_list.append(inner_dictionary)
        outer_dictionary.update({'Details': inner_dictionary_list})
        dictionary_list.append(outer_dictionary.copy())
    return dictionary_list


def read_table_data(client_id, project_id, template_id):
    table_name = get_parent_table_name(client_id, project_id, template_id)
    query = 'select * from ' + table_name + ' where deleted = false order by 1, 2'
    try:
        with connection.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
            data_inserted = [dict(zip([key[0] for key in cursor.description], row)) for row in result]
            inserted_data = [{k: v for k, v in d.items() if k not in ('deleted', 'id')} for d in data_inserted]
        return data_inserted
    except:
        return []


def insert_data(file, column_list, client_id, project_id, template_id, batch_id):
    table_name = get_parent_table_name(client_id, project_id, template_id)
    db_columns = db_column_details(table_name.lower())
    print(db_columns)
    with default_storage.open(file) as f:
        fl = f.read().decode("utf-8")
        csvfile = StringIO(fl)
        reader = csv.reader(csvfile, delimiter=',')
        records = []
        row_count = 0
        for row in reader:
            # print(row)
            col_values = ''
            col_count = 0
            for col in row:
                if col == '':
                    col_values += "null, "
                elif row_count > 0 and db_columns[col_count].get('data_type') == 'date':
                    date_value = dateutil.parser.parse(col, dayfirst=True)
                    date_value = datetime.datetime.strftime(date_value, '%Y-%m-%d')
                    col_values += "'" + date_value + "', "
                else:
                    col_values += "'" + col + "', "
                col_count = col_count + 1
            col_values = col_values[:-2]
            row_item = col_values
            records.append(row_item)
            row_count += 1
    records.pop(0)
    # new_column_list = column_list.split(",")
    new_column_list = column_list
    # new_column_list.insert(0, 'batch_id')
    values_sql_list = ''
    columns = '", "'.join(new_column_list)
    columns = '"' + columns + '"'
    insert_sql = 'insert into ' + table_name + '(' + columns + ') values '
    for record in records:
        values_sql_list += '(' + record + '), '
    insert_sql += values_sql_list[:-2]
    query = 'select * from ' + table_name + ' where deleted = false order by 1, 2'
    try:
        with connection.cursor() as cursor:
            cursor.execute(insert_sql)
            recnum = cursor.rowcount
            cursor.execute(query)
            result = cursor.fetchall()
            data_inserted = [dict(zip([key[0] for key in cursor.description], row)) for row in result]
            inserted_data = [{k: v for k, v in d.items() if k not in ('deleted', 'id')} for d in data_inserted]
    except Exception as error:
        return -1, -1, -1, str(error)
    return recnum, inserted_data, -1, {}


def update_batch_id_for_all_tables():
    templates = Template.objects.all()
    for template in templates:
        if not template.project:
            print(table_name)
            print('no table')
            continue
        table_name = get_parent_table_name(template.project.client.id, template.project.id, template.id)
        if check_if_table_exists(table_name):
            sql = 'update ' + table_name + ' SET batch_id=1'
            try:
                with connection.cursor() as cursor:
                    cursor.execute(sql)
                    print('executed')
                    print(table_name)
                    value = 0
                    for row in cursor.fetchall():
                        value = row[0]
                    if value == 1:
                        print(table_name)
                        print('done')
                    else:
                        print(table_name)
                        print('not done')
            except Exception as error:
                print(error)
                print(table_name)
                print('not done')


def get_new_batch_id_for_table(table_name):
    if check_if_table_exists(table_name):
        sql_query = 'select max(batch_id) from ' + table_name
        try:
            with connection.cursor() as cursor:
                cursor.execute(sql_query)
                value = -1
                for row in cursor.fetchall():
                    print(row)
                    value = row[0]
                    if value:
                        return value + 1
                    else:
                        return 1
        except Exception as error:
            print(error)
            return -1
    return 1


def upload_data(client_id, project_id, template_id, file, batch_id=None):
    if not batch_id:
        batch_id = get_new_batch_id_for_table
    column_list = create_parent_table(client_id, project_id, template_id)
    header_list = read_csv_header_column_list(file)
    diff_in_expected_cols, matched = confirm_csv_header_vs_model_correctness(column_list, header_list)
    if not matched:
        return -1, -1, -1, {'Failed': True, 'SchemaMismatch': True, 'SchemaDiff': diff_in_expected_cols}
    table = get_parent_table_name(client_id, project_id, template_id)
    error_list = validate_data(table, file)
    if error_list:
        return -1, -1, -1, {'Failed': True, 'DataValidationIssue': True, 'AllErrors': error_list}
    batch_id, response, data_inserted, errors = insert_data(file, column_list, client_id, project_id, template_id, batch_id)
    if batch_id == -1:
        return batch_id, response, data_inserted, {'Failed': True, 'DataIssue': True, 'FirstError': errors}
    else:
        return batch_id, response, data_inserted, {'Failed': False}
