from rest_framework.permissions import BasePermission

from v2.client.permissions import BaseClientPermission, check_if_user_has_client_feature_permission
from v2.plant.models import PlantUser, Plant

"""
    permission for plant
"""
class PlantPermission(BaseClientPermission):
    feature = 'PROGRAM_OVERVIEW'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        client = request.GET.get('client')
        plant = request.GET.get('plant')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and (client or plant):
            return True
        elif view.action == 'list':
            return False
        elif request.method == 'POST':
            client = request.data.get('client')
            return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name == 'sattva-admin':
            return True
        if request.method in ('PATCH', 'PUT', 'POST'):
            return check_if_user_has_client_feature_permission(self.feature, 'Change', obj.client, request.user)
        if request.method == 'DELETE':
            return check_if_user_has_client_feature_permission(self.feature, 'Delete', obj.client, request.user)
        if request.method == 'GET':
            return PlantUser.objects.filter(user=request.user, plant=obj).exists()
        return False

"""
    permission for plant dashboard
"""
class PlantDashboardPermission(BasePermission):
    feature = 'PROGRAM_INSIGHTS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        plant = request.GET.get('plant')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and plant:
            return True
        elif view.action == 'list':
            return False
        elif request.method == 'POST':
            plant = request.data.get('plant')
            client = Plant.objects.get(id=plant).client
            return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
        return True

"""
    permission for plant document
"""
class PlantDocumentPermission(BasePermission):
    feature = 'PROGRAM_OVERVIEW'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        plant = request.GET.get('plant')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and plant:
            return True
        elif view.action == 'list':
            return False
        elif request.method == 'POST':
            plant = request.data.get('plant')
            client = Plant.objects.get(id=plant).client
            return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
        return True

"""
    permission for plant user
"""
class PlantUserPermission(BaseClientPermission):
    feature = 'MANAGE_USERS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        plant = request.GET.get('plant')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif view.action == 'list' and plant:
            return True
        elif view.action == 'list':
            return False
        elif request.method == 'POST':
            plant = request.data.get('plant')
            client = Plant.objects.get(id=plant).client
            return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
        return True

"""
    permission for delete plant user
"""
class DeletePlantUserPermission(BaseClientPermission):
    feature = 'MANAGE_USERS'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name == 'sattva-admin':
            return True
        elif request.method == 'DELETE':
            plant = request.GET.get('plant')
            client = Plant.objects.get(id=plant).client
            return check_if_user_has_client_feature_permission(self.feature, 'Change', client, request.user)
        return False
