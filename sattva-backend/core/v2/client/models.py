from django.contrib.postgres.fields import JSONField

from config.storage_backends import StaticStorage
from domainlibraries.models import ApplicationRole
from geolibraries.models import AuditFields, validate_file_extension
from django.db import models
from django.conf import settings

import v2.user_management as user_management
from v2.notification import settings_choices

COMPLIANCE_FREQUENCY = (
    ('Monthly', 'Monthly'), ('Quarterly', 'Quarterly'), ('Half-yearly', 'Half-yearly'), ('Annually', 'Annually'))


"""
   This model will be used to store the client.
"""
class Client(AuditFields):
    client_name = models.CharField(max_length=511, unique=True)
    display_name = models.CharField(max_length=511, default='')
    website_url = models.CharField(max_length=2047, null=True, blank=True)
    client_cin = models.CharField(max_length=63, null=True, blank=True)
    plant_field_name = models.CharField(max_length=255, null=True, blank=True)
    logo = models.FileField(null=True, blank=True, validators=[validate_file_extension], storage=StaticStorage())
    license_type = models.CharField(max_length=512, default='Standard')
    engagement_model = models.CharField(max_length=512, default='Standalone')
    evaluation_start_date = models.DateTimeField(null=True, blank=True)
    is_kobo_enabled = models.BooleanField(default=False)
    compliance_frequency = models.CharField(max_length=63, choices=COMPLIANCE_FREQUENCY, default='Monthly', blank=True)
    csr_user = models.IntegerField(default=5)
    csr_admin = models.IntegerField(default=5)
    ngo_user = models.IntegerField(default=5)
    data_collector = models.IntegerField(default=5)
    csr_watcher = models.IntegerField(default=5)

    def save(self, *args, **kwargs):
        new_object = True
        if self.pk:
            new_object = False
        super(Client, self).save(*args, **kwargs)
        if new_object:
            # Create client setting
            ClientSettings.objects.create(client=self)
            # create_client_feature_permission_set(self)
            features = user_management.models.ClientFeature.objects.all()
            for feature in features:
                user_management.models.ClientFeaturePermission.objects.bulk_create([
                    user_management.models.ClientFeaturePermission(client=self, feature=feature, permission='View'),
                    user_management.models.ClientFeaturePermission(client=self, feature=feature, permission='Change'),
                    user_management.models.ClientFeaturePermission(client=self, feature=feature, permission='Delete'),
                ])

    def __str__(self):
        return self.client_name + ' / ' + self.display_name

    def get_name(self):
        return self.display_name

    # fetch all project
    def get_all_projects(self):
        from v2.project.models import Project
        return Project.objects.filter(client=self)

    # fetch all standalone_projects
    def get_all_standalone_projects(self):
        from v2.project.models import Project
        return Project.objects.filter(client=self, plant__isnull=True)

    # fetch all plants
    def get_all_plants(self):
        from v2.project.models import Plant
        return Plant.objects.filter(client=self)

    # fetch all live plants
    def get_all_live_projects(self):
        from v2.project.models import Project
        project_list = self.get_all_projects()
        project_id_list = []
        for project in project_list:
            if project.project_status and project.project_status < 100:
                project_id_list.append(project.id)
        return Project.objects.filter(id__in=project_id_list)

    # fetch all open_projects
    def get_all_open_projects(self):
        from v2.project.models import Project
        project_list = self.get_all_projects()
        project_id_list = []
        for project in project_list:
            if project.project_status == 0:
                project_id_list.append(project.id)
        return Project.objects.filter(id__in=project_id_list)

    # fetch all on_going_projects
    def get_all_on_going_projects(self):
        from v2.project.models import Project
        project_list = self.get_all_projects()
        project_id_list = []
        for project in project_list:
            if project.project_status > 0 and project.project_status < 100:
                project_id_list.append(project.id)
        return Project.objects.filter(id__in=project_id_list)

    # fetch all completed_projects
    def get_all_completed_projects(self):
        from v2.project.models import Project
        project_list = self.get_all_projects()
        project_id_list = []
        for project in project_list:
            if project.project_status >= 100:
                project_id_list.append(project.id)
        return Project.objects.filter(id__in=project_id_list)

    # fetch all projects_average_status
    def get_all_projects_average_status(self, projects=None):
        project_list = self.get_all_projects() if projects is None else projects
        avg_status = 0
        for project in project_list:
            avg_status += float(project.project_status)
        return avg_status / project_list.count() if project_list.count() else 0

    # fetch total_budget
    def get_total_budget(self, projects=None):
        project_list = self.get_all_projects() if projects is None else projects
        total_budget = 0
        for project in project_list:
            total_budget += project.budget
        return total_budget

    # fetch total_disbursement_amount
    def get_total_disbursement_amount(self, projects=None):
        project_list = self.get_all_projects() if projects is None else projects
        total_budget = 0
        for project in project_list:
            total_budget += project.total_disbursement_amount or 0
        return total_budget

    # fetch planned_disbursement_amount
    def get_planned_disbursement_amount(self, projects=None):
        project_list = self.get_all_projects() if projects is None else projects
        total_budget = 0
        for project in project_list:
            total_budget += project.planned_disbursement_amount or 0
        return total_budget

    # fetch total_utilized_amount
    def get_total_utilized_amount(self, projects=None):
        project_list = self.get_all_projects() if projects is None else projects
        total_utilized = 0
        for project in project_list:
            total_utilized += project.total_utilised_amount
        return total_utilized

    # fetch all_project_locations
    def get_all_project_locations(self):
        from v2.project.models import ProjectLocation
        all_projects = self.get_all_projects()
        locations = ProjectLocation.objects.filter(project__in=all_projects).distinct('city', 'state', 'country') \
            .values_list('location', flat=True)
        return locations

    # fetch project_focus_areas_list
    def get_project_focus_areas_list(self, projects=None):
        from v2.project.models import ProjectFocusArea
        project_list = self.get_all_projects() if projects is None else projects
        focus_area = ProjectFocusArea.objects.filter(project__in=project_list).values_list('focus_area', flat=True)
        return list(set(focus_area)) if len(focus_area) else []

    # fetch project_sub_focus_areas_list
    def get_project_sub_focus_areas_list(self, projects=None):
        from v2.project.models import ProjectSubFocusArea
        project_list = self.get_all_projects() if projects is None else projects
        sub_focus_area = ProjectSubFocusArea.objects.filter(project__in=project_list).values_list('sub_focus_area',
                                                                                                  flat=True)
        return list(set(sub_focus_area)) if len(sub_focus_area) else []

    # fetch project_target_segment_list
    def get_project_target_segment_list(self, projects=None):
        from v2.project.models import ProjectTargetSegment
        project_list = self.get_all_projects() if projects is None else projects
        target_segment = ProjectTargetSegment.objects.filter(project__in=project_list).values_list('target_segment',
                                                                                                   flat=True)
        return list(set(target_segment)) if len(target_segment) else []

    # fetch project_sub_target_segment_list
    def get_project_sub_target_segment_list(self, projects=None):
        from v2.project.models import ProjectSubTargetSegment
        project_list = self.get_all_projects() if projects is None else projects
        sub_target_segment = ProjectSubTargetSegment.objects.filter(project__in=project_list).values_list(
            'sub_target_segment', flat=True)
        return list(set(sub_target_segment)) if len(sub_target_segment) else []

    # fetch projects_sdg_goals
    def get_projects_sdg_goals(self, projects=None):
        from v2.project.models import ProjectSDGGoals
        all_projects = self.get_all_projects() if projects is None else projects
        sdg_goals = ProjectSDGGoals.objects.filter(project__in=all_projects).values_list('sdg_goal', flat=True)
        final_sdg_goals = []
        goal_ids = []
        for sdg_goal in list(sdg_goals):
            if sdg_goal and sdg_goal['id'] not in goal_ids:
                final_sdg_goals.append(sdg_goal)
                goal_ids.append(sdg_goal['id'])
        return final_sdg_goals

    # fetch project_over_utilization
    def get_project_over_utilization(self, projects=None):
        project_list = self.get_all_projects() if projects is None else projects
        utilization_focus_area = []
        for project in project_list:
            if project.total_utilised_amount > 0:
                if not project.get_focus_area() is None:
                    utilization_focus_area + list(project.get_focus_area())
        return list(set(utilization_focus_area)) if len(utilization_focus_area) else []

    # fetch last_compliance
    def get_last_compliance(self):
        return ClientComplianceEvaluation.objects.filter(client=self).order_by('date_captured').last()

    # fetch last_compliance_date
    def get_last_compliance_date(self):
        return self.get_last_compliance().date_captured if self.get_last_compliance() else ''

    # fetch count_of_risk_status
    def get_count_of_risk_status(self, start_date=None, end_date=None, projects=None):
        from v2.risk_management.models import RiskEntry
        all_project_risk_entries = RiskEntry.objects.filter(client=self)
        if projects is not None:
            all_project_risk_entries = all_project_risk_entries.filter(project__in=projects)
        if start_date is not None and end_date is not None:
            all_project_risk_entries = all_project_risk_entries.filter(created_date__gte=start_date,
                                                                       created_date__lte=end_date)
        risk_entries_count = all_project_risk_entries.count()
        if risk_entries_count:
            risk_status = {
                'Low': all_project_risk_entries.filter(severity=0).count(),
                "Medium": all_project_risk_entries.filter(severity=1).count(),
                "High": all_project_risk_entries.filter(severity=2).count(),
                "Critical": all_project_risk_entries.filter(severity=3).count(),
                "Total": all_project_risk_entries.count(),
            }
        else:
            risk_status = {}
        return risk_status

    # fetch all_client_files
    def get_all_client_files(self):
        return ClientDocument.objects.filter(client=self)

    # fetch all_client_images
    def get_all_client_images(self):
        return ClientImage.objects.filter(client=self)

    # fetch all_projects_list_by_date_range
    def get_all_projects_list_by_date_range(self, start_date, end_date):
        return self.get_all_projects().filter(start_date__lte=end_date, end_date__gte=start_date)

    # fetch all_plants_list_by_date_range
    def get_all_plants_list_by_date_range(self, start_date, end_date):
        return self.get_all_plants().filter(start_date__lte=end_date, end_date__gte=start_date)

    # fetch dashboard
    def get_dashboard(self):
        return ClientDashboard.objects.filter(client=self)


"""
   This model will be used to store the documents for client.
"""
class ClientDocument(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='', blank=True
                            )
    parent = models.CharField(default='', max_length=255)

    def __str__(self):
        return self.client.client_name + " - "

    def save(self, *args, **kwargs):
        if not self.pk:
            self.document_file.name = 'file_system/{0}/'.format(self.client.client_name) + self.document_file.name
        super(ClientDocument, self).save(*args, **kwargs)


"""
   This model will be used to store the images for client.
"""
class ClientImage(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    image_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    image_size = models.BigIntegerField(null=True, blank=True)
    path = models.TextField(default='')
    document_tag = JSONField(default=[], null=True, blank=True)

    def __str__(self):
        return self.client.client_name + " - "

    def save(self, *args, **kwargs):
        if not self.pk:
            self.image_file.name = 'file_system/{0}/images/'.format(self.client.client_name) + self.image_file.name
        super(ClientImage, self).save(*args, **kwargs)


"""
   This model will be used to store the users for client.
"""
class ClientUser(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('client', 'user')


"""
   This model will be used to store the client Overview.
"""
class ClientOverview(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    vision = models.TextField(null=True, blank=True)
    mission = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.client.client_name


"""
   This model will be used To Store the details of the client.
"""
class ClientDetail(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    contact_name = models.CharField(max_length=511)
    contact_email = models.EmailField(max_length=511)
    primary_phone = models.CharField(max_length=31)
    alternative_number = models.CharField(max_length=31, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    pin_code = models.CharField(max_length=31, null=True, blank=True)
    state = models.CharField(max_length=63, null=True, blank=True)
    city = models.CharField(max_length=63, null=True, blank=True)

    def __str__(self):
        return self.client.client_name


"""
   This model will be used To Store the Compliance CheckList for the Client.
"""
class ClientComplianceCheckList(AuditFields):
    checklist_id = models.AutoField(primary_key=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    area = models.CharField(max_length=63, null=True)
    indicator = models.CharField(max_length=511)
    compulsion = models.BooleanField(default=True)
    document_required = models.BooleanField(default=False)

    class Meta:
        unique_together = ('client', 'area', 'indicator')


"""
   This model will be used To Store the Compliance Schedule.
"""
class ClientComplianceSchedule(AuditFields):
    COMPLIANCE_FREQUENCY = (
        ('Monthly', 'Monthly'), ('Quarterly', 'Quarterly'), ('Half-yearly', 'Half-yearly'), ('Annually', 'Annually'))
    frequency = models.CharField(max_length=63, choices=COMPLIANCE_FREQUENCY, default='Monthly')


"""
   This model will be used To Store the Compliance Evaluation of the Client.
"""
class ClientComplianceEvaluation(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    compliance_checklist = models.ForeignKey(ClientComplianceCheckList, on_delete=models.CASCADE)
    value = models.NullBooleanField(null=True, blank=True)
    support_document = models.FileField(null=True, blank=True, validators=[validate_file_extension])
    date_captured = models.DateField(null=True, blank=True)
    batch_id = models.CharField(max_length=63, null=True, blank=True)

    class Meta:
        unique_together = ('client', 'compliance_checklist', 'date_captured', 'batch_id')

"""
   This model will be used To Store the User Notification Settings.
"""
class UserNotificationSettings(AuditFields):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    settings_name = models.SmallIntegerField(choices=settings_choices.TYPE_CHOICES)
    is_enabled = models.BooleanField(default=False)
    is_email_enabled = models.BooleanField(default=False)
    is_periodic = models.BooleanField(default=False)
    days = models.IntegerField(default=7)

    class Meta:
        unique_together = ('user', 'settings_name')

    @classmethod
    def user_settings_check(cls, user, settings_name):
        user_settings = cls.objects.filter(user=user, settings_name=settings_name)
        return user_settings.first().is_enabled if user_settings.exists() else True

    @classmethod
    def user_email_settings_check(cls, user, settings_name):
        user_settings = cls.objects.filter(user=user, settings_name=settings_name)
        return user_settings.first().is_email_enabled if user_settings.exists() else False


"""
   This model will be used To Store the dashboards for the client.
"""
class ClientDashboard(AuditFields):
    PROVIDER_METABASE = 0
    PROVIDER_ZOHO = 1
    PROVIDER_CHOICES = ((PROVIDER_METABASE, 'Metabase'), (PROVIDER_ZOHO, 'Zoho'))
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    provider = models.SmallIntegerField(choices=PROVIDER_CHOICES)
    dashboard_name = models.CharField(max_length=511, default='Insights')
    url = models.TextField()
    embedded_content = models.TextField()
    password = models.CharField(max_length=220, null=True, blank=True)


"""
   This model will be used To Store the Custom Folders for the client.
"""
class CustomFolder(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    folder_name = models.CharField(max_length=511)
    path = models.TextField(blank=True, null=True)
    parent = models.CharField(max_length=255)
    document_tag = models.CharField(max_length=255, blank=True, null=True)


"""
   This model will be used To Store the Settings for the client.
"""
class ClientSettings(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, unique=True)
    disbursement_workflow = models.BooleanField(default=True)
    utilisation_workflow = models.BooleanField(default=True)
    impact_workflow = models.BooleanField(default=True)


"""
   This model will be used To Store the Database Details for the client.
"""
class ClientDatabaseDetail(AuditFields):
    username = models.CharField(max_length=63)
    password = models.CharField(max_length=255)
    schema_name = models.CharField(max_length=63)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
