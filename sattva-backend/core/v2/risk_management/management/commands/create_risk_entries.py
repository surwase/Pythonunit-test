# coding=utf-8

from django.core.management.base import BaseCommand
import csv

from v2.risk_management.models import RiskCategory, Risk, RiskVariables
from v2.risk_management.triggers import project_utilization_crossed_planned_amount, \
    project_total_distribution_utilization_crossed_total_distribution


class Command(BaseCommand):
    help = 'Creating Risk Entries'

    def handle(self, *args, **options):
        try:
            project_utilization_crossed_planned_amount()
        except:
            pass
        try:
            project_total_distribution_utilization_crossed_total_distribution()
        except:
            pass
