from .views import *
from rest_framework import routers
from django.conf.urls import include, url

app_name = "domainlibraries"

# router = routers.DefaultRouter()
# router.register(r'^focusareas', FocusAreaViewSet, 'focusareas')
# urlpatterns = router.urls

urlpatterns = [

    url(r'^focusareas/$', FocusAreaViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='focusarea-list'),
    url(r'^focusareas/(?P<pk>\d+)/$', FocusAreaViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='focusarea-detail'),

    url(r'^subfocusareas/$', SubFocusAreaViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='subfocusarea-list'),
    url(r'^subfocusareas/(?P<pk>\d+)/$', SubFocusAreaViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='subfocusarea-detail'),

    url(r'^institutions/$', InstitutionViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='institution-list'),
    url(r'^institutions/(?P<pk>\d+)/$', InstitutionViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='institution-detail'),

    url(r'^sdggoals/$', SDGGoalViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='sdggoal-list'),
    url(r'^sdggoals/(?P<pk>\d+)/$', SDGGoalViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='sdggoal-detail'),

    url(r'^approles/$', ApplicationRoleViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='applicationrole-list'),
    url(r'^approles/(?P<pk>\d+)/$', ApplicationRoleViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='applicationrole-detail'),

    url(r'^targetseg/$', TargetSegmentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='targetsegment-list'),
    url(r'^targetseg/(?P<pk>\d+)/$', TargetSegmentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='targetsegment-detail'),

    url(r'^subtargetseg/$', SubTargetSegmentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='subtargetsegment-list'),
    url(r'^subtargetseg/(?P<pk>\d+)/$', SubTargetSegmentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='subtargetsegment-detail'),

    url(r'^frequencies/$', FrequencyViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='frequency-list'),
    url(r'^frequencies/(?P<pk>\d+)/$', FrequencyViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='frequency-detail'),

    url(r'^entitytype/$', EntityTypeViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='entitytype-list'),
    url(r'^entitytype/(?P<pk>\d+)/$', EntityTypeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='entitytype-detail'),

    url(r'^entities/$', EntityViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='entity-list'),
    url(r'^entities/(?P<pk>\d+)/$', EntityViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='entity-detail'),

    url(r'^getentity/$',
        get_entity_by_entity_type_focus_area_sub_focus_area),
    url(r'^getentity/(?P<entity_type>\w+)/$',
        get_entity_by_entity_type_focus_area_sub_focus_area),
    url(r'^getentity/(?P<focus_area_name>[\w\ ]+)/$',
        get_entity_by_entity_type_focus_area_sub_focus_area),
    url(r'^getentity/(?P<sub_focus_area_name>[\w\ ]+)/$',
        get_entity_by_entity_type_focus_area_sub_focus_area),
    url(r'^getentity/(?P<entity_type>\w+)/(?P<focus_area_name>[\w\ ]+)/$',
        get_entity_by_entity_type_focus_area_sub_focus_area),
    url(r'^getentity/(?P<entity_type>\w+)/(?P<sub_focus_area_name>[\w\ ]+)/$',
        get_entity_by_entity_type_focus_area_sub_focus_area),
    url(r'^getentity/(?P<entity_type>\w+)/(?P<focus_area_name>[\w\ ]+)/(?P<sub_focus_area_name>[\w\ ]+)/$',
        get_entity_by_entity_type_focus_area_sub_focus_area),

    url(r'^aggfunc/$', AggregateFunctionViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='aggfunc-list'),
    url(r'^aggfunc/(?P<pk>\d+)/$', AggregateFunctionViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='aggfunc-detail'),

    url(r'^datatype/$', DataTypeViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='datatype-list'),
    url(r'^datatype/(?P<pk>\d+)/$', DataTypeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='datatype-detail'),

    url(r'^doctype/$', DocumentTypeViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='doctype-list'),
    url(r'^doctype/(?P<pk>\d+)/$', DocumentTypeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='doctype-detail'),

    url(r'^docclass/$', DocumentClassViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='docclass-list'),
    url(r'^docclass/(?P<pk>\d+)/$', DocumentClassViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='docclass-detail'),

    url(r'^csrchecklist/$', CSRComplianceCheckListViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='csrchecklist-list'),
    url(r'^csrchecklist/(?P<pk>\d+)/$', CSRComplianceCheckListViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='csrchecklist-detail'),

    url(r'^ngoselectionchecklist/$', NGOSelectionCheckListViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='ngoselectionchecklist-list'),
    url(r'^ngoselectionchecklist/(?P<pk>\d+)/$', NGOSelectionCheckListViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='ngoselectionchecklist-detail'),

    url(r'^riskimpact/$', RiskImpactViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='riskimpact-list'),
    url(r'^riskimpact/(?P<pk>\d+)/$', RiskImpactViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='riskimpact-detail'),

    url(r'^riskprobability/$', RiskProbabilityViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='riskprobability-list'),
    url(r'^riskprobability/(?P<pk>\d+)/$', RiskProbabilityViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='riskprobability-detail'),

    url(r'^riskstatus/$', RiskStatusViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='riskstatus-list'),
    url(r'^riskstatus/(?P<pk>\d+)/$', RiskStatusViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='riskstatus-detail'),

    url(r'^risktype/$', RiskTypeViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='risktype-list'),
    url(r'^risktype/(?P<pk>\d+)/$', RiskTypeViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='risktype-detail'),

    url(r'^riskowner/$', RiskOwnerViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='riskowner-list'),
    url(r'^riskowner/(?P<pk>\d+)/$', RiskOwnerViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='riskowner-detail'),

    url(r'^mitigationstatus/$', MitigationStatusViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='mitigationstatus-list'),
    url(r'^mitigationstatus/(?P<pk>\d+)/$', MitigationStatusViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='mitigationstatus-detail'),

    url(r'^taskcategory/$', TaskCategoryViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='taskcategory-list'),
    url(r'^taskcategory/(?P<pk>\d+)/$', TaskCategoryViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='taskcategory-detail'),

]
