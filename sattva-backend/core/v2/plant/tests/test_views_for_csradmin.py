from django.urls import reverse
from .test_setup import TestSetUpCsrAdmin
from rest_framework import status
from django.test.client import RequestFactory
from core.users.models import User
from v2.plant.models import Plant,PlantDashboard,PlantImage, PlantDocument, PlantUser,PlantSDGGoals
from v2.plant.views import PlantModelViewSet,PlantDetailViewSet,PlantDocumentModelViewSet,PlantImageModelViewSet,PlantUserModelViewSet,PlantModelProjectViewSet,PlantDashboardModelViewSet,DeleteUserPlantAPIView,PlantIndicatorViewSet
from core.users.models import User
from v2.project.models import Project
from django.core.files.uploadedfile import SimpleUploadedFile

class PlantModelViewSetTestCase(TestSetUpCsrAdmin):
    def setUp(self):
        super().setUp()
        self.plant_obj= Plant.objects.create(
          plant_name = "plant1",
          plant_description = "plantdec",
          client = self.client_obj,
          budget = 1000.50,
          start_date ="2020-06-22T18:30:00Z",
          end_date = "2021-03-22T18:30:00Z"
          )
        self.data = {
          "plant_name" : "plant2",
          "plant_description" : "plantdeccc",
          "client" : self.client_obj.id,
          "budget" : 100000.50,
          "start_date" :"2020-06-22T18:30:00Z",
          "end_date" :"2021-03-22T18:30:00Z"
        }
        Project.objects.create(
            project_name = "xyz",
            project_description = "hi i am tested",
            client = self.client_obj,
            plant = self.plant_obj,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        self.dashboard=PlantDashboard.objects.create(
            plant =  self.plant_obj,
            provider = 0,
            dashboard_name = "dash1",
            url = "www.xyz.com",
            embedded_content = "hi i am dash board",
            password = "zxcvbnm"
            )

        self.dashboarddata={
            "plant" : self.plant_obj.id,
            "provider" : 0,
            "dashboard_name" : "dashhh",
            "url" : "www.xyz.com",
            "embedded_content" : "hi i am dash board 2",
            "password" : "zxcvbnm"
        }

        self.plantuser=PlantUser.objects.create(
            plant = self.plant_obj,
            user = self.user
            )

        self.plantuserdata={
            "plant" : self.plant_obj.id,
            "user" : self.user.id
        }

        self.plantimg=PlantImage.objects.create(
            plant = self.plant_obj,
            image_file  = SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            document_tag = ['tag1'],
            path = "xyz"
        )

        self.imgdata={
            "plant": self.plant_obj.id,
            "image_file ": SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "document_tag": ['tag1'],
            "path": "xyzzz"
        }

        self.doc=PlantDocument.objects.create(
            plant = self.plant_obj,
            document_file = SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            doc_size = 10,
            document_tag = ['tag1'],
            path = "xyz"
        )
        self.docdata={
            "plant" : self.plant_obj.id,
            "document_file" : SimpleUploadedFile('best_file_evaa.txt', b'these are the contents of the edited txt file'),
            "doc_size" : 10,
            "document_tag" : ['tag1'],
            "path" : "xyz"
        }

        #urls
        self.plant_url= reverse('v2:plant:plant')
        self.detail_url= reverse('v2:plant:plant',kwargs={'pk': self.plant_obj.id})
        self.plant_indicator_url= reverse('v2:plant:plant_indicator',kwargs={'pk': self.plant_obj.id})
        self.plant_details_url= reverse('v2:plant:plant_details',kwargs={'pk': self.plant_obj.id})
        self.client_project_list_url= reverse('v2:plant:client_project_list',kwargs={'pk': self.plant_obj.id})
        self.plant_userlist_url= reverse('v2:plant:plant_user')
        self.delete_plant_user_url= reverse('v2:plant:delete_plant_user')
        self.plant_dashboard_url= reverse('v2:plant:plant_dashboard',kwargs={'pk': self.plant_obj.id})
        self.plant_image_url= reverse('v2:plant:plant_doc')
        self.plant_doc_url= reverse('v2:plant:plant_doc')


    def test_plant_list(self):
        request = self.factory.get(self.plant_url)
        request.user = self.user
        view = PlantModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_plant_indicator(self):
        request = self.factory.get(self.plant_indicator_url)
        request.user = self.user
        view = PlantIndicatorViewSet.as_view( {'get': 'retrieve'})
        response = view(request,pk=self.plant_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_plant_detail(self):
        request = self.factory.get(self.detail_url)
        request.user = self.user
        view = PlantModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request, pk=self.plant_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['plant_name'], self.plant_obj.plant_name)

    def test_plant_details_url(self):
        request = self.factory.get(self.plant_details_url)
        request.user = self.user
        view = PlantDetailViewSet.as_view({'get': 'retrieve'})
        response = view(request,pk=self.plant_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 21)

    def test_plant_get(self):
        request = self.factory.get(self.plant_url, self.data, format='json')
        request.user = self.user
        view = PlantModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.plant_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['plant_name'],'plant1')

    def test_client_project_list_url(self):
        request = self.factory.get(self.client_project_list_url)
        request.user = self.user
        view = PlantModelProjectViewSet.as_view({'get': 'retrieve'})
        response = view(request,pk=self.plant_obj.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['plant_name'], self.plant_obj.plant_name)

    def test_plant_userlist_url(self):
        request = self.factory.get(self.plant_userlist_url)
        request.user = self.user
        view = PlantUserModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.user.id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_plant_userlist_url(self):
        request = self.factory.get(self.plant_userlist_url)
        request.user = self.user
        view = PlantUserModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_plant_dashboard_url(self):
        request = self.factory.get(self.plant_dashboard_url)
        request.user = self.user
        view =  PlantDashboardModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_plant_dashboard_url_get(self):
        request = self.factory.get(self.plant_dashboard_url)
        request.user = self.user
        view = PlantDashboardModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.dashboard.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 13)

    def test_plant_dashboard_url_put(self):
        request = self.factory.put(self.plant_dashboard_url,self.dashboarddata, format='json')
        request.user = self.user
        view = PlantDashboardModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.dashboard.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['dashboard_name'], self.dashboarddata['dashboard_name'])

    def test_plant_dashboard_url_patch(self):
        request = self.factory.patch(self.plant_dashboard_url,self.dashboarddata, format='json')
        request.user = self.user
        view = PlantDashboardModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.dashboard.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['dashboard_name'], self.dashboarddata['dashboard_name'])

    def test_plant_dashboard_url_delete(self):
        request = self.factory.delete(self.plant_dashboard_url,self.dashboarddata, format='json')
        request.user = self.user
        view = PlantDashboardModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.dashboard.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_plant_image_url(self):
        request = self.factory.get(self.plant_image_url)
        request.user = self.user
        view = PlantImageModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_plant_image_create(self):
    #     request = self.factory.post(self.plant_image_url,self.imgdata, format='json')
    #     request.user = self.user
    #     view = PlantImageModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), 1)

    def test_plant_image_get(self):
        request = self.factory.get(self.plant_image_url, self.imgdata, format='json')
        request.user = self.user
        view = PlantImageModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.plantimg.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "xyz")

    def test_plant_image_patch(self):
        request = self.factory.patch(self.plant_image_url, self.imgdata, format='json')
        request.user = self.user
        view = PlantImageModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.plantimg.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['path'], "xyzzz")

    def test_plant_image_delete(self):
        request = self.factory.delete(self.plant_image_url, self.imgdata, format='json')
        request.user = self.user
        view = PlantImageModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.plantimg.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

   # def test_plant_doc_url(self):
    #     request = self.factory.get(self.plant_doc_url)
    #     request.user = self.user
    #     view =  PlantDocumentModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), 1)

    # def test_plant_doc_create(self):
    #     request = self.factory.post(self.plant_doc_url, self.docdata, format='json')
    #     request.user = self.user
    #     view = PlantDocumentModelViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['doc_size'], self.docdata['doc_size'])

    def test_plant_doc_get(self):
        request = self.factory.get(self.plant_doc_url, self.docdata, format='json')
        request.user = self.user
        view =PlantDocumentModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.doc.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['doc_size'],10)

    # def test_plant_doc_put(self):
    #     request = self.factory.put(self.plant_doc_url, self.docdata, format='json')
    #     request.user = self.user
    #     view =PlantDocumentModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.doc.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['doc_size'],20)

    # def test_plant_doc_patch(self):
    #     request = self.factory.patch(self.plant_doc_url, self.docdata, format='json')
    #     request.user = self.user
    #     view =  PlantDocumentModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.doc.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(response.data['doc_size'], self.docdata['doc_size'])

    def test_plant_doc__delete(self):
        request = self.factory.delete(self.plant_doc_url, self.docdata, format='json')
        request.user = self.user
        view = PlantDocumentModelViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
        response = view(request,pk=self.doc.id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
