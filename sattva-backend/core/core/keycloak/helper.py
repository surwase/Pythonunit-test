import json

from keycloak import KeycloakAdmin, KeycloakOpenID
from django.conf import settings


class KeyclockHelper(object):
    def __init__(self):
        self.config = settings.KEYCLOAK_CONFIG

        # Read configurations
        try:
            self.server_url = self.config['KEYCLOAK_SERVER_URL']
            self.client_id = self.config['KEYCLOAK_CLIENT_ID']
            self.realm = self.config['KEYCLOAK_REALM']
        except KeyError as e:
            raise Exception("KEYCLOAK_SERVER_URL, KEYCLOAK_CLIENT_ID or KEYCLOAK_REALM not found.")

        self.client_secret_key = self.config.get('KEYCLOAK_CLIENT_SECRET_KEY', None)
        self.client_public_key = self.config.get('KEYCLOAK_CLIENT_PUBLIC_KEY', None)
        self.default_access = self.config.get('KEYCLOAK_DEFAULT_ACCESS', "DENY")
        self.method_validate_token = self.config.get('KEYCLOAK_METHOD_VALIDATE_TOKEN', "INTROSPECT")
        self.keycloak_authorization_config = self.config.get('KEYCLOAK_AUTHORIZATION_CONFIG', None)

        # Create Keycloak instance
        self.keycloak = KeycloakOpenID(server_url=self.server_url,
                                       client_id=self.client_id,
                                       realm_name=self.realm,
                                       client_secret_key=self.client_secret_key)

        # Read policies
        if self.keycloak_authorization_config:
            self.keycloak.load_authorization_config(self.keycloak_authorization_config)

        self.keycloak_admin = KeycloakAdmin(server_url=self.server_url,
                                            username=self.config['KEYCLOAK_ADMIN_USERNAME'],
                                            password=self.config['KEYCLOAK_ADMIN_PASSWORD'],
                                            realm_name=self.realm,
                                            client_id=self.client_id,
                                            client_secret_key=self.client_secret_key,
                                            verify=True)

    def create_user(self, user):
        user = self.keycloak_admin.create_user(user)
        return user

    def assign_role(self, username, role):
        user_id = self.keycloak_admin.get_user_id(username)
        client_id = self.keycloak_admin.get_client_id(self.client_id)
        role_obj = self.keycloak_admin.get_client_role(client_id, role)
        return self.keycloak_admin.assign_client_role(user_id, client_id, role_obj)

    def delete_user(self, username):
        user_id = self.keycloak_admin.get_user_id(username)
        return self.keycloak_admin.delete_user(user_id)

    def send_account_update(self, username):
        user_actions = json.dumps(['UPADATE_PASSWORD'])
        user_id = self.keycloak_admin.get_user_id(username)
        client_id = self.keycloak_admin.get_client_id(self.client_id)
        self.keycloak_admin.send_update_account(user_id, user_actions, lifespan=1209600)

    def add_mfa_to_user(self, username):
        user_actions = json.dumps(['CONFIGURE_TOTP'])
        user_id = self.keycloak_admin.get_user_id(username)
        client_id = self.keycloak_admin.get_client_id(self.client_id)
        self.keycloak_admin.send_update_account(user_id, user_actions, lifespan=1209600)
