# Generated by Django 2.2.16 on 2021-12-14 17:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domainlibraries', '0019_mcafocusarea_mcasubfocusarea'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mcafocusarea',
            name='mca_focus_area_name',
            field=models.CharField(max_length=300, unique=True),
        ),
        migrations.AlterField(
            model_name='mcasubfocusarea',
            name='mca_sub_focus_area_name',
            field=models.CharField(max_length=300, unique=True),
        ),
    ]
