import django_filters
from django_filters import rest_framework as filters
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from v2.authentication import KeycloakUserUpdateAuthentication
from v2.risk_management.models import RiskEntry, RiskCategory
from v2.risk_management.serializers import RiskEntrySerializer, RiskEntryWriteSerializer, RiskCategorySerializer
from v2.user_management.permissions import ProjectFeaturePermission
from v2.common_functions import create_xlsx_file

"""
   Filter for RiskEntry which will filter RiskEntry by exact project client and plant id
"""
class RiskFilter(django_filters.FilterSet):
    class Meta:
        model = RiskEntry
        fields = {
            'project': ['exact'],
            'client': ['exact'],
            'plant': ['exact'],
        }

"""
   Viewset for RiskEntry allows CRUD operations
"""
class RiskViewSet(viewsets.ModelViewSet):
    serializer_class = RiskEntrySerializer
    queryset = RiskEntry.objects.all().order_by('-created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = RiskFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]
    permission_classes = [ProjectFeaturePermission]

    def get_serializer_class(self):
        """
        This function will get the serializer class if method is put,post,patch it will return RiskEntryWriteSerializer else it will return RiskEntrySerializer
        """
        method = self.request.method
        if method == 'PUT' or method == 'POST' or method == 'PATCH':
            return RiskEntryWriteSerializer
        else:
            return RiskEntrySerializer

"""
   Viewset for RiskCategory allows read operations
"""
class RiskCategoryViewSet(viewsets.ModelViewSet):
    serializer_class = RiskCategorySerializer
    queryset = RiskCategory.objects.all()
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]


"""
    Api for Create program Risk .xlsx FileFile
"""
class ProgramRiskView(APIView):
    @staticmethod
    def get(request):
        try:
            plant_id = request.GET.get('plant')
            query = RiskEntry.objects.filter(plant=plant_id).values('project__project_name', 'project__start_date__date',
                                                                    'project__end_date__date', 'category__name',
                                                                    'severity', 'content',
                                                                    'comments').order_by('-created_date')
            risk_header = ['Project', 'Start Date', 'End Date', 'Category', 'Severity', 'Particular', 'Mitigation Comment']
            return create_xlsx_file(['Program Risk'], [risk_header], [query])

        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)


"""
    Api for Create Project Risk .xlsx FileFile
"""
class ProjectRiskView(APIView):
    @staticmethod
    def get(request):
        try:
            project_id = request.GET.get('project')
            query = RiskEntry.objects.filter(project=project_id).values('category__name', 'severity', 'content',
                                                                        'comments', 'milestone__milestone_name').order_by('-created_date')
            risk_header = ['Category', 'Severity', 'Particular', 'Mitigation Comment', 'Milestone Name']
            return create_xlsx_file(['Project Risk'], [risk_header], [query])

        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)


"""
    Api for Create Client Risk .xlsx FileFile
"""
class ClientRiskView(APIView):
    @staticmethod
    def get(request):
        try:
            client_id = request.GET.get('client')
            query = RiskEntry.objects.filter(client=client_id).values('plant__plant_name', 'project__project_name',
                                                                      'project__start_date__date', 'project__end_date__date',
                                                                      'category__name', 'severity', 'content',
                                                                        'comments').order_by('-created_date')
            risk_header = ['Program', 'Project', 'Start Date', 'End Date', 'Category', 'Severity', 'Particular', 'Mitigation Comment']
            return create_xlsx_file(['Client Risk'], [risk_header], [query])

        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)
