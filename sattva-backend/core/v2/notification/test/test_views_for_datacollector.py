from django.urls import reverse
from .test_setup import TestSetUpdatacollector
from rest_framework import status
from core.users.models import User
from v2.notification.models import Notification,NotificationSentLog
from v2.notification.views import NotificationViewSet,NotificationCountViewSet,UserNotificationsMarkAllRead,UserNotificationsSettings

class TestNotificationview(TestSetUpdatacollector):

    def setUp(self):
        super().setUp()
        self.noti=Notification.objects.create(
            text = "text1",
            title ="title1",
            notification_type = 0,
            notification_tag = "tag1",
            category = 0,
            action_user = self.user,
            action_performed_on_user =self.user,
            action_params = ['prem1'],
            app_url = 'https://newsapi.org/v2/everything?q=tesla&from=2021-08-16&sortBy=publishedAt&apiKey=API_KEY',
            )
        self.data={
            "text" : "text22",
            "title" :"title22",
            "notification_type" : 0,
            "notification_tag" : "tag1",
            "category" : 0,
            "action_user" : self.user.id,
            "action_performed_on_user" :self.user.id,
            "action_params" : ['prem1'],
            "app_url" : 'https://newsapi.org/v2/everything?q=tesla&from=2021-08-16&sortBy=publishedAt&apiKey=API_KEY',
        }

        self.notilog=NotificationSentLog.objects.create(
            notification = self.noti,
            user = self.user,
            read = False
            )
        # self.dataa={
        #     "notification" : self.noti.id,
        #     "user" : self.user.id,
        #     "read" : False,
        # }

        #urls
        self.user_notifications= reverse('v2:notification:user_notifications')
        self.user_settings_url= reverse('v2:notification:user_settings')
        self.user_count_url= reverse('v2:notification:user_notifications',kwargs={'pk': self.noti.id})

    def test_notification_list(self):
        request = self.factory.get(self.user_notifications)
        request.user = self.user
        view = NotificationViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)

    # def test_notification_create(self):
    #     request = self.factory.post(self.user_notifications,self.data, format='json')
    #     request.user = self.user
    #     view = NotificationViewSet.as_view({'get': 'list', 'post': 'create'})
    #     response = view(request)
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data['count'], 1)

    # def test_notification_get(self):
    #     request = self.factory.get(self.user_notifications, self.data, format='json')
    #     request.user = self.user
    #     view = NotificationViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.noti.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data),14)
    #     self.assertEqual(response.data['notification_text'],"text1")

    # def test_notification_put(self):
    #     request = self.factory.put(self.user_notifications, self.data, format='json')
    #     request.user = self.user
    #     view = NotificationViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.noti.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data),14)
    #     self.assertEqual(response.data['notification_text'],self.noti.text)

    # def test_notification_patch(self):
    #     request = self.factory.patch(self.user_notifications, self.data, format='json')
    #     request.user = self.user
    #     view = NotificationViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.noti.id)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data),14)
    #     self.assertEqual(response.data['notification_text'],"text1")

    # def test_notification_delete(self):
    #     request = self.factory.delete(self.user_notifications, self.data, format='json')
    #     request.user = self.user
    #     view = NotificationViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})
    #     response = view(request,pk=self.noti.id)
    #     self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_count_url(self):
        request = self.factory.get(self.user_count_url)
        request.user = self.user
        view = NotificationCountViewSet.as_view({'get': 'retrieve'})
        response = view(request,pk=self.user.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["total_notification_count"],1)

    def test_usernotification_setting_url(self):
        request = self.factory.get(self.user_settings_url)
        request.user = self.user
        view = UserNotificationsSettings.as_view()
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

