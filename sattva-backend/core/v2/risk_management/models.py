import uuid

from django.db import models

# Create your models here.
from geolibraries.models import AuditFields
from v2.client.models import Client
from v2.plant.models import Plant
from v2.project.models import Project, Milestone


"""
    This Model is used to store the Risk Category.
"""
class RiskCategory(AuditFields):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


"""
    This Model is used to store the Risk.
"""
class Risk(AuditFields):
    SEVERITY_LOW = 0
    SEVERITY_MEDIUM = 1
    SEVERITY_HIGH = 2
    SEVERITY_CRITICAL = 3
    SEVERITY_CHOICES = ((SEVERITY_LOW, 'LOW'), (SEVERITY_HIGH, 'HIGH'), (SEVERITY_MEDIUM, 'MEDIUM'),
                        (SEVERITY_CRITICAL, 'CRITICAL'))
    severity = models.SmallIntegerField(choices=SEVERITY_CHOICES, default=SEVERITY_LOW)
    name = models.CharField(max_length=255)
    trigger_name = models.CharField(max_length=255)
    category = models.ForeignKey(RiskCategory, on_delete=models.CASCADE)
    description = models.TextField()

    def __str__(self):
        return self.name

    @classmethod
    def get_risk_from_trigger(cls,trigger):
        return cls.objects.filter(trigger_name=trigger).first()


"""
    This Model is used to store the Risk entry.
"""
class RiskEntry(AuditFields):
    STATUS_OPEN = 0
    STATUS_CLOSED = 1
    STATUS_CHOICES = ((STATUS_OPEN, 'OPEN'), (STATUS_CLOSED, 'CLOSED'))
    RISK_TYPE_AUTO = 0
    RISK_TYPE_MANUAL = 1
    RISK_TYPE_CHOICES = ((RISK_TYPE_AUTO, 'AUTO'), (RISK_TYPE_MANUAL, 'MANUAL'))
    SEVERITY_LOW = 0
    SEVERITY_MEDIUM = 1
    SEVERITY_HIGH = 2
    SEVERITY_CRITICAL = 3
    SEVERITY_CHOICES = ((SEVERITY_LOW, 'LOW'), (SEVERITY_HIGH, 'HIGH'), (SEVERITY_MEDIUM, 'MEDIUM'),
                        (SEVERITY_CRITICAL, 'CRITICAL'))
    name = models.CharField(max_length=255)
    code = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    project = models.ForeignKey(Project, blank=True, null=True, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE)
    plant = models.ForeignKey(Plant, blank=True, null=True, on_delete=models.CASCADE)
    milestone = models.ForeignKey(Milestone, blank=True, null=True, on_delete=models.CASCADE)
    risk = models.ForeignKey(Risk, blank=True, null=True, on_delete=models.CASCADE)
    status = models.SmallIntegerField(choices=STATUS_CHOICES, default=STATUS_OPEN)
    severity = models.SmallIntegerField(choices=SEVERITY_CHOICES, default=SEVERITY_LOW)
    risk_type = models.SmallIntegerField(choices=RISK_TYPE_CHOICES, default=RISK_TYPE_MANUAL)
    category = models.ForeignKey(RiskCategory,blank=True, null=True,  on_delete=models.CASCADE)
    content = models.TextField()
    comments = models.TextField(blank=True)

    def __str__(self):
        return self.name

    def is_opened(self):
        return True if self.status == 0 else False

    def is_closed(self):
        return True if self.status == 1 else False

    def set_closed(self):
        self.status = 1
        self.save()


    @classmethod
    def get_last_risk_entries_for_project(cls, trigger, project):
        risk = Risk.get_risk_from_trigger(trigger)
        if risk:
            risk_entries = cls.objects.filter(risk=risk, project=project).order_by('created_date').last()
            return risk_entries

    @classmethod
    def get_opened_risk_entry_for_project(cls, trigger, project):
        risk = Risk.get_risk_from_trigger(trigger)
        if risk:
            risk_entries = cls.objects.filter(risk=risk, project=project, status=0).order_by('created_date').last()
            return risk_entries

    def get_project(self):
        return self.project if not self.project is None else None

    def get_client(self):
        if self.client is None:
            client = self.get_project().get_client() if self.get_project() else None
        else:
            client = self.client
        return client

    def get_plant(self):
        if self.plant is None:
            plant = self.get_project().get_plant() if self.get_project() else None
        else:
            plant = self.plant
        return plant

    @staticmethod
    def create(risk, content, name="", severity=0, status=0, risk_type=0, project=None, client=None, plant=None, category=None):
        risk_name = risk.name if not len(name) else name
        if project is None and client is None and plant is None :
            raise ValueError("Project or Client or Plant is must to create a trigger")
        elif not project is None:
            project = project
            plant = project.get_plant()
            client = project.get_client()
        elif not plant is None:
            project = None
            plant = plant
            client = plant.get_client()
        elif not client is None:
            project = None
            plant = None
            client = client

        return RiskEntry.objects.create(
                    name=risk_name,
                    project=project,
                    plant=plant,
                    client=client,
                    milestone=milestone,
                    risk=risk,
                    status=status,
                    severity=severity,
                    risk_type=risk_type,
                    content=content,
                    category=category,
                )


"""
    This Model is used to store the Risk template.
"""
class RiskTemplate(AuditFields):
    risk = models.ForeignKey(Risk, blank=True, null=True, on_delete=models.CASCADE)
    content = models.TextField()

    def __str__(self):
        return self.risk.name

    @classmethod
    def get_template_from_risk(cls, risk):
        return cls.objects.filter(risk=risk).first()


"""
    This Model is used to store the Risk variables.
"""
class RiskVariables(AuditFields):
    risk = models.ForeignKey(Risk, blank=True, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    display_name = models.TextField()
    default_value = models.FloatField(default=0.0)
    variable_type = models.CharField(max_length=255)

    def __str__(self):
        return self.risk.name



"""
    This Model is used to store the Risk variable client settings.
"""
class RiskVariablesClientSettings(AuditFields):
    risk_variable = models.ForeignKey(RiskVariables, blank=True, null=True, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE)
    new_value = models.CharField(max_length=255)

    def __str__(self):
        return self.risk_variable.risk.name

