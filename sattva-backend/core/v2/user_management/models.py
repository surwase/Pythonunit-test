from django.db import models

from geolibraries.models import AuditFields
from v2.client.models import Client
from v2.project.models import Project
from django.contrib.postgres.fields import JSONField


"""
    This model will contain the list of all the features
    sections that are supposed to be there in the client section
    For example: User Management, Program Management and so on.
"""
class ClientFeature(AuditFields):
    feature_name = models.CharField(max_length=511, unique=True)
    feature_code = models.CharField(max_length=31, primary_key=True)
    default_permissions = JSONField(default={})
    applicable_roles = JSONField(default=[])

    def __str__(self):
        return self.feature_code

    def save(self, *args, **kwargs):
        new_object = self._state.adding
        super(ClientFeature, self).save(*args, **kwargs)
        # NOW Create Client Feature for each Client available
        if new_object:
            clients = Client.objects.all()
            for client in clients:
                ClientFeaturePermission.objects.bulk_create([
                    ClientFeaturePermission(client=client, feature=self, permission='View'),
                    ClientFeaturePermission(client=client, feature=self, permission='Change'),
                    ClientFeaturePermission(client=client, feature=self, permission='Delete'),
                ])

"""
    This model will store the list of all the client-feature map
    to help the user have permissions.
"""
class ClientFeaturePermission(AuditFields):
    PERMISSION_CHOICES = (('View', 'View'), ('Change', 'Change'), ('Delete', 'Delete'))

    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    feature = models.ForeignKey(ClientFeature, on_delete=models.CASCADE)
    permission = models.CharField(choices=PERMISSION_CHOICES, max_length=63)

    # def __str__(self):
    #     return self.client.client_name + '|' + self.feature.feature_name + '|' + self.permission


"""
    This model will store the list of all the features
    sections that are supposed to be there in the application
    For example: Project Overview, NGO On-boarding and so on.
"""
class ProjectFeature(AuditFields):
    feature_name = models.CharField(max_length=511)
    feature_code = models.CharField(max_length=31, primary_key=True)
    default_permissions = JSONField(default={})
    applicable_roles = JSONField(default=[])

    def __str__(self):
        return self.feature_code

    def save(self, *args, **kwargs):
        new_object = self._state.adding
        super(ProjectFeature, self).save(*args, **kwargs)
        # NOW Create Client Feature for each Client available
        if new_object:
            projects = Project.objects.all()
            for project in projects:
                ProjectFeaturePermission.objects.bulk_create([
                    ProjectFeaturePermission(project=project, feature=self, permission='View'),
                    ProjectFeaturePermission(project=project, feature=self, permission='Change'),
                    ProjectFeaturePermission(project=project, feature=self, permission='Delete'),
                ])


"""
    This model will store the list of all the project-feature map
    to help the user have permissions.
"""
class ProjectFeaturePermission(AuditFields):
    PERMISSION_CHOICES = (('View', 'View'), ('Change', 'Change'), ('Delete', 'Delete'))

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    feature = models.ForeignKey(ProjectFeature, on_delete=models.CASCADE)
    permission = models.CharField(choices=PERMISSION_CHOICES, max_length=63)

    # def __str__(self):
    #     return self.client.client_name + '|' + self.feature.feature_name + '|' + self.permission
