from django.contrib.postgres.fields import JSONField
from django.db import models
from geolibraries.models import AuditFields, validate_file_extension
from v2.client.models import Client


"""
    This Model is used to store the NGO Partner.
"""
class NGOPartner(AuditFields):
    ngo_name = models.CharField(max_length=1023)
    company_type = models.CharField(max_length=127, default='', null=True, blank=True)
    website_url = models.CharField(max_length=1024, default='', null=True, blank=True)
    about = models.TextField(default='', null=True, blank=True)
    address = models.TextField(default='', null=True, blank=True)
    employee_count = models.IntegerField(default=1, null=True, blank=True)
    annual_budget = models.IntegerField(default=0, null=True, blank=True)
    experience = models.IntegerField(default=1, null=True, blank=True)
    awards = models.CharField(max_length=1027, default='', null=True, blank=True)
    state = models.CharField(max_length=63, default='', null=True, blank=True)
    logo = models.FileField(null=True, blank=True, validators=[validate_file_extension])
    key_funders = JSONField(null=True, blank=True)
    key_work = JSONField(null=True, blank=True)
    clients = models.ManyToManyField(Client, blank=True)

    def __str__(self):
        return self.ngo_name


"""
    This Model is used to store the Point of Contact for ngo.
"""
class NGOPointOfContact(AuditFields):
    ngo = models.ForeignKey(NGOPartner, on_delete=models.CASCADE)
    name = models.CharField(max_length=511)
    phone_number = models.CharField(max_length=31)
    email_address = models.EmailField(max_length=511)


"""
    This Model is used to store the Focus Area for ngo.
"""
class NGOFocusArea(AuditFields):
    ngo = models.ForeignKey(NGOPartner, on_delete=models.CASCADE)
    focus_area = models.CharField(max_length=255)


"""
    This Model is used to store the Sub Focus Area for ngo.
"""
class NGOSubFocusArea(AuditFields):
    ngo = models.ForeignKey(NGOPartner, on_delete=models.CASCADE)
    sub_focus_area = models.CharField(max_length=255)


"""
    This Model is used to store the location for ngo.
"""
class NGOLocation(AuditFields):
    ngo = models.ForeignKey(NGOPartner, on_delete=models.CASCADE)
    location = JSONField(default=[])
    area = models.CharField(max_length=256, default='', null=True)
    city = models.CharField(max_length=256, default='', null=True)
    state = models.CharField(max_length=256, default='', null=True)
    country = models.CharField(max_length=256, default='', null=True)
    latitude = models.FloatField(max_length=31, default=0, null=True)
    longitude = models.FloatField(max_length=31, default=0, null=True)


"""
    This Model is used to store the target segments for ngo.
"""
class NGOTargetSegment(AuditFields):
    ngo = models.ForeignKey(NGOPartner, on_delete=models.CASCADE)
    target_segment = models.CharField(max_length=255)


"""
    This Model is used to store the sub target segments for ngo.
"""
class NGOSubTargetSegment(AuditFields):
    ngo = models.ForeignKey(NGOPartner, on_delete=models.CASCADE)
    sub_target_segment = models.CharField(max_length=255)


"""
    This Model is used to store the NGO SDG goals.
"""
class NGOSDGGoals(AuditFields):
    ngo = models.ForeignKey(NGOPartner, on_delete=models.CASCADE)
    sdg_goal_name = models.CharField(max_length=255, null=True, blank=True)
    sdg_goal = JSONField(default=[], null=True)
