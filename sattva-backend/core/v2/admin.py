from django.apps import apps
from django.contrib import admin
field_excluded = ['old_value', 'new_value', 'updated_by_username', 'updated_by', 'updated_date']
filter_list = ['action', 'created_by']


class ListAdminMixin(object):
    def __init__(self, model, admin_site):
        self.list_display = [field.name for field in model._meta.fields if field.name not in field_excluded]
        self.list_filter = [field.name for field in model._meta.fields if field.name in filter_list]
        super(ListAdminMixin, self).__init__(model, admin_site)


models = apps.get_models()
for model in models:
    admin_class = type('AdminClass', (ListAdminMixin, admin.ModelAdmin), {})
    try:
        admin.site.register(model, admin_class)
    except admin.sites.AlreadyRegistered:
        pass
