import csv
from datetime import datetime, timedelta
import dateutil.parser

from v2.project.models import Project, Utilisation, \
    UtilisationExpense, UtilisationUpload, ProjectTargetSegment


# Read CSV File
def read_csv(csv_file):
    import csv

    csv_rows = []
    with open(csv_file) as csvfile:
        reader = csv.reader(
            csvfile,
            quotechar='"',
            delimiter='|',
            quoting=csv.QUOTE_ALL,
            skipinitialspace=True
        )

        titles = next(reader)

        data = []

        for row in reader:
            row_data = {}
            for i, value in enumerate(row):
                title = titles[i]
                row_data[title] = value
            data.append(row_data)
        return data

def format_data(data):
    from datetime import datetime, timezone

    INTEGER_FIELDS = [
        "project_id",
        "planned_cost",
        "avg_unit",
        "avg_unit_cost",
        "total_estimate_cost",
        "estimate_cost",
        "number_of_units",
        "unit_cost",
        "actual_cost"
    ]
    TIMESTAMP_FIELDS = [
        "from_date",
        "to_date",
        "period_start_date",
        "period_end_date"
    ]
    for row in data:
        for key, value in row.items():
            if key in INTEGER_FIELDS and value:
                try:
                    row[key] = int(value)
                except ValueError:
                    raise Exception(f"Invalid value for key: {key}, value: {value} - Expected Integer")

            if key in TIMESTAMP_FIELDS and value:
                try:
                    date = dateutil.parser.parse(value, dayfirst=True)
                    date = date.replace(tzinfo=timezone.utc).astimezone(tz=None)
                    date = date + timedelta(hours=11)
                    row[key] = date
                except ValueError:
                    raise Exception(f"Invalid timestamp value for key: {key}, value: {value}")

            if key == "tags":
                row[key] = value.split(",")
    return data


def validate_data(data):
    REQUIRED_FIELDS = ["project_id"]
    for row in data:
        for key, value in row.items():
            if key in REQUIRED_FIELDS and not value:
                raise Exception(f"Required Field Empty, key: {key}, value: {value}")


def save_utilisation_expenses(file_name, data):
    from v2.project.models import Disbursement, Project, Utilisation, \
        UtilisationExpense, UtilisationUpload, ProjectTargetSegment

    def _get_utilisation_expense_object(data_dict, utilisation_upload):
        project_obj = Project.objects.filter(
            id=data_dict.get("project_id")
        ).first()

        disbursement_obj = Disbursement.objects.filter(
            project=project_obj,
            disbursement_name=data_dict.get("disbursement_name")
        ).first()

        # Mutliple utilisation_obj?
        utilisation_obj = Utilisation.objects.filter(
            project=project_obj,
            disbursement=disbursement_obj
        ).first()

        if not utilisation_obj:
            utilisation_obj = Utilisation.objects.create(
                project=project_obj,
                disbursement=disbursement_obj
            )

        return UtilisationExpense(
            expense_category=data_dict.get("expense_category"),
            particulars=data_dict.get("particulars"),
            disbursement_description=data_dict.get("disbursement_description"),
            planned_cost=data_dict.get("planned_cost"),
            avg_unit=data_dict.get("avg_unit"),
            avg_unit_cost=data_dict.get("avg_unit_cost"),
            from_date=data_dict.get("from_date"),
            to_date=data_dict.get("to_date"),
            total_estimate_cost=data_dict.get("total_estimate_cost"),
            split_frequency=data_dict.get("split_frequency"),
            period_start_date=data_dict.get("period_start_date"),
            period_end_date=data_dict.get("period_end_date"),
            estimate_cost=data_dict.get("estimate_cost"),
            number_of_units=data_dict.get("number_of_units"),
            unit_cost=data_dict.get("unit_cost"),
            actual_cost=data_dict.get("actual_cost"),
            tags=data_dict.get("tags"),
            remarks=data_dict.get("remarks"),
            comments=data_dict.get("comments"),
            utilisation=utilisation_obj,
            utilisation_upload=utilisation_upload
        )


    upload_obj = UtilisationUpload.objects.create(
        file_name=file_name.split("/")[-1],
        upload_type="document"
    )
    utlization_expenses = []
    for item in data:
        utlization_expenses.append(
            _get_utilisation_expense_object(item, upload_obj)
        )

    UtilisationExpense.objects.bulk_create(utlization_expenses)
    print("BULK CREATE SUCCESSFUL!")


# if __name__ == "__main__":
print("Starting script!")
file_name = "v2/project/templates/template.csv"
data = read_csv(file_name)
data = format_data(data)
validate_data(data)
save_utilisation_expenses(file_name, data)
