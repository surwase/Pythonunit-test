from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Sum
from rest_framework import serializers
from rest_framework_bulk import BulkListSerializer, BulkSerializerMixin

from core.users.models import User
from core.utils import check_null_and_replace_to_none
from v2.ngo.serializers import NGOPartnerSerializer, MiniNGOPartnerSerializer
from v2.client.models import Client
from v2.plant.models import Plant
from v2.project.models import CSVUpload, Disbursement, DisbursementComment, DisbursementDocument, Milestone, \
    MilestoneDocument, MilestoneNote, Project, ProjectTag, ProjectDocument, ProjectImage, ProjectUser, \
    SubTask, Task, TaskComment, TaskDocument, Template, TemplateAttribute, TemplateType, Utilisation, \
    UtilisationComment, ProjectDashboard, ProjectScheduleVII, UtilisationDocument, ProjectLocation, ProjectFocusArea, \
    ProjectSubFocusArea, ProjectTargetSegment, ProjectSDGGoals, ProjectSubTargetSegment, ProjectRemark, \
    IndicatorOutcome, ProjectIndicatorOutcome,  DisbursementWorkflowRuleSet, DisbursementWorkflowRule, UtilisationWorkflowRuleSet, \
    UtilisationWorkflowRule, DisbursementWorkflowRuleAssignee, UtilisationWorkflowRuleAssignee, DisbursementStatus, \
    DisbursementActivityLog, UtilisationStatus, UtilisationActivityLog, UtilisationUpload, UtilisationExpense, \
    ProjectSetting, UtilisationExpenseDocument, UtilisationUploadDocument, ImpactWorkflowRule, ImpactWorkflowRuleSet, \
    ImpactWorkflowRuleAssignee, ProjectOutputDocument, ProjectOutcomeDocument, ProjectIndicator, ProjectOutput, \
    ProjectOutcome, ProjectOutputStatus, ProjectOutcomeStatus, ImpactUploadStatus, IndicatorDocument, IndicatorComment, \
    IndicatorActivityLog, ImpactCaseStudy, CaseStudyDocuments, CaseStudyActivityLog, ImpactDataAndTemplateActivityLog, \
    TaskActivityLog, MilestoneActivityLog, BulkUpload, Bulkfiles, ProjectMCA, ProjectIndicatorLocation

"""
   This class will serialize the project model and return fields id and name.
"""
class ProjectMiniSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.get_name()

    class Meta:
        model = Project
        fields = ['id', 'name']


"""
   This class will serialize the project model and return fields id, project_name, start_date, end_date.
"""
class ProjectMicroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['id', 'project_name', 'start_date', 'end_date']


"""
   This class will serialize user model.
"""
class UserSerializerForProject(serializers.ModelSerializer):
    application_role = serializers.SerializerMethodField()

    def get_application_role(self, obj):
        return obj.application_role.role_name

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'profile_picture',
                  'username', 'email', 'status', 'application_role', 'id')


"""
   This class will serialize user model and return fields first_name, last_name, id.
"""
class MiniUserSerializerForProject(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'id')


"""
   This class will serialize Project model for write operation.
"""
class ProjectWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'

"""
   This class will serialize Project model.
"""
class ProjectSerializer(serializers.ModelSerializer):
    project_users = serializers.SerializerMethodField()
    total_utilized_amount = serializers.SerializerMethodField()
    total_disbursed_amount = serializers.SerializerMethodField()
    planned_disbursed_amount = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()
    focus_area = serializers.SerializerMethodField()
    sub_focus_area = serializers.SerializerMethodField()
    target_segment = serializers.SerializerMethodField()
    sub_target_segment = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    mca_sector = serializers.SerializerMethodField()
    ngo_partner = NGOPartnerSerializer(allow_null=True, read_only=True)
    tags = serializers.SerializerMethodField()
    plant_name = serializers.SerializerMethodField()
    unutilised_amount = serializers.SerializerMethodField()

    @staticmethod
    #fetch project users
    def get_project_users(project):
        project_users = ProjectUser.objects.filter(project=project)
        return ProjectUserReadOnlySerializer(project_users, many=True).data

    @staticmethod
    def get_total_utilized_amount(obj):
        return obj.get_total_utilised_amount()

    @staticmethod
    def get_total_disbursed_amount(obj):
        return obj.get_all_disbursement_total_amount()

    @staticmethod
    def get_planned_disbursed_amount(obj):
        return obj.get_all_disbursement_planned_amount()

    @staticmethod
    def get_location(obj):
        locations = ProjectLocation.objects.filter(project=obj).values_list('location', flat=True)
        return locations

    @staticmethod
    def get_focus_area(obj):
        focus_area = ProjectFocusArea.objects.filter(project=obj).values_list('focus_area', flat=True)
        return focus_area

    @staticmethod
    def get_sub_focus_area(obj):
        sub_focus_area = ProjectSubFocusArea.objects.filter(project=obj).values_list('sub_focus_area', flat=True)
        return sub_focus_area

    @staticmethod
    def get_target_segment(obj):
        target_segment = ProjectTargetSegment.objects.filter(project=obj).values_list('target_segment', flat=True)
        return target_segment

    @staticmethod
    def get_sub_target_segment(obj):
        sub_target_segment = ProjectSubTargetSegment.objects.filter(project=obj).values_list('sub_target_segment',
                                                                                             flat=True)
        return sub_target_segment

    @staticmethod
    def get_sdg_goals(obj):
        sdg_goals = ProjectSDGGoals.objects.filter(project=obj).values_list('sdg_goal', flat=True)
        return sdg_goals

    @staticmethod
    def get_mca_sector(obj):
        mca_sector = ProjectMCA.objects.filter(project=obj).values_list('mca', flat=True)
        return mca_sector

    @staticmethod
    def get_tags(obj):
        return obj.get_tags()

    @staticmethod
    def get_plant_name(obj):
        if obj.plant:
          return obj.plant.plant_name
        else:
          return None

    @staticmethod
    def get_unutilised_amount(obj):
        return obj.get_all_disbursement_total_amount()-obj.get_total_utilised_amount()

    class Meta:
        model = Project
        fields = '__all__'

"""
   This class will serialize Project details.
"""
class ListProjectSerializer(serializers.ModelSerializer):
    project_users = serializers.SerializerMethodField()
    project_users_count = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    focus_area = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    ngo_partner = MiniNGOPartnerSerializer(allow_null=True, read_only=True)

    @staticmethod
    def get_project_users(project):
        project_users = ProjectUser.objects.filter(project=project)[:2]
        return ProjectUserReadOnlySerializer(project_users, many=True).data

    @staticmethod
    def get_project_users_count(project):
        project_users = ProjectUser.objects.filter(project=project).count()
        return project_users

    @staticmethod
    def get_tags(obj):
        return obj.get_tags()

    @staticmethod
    def get_focus_area(obj):
        return obj.get_focus_area()

    @staticmethod
    def get_sdg_goals(obj):
        return obj.get_sdg_goals()

    class Meta:
        model = Project
        fields = ['id', 'project_users', 'tags', 'focus_area', 'sdg_goals', 'start_date', 'end_date', 'project_name',
                  'project_description', 'project_users_count', 'plant', 'client', 'budget', 'ngo_partner']
        # fields = ['id', 'project_users', 'tags', 'start_date', 'end_date', 'project_name',
        #           'project_users_count', 'plant', 'client', 'ngo_partner']


"""
   This class will serialize Project model and return field project_name.
"""
class ProjectListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('project_name',)

# apply filters for project milestone
def apply_filters_for_project_milestones(self, obj):
    start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
    end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
    location = check_null_and_replace_to_none(self.context.get('request').query_params.get('location'))
    focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('focus_area'))
    sub_focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('sub_focus_area'))
    total_milestones = obj.get_milestones()
    if not ((start_date is None) | (end_date is None)):
        total_milestones = total_milestones.filter(start_date__lte=end_date, end_date__gte=start_date)
    if location is not None:
        total_milestones = total_milestones.filter(location__in=location)
    if sub_focus_area is not None:
        total_milestones = total_milestones.filter(sub_focus_area__in=[sub_focus_area])
    return total_milestones

# apply filters for project disbursement
def apply_filter_get_project_disbursement(self, obj):
    start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
    end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
    # project_milestones = apply_filters_for_project_milestones(self, obj)
    project_disbursements = obj.get_all_disbursements()
    if not ((start_date is None) | (end_date is None)):
        project_disbursements = project_disbursements.filter(start_date__lte=end_date, end_date__gte=start_date)
    return project_disbursements

# apply filters for project utilisation
def apply_filter_get_project_utilisation(self, obj):
    start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
    end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
    # project_milestones = apply_filters_for_project_milestones(self, obj)
    project_utilisations = obj.get_all_utilisations()
    if not ((start_date is None) | (end_date is None)):
        project_utilisations = project_utilisations.filter(start_date__lte=end_date, end_date__gte=start_date)
    return project_utilisations


"""
   This class will serialize project details.
"""
class ProjectDetailSerializer(serializers.ModelSerializer):
    location = serializers.SerializerMethodField()
    focus_area = serializers.SerializerMethodField()
    sub_focus_area = serializers.SerializerMethodField()
    target_segment = serializers.SerializerMethodField()
    sub_target_segment = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    mca_sector = serializers.SerializerMethodField()
    client_details = serializers.SerializerMethodField()
    # images = serializers.SerializerMethodField()
    project_status = serializers.SerializerMethodField()
    milestones = serializers.SerializerMethodField()
    total_budget = serializers.SerializerMethodField()
    total_utilized_amount = serializers.SerializerMethodField()
    financial_percentage_by_categories = serializers.SerializerMethodField()
    milestone_status = serializers.SerializerMethodField()
    disbursement = serializers.SerializerMethodField()
    output = serializers.SerializerMethodField()
    outcome = serializers.SerializerMethodField()
    risk_status = serializers.SerializerMethodField()
    dashboard = serializers.SerializerMethodField()
    project_users = serializers.SerializerMethodField()
    ngo_partner = NGOPartnerSerializer()
    partner_name = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    schedule_vii_activities = serializers.SerializerMethodField()

    @staticmethod
    # fetch location
    def get_location(obj):
        locations = ProjectLocation.objects.filter(project=obj).values_list('location', flat=True)
        return locations

    @staticmethod
    # fetch focus area
    def get_focus_area(obj):
        focus_area = ProjectFocusArea.objects.filter(project=obj).values_list('focus_area', flat=True).order_by(
            'focus_area')
        return focus_area

    @staticmethod
    # fetch sub focus area
    def get_sub_focus_area(obj):
        sub_focus_area = ProjectSubFocusArea.objects.filter(project=obj).values_list('sub_focus_area',
                                                                                     flat=True).order_by(
            'sub_focus_area')
        return sub_focus_area

    @staticmethod
    # fetch target segments
    def get_target_segment(obj):
        target_segment = ProjectTargetSegment.objects.filter(project=obj).values_list('target_segment',
                                                                                      flat=True).order_by(
            'target_segment')
        return target_segment

    @staticmethod
    # fetch sub target segments
    def get_sub_target_segment(obj):
        sub_target_segment = ProjectSubTargetSegment.objects.filter(project=obj).values_list('sub_target_segment',
                                                                                             flat=True).order_by(
            'sub_target_segment')
        return sub_target_segment

    @staticmethod
    # fetch sdg_goals
    def get_sdg_goals(obj):
        sdg_goals = ProjectSDGGoals.objects.filter(project=obj).values_list('sdg_goal', flat=True).order_by(
            'sdg_goal_name')
        return sdg_goals

    @staticmethod
    # fetch mca_focus_area
    def get_mca_sector(obj):
        mca_sector = ProjectMCA.objects.filter(project=obj).values_list('mca', flat=True).order_by(
            'mca_focus_area')
        return mca_sector

    @staticmethod
    # fetch tags
    def get_tags(obj):
        return obj.get_tags()

    @staticmethod
    # fetch project users
    def get_project_users(project):
        project_users = ProjectUser.objects.filter(project=project)
        return ProjectUserReadOnlySerializer(project_users, many=True).data

    # fetch client_details
    def get_client_details(self, obj):
        from v2.client.serializers import ClientWriteSerializer
        return ClientWriteSerializer(obj.client).data

    # fetch images
    def get_images(self, obj):
        return ProjectImageSerializer(ProjectImage.objects.filter(project=obj), many=True).data

    # fetch project_status
    def get_project_status(self, obj):
        return str((obj.project_status)) + "%"

    # fetch total_budget
    def get_total_budget(self, obj):
        project_disbursement = apply_filter_get_project_disbursement(self, obj)
        return obj.get_all_disbursement_total_amount(disbursement=project_disbursement)

    # fetch total_utilized_amount
    def get_total_utilized_amount(self, obj):
        project_disbursement = apply_filter_get_project_disbursement(self, obj)
        standalone_utilisation = apply_filter_get_project_utilisation(self, obj)
        standalone_utilisation = standalone_utilisation.filter(disbursement=None)
        standalone_utilization_amount = standalone_utilisation.aggregate(Sum('actual_cost')).get("actual_cost__sum",
                                                                                                 0) or 0
        return obj.get_total_utilised_amount(disbursement=project_disbursement) + standalone_utilization_amount

    # fetch financial_percentage_by_categories
    def get_financial_percentage_by_categories(self, obj):
        project_utilisations = apply_filter_get_project_utilisation(self, obj)
        return obj.get_percentage_of_utilisation_by_category(utilisations=project_utilisations)

    # fetch milestone status
    def get_milestone_status(self, obj):
        project_milestones = apply_filters_for_project_milestones(self, obj)
        return obj.get_percentages_of_milestones_status(milestones=project_milestones)

    # fetch disbursement
    def get_disbursement(self, obj):
        project_disbursement = apply_filter_get_project_disbursement(self, obj)
        return DisbursementDetailSerializer(project_disbursement, many=True).data

    # fetch output
    def get_output(self, obj):
        start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
        end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
        location = check_null_and_replace_to_none(self.context.get('request').query_params.get('location'))
        focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('focus_area'))
        sub_focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('sub_focus_area'))
        project_indicator = ProjectIndicator.objects.filter(project=obj)
        project_indicator = project_indicator.filter(milestone__location__in=location) \
            if location else project_indicator
        project_indicator = project_indicator.filter(milestone__sub_focus_area__in=[sub_focus_area]) \
            if sub_focus_area else project_indicator
        project_indicator = project_indicator.values_list('id', flat=True)
        indicators = []
        for indicator in project_indicator:
            indicators.append(indicator)
        project_output = ProjectOutput.objects.filter(project_indicator__in=indicators). \
            filter(scheduled_date__gte=start_date, scheduled_date__lte=end_date) \
            if not ((start_date is None) | (end_date is None)) else ProjectOutput.objects.filter(project_indicator__in=indicators)
        project_output = project_output.order_by('scheduled_date')
        return ProjectOutputSerializer(project_output, many=True).data

    # fetch outcome
    def get_outcome(self, obj):
        start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
        end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
        location = check_null_and_replace_to_none(self.context.get('request').query_params.get('location'))
        focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('focus_area'))
        sub_focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('sub_focus_area'))
        project_indicator = ProjectIndicator.objects.filter(project=obj)
        project_indicator = project_indicator.filter(milestone__location__in=location) \
            if location else project_indicator
        project_indicator = project_indicator.filter(milestone__sub_focus_area__in=[sub_focus_area]) \
            if sub_focus_area else project_indicator
        project_indicator = project_indicator.values_list('id', flat=True)
        indicators = []
        for indicator in project_indicator:
            indicators.append(indicator)
        project_outcome = ProjectOutcome.objects.filter(project_indicator__in=indicators). \
            filter(scheduled_date__gte=start_date, scheduled_date__lte=end_date) \
            if not ((start_date is None) | (end_date is None)) else ProjectOutcome.objects.filter(project_indicator__in=indicators)
        project_outcome = project_outcome.order_by('scheduled_date')
        return ProjectOutcomeSerializer(project_outcome, many=True).data

    # fetch milestones
    def get_milestones(self, obj):
        project_milestones = apply_filters_for_project_milestones(self, obj)
        return MilestoneSerializer(project_milestones, many=True).data

    # fetch risk status
    def get_risk_status(self, obj):
        start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
        end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
        return obj.get_count_of_risk_status(start_date=start_date, end_date=end_date)

    # fetch dashboard
    def get_dashboard(self, obj):
        return ProjectDashboardMiniSerializer(obj.get_dashboard(), many=True).data

    # fetch partner_name
    def get_partner_name(self, obj):
        return obj.get_partner_display()

    # fetch schedule_vii_activities
    def get_schedule_vii_activities(self, obj):
        schedule_vii_activities = ProjectScheduleVII.objects.filter(project=obj).values_list(
            'schedule_vii_activity__activity_description', flat=True)
        schedule_vii_activities = set(list(schedule_vii_activities))
        return schedule_vii_activities

    class Meta:
        model = Project
        fields = ['id', 'project_name', "project_description", "start_date", "end_date", "budget", "location",
                  "focus_area", "sub_focus_area", "target_segment", "sub_target_segment", "client_details",
                  "total_utilized_amount", "total_budget", "milestone_status", "disbursement", "milestones",
                  "project_status", "financial_percentage_by_categories", "plant",
                  "output", "outcome", "sdg_goals", 'risk_status', "dashboard", "project_users",
                  "partner_name", "ngo_partner", "tags", "schedule_vii_activities", "mca_sector"]


"""
   This class serialize project model and return field id, project_name, plant, client
"""
class ProjectDetailMiniSerializer(serializers.ModelSerializer):
    project_status = serializers.SerializerMethodField()
    plant_name = serializers.SerializerMethodField()
    output = serializers.SerializerMethodField()
    outcome = serializers.SerializerMethodField()
    financial_utilization = serializers.SerializerMethodField()
    total_disbursement = serializers.SerializerMethodField()
    planned_disbursement = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    schedule_vii_activities = serializers.SerializerMethodField()

    def get_sdg_goals(self, obj):
        return obj.get_sdg_goals()

    def get_project_status(self, obj):
        return str(obj.project_status) + "%"

    def get_plant_name(self, obj):
        return obj.get_plant().plant_name if obj.is_plant_mapped() else ''

    def get_financial_utilization(self, obj):
        if obj.total_disbursement_amount > 0:
            unutilized_amount = obj.total_disbursement_amount - obj.total_utilised_amount
            return [{"Utilised": obj.total_utilised_amount,
                     "Not Utilised": 0 if unutilized_amount < 0 else unutilized_amount}]
        else:
            return []

    def get_total_disbursement(self, obj):
        return obj.total_disbursement_amount

    def get_planned_disbursement(self, obj):
        return obj.planned_disbursement_amount

    def get_output(self, obj):
        project_output = ProjectOutput.objects.filter(project=obj).order_by(
            'scheduled_date')
        return ProjectOutputSerializer(project_output, many=True).data

    def get_outcome(self, obj):
        project_outcome = ProjectOutcome.objects.filter(project=obj).order_by(
            'scheduled_date')
        return ProjectOutcomeSerializer(project_outcome, many=True).data

    def get_schedule_vii_activities(self, obj):
        schedule_vii_activities = ProjectScheduleVII.objects.filter(project=obj).values_list(
            'schedule_vii_activity__activity_description', flat=True)
        schedule_vii_activities = set(list(schedule_vii_activities))
        return schedule_vii_activities

    class Meta:
        model = Project
        fields = ['id', "plant_name", 'project_name', "project_description", "project_status", "financial_utilization",
                  "output", "outcome", "total_disbursement", "planned_disbursement", "sdg_goals",
                  "schedule_vii_activities"]


"""
   This class serialize project model and return field id, project_name, plant, client.
"""
class ProjectOptionSerializer(serializers.ModelSerializer):
    plant = serializers.SlugRelatedField(read_only=True, slug_field='plant_name')
    client = serializers.SlugRelatedField(read_only=True, slug_field='display_name')

    class Meta:
        model = Project
        fields = ('id', 'project_name', 'plant', 'client')


"""
   This class serialize project model and return field id, project_name, plant, client.
"""
class ProjectMiniMicroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'project_name', 'plant', 'client')


"""
   This class serialize ProjectUser model.
"""
class ProjectUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectUser
        fields = '__all__'


"""
   This class serialize ProjectUser model and return fields user, id.
"""
class ProjectUserReadOnlySerializer(serializers.ModelSerializer):
    user = UserSerializerForProject(many=False, read_only=True)

    class Meta:
        model = ProjectUser
        fields = ['user', 'id']


"""
   This class serialize ProjectDocument model.
"""
class ProjectDocumentSerializer(serializers.ModelSerializer):
    doc_name = serializers.SerializerMethodField()

    def get_doc_name(self, obj):
        return str(obj.document_file) if obj.document_file else ''

    class Meta:
        model = ProjectDocument
        fields = '__all__'

"""
   This class serialize ProjectImage model.
"""
class ProjectImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectImage
        fields = '__all__'


"""
   This class serialize Milestone model.
"""
class MilestoneSerializer(serializers.ModelSerializer):
    task = serializers.SerializerMethodField('get_milestone_task')
    participants = ProjectUserReadOnlySerializer(many=True)
    documents = serializers.SerializerMethodField()
    recent_notes = serializers.SerializerMethodField()

    @staticmethod
    def get_milestone_task(milestone):
        tasks = Task.objects.filter(milestone=milestone).order_by('position')
        serializer = TaskReadSerializer(tasks, many=True)
        return serializer.data

    @staticmethod
    def get_documents(milestone):
        # Returns all the documents linked to the disbursement
        return MilestoneDocumentSerializer(MilestoneDocument.objects.filter(milestone=milestone),
                                           many=True).data

    @staticmethod
    def get_recent_notes(milestone):
        unread_notes = MilestoneNote.objects.filter(is_read=False, milestone=milestone).count()
        return unread_notes

    class Meta:
        model = Milestone
        fields = '__all__'

"""
   This class serialize Milestone model and return complete milestone detail.
"""
class MilestoneListSerializer(serializers.ModelSerializer):
    task = serializers.SerializerMethodField('get_milestone_task')
    participants = ProjectUserReadOnlySerializer(many=True)
    documents = serializers.SerializerMethodField()
    recent_notes = serializers.SerializerMethodField()

    @staticmethod
    def get_milestone_task(milestone):
        tasks = Task.objects.filter(milestone=milestone)
        serializer = TaskReadSerializer(tasks, many=True)
        return serializer.data

    @staticmethod
    def get_documents(milestone):
        return MilestoneDocument.objects.filter(milestone=milestone).count()

    @staticmethod
    def get_recent_notes(milestone):
        unread_notes = MilestoneNote.objects.filter(is_read=False, milestone=milestone).count()
        return unread_notes

    class Meta:
        model = Milestone
        fields = ('milestone_name', 'documents', 'recent_notes', 'task', 'participants',
         'milestone_description', 'milestone_name', 'participants', 'project', 'status', 'tags',
         'id', 'start_date', 'end_date', 'actual_start_date', 'actual_end_date', 'sub_focus_area',
         'sub_target_segment', 'location', 'position')


"""
   This class serialize ProjectTag model.
"""
class ProjectTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectTag
        fields = ('project', 'tag', 'id')

"""
   This class serialize Milestone model and return fields milestone_name, id, start_date, end_date, actual_start_date, actual_end_date .
"""
class MilestoneMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = Milestone
        fields = ('milestone_name', 'id', 'start_date', 'end_date', 'actual_start_date', 'actual_end_date')


"""
   This class serialize Milestone model for write operation.
"""
class MilestoneWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Milestone
        fields = '__all__'


"""
   This class serialize MilestoneNote model .
"""
class MilestoneNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = MilestoneNote
        fields = '__all__'


"""
   This class serialize MilestoneDocument model .
"""
class MilestoneDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = MilestoneDocument
        fields = '__all__'


"""
   This class serialize Task model .
"""
class TaskWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'


"""
   This class serialize Task model for read operation .
"""
class TaskReadSerializer(serializers.ModelSerializer):
    sub_task = serializers.SerializerMethodField('get_sub_tasks')
    client_name = serializers.SerializerMethodField()
    project_name = serializers.SerializerMethodField()
    milestone_name = serializers.SerializerMethodField()
    assignee = serializers.SerializerMethodField()
    assignee_project_user = serializers.SerializerMethodField()
    client_assignee = serializers.SerializerMethodField()
    assignee_client_user = serializers.SerializerMethodField()
    documents = serializers.SerializerMethodField()
    recent_comments = serializers.SerializerMethodField()

    @staticmethod
    def get_recent_comments(task):
        unread_comments = TaskComment.objects.filter(is_read=False, task=task).count()
        return unread_comments

    @staticmethod
    def get_sub_tasks(task):
        sub_tasks = SubTask.objects.filter(task=task)
        serializer = SubTaskSerializer(sub_tasks, many=True)
        return serializer.data

    def get_client_name(self, obj):
        return obj.client.get_name() if obj.client else None

    def get_project_name(self, obj):
        return obj.project.get_name() if obj.project else None

    def get_milestone_name(self, obj):
        return obj.milestone.get_name() if obj.milestone else None

    def get_assignee(self, obj):
        from v2.user_management.serializers import UserMicroSerializer
        return UserMicroSerializer(obj.assignee.user).data if obj.assignee else None

    @staticmethod
    def get_assignee_project_user(obj):
        return ProjectUserReadOnlySerializer(obj.assignee).data

    def get_client_assignee(self, obj):
        from v2.user_management.serializers import UserMicroSerializer
        return UserMicroSerializer(obj.client_assignee.user).data if obj.client_assignee else None

    @staticmethod
    def get_assignee_client_user(obj):
        from v2.client.serializers import MiniClientUserReadSerializer
        return MiniClientUserReadSerializer(obj.client_assignee).data

    @staticmethod
    def get_documents(task):
        # Returns all the documents linked to the disbursement
        return TaskDocumentSerializer(TaskDocument.objects.filter(task=task),
                                      many=True).data

    class Meta:
        model = Task
        fields = ['id', 'task_name', 'task_description', 'priority', 'status', 'start_date', 'end_date',
                  'actual_end_date', 'actual_start_date',
                  'task_tag', 'tags', 'sub_task', 'client_name', 'project_name', 'milestone_name', 'assignee',
                  'project', 'documents', 'position',
                  'assignee_project_user', 'percentage_completed', 'client_assignee', 'assignee_client_user',
                  'recent_comments']


"""
   This class serialize SubTask model.
"""
class SubTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubTask
        fields = '__all__'


"""
   This class serialize TaskComment model.
"""
class TaskCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskComment
        fields = '__all__'


"""
   This class serialize TaskDocument model.
"""
class TaskDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskDocument
        fields = '__all__'

"""
   This class serialize TaskActivityLog model.
"""
class TaskActivityLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskActivityLog
        fields = '__all__'

"""
   This class serialize  MilestoneActivityLog model.
"""
class MilestoneActivityLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = MilestoneActivityLog
        fields = '__all__'

"""
    Serializer for Disbursement
    To be used only for writing to the model
"""
class DisbursementWriteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Disbursement
        fields = '__all__'

"""
    Serializer for Disbursement
"""
class DisbursementMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = Disbursement
        fields = ('disbursement_name', 'id', 'expected_amount', 'actual_amount', 'start_date', 'end_date')

"""
    Serializer for Disbursement to get entire data.
    To be used only for reading the data of Disbursement
"""
class DisbursementSerializer(serializers.ModelSerializer):

    utilisation = serializers.SerializerMethodField('get_disbursement_utilisation')
    milestone = serializers.SerializerMethodField('get_milestones')
    rules = serializers.SerializerMethodField()
    rule_assignees = serializers.SerializerMethodField()
    current_status = serializers.SerializerMethodField()
    next_rule = serializers.SerializerMethodField()
    final_approval_date = serializers.SerializerMethodField()
    payment_date = serializers.SerializerMethodField()
    documents = serializers.SerializerMethodField()
    ruleset = []
    rule_list = []
    rule_assignee_list = []
    recent_comments = serializers.SerializerMethodField()

    @staticmethod
    def get_recent_comments(disbursement):
        unread_comments = DisbursementComment.objects.filter(is_read=False, disbursement=disbursement).count()
        return unread_comments

    def get_disbursement_utilisation(self, disbursement):
        # Returns serialized utilisations linked to the disbursement
        utilisations = Utilisation.objects.filter(disbursement=disbursement).order_by('position')
        if self.context.get('request'):
            expense_category = self.context.get('request').query_params.get('expense_category')
            start_date = self.context.get('request').query_params.get('start_date')
            end_date = self.context.get('request').query_params.get('end_date')
            if not ((start_date is None) | (end_date is None)):
                utilisations = utilisations.filter(start_date__lte=start_date, end_date__gte=end_date)
            if expense_category:
                utilisations = utilisations.filter(expense_category=expense_category)
        serializer = UtilisationSerializer(utilisations, many=True)
        return serializer.data

    @staticmethod
    def get_milestones(disbursement):
        # Returns milestone data for the disbursement
        return {'milestone_name': disbursement.milestone.get_name(),
                'id': disbursement.milestone.id} if disbursement.milestone else None

    def get_rules(self, disbursement):
        # Returns all the rules for the disbursement if custom workflow is on
        try:
            self.ruleset = DisbursementWorkflowRuleSet.objects.get(projects__in=[disbursement.project])
            self.rule_list = self.ruleset.get_rules()
            if not disbursement.due_diligence:
                self.rule_list = self.rule_list.exclude(rule_type='CHECKER')
            self.rule_assignee_list = DisbursementWorkflowRuleAssignee.objects.filter(rule__in=self.rule_list)
            return DisbursementWorkflowRuleSerializer(self.rule_list, many=True).data
        except ObjectDoesNotExist as ex:
            return []

    def get_rule_assignees(self, disbursement):
        # Returns rule assignees for all the rules if custom workflow is on
        return DisbursementWorkflowAssigneeSerializer(self.rule_assignee_list, many=True).data

    @staticmethod
    def get_current_status(disbursement):
        # returns current status for the disbursement
        # TODO: if duplicate from the model function, then remove the duplicate code
        try:
            status = DisbursementStatus.objects.filter(disbursement=disbursement).order_by('id').reverse()[0]
            if status.status in ['SCHEDULED', 'PAYMENT_RELEASED', 'PAYMENT_RECEIVED']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.status in ['REQUESTED']:
                return {'status': status.status, 'status_text': 'New Request'}
            elif status.rule:
                same_category_rules = DisbursementWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                              rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    if status.rule.rule_type == 'CHECKER':
                        status_text = 'L{0} - Verified'.format(category_level)
                    else:
                        status_text = 'L{0} - Approved'.format(category_level)
                else:
                    if status.rule.rule_type == 'CHECKER':
                        status_text = 'L{0} - Verification Rejected'.format(category_level)
                    else:
                        status_text = 'L{0} - Approval Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_next_rule(self, disbursement):
        # returns next rule for the disbursement
        # TODO: if duplicate from the model function, then remove the duplicate code
        try:
            last_rule = self.rule_list.reverse()[0] if self.rule_list else None
            status = DisbursementStatus.objects.filter(disbursement=disbursement).order_by('id').reverse()[0]
            if status.status == 'SCHEDULED':
                return DisbursementWorkflowRuleSerializer(self.rule_list[0]).data
            elif status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                rule = self.rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                return DisbursementWorkflowRuleSerializer(rule).data
            elif status.rule and status.status == 'REJECTED':
                return DisbursementWorkflowRuleSerializer(status.rule).data
            elif status.rule and status.rule == last_rule:
                return {'next_status': 'PAYMENT_RELEASED'}
            elif status.status == 'PAYMENT_RELEASED':
                return {'next_status': 'PAYMENT_RECEIVED'}
        except IndexError:
            return None

    @staticmethod
    def get_payment_date(disbursement):
        # returns the payment date of the disbursement
        try:
            status = DisbursementStatus.objects.get(disbursement=disbursement, status='PAYMENT_RELEASED')
            return status.created_date
        except Exception as ex:
            return None

    def get_final_approval_date(self, disbursement):
        # returns final approval date of the disbursement
        # TODO: if duplicate from the model function, then remove the duplicate code
        try:
            last_rule = self.rule_list.reverse()[0] if self.rule_list else None
            status = DisbursementStatus.objects.get(disbursement=disbursement, status='APPROVED', rule=last_rule)
            return status.created_date
        except Exception as ex:
            return None

    @staticmethod
    def get_documents(disbursement):
        # Returns document count linked to the disbursement
        return DisbursementDocument.objects.filter(disbursement=disbursement).count()

    class Meta:
        model = Disbursement
        fields = '__all__'

"""
    Serializer for Disbursement Status
    Can be used to read and write
"""
class DisbursementStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = DisbursementStatus
        fields = '__all__'


"""
    Serializer for disbursement activity log
"""
class DisbursementActivityLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = DisbursementActivityLog
        fields = '__all__'

"""
   Serializer to return details of the disbursement
"""
class DisbursementDetailSerializer(serializers.ModelSerializer):
    utilisation = serializers.SerializerMethodField('get_disbursement_utilisation')
    total_utilisation_amount = serializers.SerializerMethodField()

    def get_total_utilisation_amount(self, obj):
        # Returns total utilisation amount
        return obj.get_overall_utilistation_total()

    @staticmethod
    def get_disbursement_utilisation(disbursement):
        # Returns utilisations linked to the disbursement
        return UtilisationSerializer(disbursement.get_all_utilisations(), many=True).data

    class Meta:
        model = Disbursement
        fields = ['id', 'disbursement_name', 'disbursement_description', 'status', 'start_date', 'end_date',
                  'expected_amount', 'actual_amount', 'expense_category', 'expected_date', 'utilisation',
                  'total_utilisation_amount', 'tags']

"""
   Serializer for Disbursement Comment
"""
class DisbursementCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = DisbursementComment
        fields = '__all__'

"""
   Serializer for Disbursement Document
"""
class DisbursementDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = DisbursementDocument
        fields = '__all__'

"""
    Serializer for Utilisation
    To be used for reading
"""
class UtilisationSerializer(serializers.ModelSerializer):
    rules = serializers.SerializerMethodField()
    rule_assignees = serializers.SerializerMethodField()
    current_status = serializers.SerializerMethodField()
    next_rule = serializers.SerializerMethodField()
    documents = serializers.SerializerMethodField()
    ruleset = []
    rule_list = []
    rule_assignee_list = []
    recent_comments = serializers.SerializerMethodField()

    @staticmethod
    def get_recent_comments(utilisation):
        unread_comments = UtilisationComment.objects.filter(is_read=False, utilisation=utilisation).count()
        return unread_comments

    @staticmethod
    def get_documents(utilisation):
        # returns Utilisation Document
        return UtilisationDocumentSerializer(UtilisationDocument.objects.filter(utilisation=utilisation),
                                             many=True).data

    def get_rules(self, utilisation):
        # Returns all the rules for the utilisation if custom workflow is on
        try:
            self.ruleset = UtilisationWorkflowRuleSet.objects.get(projects__in=[utilisation.project])
            self.rule_list = self.ruleset.get_rules()
            self.rule_assignee_list = UtilisationWorkflowRuleAssignee.objects.filter(rule__in=self.rule_list)
            return UtilisationWorkflowRuleSerializer(self.rule_list, many=True).data
        except ObjectDoesNotExist as ex:
            return []

    def get_rule_assignees(self, utilisation):
        # Returns Rule assignees for the utilisation if custom workflow is on
        return UtilisationWorkflowAssigneeSerializer(self.rule_assignee_list, many=True).data

    def get_current_status(self, utilisation):
        # Returns current status of the utilisation if custom workflow is on
        try:
            status = UtilisationStatus.objects.filter(utilisation=utilisation).order_by('id').reverse()[0]
            if status.status in ['NEW_REQUEST']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.rule:
                same_category_rules = UtilisationWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                             rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_next_rule(self, utilisation):
        # returns the next rule that is applicable to a utilisation if custom workflow is ON.
        try:
            last_rule = self.rule_list.reverse()[0] if self.rule_list else None
            status = UtilisationStatus.objects.filter(utilisation=utilisation).order_by('id').reverse()[0]
            if status.status == 'SCHEDULED':
                return UtilisationWorkflowRuleSerializer(self.rule_list[0]).data
            elif status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                rule = self.rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                return UtilisationWorkflowRuleSerializer(rule).data
            elif status.rule and status.status == 'REJECTED':
                return UtilisationWorkflowRuleSerializer(status.rule).data
            elif status.rule and status.rule == last_rule:
                return {'next_status': None}
        except IndexError:
            return None

    class Meta:
        model = Utilisation
        fields = '__all__'

"""
   Serializer for Utilisation Comment
"""
class UtilisationCommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = UtilisationComment
        fields = '__all__'

"""
    Serializer for Utilisation Document
"""
class UtilisationDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = UtilisationDocument
        fields = '__all__'

"""
   Serializer for Utilisation Status
"""
class UtilisationStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = UtilisationStatus
        fields = '__all__'

"""
   Serializer for ProjectOutput Status
"""
class ProjectOutputStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectOutputStatus
        fields = '__all__'

"""
   Serializer for ProjectOutcome Status
"""
class ProjectOutcomeStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectOutcomeStatus
        fields = '__all__'

"""
   Serializer for Utilisation Activity Log
"""
class UtilisationActivityLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = UtilisationActivityLog
        fields = '__all__'

"""
    Serializer for Utilisation Activity Log
"""
class UtilisationUploadSerializer(serializers.ModelSerializer):

    document_count = serializers.SerializerMethodField()

    def get_document_count(self, utilisation_upload):
        return UtilisationUploadDocument.objects.filter(utilisation_upload=utilisation_upload).count()

    class Meta:
        model = UtilisationUpload
        fields = '__all__'

"""
    Serializer for UtilisationExpense (Child Utilisation)
"""
class UtilisationExpenseSerializer(serializers.ModelSerializer):

    class Meta:
        model = UtilisationExpense
        fields = '__all__'


"""
    Serializer for Utilisation Expense Document
"""
class UtilisationExpenseDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = UtilisationExpenseDocument
        fields = '__all__'

"""
    Serializer for Project Output Document
"""
class ProjectOutputDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectOutputDocument
        fields = '__all__'

"""
    Serializer for Project Outcome Document
"""
class ProjectOutcomeDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProjectOutcomeDocument
        fields = '__all__'

"""
    Serializer for Utilisation Upload Document
"""
class UtilisationUploadDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = UtilisationUploadDocument
        fields = '__all__'


"""
    Serializer for ProjectIndicator
"""
class ProjectIndicatorSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = ProjectIndicator
        fields = '__all__'


"""
    Serializer for ProjectScheduleVII
"""
class ProjectScheduleVIIActivitySerializer(BulkSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = ProjectScheduleVII
        fields = '__all__'

"""
    Serializer for ProjectScheduleVII
"""
class ProjectScheduleVIIAggregationSerializer(serializers.ModelSerializer):
    schedule_vii_activity = serializers.SlugRelatedField('activity_description', read_only=True)

    class Meta:
        model = ProjectScheduleVII
        fields = ['schedule_vii_activity']

"""
   Serializer for Impact Activity Log
"""
class IndicatorActivityLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = IndicatorActivityLog
        fields = '__all__'

"""
   Serializer for Indicator Document
"""
class IndicatorDocumentSerializer(serializers.ModelSerializer):

    class Meta:
        model = IndicatorDocument
        fields = '__all__'


"""
   Serializer for Indicator Comment
"""
class IndicatorCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndicatorComment
        fields = '__all__'

"""
   Serializer for ProjectIndicator used for read operation.
"""
class ProjectIndicatorReadSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    milestone = serializers.SerializerMethodField()
    planned_project_output = serializers.SerializerMethodField()
    project_output = serializers.SerializerMethodField()
    project_outcome = serializers.SerializerMethodField()
    planned_project_outcome = serializers.SerializerMethodField()
    actual_project_output = serializers.SerializerMethodField()
    actual_project_outcome = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()

    @staticmethod
    def get_project_output(id):
        project_output = ProjectOutput.objects.filter(project_indicator=id)
        return ProjectOutputMiniSerializer(project_output, many=True).data

    @staticmethod
    def get_project_outcome(id):
        project_outcome = ProjectOutcome.objects.filter(project_indicator=id)
        return ProjectOutcomeMiniSerializer(project_outcome, many=True).data

    def get_planned_project_output(self, obj):
        return obj.get_planned_project_output()

    def get_planned_project_outcome(self, obj):
        return obj.get_planned_project_outcome()

    def get_actual_project_output(self, obj):
        return obj.get_actual_project_output()

    def get_actual_project_outcome(self, obj):
        return obj.get_actual_project_outcome()

    @staticmethod
    def get_milestone(indicator):
        return MilestoneMiniSerializer(indicator.milestone).data

    @staticmethod
    def get_location(id):
        project_location = ProjectIndicatorLocation.objects.filter(indicator=id).values_list('location', flat=True)
        return project_location

    class Meta:
        model = ProjectIndicator
        list_serializer_class = BulkListSerializer
        fields = ['id', 'milestone', 'project_outcome', 'project_output', 'planned_project_output', 'planned_project_outcome', 'actual_project_output', 'actual_project_outcome',
                 'frequency', 'project', 'output_indicator', 'outcome_indicator', 'output_calculation', 'outcome_calculation'
                , 'output_status', 'outcome_status', 'position', 'location']

"""
   Serializer for ProjectIndicator
"""
class ProjectIndicatorListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIndicator
        fields = ['output_indicator', 'outcome_indicator', 'project', 'id']

"""
   Serializer for ProjectDetailIndicator
"""
class ProjectDetailIndicatorSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    milestone = serializers.SerializerMethodField()
    project_output = serializers.SerializerMethodField()
    project_outcome = serializers.SerializerMethodField()
    project_indicator_outcome = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()

    @staticmethod
    def get_project_output(id):
        project_output = ProjectOutput.objects.filter(project_indicator=id)
        return ProjectOutputMiniSerializer(project_output, many=True).data

    @staticmethod
    def get_project_outcome(id):
        project_outcome = ProjectOutcome.objects.filter(project_indicator=id)
        return ProjectOutcomeMiniSerializer(project_outcome, many=True).data

    @staticmethod
    def get_project_indicator_outcome(id):
        project_indicator_outcome = ProjectIndicatorOutcome.objects.filter(project_indicator=id)
        return ProjectIndicatorOutcomeSerializer(project_indicator_outcome, many=True).data

    @staticmethod
    def get_milestone(indicator):
        return MilestoneMiniSerializer(indicator.milestone).data

    @staticmethod
    def get_location(id):
        project_location = ProjectIndicatorLocation.objects.filter(indicator=id).values_list('location', flat=True)
        return project_location

    class Meta:
        model = ProjectIndicator
        list_serializer_class = BulkListSerializer
        fields = ['id', 'milestone', 'project_output', 'project_outcome', 'project_indicator_outcome',
                'frequency', 'period_name', 'project', 'output_status', 'outcome_status', 'is_beneficiary',
                'output_indicator', 'outcome_indicator', 'output_calculation', 'outcome_calculation', 'is_custom_workflow', 'location']


"""
   Serializer for ProjectOutput
"""
class ProjectOutputMicroSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectOutput
        fields = ['planned_output', 'actual_output', 'project_indicator', 'scheduled_date']

"""
   Serializer for ProjectOutcome
"""
class ProjectOutcomeMicroSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectOutcome
        fields = ['planned_outcome', 'actual_outcome', 'project_indicator', 'scheduled_date']


"""
   Serializer for ProjectOutput
"""
class ProjectOutputSerializer(serializers.ModelSerializer):
    frequency = serializers.SerializerMethodField()
    output_indicator = serializers.SerializerMethodField()

    def get_frequency(self, projectOutput):
        frequency = ProjectIndicator.objects.get(id=projectOutput.project_indicator.id).frequency
        return frequency

    def get_output_indicator(self, project_output):
        return project_output.project_indicator.output_indicator

    class Meta:
        model = ProjectOutput
        fields = ['planned_output', 'actual_output', 'frequency', 'output_indicator', 'project_indicator', 'scheduled_date', 'project']

"""
   Serializer for ProjectOutput for read operation
"""
class ProjectOutcomeSerializer(serializers.ModelSerializer):
    frequency = serializers.SerializerMethodField()
    outcome_indicator = serializers.SerializerMethodField()

    def get_frequency(self, projectOutcome):
        frequency = ProjectIndicator.objects.get(id=projectOutcome.project_indicator.id).frequency
        return frequency

    def get_outcome_indicator(self, project_outcome):
        return project_outcome.project_indicator.outcome_indicator

    class Meta:
        model = ProjectOutcome
        fields = ['planned_outcome', 'actual_outcome', 'frequency', 'outcome_indicator', 'project_indicator', 'scheduled_date', 'project']

"""
   Serializer for ProjectOutput for read operation
"""
class ProjectOutputReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectOutput
        fields = '__all__'

"""
   Serializer for ProjectOutcome for read operation
"""
class ProjectOutcomeReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectOutcome
        fields = '__all__'

"""
   Serializer for ProjectOutput for read operation
"""
class ProjectOutputMiniSerializer(serializers.ModelSerializer):
    current_status = serializers.SerializerMethodField()
    next_rule = serializers.SerializerMethodField()
    rules = serializers.SerializerMethodField()
    rule_assignees = serializers.SerializerMethodField()
    ruleset = []
    rule_list = []
    rule_assignee_list = []

    @staticmethod
    def get_current_status(self):
        # Returns the current status of the projectOutput if the workflow is ON
        try:
            status = ProjectOutputStatus.objects.filter(projectOutput=self).order_by('id').reverse()[0]
            if status.status in ['UNVERIFIED']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                             rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_rules(self, projectOutput):
        # Returns all the rules for the Project output if custom workflow is on
        try:
            self.ruleset = ImpactWorkflowRuleSet.objects.get(projects__in=[projectOutput.project])
            self.rule_list = self.ruleset.get_rules()
            self.rule_assignee_list = ImpactWorkflowRuleAssignee.objects.filter(rule__in=self.rule_list)
            return ImpactWorkflowRuleSerializer(self.rule_list, many=True).data
        except ObjectDoesNotExist as ex:
            return []

    def get_rule_assignees(self, projectOutput):
        try:
            self.ruleset = ImpactWorkflowRuleSet.objects.get(projects__in=[projectOutput.project])
            self.rule_list = self.ruleset.get_rules()
            self.rule_assignee_list = ImpactWorkflowRuleAssignee.objects.filter(rule__in=self.rule_list)
            # Returns Rule assignees for the utilisation if custom workflow is on
            return ImpactWorkflowAssigneeSerializer(self.rule_assignee_list, many=True).data
        except ObjectDoesNotExist as ex:
            return []

    def get_next_rule(self, projectOutput):
        # returns the next rule that is applicable to a project output if custom workflow is ON.
        try:
            last_rule = self.rule_list.reverse()[0] if self.rule_list else None
            status = ProjectOutputStatus.objects.filter(projectOutput=projectOutput).order_by('id').reverse()[0]
            if status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                rule = self.rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                return ImpactWorkflowRuleSerializer(rule).data
            elif status.rule and status.status == 'REJECTED':
                return ImpactWorkflowRuleSerializer(status.rule).data
            elif status.rule and status.rule == last_rule:
                return {'next_status': None}
        except IndexError:
            return None

    class Meta:
        model = ProjectOutput
        fields = ['id', 'project_indicator', 'scheduled_date', 'period_end_date', 'actual_output', 'planned_output',
                "period_name", "project", "rule_assignees", "rules", 'is_included', 'comments', 'current_status', 'next_rule']

"""
   Serializer for ProjectOutcome.
"""
class ProjectOutcomeMiniSerializer(serializers.ModelSerializer):
    current_status = serializers.SerializerMethodField()
    next_rule = serializers.SerializerMethodField()
    rule_assignees = serializers.SerializerMethodField()
    rules = serializers.SerializerMethodField()
    ruleset = []
    rule_list = []
    rule_assignee_list = []

    @staticmethod
    def get_current_status(self):
        # Returns the current status of the project output if the workflow is ON
        try:
            status = ProjectOutcomeStatus.objects.filter(projectOutcome=self).order_by('id').reverse()[0]
            if status.status in ['UNVERIFIED']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                             rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_rules(self, projectOutcome):
        # Returns all the rules for the Project output if custom workflow is on
        try:
            self.ruleset = ImpactWorkflowRuleSet.objects.get(projects__in=[projectOutcome.project])
            self.rule_list = self.ruleset.get_rules()
            self.rule_assignee_list = ImpactWorkflowRuleAssignee.objects.filter(rule__in=self.rule_list)
            return ImpactWorkflowRuleSerializer(self.rule_list, many=True).data
        except ObjectDoesNotExist as ex:
            return []

    def get_rule_assignees(self, projectOutcome):
        # Returns Rule assignees for the utilisation if custom workflow is on
        return ImpactWorkflowAssigneeSerializer(self.rule_assignee_list, many=True).data

    def get_next_rule(self, projectOutcome):
        # returns the next rule that is applicable to a project output if custom workflow is ON.
        try:
            last_rule = self.rule_list.reverse()[0] if self.rule_list else None
            status = ProjectOutcomeStatus.objects.filter(projectOutcome=projectOutcome).order_by('id').reverse()[0]
            if status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                rule = self.rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                return ImpactWorkflowRuleSerializer(rule).data
            elif status.rule and status.status == 'REJECTED':
                return ImpactWorkflowRuleSerializer(status.rule).data
            elif status.rule and status.rule == last_rule:
                return {'next_status': None}
        except IndexError:
            return None

    class Meta:
        model = ProjectOutcome
        fields = ['id', 'project_indicator', 'scheduled_date', 'period_end_date', 'actual_outcome', 'planned_outcome',
                    "period_name", "rules", "rule_assignees", "project", 'is_included', 'comments', 'current_status', 'next_rule']


"""
   Serializer for IndicatorOutcome
"""
class IndicatorOutcomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndicatorOutcome
        fields = '__all__'


"""
   Serializer for ProjectIndicatorOutcome
"""
class ProjectIndicatorOutcomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIndicatorOutcome
        fields = '__all__'


"""
   Serializer for  ProjectRemark
"""
class ProjectRemarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectRemark
        fields = '__all__'

"""
   Serializer for TemplateType
"""
class TemplateTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TemplateType
        fields = ['template_type_id', 'template_type', 'created_date']

"""
   Serializer for Template
"""
class TemplateSerializer(serializers.ModelSerializer):
    table_name = serializers.SerializerMethodField()

    def get_table_name(self,obj):
        table_name = obj.get_table_name()
        return table_name

    class Meta:
        model = Template
        fields = ['id','template_name','focus_area','template_type','project','client','is_added','is_standard','standardization_requested','table_created','data_status','table_name']

"""
   Serializer for TemplateAttribute
"""
class TemplateAttributeSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = TemplateAttribute
        list_serializer_class = BulkListSerializer
        fields = ['id', 'attribute_grouping', 'attribute_name', 'attribute_order', 'attribute_required',
             'attribute_requirement','attribute_type', 'template', 'created_by']


"""
   Serializer for CSVUpload
"""
class CSVUploadSerializer(serializers.ModelSerializer):
    current_status = serializers.SerializerMethodField()
    next_rule = serializers.SerializerMethodField()
    rules = serializers.SerializerMethodField()
    rule_assignees = serializers.SerializerMethodField()
    ruleset = []
    rule_list = []
    rule_assignee_list = []

    @staticmethod
    def get_current_status(self):
        # Returns the current status of the impact csv uploads if the workflow is ON
        try:
            status = ImpactUploadStatus.objects.filter(impact_upload=self).order_by('id').reverse()[0]
            if status.status in ['UNVERIFIED']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                             rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_rules(self, impact_upload):
        # Returns all the rules for the Impact csv uploads if custom workflow is on
        try:
            self.ruleset = ImpactWorkflowRuleSet.objects.get(projects__in=[impact_upload.project])
            self.rule_list = self.ruleset.get_rules()
            self.rule_assignee_list = ImpactWorkflowRuleAssignee.objects.filter(rule__in=self.rule_list)
            return ImpactWorkflowRuleSerializer(self.rule_list, many=True).data
        except ObjectDoesNotExist as ex:
            return []

    def get_rule_assignees(self, impact_upload):
        try:
            self.ruleset = ImpactWorkflowRuleSet.objects.get(projects__in=[impact_upload.project])
            self.rule_list = self.ruleset.get_rules()
            self.rule_assignee_list = ImpactWorkflowRuleAssignee.objects.filter(rule__in=self.rule_list)
            return ImpactWorkflowAssigneeSerializer(self.rule_assignee_list, many=True).data
        except ObjectDoesNotExist as ex:
            return []

    def get_next_rule(self, impact_upload):
        # returns the next rule that is applicable to a impact csv upload if custom workflow is ON.
        try:
            last_rule = self.rule_list.reverse()[0] if self.rule_list else None
            status = ImpactUploadStatus.objects.filter(impact_upload=impact_upload).order_by('id').reverse()[0]
            if status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                rule = self.rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                return ImpactWorkflowRuleSerializer(rule).data
            elif status.rule and status.status == 'REJECTED':
                return ImpactWorkflowRuleSerializer(status.rule).data
            elif status.rule and status.rule == last_rule:
                return {'next_status': None}
        except IndexError:
            return None

    class Meta:
        model = CSVUpload
        fields = ['id', 'csv_file', 'client', 'project', 'template', 'status', "table_name", "batch_id", 'is_custom_workflow',
                "rule_assignees", "rules", 'upload_successful', 'is_deleted', 'current_status', 'next_rule', 'updated_date',
                'updated_by', 'created_date']

"""
    Serializer for Impact Data And Template Activity Log
"""
class ImpactDataAndTemplateActivityLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = ImpactDataAndTemplateActivityLog
        fields = '__all__'

"""
   Serializer for ProjectOutput Status
"""
class ImpactUploadStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = ImpactUploadStatus
        fields = '__all__'

"""
   Serializer for ProjectDocument
"""
class FilesCommonProjectSerializer(serializers.ModelSerializer):
    file_name = serializers.SerializerMethodField()
    file_path = serializers.SerializerMethodField()
    file_type = serializers.SerializerMethodField()

    def get_file_name(self, obj):
        return obj.document_file.url.split("/")[-1] if obj.document_file else ''

    def get_file_path(self, obj):
        return obj.document_file.url if obj.document_file else ''

    def get_file_type(self, obj):
        file_parts = obj.document_file.name.split(".") if obj.document_file else []
        if len(file_parts) > 1:
            return file_parts[1].lower()
        return ""

    class Meta:
        model = ProjectDocument
        fields = ['id', 'file_name', 'file_path', 'file_type', 'created_date']


"""
   Serializer for ImpactCaseStudy
"""
class ImpactCaseStudySerializer(serializers.ModelSerializer):
    class Meta:
        model = ImpactCaseStudy
        fields = '__all__'

"""
   Serializer for CaseStudyDocuments
"""
class CaseStudyDocumentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaseStudyDocuments
        fields = '__all__'

"""
   Serializer for case study activity log
"""
class CaseStudyActivityLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = CaseStudyActivityLog
        fields = '__all__'

"""
   Serializer for DisbursementDocument
"""
class FilesCommonDisbursementSerializer(serializers.ModelSerializer):
    file_name = serializers.SerializerMethodField()
    file_path = serializers.SerializerMethodField()
    file_type = serializers.SerializerMethodField()

    def get_file_name(self, obj):
        return obj.document_file.url.split("/")[-1] if obj.document_file else ''

    def get_file_path(self, obj):
        return obj.document_file.url if obj.document_file else ''

    def get_file_type(self, obj):
        file_parts = obj.document_file.name.split(".") if obj.document_file else []
        if len(file_parts) > 1:
            return file_parts[1].lower()
        return ""

    class Meta:
        model = DisbursementDocument
        fields = ['id', 'file_name', 'file_path', 'file_type', 'created_date']

"""
   Serializer for ProjectImage
"""
class FilesCommonProjectImageSerializer(serializers.ModelSerializer):
    file_name = serializers.SerializerMethodField()
    file_path = serializers.SerializerMethodField()
    file_type = serializers.SerializerMethodField()

    def get_file_name(self, obj):
        return obj.image_file.url.split("/")[-1] if obj.image_file else ''

    def get_file_path(self, obj):
        return obj.image_file.url if obj.image_file else ''

    def get_file_type(self, obj):
        file_parts = obj.image_file.name.split(".") if obj.image_file else []
        if len(file_parts) > 1:
            return file_parts[1].lower()
        return ""

    class Meta:
        model = ProjectImage
        fields = ['id', 'file_name', 'file_path', 'file_type', 'created_date']

"""
   Serializer for ProjectDashboard
"""
class ProjectDashboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectDashboard
        fields = '__all__'

"""
   Serializer for ProjectDashboard
"""
class ProjectDashboardMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectDashboard
        fields = ['url', 'embedded_content', 'password', 'dashboard_name', 'id']

"""
   Serializer for Disbursement Custom workflow Rule
"""
class DisbursementWorkflowRuleSerializer(serializers.ModelSerializer):

    class Meta:
        model = DisbursementWorkflowRule
        fields = '__all__'

"""
   Serializer for Disbursement Custom workflow Ruleset
"""
class DisbursementWorkflowRuleSetSerializer(serializers.ModelSerializer):

    # rules = serializers.SerializerMethodField()
    requester = serializers.SerializerMethodField()
    checker = serializers.SerializerMethodField()
    approver = serializers.SerializerMethodField()
    added_projects = serializers.SerializerMethodField()

    # def get_rules(self, obj):
    #     rules = obj.get_rules()
    #     return DisbursementWorkflowRuleSerializer(rules, many=True).data

    def get_requester(self, obj):
        # returns requestor rule
        rules = obj.get_rules()
        rules = rules.filter(rule_type='REQUESTER')
        return DisbursementWorkflowRuleSerializer(rules, many=True).data

    def get_checker(self, obj):
        # returns checked rule
        rules = obj.get_rules()
        rules = rules.filter(rule_type='CHECKER')
        return DisbursementWorkflowRuleSerializer(rules, many=True).data

    def get_approver(self, obj):
        # returns approver rule
        rules = obj.get_rules()
        rules = rules.filter(rule_type='APPROVER')
        return DisbursementWorkflowRuleSerializer(rules, many=True).data

    def get_added_projects(self, obj):
        # returns added projects to a ruleset
        projects = obj.projects.all()
        if self.context.get('request') and self.context.get('request').query_params.get('project'):
            projects = projects.filter(id=self.context.get('request').query_params.get('project'))
        project_list = []
        for project in projects:
            project_dict = ProjectMicroSerializer(project).data
            project_users = ProjectUser.objects.filter(project=project)
            project_dict['users'] = ProjectUserReadOnlySerializer(project_users, many=True).data
            added_users = DisbursementWorkflowRuleAssignee.objects.filter(project=project)
            for user in added_users:
                if user.rule.level in project_dict:
                    project_dict[user.rule.level].append(user.user.id)
                    project_dict[str(user.rule.level) + 'user'].append(user.user.user.id)
                else:
                    project_dict[user.rule.level] = [user.user.id]
                    project_dict[str(user.rule.level) + 'user'] = [user.user.user.id]
            project_list.append(project_dict)
        return project_list

    class Meta:
        model = DisbursementWorkflowRuleSet
        fields = '__all__'

"""
   Serializer for Disbursement Rule Assignee
"""
class DisbursementWorkflowAssigneeSerializer(serializers.ModelSerializer):

    user_id = serializers.SerializerMethodField()

    def get_user_id(self, assignee):
        # Returns used id of the assignee
        return assignee.user.user.id

    class Meta:
        model = DisbursementWorkflowRuleAssignee
        fields = '__all__'

"""
   Serializer for Utilisation Rule Assignee
"""
class UtilisationWorkflowRuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = UtilisationWorkflowRule
        fields = '__all__'

"""
   Serializer for UtilisationWorkflowRuleSet
"""
class UtilisationWorkflowRuleSetSerializer(serializers.ModelSerializer):
    # rules = serializers.SerializerMethodField()
    requester = serializers.SerializerMethodField()
    checker = serializers.SerializerMethodField()
    added_projects = serializers.SerializerMethodField()

    # def get_rules(self, obj):
    #     rules = obj.get_rules()
    #     return UtilisationWorkflowRuleSerializer(rules, many=True).data

    def get_requester(self, obj):
        rules = obj.get_rules()
        rules = rules.filter(rule_type='REQUESTER')
        return UtilisationWorkflowRuleSerializer(rules, many=True).data

    def get_checker(self, obj):
        rules = obj.get_rules()
        rules = rules.filter(rule_type='CHECKER')
        return UtilisationWorkflowRuleSerializer(rules, many=True).data

    def get_added_projects(self, obj):
        projects = obj.projects.all()
        project_list = []
        for project in projects:
            project_dict = ProjectMicroSerializer(project).data
            project_users = ProjectUser.objects.filter(project=project)
            project_dict['users'] = ProjectUserReadOnlySerializer(project_users, many=True).data
            added_users = UtilisationWorkflowRuleAssignee.objects.filter(project=project)
            for user in added_users:
                if user.rule.level in project_dict:
                    project_dict[user.rule.level].append(user.user.id)
                    project_dict[str(user.rule.level) + 'user'].append(user.user.user.id)
                else:
                    project_dict[user.rule.level] = [user.user.id]
                    project_dict[str(user.rule.level) + 'user'] = [user.user.user.id]
            project_list.append(project_dict)
        return project_list

    class Meta:
        model = UtilisationWorkflowRuleSet
        fields = '__all__'

"""
   Serializer for ImpactWorkflowRule
"""
class UtilisationWorkflowAssigneeSerializer(serializers.ModelSerializer):
    user_id = serializers.SerializerMethodField()

    def get_user_id(self, assignee):
        return assignee.user.user.id

    class Meta:
        model = UtilisationWorkflowRuleAssignee
        fields = '__all__'

"""
   Serializer for ImpactWorkflowRule
"""
class ImpactWorkflowRuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImpactWorkflowRule
        fields = '__all__'

"""
   Serializer for ImpactWorkflowRuleSet
"""
class ImpactWorkflowRuleSetSerializer(serializers.ModelSerializer):
    # rules = serializers.SerializerMethodField()
    requester = serializers.SerializerMethodField()
    checker = serializers.SerializerMethodField()
    added_projects = serializers.SerializerMethodField()

    # def get_rules(self, obj):
    #     rules = obj.get_rules()
    #     return ImpactWorkflowRuleSerializer(rules, many=True).data

    def get_requester(self, obj):
        rules = obj.get_rules()
        rules = rules.filter(rule_type='REQUESTER')
        return ImpactWorkflowRuleSerializer(rules, many=True).data

    def get_checker(self, obj):
        rules = obj.get_rules()
        rules = rules.filter(rule_type='CHECKER')
        return ImpactWorkflowRuleSerializer(rules, many=True).data

    def get_added_projects(self, obj):
        projects = obj.projects.all()
        project_list = []
        for project in projects:
            project_dict = ProjectMicroSerializer(project).data
            project_users = ProjectUser.objects.filter(project=project)
            project_dict['users'] = ProjectUserReadOnlySerializer(project_users, many=True).data
            added_users = ImpactWorkflowRuleAssignee.objects.filter(project=project)
            for user in added_users:
                if user.rule.level in project_dict:
                    project_dict[user.rule.level].append(user.user.id)
                    project_dict[str(user.rule.level) + 'user'].append(user.user.user.id)
                else:
                    project_dict[user.rule.level] = [user.user.id]
                    project_dict[str(user.rule.level) + 'user'] = [user.user.user.id]
            project_list.append(project_dict)
        return project_list

    class Meta:
        model = ImpactWorkflowRuleSet
        fields = '__all__'

"""
   Serializer for ImpactWorkflowRuleAssignee
"""
class ImpactWorkflowAssigneeSerializer(serializers.ModelSerializer):
    user_id = serializers.SerializerMethodField()

    def get_user_id(self, assignee):
        return assignee.user.user.id

    class Meta:
        model = ImpactWorkflowRuleAssignee
        fields = '__all__'


"""
   Serializer for ProjectSetting
"""
class ProjectSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectSetting
        fields = '__all__'

class BulkUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = BulkUpload
        fields = '__all__'

class BulkfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bulkfiles
        fields = '__all__'


